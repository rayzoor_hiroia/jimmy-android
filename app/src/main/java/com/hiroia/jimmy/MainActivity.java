package com.hiroia.jimmy;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.hiroia.jimmy.view.base.BaseActivity;
import com.hiroia.jimmy.view.login.LoginActivity;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private Button m_btnEmailLogin;
    private FrameLayout m_flvFBLogin;
    private TextView m_tvCreateAccount;

    //--------------------------------------------------------
    // Activity Life Cycle
    //--------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gotoLogInActivity();



//        setContentView(R.layout.activity_main, false);
//
//        //----------------------------------------------------
//        // init view
//        m_btnEmailLogin = findViewById(R.id.activity_main_email_login_btn);
//        m_flvFBLogin = findViewById(R.id.activity_login_main_framelayout);
//        m_tvCreateAccount = findViewById(R.id.activity_main_create_account_tv);
//
//        //----------------------------------------------------
//        // set data
//        m_btnEmailLogin.setOnClickListener(this);
//        m_flvFBLogin.setOnClickListener(this);
//        m_tvCreateAccount.setOnClickListener(this);

    }

    private void gotoLogInActivity() {
        getActManager().start(LoginActivity.class)
                .finish()
                .retrieve();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_main_email_login_btn: //  E-mail Login
                gotoLogInActivity();
                break;
            case R.id.activity_login_main_framelayout: // FB Login
                break;
            case R.id.activity_main_create_account_tv: // Create Account
                break;
        }
    }
}
