package com.hiroia.jimmy.model;

import com.hiroia.jimmy.comp.comp.RecordData;
import com.hiroia.jimmy.comp.comp.WaterVolStep;
import com.library.android_common.component.common.Pair;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.hms.MS;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bruce on 2019/1/30.
 */
public class ModelCreateRecipe {

    private int mode = 0; // 1 = Espresso Mode、2 = Pour over Mode
    private double water = 0.0;
    private double time1 = 0.0;
    private double time2 = 0.0;
    private double bean_gram = 0.0;
    private double bean_degree = 0.0;
    private double ratio = 0.0;
    private double temperature = 0.0;
    private double pressure = 0.0;

    private RecordData record_data = new RecordData();
    private RecordData recipe_data = new RecordData();

    private String grinder = StrUtil.EMPTY;
    private String name = StrUtil.EMPTY;
    private String note = StrUtil.EMPTY;

}
