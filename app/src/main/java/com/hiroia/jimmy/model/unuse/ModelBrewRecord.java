package com.hiroia.jimmy.model.unuse;

import com.library.android_common.component.date.ymd.YMD;
import com.library.android_common.util.StrUtil;

import org.json.JSONObject;

/**
 * Created by Bruce on 2018/11/21.
 */
public class ModelBrewRecord {

    // Final Member //
    private final String DATA_RECORDS = "Records";

    // Comp //
    private String m_sRecordName = StrUtil.EMPTY;
    private YMD m_ymd = YMD.nowYMD();
    private int m_nRecordSize = 0;

    //------------------------------------------------------
    // Construct
    //------------------------------------------------------

    public ModelBrewRecord() {
        // do nothing
    }

    public ModelBrewRecord(String recordName, YMD ymd, int recordSize) {
        m_sRecordName = recordName;
        m_ymd = ymd;
        m_nRecordSize = recordSize;
    }

    public static ModelBrewRecord of(String recordName, YMD ymd, int recordSize) {
        return new ModelBrewRecord(recordName, ymd, recordSize);
    }

    public static ModelBrewRecord decodeJson(JSONObject jsObj) {
        ModelBrewRecord m = new ModelBrewRecord();
        return m;
    }

    //------------------------------------------------------
    // Public Method
    //------------------------------------------------------
    public void println_d() {
        println_d(StrUtil.EMPTY);
    }

    public void println_d(String filter) {

    }

    //-------------------------------------------------------
    // Getter and Setter
    //-------------------------------------------------------

    public YMD getYMD() {
        return m_ymd;
    }

    public void setYMD(YMD ymd) {
        m_ymd = ymd;
    }

    public String getRecordName() {
        return m_sRecordName;
    }

    public void setRecordName(String recordName) {
        m_sRecordName = recordName;
    }

    public int getRecordSize() {
        return m_nRecordSize;
    }

    public String getRecordSizeInfo(){
        return getStrRecordSize() + StrUtil.spaceOf(DATA_RECORDS);
    }

    public String getStrRecordSize() {
        return getRecordSize() + "";
    }

    public void setRecordSize(int recordSize) {
        m_nRecordSize = recordSize;
    }
}
