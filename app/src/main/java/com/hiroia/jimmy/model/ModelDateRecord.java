package com.hiroia.jimmy.model;

import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.ymd.YMD;
import com.library.android_common.util.StrUtil;

/**
 * Create By Ray on 2018/12/27
 */
public class ModelDateRecord {

    private String m_deviceName = StrUtil.EMPTY;
    private int m_size = 0;
    private YMD m_ymd = YMD.nowYMD();
    private Lst<Integer> m_indexes = Lst.of();

    //-----------------------------------------------------------------
    // Construct
    //-----------------------------------------------------------------

    public ModelDateRecord(){

    }

    public ModelDateRecord(String deviceName, int recordSize, YMD ymd, Lst<Integer> indexes) {
        m_deviceName = deviceName;
        m_size = recordSize;
        m_ymd = ymd;
        m_indexes = indexes;
    }

    //-----------------------------------------------------------------
    // Getter and Setter
    //-----------------------------------------------------------------


    public Lst<Integer> getIndexes() {
        return m_indexes;
    }

    public void setIndexes(Lst<Integer> m_indexes) {
        this.m_indexes = m_indexes;
    }

    public String getDeviceName() {
        return m_deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.m_deviceName = deviceName;
    }

    public int getSize() {
        return m_size;
    }

    public void setSize(int size) {
        this.m_size = size;
    }

    public YMD getYmd() {
        return m_ymd;
    }

    public void setYmd(YMD ymd) {
        this.m_ymd = ymd;
    }

}
