package com.hiroia.jimmy.model.record;

import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.hiroia.jimmy.comp.comp.RecordData;
import com.hiroia.jimmy.comp.comp.TrainingData;
import com.hiroia.jimmy.cs.HttpCs;
import com.library.android_common.component.common.Opt;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.YMDHMS;
import com.library.android_common.component.date.ymd.YMD;
import com.library.android_common.util.JSONUtil;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Create By Ray on 2019/1/29
 */
public class ModelJRecord {

    private long id = 0;
    private String uuid = StrUtil.EMPTY;

    private long jscale_id = 0;
    private long jrecipe_id = 0;
    private boolean isStandard = false;

    private double water = 0.0;
    private double time1 = 0.0;
    private double time2 = 0.0;

    private int score = 0;
    private int stars = 5; // star count Max 為 5

    private YMDHMS record_time = YMDHMS.nowYMDHMS();
    private YMDHMS created_time = YMDHMS.nowYMDHMS();
    private YMDHMS updated_time = YMDHMS.nowYMDHMS();

    private JScaleMode mode = JScaleMode.E_MODE;
    private EState state = EState.DEFAULT_STATE;

    private RecordData recordData = new RecordData();
    private TrainingData trainingData = new TrainingData();


    // stars //
    public Lst<Integer> m_scoreRules = Lst.of(801, 3001, 4501, 7501);


    //------------------------------------------------------
    // Construct
    //------------------------------------------------------

    public static ModelJRecord decodeJSON(JSONObject jsObj) {

        ModelJRecord m = new ModelJRecord();
        // Check if jsObj is null //
        if (jsObj == null) {
            LogUtil.e(ModelJRecord.class, " decodeJSON(), jsObj is null reference !!");
            return m;
        }

        m.setId(Opt.ofJson(jsObj, "id").parseToLong().get());
        m.setUuid(Opt.ofJson(jsObj, "uuid").get());
        m.setJscale_id(Opt.ofJson(jsObj, "jscale_id").parseToLong().get());
        m.setJrecipe_id(Opt.ofJson(jsObj, "jrecipe_id").parseToLong().get());
        m.setStandard(jsObj.optBoolean("is_standard"));
        m.setMode(getMode(Opt.ofJson(jsObj, "mode").parseToInt().get()));
        m.setState(getState(Opt.ofJson(jsObj, "state").parseToInt().get()));
        m.setWater(Opt.ofJson(jsObj, "water").parseToDouble().get());
        m.setTime1(Opt.ofJson(jsObj, "time1").parseToDouble().get());
        m.setTime2(Opt.ofJson(jsObj, "time2").parseToDouble().get());
        m.setRecord_time(YMDHMS.parseByISO8601(Opt.ofJson(jsObj, "record_time").get()));
        m.setScore(Opt.ofJson(jsObj, "score").parseToInt().get());
        m.setRecordData(RecordData.parse(Opt.ofJson(jsObj, "record_data").get()));
        m.setTrainingData(TrainingData.parse(Opt.ofJson(jsObj, "training_data").get()));

        m.setCreated_time(YMDHMS.parseByISO8601(Opt.ofJson(jsObj, "created_at").get()));
        m.setUpdated_time(YMDHMS.parseByISO8601(Opt.ofJson(jsObj, "updated_at").get()));

        return m;
    }

    // Parse Standard 的 JSON Format 不同於 decode JSON //
    public static ModelJRecord decodeJSONStd(JSONObject jsObj){
        if (jsObj == null) {
            LogUtil.e(ModelJRecord.class, " decodeJSON(), jsObj is null reference !!");
            return new ModelJRecord();
        }

        String rlt = Opt.ofJson(jsObj, HttpCs.RESULT).get();
        if (rlt.isEmpty()){
            LogUtil.e(ModelJRecord.class, " decodeJSONStd(), jsObj has not found result key !!");
            return new ModelJRecord();
        }

        jsObj = JSONUtil.toJSObj(rlt);
        if (jsObj == null){
            LogUtil.e(ModelJRecord.class, " decodeJSONStd(), is not JSON format !!");
            return new ModelJRecord();
        }
        return decodeJSON(jsObj);
    }


    public static Lst<ModelJRecord> decodeJSONs(JSONArray jsArr) {
        Lst<ModelJRecord> ms = Lst.of();
        for (int i = 0; i < jsArr.length(); i++) {
            try {
                JSONObject jsObj = jsArr.getJSONObject(i);
                ModelJRecord m = ModelJRecord.decodeJSON(jsObj);
                ms.add(m);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ms;
    }

    //------------------------------------------------------
    // Public Method
    //------------------------------------------------------

    public static Lst<ModelJRecord> sortByDate(Lst<ModelJRecord> ms) {

        Lst<ModelJRecord> rlts = Lst.of();
        for (int i = 0; i < ms.size(); i++) {
            ModelJRecord m = ms.get(i);
            YMD date = m.getCreated_time().ymd();

            if (rlts.size() == 0 || rlts.size() == i + 1) {
                rlts.add(ms.get(i));
            } else if (rlts.last().getCreated_time().ymd().lessThan(date))
                rlts.add(ms.get(i));
            else
                rlts.add(rlts.size() - 1, ms.get(i));
        }
        return rlts;
    }

    public void println(String filter) {
        LogUtil.d(ModelJRecord.class, filter + " id = " + getId());
        LogUtil.d(ModelJRecord.class, filter + " uuid = " + getUuid());
        LogUtil.d(ModelJRecord.class, filter + " JScale Id = " + getJscale_id());
        LogUtil.d(ModelJRecord.class, filter + " JRecipe Id = " + getJrecipe_id());
        LogUtil.d(ModelJRecord.class, filter + " is Standard = " + isStandard());
        LogUtil.d(ModelJRecord.class, filter + " Mode = " + getMode().name());
        LogUtil.d(ModelJRecord.class, filter + " State = " + getState().name());
        LogUtil.d(ModelJRecord.class, filter + " Water = " + getWater());
        LogUtil.d(ModelJRecord.class, filter + " Time 1 = " + getTime1());
        LogUtil.d(ModelJRecord.class, filter + " Time 2 = " + getTime2());
        LogUtil.d(ModelJRecord.class, filter + " Record Time = " + getRecord_time().formatISO8601());
        LogUtil.d(ModelJRecord.class, filter + " Score = " + getScore());
        LogUtil.d(ModelJRecord.class, filter + " Training Data = " + getTrainingData().datas());
        LogUtil.d(ModelJRecord.class, filter + " Record Data = " + getRecordData().datas());
        LogUtil.d(ModelJRecord.class, filter + " Created Time = " + getCreated_time().formatISO8601());
        LogUtil.d(ModelJRecord.class, filter + " Updated Time = " + getUpdated_time().formatISO8601());
        LogUtil.d(ModelJRecord.class, filter + StrUtil.DIVIDER);
    }


    //------------------------------------------------------
    // Getter and Setter
    //------------------------------------------------------
    public boolean isSingleMode() {
        return getMode() == JScaleMode.TS_MODE;
    }

    public boolean isRandomMode() {
        return getMode() == JScaleMode.TR_MODE;
    }

    public boolean isEMode() {
        return getMode() == JScaleMode.E_MODE;
    }

    public boolean isPMode() {
        return getMode() == JScaleMode.P_MODE;
    }

    public static JScaleMode getMode(int mode) {
        return Lst.of(JScaleMode.class.getEnumConstants()).get(mode);
    }

    public static EState getState(int state) {
        return Lst.of(EState.class.getEnumConstants()).get(state);
    }

    public RecordData getRecordData() {
        return recordData;
    }

    public void setRecordData(RecordData recordData) {
        this.recordData = recordData;
    }

    public TrainingData getTrainingData() {
        return trainingData;
    }

    public void setTrainingData(TrainingData trainingData) {
        this.trainingData = trainingData;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public YMDHMS getCreated_time() {
        return created_time;
    }

    public void setCreated_time(YMDHMS created_time) {
        this.created_time = created_time;
    }

    public YMDHMS getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(YMDHMS updated_time) {
        this.updated_time = updated_time;
    }

    public long getJscale_id() {
        return jscale_id;
    }

    public void setJscale_id(long jscale_id) {
        this.jscale_id = jscale_id;
    }

    public long getJrecipe_id() {
        return jrecipe_id;
    }

    public void setJrecipe_id(long jrecipe_id) {
        this.jrecipe_id = jrecipe_id;
    }

    public boolean isStandard() {
        return isStandard;
    }

    public void setStandard(boolean standard) {
        isStandard = standard;
    }

    public double getWater() {
        return water;
    }

    public void setWater(double water) {
        this.water = water;
    }

    public double getTime1() {
        return time1;
    }

    public void setTime1(double time1) {
        this.time1 = time1;
    }

    public double getTime2() {
        return time2;
    }

    public void setTime2(double time2) {
        this.time2 = time2;
    }

    public int getScore() {
        return score;
    }

    public String getStrScore() {
        return String.valueOf(getScore());
    }

    public void setScore(int score) {
        this.score = score;
    }

    public YMDHMS getRecord_time() {
        return record_time;
    }

    public void setRecord_time(YMDHMS record_time) {
        this.record_time = record_time;
    }

    public JScaleMode getMode() {
        return mode;
    }

    public void setMode(JScaleMode mode) {
        this.mode = mode;
    }

    public EState getState() {
        return state;
    }

    public void setState(EState state) {
        this.state = state;
    }

    public void setState(JimmyInfo.Mode mode) {
        switch (mode) {
            default:
                state = EState.DEFAULT_STATE;
            case E1:
                state = EState.E1_STATE;
                break;
            case E2:
                state = EState.E2_STATE;
                break;
            case E3:
                state = EState.E3_STATE;
                break;
        }
    }

    public int getStars() {
        for (int i = 0; i < m_scoreRules.size(); i++)
            if (getScore() < m_scoreRules.get(i)) {
                stars = i + 1;
                break;
            }
        return stars; // 最高為 5 顆星
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    //------------------------------------------------------
    // Enum
    //------------------------------------------------------

    public enum EState {
        DEFAULT_STATE("Default State", 0),
        E1_STATE("Espresso 1 State", 1),
        E2_STATE("Espresso 2 State", 2),
        E3_STATE("Espresso 3 State", 3);

        //---------------------------------------
        private String m_desc;
        private int m_id;

        EState(String desc, int id) {
            m_desc = desc;
            m_id = id;
        }

        //---------------------------------------

        public String desc() {
            return m_desc;
        }

        public int id() {
            return m_id;
        }
    }

    public enum JScaleMode {

        E_MODE("Espresso Mode", 1),
        P_MODE("Pour Over Mode", 2),
        TS_MODE("Training Single Mode", 3),
        TR_MODE("Training Random Mode", 4);

        //---------------------------------------

        private String m_desc;
        private int m_id;

        JScaleMode(String desc, int id) {
            m_desc = desc;
            m_id = id;
        }

        //---------------------------------------

        public String desc() {
            return m_desc;
        }

        public int id() {
            return m_id;
        }

        public String strId() {
            return m_id + "";
        }

    }
}
