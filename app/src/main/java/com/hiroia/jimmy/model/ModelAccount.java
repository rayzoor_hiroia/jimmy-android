package com.hiroia.jimmy.model;

import com.hiroia.jimmy.cs.HttpCs;
import com.library.android_common.component.common.Opt;
import com.library.android_common.component.date.ymd.YMD;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Create By Ray on 2019/1/18
 * To save account of model
 */
public class ModelAccount {

    private ModelAccount m_self;

    private long id = 0;
    private String token = StrUtil.EMPTY;
    private String Email = StrUtil.EMPTY;
    private String Password = StrUtil.EMPTY;
    private YMD birth = YMD.of(1980, 1, 1);

    private int sexual = 0; // 0 = male, 1 = female;
    private String intro = StrUtil.EMPTY;
    private String photo_url = StrUtil.EMPTY;
    private String userName = StrUtil.EMPTY;

    private String s_facebook_email = StrUtil.EMPTY;
    private String userEmail = StrUtil.EMPTY;
    private int userType = 0;
    private String fbId = StrUtil.EMPTY;
    private String fbPhoto = StrUtil.EMPTY;
    private int userLevel = 0;
    private int userSn = 0;
    private int emailAuthState = 0;
    private String fbToken = StrUtil.EMPTY;
    private String uuid = StrUtil.EMPTY;
    private String recentToken = StrUtil.EMPTY;

    //--- Update Account ---
    private int update_count = 0;
    private int delete_count = 0;

    //---Create Account Fail---
    private String Message = StrUtil.EMPTY;
    private String Name = StrUtil.EMPTY;


    //---FB 官方---
    private String fb_ID = StrUtil.EMPTY;
    private String fb_Name = StrUtil.EMPTY;
    private String fb_Email = StrUtil.EMPTY;

    //----------------------------------------------------------
    // Construct
    //----------------------------------------------------------
    public ModelAccount() {
        m_self = this;
    }

    public static ModelAccount create() {
        return new ModelAccount();
    }

    // Upload Photo //
    static public ModelAccount decodeUploadPhotoJSON(JSONObject jsObj) throws JSONException {
        ModelAccount m = new ModelAccount();
        jsObj = jsObj.getJSONObject(HttpCs.RESULT);
//        jsObj = Opt.ofJson(jsObj, HttpCs.RESULT).toJsonObj().get(); // test

        m.userEmail = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.USER_EMAIL).replaceToStrEmpty(StrUtil.STR_NULL).trim().get();
        m.userType = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.USER_TYPE).replace(StrUtil.STR_NULL, "0").parseToInt().get();
//        m.userToken = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), "userToken").replaceToStrEmpty(StrUtil.STR_NULL).trim().get();
        m.fbPhoto = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.FBPHOTO).replaceToStrEmpty(StrUtil.STR_NULL).trim().get();
        m.userName = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.USER_NAME).replaceToStrEmpty(StrUtil.STR_NULL).trim().get();
        m.userSn = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.USER_SN).replace(StrUtil.STR_NULL, "0").parseToInt().get();
        m.uuid = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.UUID).replaceToStrEmpty(StrUtil.STR_NULL).trim().get();
        m.recentToken = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.RECENTTOKEN).replaceToStrEmpty(StrUtil.STR_NULL).trim().get();

        return m;
    }


    // Create Account Fail //
    static public ModelAccount decodeRegisterFailJSON(JSONObject jsObj) throws JSONException {
        ModelAccount m = new ModelAccount();
        m.Message = jsObj.optString(HttpCs.ERROR_MESSAGE);
        jsObj = jsObj.getJSONObject(HttpCs.DETAIL);

        m.Name = Opt.ofJson(jsObj, HttpCs.NAME).replaceToStrEmpty(StrUtil.STR_NULL).trim().get();
        m.Email = Opt.ofJson(jsObj, HttpCs.EMAIL).replaceToStrEmpty(StrUtil.STR_NULL).trim().get();
        m.Password = Opt.ofJson(jsObj, HttpCs.PASSWORD).replaceToStrEmpty(StrUtil.STR_NULL).trim().get();

        return m;
    }


    // Update Account decode //
    static public ModelAccount decodeUploadAccountJSON(JSONObject jsObj) throws JSONException {
        ModelAccount m = new ModelAccount();
        jsObj = jsObj.getJSONObject(HttpCs.RESULT);

        m.update_count = Opt.ofJson(jsObj, HttpCs.UPDATE_COUNT).replace(StrUtil.STR_NULL, "0").parseToInt().get();
        m.delete_count = Opt.ofJson(jsObj, HttpCs.DELETE_COUNT).replace(StrUtil.STR_NULL, "0").parseToInt().get();
        m.userEmail = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.USER_EMAIL).replaceToStrEmpty(StrUtil.STR_NULL).trim().get();
        m.userType = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.USER_TYPE).replace(StrUtil.STR_NULL, "0").parseToInt().get();
        m.fbPhoto = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.FBPHOTO).replaceToStrEmpty(StrUtil.STR_NULL).trim().get();
        m.userName = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.USER_NAME).replaceToStrEmpty(StrUtil.STR_NULL).trim().get();
        m.userSn = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.USER_SN).replace(StrUtil.STR_NULL, "0").parseToInt().get();
        m.uuid = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.UUID).replaceToStrEmpty(StrUtil.STR_NULL).trim().get();

        return m;
    }


    // Email Login decode //
    static public ModelAccount decodeEmailJSON(JSONObject jsObj) throws JSONException {
        ModelAccount m = new ModelAccount();
        jsObj = jsObj.getJSONObject(HttpCs.RESULT);

        m.userEmail = jsObj.getJSONObject(HttpCs.USER).getString(HttpCs.USER_EMAIL);
        m.userType = jsObj.getJSONObject(HttpCs.USER).getInt(HttpCs.USER_TYPE);
        m.fbPhoto = Opt.ofJson(jsObj.getJSONObject(HttpCs.USER), HttpCs.FBPHOTO).replaceToStrEmpty(StrUtil.STR_NULL).trim().get();
        m.userName = jsObj.getJSONObject(HttpCs.USER).getString(HttpCs.USER_NAME);
        m.userSn = jsObj.getJSONObject(HttpCs.USER).getInt(HttpCs.USER_SN);
        m.uuid = jsObj.getJSONObject(HttpCs.USER).getString(HttpCs.UUID);
        m.recentToken = jsObj.getJSONObject(HttpCs.USER).getString(HttpCs.RECENTTOKEN);
        m.token = jsObj.getString(HttpCs.TOKEN);
        m.Email = jsObj.getString(HttpCs.EMAIL);

        return m;
    }


    // FB Login (JIMMY API) decode //
    static public ModelAccount decodeFBJimmyJSON(JSONObject jsObj) throws JSONException {
        ModelAccount m = new ModelAccount();
        jsObj = jsObj.getJSONObject(HttpCs.RESULT);

        m.userEmail = jsObj.getJSONObject(HttpCs.USER).getString(HttpCs.USER_EMAIL);
        m.userName = jsObj.getJSONObject(HttpCs.USER).getString(HttpCs.USER_NAME);
        m.userType = jsObj.getJSONObject(HttpCs.USER).getInt(HttpCs.USER_TYPE);
        m.userLevel = jsObj.getJSONObject(HttpCs.USER).getInt(HttpCs.USER_LEVEL);
        m.emailAuthState = jsObj.getJSONObject(HttpCs.USER).getInt(HttpCs.EMAIL_AUTH_STATE);
        m.uuid = jsObj.getJSONObject(HttpCs.USER).getString(HttpCs.UUID);
        m.s_facebook_email = jsObj.getJSONObject(HttpCs.USER).getString(HttpCs.S_FACEBOOK_EMAIL);
        m.recentToken = jsObj.getJSONObject(HttpCs.USER).getString(HttpCs.RECENTTOKEN);
        m.fbToken = jsObj.getJSONObject(HttpCs.USER).getString(HttpCs.FBTOKEN);
        m.fbId = jsObj.getJSONObject(HttpCs.USER).getString(HttpCs.FB_ID);
        m.fbPhoto = jsObj.getJSONObject(HttpCs.USER).getString(HttpCs.FBPHOTO);
        m.token = jsObj.getString(HttpCs.TOKEN);

        return m;
    }


    // FB Login (FB 官方) decode //
    static public ModelAccount decodeFBJSON(JSONObject jsObj) {
        ModelAccount m = new ModelAccount();

        try {
            m.fb_ID = jsObj.getString(HttpCs.ID);
            m.fb_Name = jsObj.getString(HttpCs.NAME);
            m.fb_Email = jsObj.getString(HttpCs.EMAIL);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return m;
    }


    //----------------------------------------------------------
    // Public Method
    //----------------------------------------------------------

    public boolean isEmpty() {
        return getToken().isEmpty();
    }

    public void println() {
        println(StrUtil.EMPTY);
    }

    public void println(String filter) {
        LogUtil.d(ModelAccount.class, filter + " Id = " + getId());
        LogUtil.d(ModelAccount.class, filter + " Token = " + getToken());
        LogUtil.d(ModelAccount.class, filter + " Email = " + getEmail());
        LogUtil.d(ModelAccount.class, filter + " Password = " + getPassword());
        LogUtil.d(ModelAccount.class, filter + " Birth = " + getBirth());
        LogUtil.d(ModelAccount.class, filter + " Sexual = " + getSexual());
        LogUtil.d(ModelAccount.class, filter + " Introduction = " + getIntro());
        LogUtil.d(ModelAccount.class, filter + " Photo_url = " + getPhoto_url());
        LogUtil.d(ModelAccount.class, filter + " EmailAuthState = " + getEmailAuthState());
        LogUtil.d(ModelAccount.class, filter + " FB官方 Fb_Name = " + getFb_Name());
        LogUtil.d(ModelAccount.class, filter + " FB官方 Fb_Email = " + getFb_Email());
        LogUtil.d(ModelAccount.class, filter + " FB官方 Fb_ID = " + getFb_ID());
        LogUtil.d(ModelAccount.class, filter + " FbId = " + getFbId());
        LogUtil.d(ModelAccount.class, filter + " FbPhoto = " + getFbPhoto());
        LogUtil.d(ModelAccount.class, filter + " FbToken = " + getFbToken());
        LogUtil.d(ModelAccount.class, filter + " S_facebook_email = " + getS_facebook_email());
        LogUtil.d(ModelAccount.class, filter + " RecentToken = " + getRecentToken());
        LogUtil.d(ModelAccount.class, filter + " Uuid = " + getUuid());
        LogUtil.d(ModelAccount.class, filter + " UserName = " + getUserName());
        LogUtil.d(ModelAccount.class, filter + " UserEmail = " + getUserEmail());
        LogUtil.d(ModelAccount.class, filter + " UserLevel = " + getUserLevel());
        LogUtil.d(ModelAccount.class, filter + " UserSn = " + getUserSn());
        LogUtil.d(ModelAccount.class, filter + " UserType = " + getUserType());
        LogUtil.d(ModelAccount.class, filter + StrUtil.DIVIDER);
    }

    //----------------------------------------------------------
    // Getter and Setter
    //----------------------------------------------------------
    public long getId() {
        return id;
    }

    public ModelAccount setId(long id) {
        this.id = id;
        return m_self;
    }

    public String getToken() {
        return token;
//        return "V3HIROIATK5c622aa0dfd4a"; // TODO 需要改回來
    }

    public ModelAccount setToken(String token) {
        this.token = token;
        return m_self;
    }

    public String getEmail() {
        return Email;
    }

    public ModelAccount setEmail(String email) {
        Email = email;
        return m_self;
    }

    public String getPassword() {
        return Password;
    }

    public ModelAccount setPassword(String password) {
        Password = password;
        return m_self;
    }

    public YMD getBirth() {
        return birth;
    }

    public ModelAccount setBirth(YMD birth) {
        this.birth = birth;
        return m_self;
    }

    public int getSexual() {
        return sexual;
    }

    public ModelAccount setSexual(int sexual) {
        this.sexual = sexual;
        return m_self;
    }

    public String getIntro() {
        return intro;
    }

    public ModelAccount setIntro(String intro) {
        this.intro = intro;
        return m_self;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public ModelAccount setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
        return m_self;
    }

    public String getUserName() {
        return userName;
    }

    public ModelAccount setUserName(String userName) {
        this.userName = userName;
        return m_self;
    }

    public String getS_facebook_email() {
        return s_facebook_email;
    }

    public ModelAccount setS_facebook_email(String s_facebook_email) {
        this.s_facebook_email = s_facebook_email;
        return m_self;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public ModelAccount setUserEmail(String userEmail) {
        this.userEmail = userEmail;
        return m_self;
    }

    public int getUserType() {
        return userType;
    }

    public ModelAccount setUserType(int userType) {
        this.userType = userType;
        return m_self;
    }

    public String getFbId() {
        return fbId;
    }

    public ModelAccount setFbId(String fbId) {
        this.fbId = fbId;
        return m_self;
    }

    public String getFbPhoto() {
        return fbPhoto;
    }

    public ModelAccount setFbPhoto(String fbPhoto) {
        this.fbPhoto = fbPhoto;
        return m_self;
    }

    public int getUserLevel() {
        return userLevel;
    }

    public ModelAccount setUserLevel(int userLevel) {
        this.userLevel = userLevel;
        return m_self;
    }

    public int getUserSn() {
        return userSn;
    }

    public ModelAccount setUserSn(int userSn) {
        this.userSn = userSn;
        return m_self;
    }

    public int getEmailAuthState() {
        return emailAuthState;
    }

    public ModelAccount setEmailAuthState(int emailAuthState) {
        this.emailAuthState = emailAuthState;
        return m_self;
    }

    public String getFbToken() {
        return fbToken;
    }

    public ModelAccount setFbToken(String fbToken) {
        this.fbToken = fbToken;
        return m_self;
    }

    public String getUuid() {
        return uuid;
    }

    public ModelAccount setUuid(String uuid) {
        this.uuid = uuid;
        return m_self;
    }

    public String getRecentToken() {
        return recentToken;
    }

    public ModelAccount setRecentToken(String recentToken) {
        this.recentToken = recentToken;
        return m_self;
    }

    public String getMessage() {
        return Message;
    }

    public ModelAccount setMessage(String message) {
        Message = message;
        return m_self;
    }

    public String getName() {
        return Name;
    }

    public ModelAccount setName(String name) {
        Name = name;
        return m_self;
    }

    // FB 官方
    public String getFb_ID() {
        return fb_ID;
    }

    public ModelAccount setFb_ID(String fb_ID) {
        this.fb_ID = fb_ID;
        return m_self;
    }

    public String getFb_Name() {
        return fb_Name;
    }

    public ModelAccount setFb_Name(String fb_Name) {
        this.fb_Name = fb_Name;
        return m_self;
    }

    public String getFb_Email() {
        return fb_Email;
    }

    public ModelAccount setFb_Email(String fb_Email) {
        this.fb_Email = fb_Email;
        return m_self;
    }

}
