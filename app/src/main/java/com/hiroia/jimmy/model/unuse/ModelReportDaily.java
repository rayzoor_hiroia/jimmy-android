package com.hiroia.jimmy.model.unuse;

import com.library.android_common.component.common.Opt;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.hms.HMS;
import com.library.android_common.component.date.hms.MS;
import com.library.android_common.component.date.ymd.YMD;
import com.library.android_common.util.IntUtil;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.json.JSONObject;

/**
 * Created by Bruce on 2018/12/6.
 */
public class ModelReportDaily {

    private long m_id = 0;
    private Double m_dExtraction = 0.0;
    private String m_sTime = StrUtil.EMPTY;
    private HMS m_hms = HMS.nowHMS();

    //---------------------------------------
    // Construct
    //---------------------------------------

    public ModelReportDaily() {

    }

    public ModelReportDaily(Double extraction, String time, HMS hms) {
        m_id =  Opt.of(YMD.nowYMD().toSymbolString(StrUtil.EMPTY) + HMS.nowHMS().toPureString()).parseToLong().get();
        m_dExtraction = extraction;
        m_sTime = time;
        m_hms = hms;
    }

    public static ModelReportDaily of(Double extraction, String time, HMS hms) {
        return new ModelReportDaily(extraction, time, hms);
    }

    public static ModelReportDaily decodeJSONObject(JSONObject obj) {
        ModelReportDaily model = new ModelReportDaily();
        return model;
    }

    // test
    public static Lst<ModelReportDaily> _testData() {
        Lst<ModelReportDaily> recordTest = Lst.of();
        for (int i = 0; i < IntUtil.rangeRandom(20, 70); i++) {
            recordTest.add(ModelReportDaily.of(IntUtil.rangeRandom(10, 30).doubleValue(), "1+20s", HMS.random()));
        }
        return recordTest;
    }


    //----------------------------------------------------------------
    // Println
    //----------------------------------------------------------------

    public void printlnt(String filter) {
        LogUtil.d(ModelReportDaily.class, "Extraction = " + getExtraction());
        LogUtil.d(ModelReportDaily.class, "Time = " + getTime());
        LogUtil.d(ModelReportDaily.class, "TotalTime = " + getHms());
        LogUtil.d(ModelReportDaily.class, " // " + StrUtil.DIVIDER);
    }


    //----------------------------------------------------------------
    // Getter and Setter
    //----------------------------------------------------------------


    public long getId() {
        return m_id;
    }

    public void setId(long id) {
        this.m_id = id;
    }

    public Double getExtraction() {
        return m_dExtraction;
    }

    public void setExtraction(Double extraction) {
        m_dExtraction = extraction;
    }

    public String getTime() {
        return m_sTime;
    }

    public void setTime(String time) {
        m_sTime = time;
    }

    public HMS getHms() {
        return m_hms;
    }

    public void setHms(HMS hms) {
        this.m_hms = hms;
    }

    public MS getMS() {
        return m_hms.ms();
    }
}
