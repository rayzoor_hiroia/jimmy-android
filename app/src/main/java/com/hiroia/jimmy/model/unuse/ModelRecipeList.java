package com.hiroia.jimmy.model.unuse;

import com.library.android_common.component.date.ymd.YMD;
import com.library.android_common.util.StrUtil;

import org.json.JSONObject;

/**
 * Created by Bruce on 2018/12/11.
 */
public class ModelRecipeList {

    private String m_sRecipeName = StrUtil.EMPTY;
    private YMD m_ymdDate = YMD.nowYMD();
    private double m_dBeanWeight = 0.0;
    private int m_nWaterVol = 0;
    private int m_nTemperature = 0;

    private String m_sRatio = StrUtil.EMPTY;


    //------------------------------------------------------
    // Construct
    //------------------------------------------------------
    public ModelRecipeList() {

    }

    public ModelRecipeList(String recipeName, YMD date, double beanWeight, String ratio, int waterVol, int temperature) {
        m_sRecipeName = recipeName;
        m_ymdDate = date;
        m_dBeanWeight = beanWeight;
        m_sRatio = ratio;
        m_nWaterVol = waterVol;
        m_nTemperature = temperature;
    }

    public static ModelRecipeList of(String recipeName, YMD date, double beanWeight, String ratio, int waterVol, int temperature) {
        return new ModelRecipeList(recipeName, date, beanWeight, ratio, waterVol, temperature);
    }

    public static ModelRecipeList decodeJson(JSONObject obj) {
        ModelRecipeList model = new ModelRecipeList();
        return model;
    }

    //------------------------------------------------------
    // Public Method
    //------------------------------------------------------


    //-------------------------------------------------------
    // Getter and Setter
    //-------------------------------------------------------

    public String getRecipeName() {
        return m_sRecipeName;
    }

    public void setRecipeName(String recipeName) {
        this.m_sRecipeName = recipeName;
    }

    public YMD getDate() {
        return m_ymdDate;
    }

    public void setDate(YMD date) {
        this.m_ymdDate = date;
    }

    public double getBeanWeight() {
        return m_dBeanWeight;
    }

    public void setBeanWeight(double beanWeight) {
        this.m_dBeanWeight = beanWeight;
    }

    public int getWaterVol() {
        return m_nWaterVol;
    }

    public void setWaterVol(int waterVol) {
        this.m_nWaterVol = waterVol;
    }

    public int getTemperature() {
        return m_nTemperature;
    }

    public void setTemperature(int temperature) {
        this.m_nTemperature = temperature;
    }

    public String getRatio() {
        return m_sRatio;
    }

    public void setRatio(String ratio) {
        this.m_sRatio = ratio;
    }
}
