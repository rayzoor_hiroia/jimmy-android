package com.hiroia.jimmy.model;

import android.content.ContentValues;

import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.ymd.YMD;
import com.library.android_common.util.IntUtil;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

/**
 * Create By Ray on 2018/12/12
 */
public class ModelTModeRecord {

    private long m_id = 0;
    private int m_mode = 0; // single(0) or random(1)
    private int m_score = 0; // 分數
    private int m_stars = 0; // 星星數（不會超過 5
    private YMD m_ymd = YMD.nowYMD();

    //----------------------------------------------------------------
    // Construct
    //----------------------------------------------------------------

    public ModelTModeRecord() {

    }

    public ModelTModeRecord(int mode, int score, int stars, YMD ymd) {
        m_mode = mode;
        m_score = score;
        m_stars = stars;
        m_ymd = ymd;
    }

    public static ModelTModeRecord of(int mode, int score, int stars, YMD ymd) {
        return new ModelTModeRecord(mode, score, stars, ymd);
    }

    //----------------------------------------------------------------
    // Public Method
    //----------------------------------------------------------------
    // 測試用 //
    public static Lst<ModelTModeRecord> _fakeRecords() {
        Lst<ModelTModeRecord> records = Lst.of();
        for (int i = 0; i < IntUtil.rangeRandom(50, 100); i++) {
            records.add(ModelTModeRecord.of(Lst.of(0, 1).randomGet(), IntUtil.rangeRandom(50, 10000), IntUtil.rangeRandom(1, 5), YMD.random()));
        }
        return records;
    }

    //----------------------------------------------------------------
    // Println
    //----------------------------------------------------------------

    public void printlnt(){
        println(StrUtil.EMPTY);
    }

    public void println(String filter){
        LogUtil.d(ModelTModeRecord.class, filter.concat(" id = " + getId()));
        LogUtil.d(ModelTModeRecord.class, filter.concat(" mode = " + Lst.of("single", "random").get(getMode())));
        LogUtil.d(ModelTModeRecord.class, filter.concat(" score = " + getScore()));
        LogUtil.d(ModelTModeRecord.class, filter.concat(" stars = " + getStars()));
        LogUtil.d(ModelTModeRecord.class, filter.concat(" YMD = " + getYmd().toDashString()));
        LogUtil.d(ModelTModeRecord.class, filter.concat(StrUtil.DIVIDER));
    }

    //----------------------------------------------------------------
    // Getter and Setter
    //----------------------------------------------------------------

    public boolean isSingleRecord() {
        return m_mode == 0;
    }

    public boolean isRandomRecord() {
        return m_mode == 1;
    }

    public long getId() {
        return m_id;
    }

    public void setId(long id) {
        this.m_id = id;
    }

    public int getMode() {
        return m_mode;
    }

    public void setMode(int mode) {
        this.m_mode = mode;
    }

    public int getScore() {
        return m_score;
    }

    public String getStrScore() {
        return String.valueOf(m_score);
    }

    public void setScore(int score) {
        this.m_score = score;
    }

    public int getStars() {
        return m_stars;
    }

    public void setStars(int stars) {
        this.m_stars = stars;
    }

    public YMD getYmd() {
        return m_ymd;
    }

    public void setYmd(YMD ymd) {
        this.m_ymd = ymd;
    }
}
