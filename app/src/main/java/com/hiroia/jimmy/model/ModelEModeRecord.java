package com.hiroia.jimmy.model;

import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.ymd.YMD;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

/**
 * Create By Ray on 2018/12/24
 */
public class ModelEModeRecord {

    private long m_id = 0;
    private YMD m_ymd = YMD.nowYMD();
    private String m_deviceName = StrUtil.EMPTY;
    private double m_weight = 0.0;
    private String m_ttime = StrUtil.EMPTY; // total time
    private long m_time = 0;
    private boolean m_isStad = false; // If is standard

    // Comp //
    private static YMD m_currYmd = YMD.of(1, 1, 1); // default date

    //------------------------------------------------------------
    // Construct
    //------------------------------------------------------------

    public ModelEModeRecord() {

    }

    public ModelEModeRecord(long id, YMD ymd, String deviceName, double weight, String ttime, long time, boolean isStad) {
        m_id = id;
        m_ymd = ymd;
        m_deviceName = deviceName;
        m_weight = weight;
        m_ttime = ttime;
        m_time = time;
        m_isStad = isStad;
    }

    public static ModelEModeRecord of(long id, YMD ymd, String deviceName, double weight, String ttime, long time, boolean isStad) {
        return new ModelEModeRecord(id, ymd, deviceName, weight, ttime, time, isStad);
    }

    public static Lst<ModelEModeRecord> distinctByDate(Lst<ModelEModeRecord> records) {
        Lst<ModelEModeRecord> ms = Lst.of();
        Lst<String> ymds = Lst.of();
        Lst<Integer> indexes = Lst.of();
        for (int i = 0; i < records.size(); i++) {
            ModelEModeRecord m = records.get(i);
            if (!ymds.contains(m.getYMD().toString())) {
                ymds.add(m.getYMD().toString());
                indexes.add(i);
            }
        }

        for (Integer i : indexes.toList()) {
            ms.add(records.get(i));
        }
        return ms;
    }

    // 變成 EMode 首頁要使用的 E Mode report
    public static Lst<ModelDateRecord> toDateRecords(ArrayList<ModelEModeRecord> records) {
        Lst<ModelDateRecord> ms = Lst.of();

        // 取得日期的 distinct 數 //
        Lst<ModelEModeRecord> distinctDateMs = distinctByDate(Lst.of(records));
        for (ModelEModeRecord m : distinctDateMs.toList()) {
            int ymdSize = getYMDSize(Lst.of(records), m.getYMD());
            Lst<Integer> indexes = getYMDIndex(Lst.of(records), m.getYMD());
            ms.add(new ModelDateRecord(m.getDeviceName(), ymdSize, m.getYMD(), indexes));
        }

        // Sort by date //
        Lst<ModelDateRecord> sortMs = Lst.of();
        for (int i = 0; i < ms.size(); i++) {
            if (sortMs.size() == 0 || sortMs.size() == i + 1) {
                sortMs.add(ms.get(i));
            } else if (sortMs.last().getYmd().lessThan(ms.get(i).getYmd()))
                sortMs.add(ms.get(i));
            else
                sortMs.add(sortMs.size() - 1, ms.get(i));
        }

        return sortMs;
    }

    private static int getYMDSize(Lst<ModelEModeRecord> records, final YMD ymd) {
        int size = 0;
        for (ModelEModeRecord m : records.toList()) {
            if (ymd.dateEquals(m.getYMD()))
                size += 1;
        }
        return size;
    }

    private static Lst<Integer> getYMDIndex(Lst<ModelEModeRecord> records, final YMD ymd) {
        Lst<Integer> indexes = Lst.of();
        for (int i = 0; i < records.toList().size(); i++) {
            if (ymd.dateEquals(records.get(i).getYMD()))
                indexes.add(i);
        }
        return indexes;
    }


    //-----------------------------------------------------------
    // Inner Class
    //-----------------------------------------------------------
//    public class ModelDateRecord {
//
//        private String m_deviceName;
//        private int m_size;
//        private YMD m_ymd;
//
//        public ModelDateRecord(String deviceName, int recordSize, YMD ymd) {
//
//        }
//
//        public String getDeviceName() {
//            return m_deviceName;
//        }
//
//        public void setDeviceName(String deviceName) {
//            this.m_deviceName = deviceName;
//        }
//
//        public int getSize() {
//            return m_size;
//        }
//
//        public void setSize(int size) {
//            this.m_size = size;
//        }
//
//        public YMD getYmd() {
//            return m_ymd;
//        }
//
//        public void setYmd(YMD ymd) {
//            this.m_ymd = ymd;
//        }
//    }


    //------------------------------------------------------------
    // Public Method
    //------------------------------------------------------------
    public void println() {
        println(StrUtil.EMPTY);
    }

    public void println(String filter) {
        LogUtil.d(getClass(), filter.concat(" id = " + m_id));
        LogUtil.d(getClass(), filter + " ymd = " + m_ymd.toSlashString());
        LogUtil.d(getClass(), filter + " device name = " + m_deviceName);
        LogUtil.d(getClass(), filter + " weight = " + m_weight);
        LogUtil.d(getClass(), filter + " total time = " + m_ttime);
        LogUtil.d(getClass(), filter + " time = " + m_time);
        LogUtil.d(getClass(), filter + " is standard = " + m_isStad);
        LogUtil.d(getClass(), filter + StrUtil.DIVIDER);
    }

    public static void println(Lst<ModelEModeRecord> ms, final String filter) {
        ms.forEach(new Lst.IConsumer<ModelEModeRecord>() {
            @Override
            public void runEach(int i, ModelEModeRecord m) {
                m.println(filter);
            }
        });
    }

    //------------------------------------------------------------
    // Getter and Setter
    //------------------------------------------------------------

    public boolean isStd(){
        return m_isStad;
    }

    public long getId() {
        return m_id;
    }

    public void setId(long id) {
        this.m_id = id;
    }

    public String getDeviceName() {
        return m_deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.m_deviceName = deviceName;
    }

    public double getWeight() {
        return m_weight;
    }

    public void setWeight(double weight) {
        this.m_weight = weight;
    }

    public String getTtime() {
        return m_ttime;
    }

    public void setTtime(String ttime) {
        this.m_ttime = ttime;
    }

    public long getTime() {
        return m_time;
    }

    public void setTime(long time) {
        this.m_time = time;
    }

    public boolean getStandard() {
        return m_isStad;
    }

    public int getNumStandard() {
        return getStandard() ? 0 : 1;
    }

    public void setStandard(boolean stad) {
        m_isStad = stad;
    }

    public YMD getYMD() {
        return m_ymd;
    }

    public void setYMD(YMD m_date) {
        this.m_ymd = m_date;
    }


}
