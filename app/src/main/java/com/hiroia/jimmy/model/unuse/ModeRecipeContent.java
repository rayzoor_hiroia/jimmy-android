package com.hiroia.jimmy.model.unuse;

import com.library.android_common.component.common.Pair;
import com.library.android_common.component.common.Pairs;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

/**
 * Created by Bruce on 2018/12/12.
 */
public class ModeRecipeContent {

    public static final String BEAN_WEIGHT = "Bean Weight";
    public static final String RATIO = "Ratio";
    public static final String WATER_VOLUME = "Water Volume";
    public static final String TEMPERATURE = "Temperature";

    private Pair<String, String> m_pBean = Pair.of(StrUtil.EMPTY, StrUtil.EMPTY);
    private Pair<String, String> m_pRatio = Pair.of(StrUtil.EMPTY, StrUtil.EMPTY);
    private Pair<String, String> m_pWaterVol = Pair.of(StrUtil.EMPTY, StrUtil.EMPTY);
    private Pair<String, String> m_pTemperature = Pair.of(StrUtil.EMPTY, StrUtil.EMPTY);


    // To ModelRecipeList
    public static ModeRecipeContent getItemDataToRecipeList(ModelRecipeList model) {

        ModeRecipeContent itemDataList = new ModeRecipeContent();
        itemDataList.setpBean(Pair.of(BEAN_WEIGHT, model.getBeanWeight() + "g"));
        itemDataList.setpRatio(Pair.of(RATIO, model.getRatio()));
        itemDataList.setpWaterVol(Pair.of(WATER_VOLUME, model.getWaterVol() + "ml"));
        itemDataList.setpTemperature(Pair.of(TEMPERATURE, model.getTemperature() + StrUtil.DEGREE_C));

        return itemDataList;
    }

    public static ModeRecipeContent getItemData(ModeRecipeContent model) {

        ModeRecipeContent itemData = new ModeRecipeContent();
        itemData.setpBean(Pair.of(getKName(BEAN_WEIGHT), model.getpBean().v()));
        itemData.setpRatio(Pair.of(getKName(RATIO), model.getpRatio().v()));
        itemData.setpWaterVol(Pair.of(getKName(WATER_VOLUME), model.getpWaterVol().v()));
        itemData.setpTemperature(Pair.of(getKName(TEMPERATURE), model.getpTemperature().v()));

        return itemData;
    }


    //----------------------------------------------
    // Public Method
    //----------------------------------------------
    public ArrayList<Pair<String, String>> getAll() {
        return Lst.of(getpBean(), getpRatio(), getpWaterVol(), getpTemperature()).toList();
    }

    public ArrayList<Pair<String, String>> getpAllWithoutEmpty() {
        return Pairs.of(getAll()).removeByVal(StrUtil.EMPTY).toList();
    }

    // 要改多國語言
    public static String getKName(String k) {
        switch (k) {
            case BEAN_WEIGHT:
                return k;
            case RATIO:
                return k;
            case WATER_VOLUME:
                return k;
            case TEMPERATURE:
                return k;
            default:
                return k;
        }
    }


    //-------------------------------------------------------
    // Getter and Setter
    //-------------------------------------------------------

    public Pair<String, String> getpBean() {
        return m_pBean;
    }

    public void setpBean(Pair<String, String> bean) {
        this.m_pBean = bean;
    }

    public Pair<String, String> getpRatio() {
        return m_pRatio;
    }

    public void setpRatio(Pair<String, String> ratio) {
        this.m_pRatio = ratio;
    }

    public Pair<String, String> getpWaterVol() {
        return m_pWaterVol;
    }

    public void setpWaterVol(Pair<String, String> waterVol) {
        this.m_pWaterVol = waterVol;
    }

    public Pair<String, String> getpTemperature() {
        return m_pTemperature;
    }

    public void setpTemperature(Pair<String, String> temperature) {
        this.m_pTemperature = temperature;
    }
}
