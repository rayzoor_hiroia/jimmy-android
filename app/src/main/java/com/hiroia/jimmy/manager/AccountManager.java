package com.hiroia.jimmy.manager;

import com.hiroia.jimmy.model.ModelAccount;

/**
 * 以調整過
 */

public class AccountManager {

    private static ModelAccount sm_account = new ModelAccount();

    //--------------------------------------------------------
    // Getter and Setter
    //--------------------------------------------------------

    public static ModelAccount getAccountModel() {
        return sm_account;
    }

    // sync 一次，”就是將 model sync 到 Manager“，這樣之後的取值就都從 Manager 身上取得
    public static void syncAccountModel(ModelAccount m) {
        AccountManager.sm_account = m;
    }

    public static boolean isEmpty(){
        return sm_account.isEmpty();
    }

    // 是否已登入過
    public static boolean isAlreadyLogin(){
        return !sm_account.isEmpty();
    }

    public static void clear(){
        sm_account = new ModelAccount();
    }
}
