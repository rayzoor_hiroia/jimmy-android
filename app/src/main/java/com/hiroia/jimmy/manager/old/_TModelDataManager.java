package com.hiroia.jimmy.manager.old;

import com.hiroia.jimmy.model.ModelTModeRecord;

import java.util.ArrayList;

/**
 * Create By Ray on 2018/12/13
 */
public class _TModelDataManager {

    private static boolean sm_bIsQuery = false;
    private static ArrayList<ModelTModeRecord> sm_modelBrewRecords = new ArrayList<>();


    //---------------------------------------------------------------------
    // Getter and Setter
    //---------------------------------------------------------------------

    public static boolean isModelBrewRecordEmpty(){
        return (getModelBrewRecords().size() == 0)  && (sm_bIsQuery == false);
    }

    public static void setIsAlreadyQuery(){
        sm_bIsQuery = true;
    }

    public static ArrayList<ModelTModeRecord> getModelBrewRecords() {
        return sm_modelBrewRecords;
    }

    public static void add(ModelTModeRecord m){
        sm_modelBrewRecords.add(m);
    }

    public static void setModelBrewRecords(ArrayList<ModelTModeRecord> modelBrewRecords) {
        _TModelDataManager.sm_modelBrewRecords = modelBrewRecords;
    }
}
