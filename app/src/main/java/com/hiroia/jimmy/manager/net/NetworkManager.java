package com.hiroia.jimmy.manager.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.library.android_common.util.LogUtil;

/**
 * Create By Ray on 2019/2/13
 */
public class NetworkManager {

    private static Context sm_ctx;
    private static ConnectivityManager sm_cm;
    private static NetworkInfo sm_netInfo;

    //----------------------------------------------
    // Construct
    //----------------------------------------------

    public static void init(Context context) {
        sm_ctx = context;

        if (sm_ctx != null)
            sm_cm = (ConnectivityManager) sm_ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (sm_cm != null)
            sm_netInfo = sm_cm.getActiveNetworkInfo();
    }

    //----------------------------------------------
    // Public Method
    //----------------------------------------------

    public static boolean isConnected() {
        return sm_netInfo != null && sm_netInfo.isConnectedOrConnecting();
    }

    public static boolean isOffLine(){
        return !isConnected();
    }

    public static boolean isWifi() {
        return sm_netInfo != null && sm_netInfo.getType() == ConnectivityManager.TYPE_WIFI;
    }

    public static boolean isMobileNetWork() {
        return sm_netInfo != null && sm_netInfo.getType() == ConnectivityManager.TYPE_MOBILE;
    }

    public static void println_offLine_e(String filter){
        LogUtil.d(NetworkManager.class, filter.concat(" Network connection loss. "));
    }

    public static boolean isOffLineLog_e(String filter){
        boolean isOffLine = isOffLine();
        if (isOffLine)
            println_offLine_e(filter);
        return isOffLine;
    }

}
