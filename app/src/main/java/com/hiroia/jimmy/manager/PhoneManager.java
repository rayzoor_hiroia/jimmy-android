package com.hiroia.jimmy.manager;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.text.format.Formatter;

import com.hiroia.jimmy.comp.comp.PermissionChecker;
import com.hiroia.jimmy.comp.cs.SamanthaCs;
import com.library.android_common.component.common.Opt;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Bruce on 2018/2/12.
 * 查詢 手機相關 資訊
 */
public class PhoneManager {

    private static String BRAND = "廠牌 : ";
    private static String MODEL = "型號 : ";
    private static String VERSION = "Android Version : ";
    private static String TOTAL_RAM = "Total RAM : ";
    private static String LAST_RAM = "Last RAM : ";
    private static String ROOT = "Root : ";
    private static String PACKAGE = "Full Package Path: ";
    private static String VERSION_NAME = "VersionName : ";
    private static String VERSION_CODE = "VersionCode : ";
    private static String CPU_MODEL = "CPU型號 : ";
    private static String CPU_FREQUENCY = "CPU頻率 : ";
    private static String AVAILABLE_SPACE = "可用空間 : ";
    private static String ALL_SPACE = "全部空間(已扣除系統預設空間) : ";
    private static String BLE_PERMISSION = "Bluetooth Permission : ";
    private static String BLE_CHECKER = "Bluetooth Adapter On : ";
    private static final float DATUM_RAM = 1.8f;
    private static Context sm_ctx = null;

    static public void init(Context ctx) {
        sm_ctx = ctx;
        println_d();
    }

    /**
     * 取得全部RAM相關資訊
     */
    static public void getTotalMemory() {
        String path = "/proc/meminfo";
        String memory = StrUtil.EMPTY;
        try {
            FileReader fr = new FileReader(path);
            BufferedReader localBufferedReader = new BufferedReader(fr, 8192);
            while ((memory = localBufferedReader.readLine()) != null) {
                LogUtil.d(PhoneManager.class, "---" + memory);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 確認是否開起來藍牙的接口
     */
    static public boolean isBLEOn(boolean check) {
        return check;
    }

    /**
     * 取得目前可用RAM(剩餘RAM) & 所有的RAM
     *
     * @return 可用的RAM
     */
    static public String getAvailMemory() {
        ActivityManager am = (ActivityManager) sm_ctx.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        am.getMemoryInfo(mi);
        return formatSize(mi.availMem) + StrUtil.WRAP + TOTAL_RAM + formatSize(mi.totalMem);
    }

    static public float getCurrentRam(){
        String[] val = {};
        val = Opt.of(getAvailMemory()).split("GB").get();
        return Opt.of(val[0]).parseToFloat().get();
    }

    static public boolean isLargeRam() {
        return getCurrentRam() > DATUM_RAM;
    }

    static public boolean isTinyRam() {
        return getCurrentRam() < DATUM_RAM;
    }


    /**
     * 格式化數據
     *
     * @return KB、MB、GB 數據格式
     */
    static private String formatSize(long size) {
        String suffix = null;
        float fSize = 0;

        if (size >= 1024) {
            suffix = "KB";
            fSize = size / 1024;
            if (fSize >= 1024) {
                suffix = "MB";
                fSize /= 1024;
            }
            if (fSize >= 1024) {
                suffix = "GB";
                fSize /= 1024;
            }
        } else {
            fSize = size;
        }
        java.text.DecimalFormat df = new java.text.DecimalFormat("#0.00");
        StringBuilder resultBuffer = new StringBuilder(df.format(fSize));
        if (suffix != null)
            resultBuffer.append(suffix);
        return resultBuffer.toString();
    }

    /**
     * 取得手機Android版本
     *
     * @return Android版本号
     */
    static public String getSystemVersion() {
        return android.os.Build.VERSION.RELEASE;
    }

    /**
     * 取得手機型號
     *
     * @return 手機型號
     */
    static public String getSystemModel() {
        return android.os.Build.MODEL;
    }

    /**
     * 取得手機廠商
     *
     * @return 手機廠商
     */
    static public String getDeviceBrand() {
        return android.os.Build.BRAND;
    }

    /**
     * 取得 User 目前安裝 App 相關資訊
     *
     * @return Package、versionName、versionCode
     */
    static public String getPackage() {
        try {
            String pkName = sm_ctx.getPackageName();
            String versionName = sm_ctx.getPackageManager().getPackageInfo(pkName, 0).versionName;
            int versionCode = sm_ctx.getPackageManager().getPackageInfo(pkName, 0).versionCode;
            return PACKAGE + pkName + "\n" + VERSION_NAME + versionName + "\n" + VERSION_CODE + versionCode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 獲取手機是否root資訊
     *
     * @return 是否root
     */
    static public String isRoot() {
        String bool = ROOT + "false";
        try {
            if ((!new File("/system/bin/su").exists()) && (!new File("/system/xbin/su").exists()))
                bool = ROOT + "false";
            else
                bool = ROOT + "true";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bool;
    }

    /**
     * 取得CPU資訊
     *
     * @return CPU型號 & CPU頻率
     */
    static public String getCpuInfo() {
        String str1 = "/proc/cpuinfo";
        String str2 = StrUtil.EMPTY;
        String[] cpuInfo = {"", ""};
        String[] arrayOfString;
        try {
            FileReader fr = new FileReader(str1);
            BufferedReader localBufferedReader = new BufferedReader(fr, 8192);
            str2 = localBufferedReader.readLine();
            arrayOfString = str2.split("\\s+");
            for (int i = 2; i < arrayOfString.length; i++) {
                cpuInfo[0] = cpuInfo[0] + arrayOfString[i] + " ";
            }
            str2 = localBufferedReader.readLine();
            arrayOfString = str2.split("\\s+");
            cpuInfo[1] += arrayOfString[2];
            localBufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return CPU_MODEL + cpuInfo[0] + "\n" + CPU_FREQUENCY + cpuInfo[1];
    }

    /**
     * 取得ROM資訊
     *
     * @return 可用ROM & 全部ROM
     */
    static public String getRomSpace() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        long availableBlocks = stat.getAvailableBlocks();

        String totalSize = Formatter.formatFileSize(getApplicationContext(), totalBlocks * blockSize);
        String availableSize = Formatter.formatFileSize(getApplicationContext(), availableBlocks * blockSize);

        return AVAILABLE_SPACE + availableSize + "\n" + ALL_SPACE + totalSize;
    }

    static public String getPhoneInfo(boolean isBLEOn) {
        return String.valueOf(
                BRAND + getDeviceBrand() + StrUtil.WRAP +
                        MODEL + getSystemModel() + StrUtil.WRAP +
                        VERSION + getSystemVersion() + StrUtil.WRAP +
                        LAST_RAM + getAvailMemory() + StrUtil.WRAP +
                        isRoot() + StrUtil.WRAP +
                        getPackage() + StrUtil.WRAP +
                        getCpuInfo() + StrUtil.WRAP +
                        getRomSpace() + StrUtil.WRAP +
                        BLE_PERMISSION + PermissionChecker.hasLocationPermission(sm_ctx) + StrUtil.WRAP +
                        BLE_CHECKER + isBLEOn(isBLEOn));
    }

    static public void println_d() {
        LogUtil.d(PhoneManager.class, getPhoneInfo(false));
    }


}
