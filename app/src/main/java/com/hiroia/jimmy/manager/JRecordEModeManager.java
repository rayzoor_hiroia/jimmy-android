package com.hiroia.jimmy.manager;

import com.hiroia.jimmy.model.ModelEModeRecord;
import com.hiroia.jimmy.model.record.ModelJRecord;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.DoubleUtil;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

/**
 * Create By Ray on 2019/2/15
 */
public class JRecordEModeManager {

    private static ArrayList<ModelJRecord> sm_ms = new ArrayList<>();
    private static boolean sm_bIsAlreadySync = false;

    //------------------------------------------------------------
    // Getter and Setter
    //------------------------------------------------------------

    // 這裡只是為了讓 ModelJRecord 轉成 ModelBrewRecord 用 //
    public static Lst<ModelEModeRecord> getModelEModeRecords() {
        Lst<ModelEModeRecord> brewRecordLst = Lst.of();
        for (ModelJRecord m : sm_ms) {
            brewRecordLst.add(
                    ModelEModeRecord.of(
                            m.getId(),
                            m.getCreated_time().ymd(),
                            "HIROIA JIMMY", m.getWater(),
                            DoubleUtil.of(m.getTime1()).intValue() + StrUtil.PLUS + DoubleUtil.of(m.getTime2()).intValue(),
                            m.getRecordData().getTotalTimeSec(),
                            m.isStandard()
                    ));
        }
        return brewRecordLst;
    }

    public static Lst<ModelJRecord> getModels() {
        return Lst.of(sm_ms);
    }

    public static void syncModels(Lst<ModelJRecord> ms) {
        sm_ms.clear();
        sm_ms.addAll(ms.toList());
    }

    public static void setModel(ArrayList<ModelJRecord> ms) {
        sm_ms = ms;
    }

    public static void addModel(ModelJRecord m) {
        sm_ms.add(m);
    }

    public static void removeModel(long id){
        for (int i = 0; i < sm_ms.size(); i++){
            if (sm_ms.get(i).getId() == id)
                sm_ms.remove(i);
        }
    }

    public static void updateModel(ModelJRecord m){
        removeModel(m.getId());
        sm_ms.add(0, m);
    }

    public static void addAllModels(Lst<ModelJRecord> ms) {
        sm_ms.addAll(ms.toList());
    }

    public static ModelJRecord getModelById(long id) {
        for (ModelJRecord m : sm_ms) {
            if (m.getId() == id) {
                return m;
            }
        }
        return new ModelJRecord();
    }

    public static void setAlreadySync() {
        sm_bIsAlreadySync = true;
    }

    public static boolean isSync() {
        return (sm_bIsAlreadySync == true);
    }


}
