package com.hiroia.jimmy.manager;

import com.hiroia.jimmy.model.record.ModelJRecord;
import com.library.android_common.component.common.lst.Lst;

import java.util.ArrayList;

/**
 * Create By Ray on 2019/2/12
 */
public class JRecordTModelManager {


    private static ArrayList<ModelJRecord> sm_ms = new ArrayList<>();
    private static boolean sm_bIsAlreadySync = false;

    //------------------------------------------------------------
    // Getter and Setter
    //------------------------------------------------------------

    public static ArrayList<ModelJRecord> getModels() {
        return sm_ms;
    }

    public static Lst<ModelJRecord> getSingleModels() {
        Lst<ModelJRecord> ms = Lst.of();
        for (ModelJRecord m : getModels())
            if (m.isSingleMode())
                ms.add(m);
        return ModelJRecord.sortByDate(ms);
    }

    public static Lst<ModelJRecord> getRandomModels() {
        Lst<ModelJRecord> ms = Lst.of();
        for (ModelJRecord m : getModels())
            if (m.isRandomMode())
                ms.add(m);
        return ModelJRecord.sortByDate(ms);
    }

    public static void syncModels(Lst<ModelJRecord> ms){
        sm_ms.clear();
        sm_ms.addAll(ms.toList());
    }

    public static void setModel(ArrayList<ModelJRecord> ms) {
        sm_ms = ms;
    }

    public static void addModel(ModelJRecord m) {
        sm_ms.add(m);
    }

    public static void  addAllModels(Lst<ModelJRecord> ms){
        sm_ms.addAll(ms.toList());
    }

    public static ModelJRecord getModelById(long id) {
        for (ModelJRecord m : sm_ms) {
            if (m.getId() == id) {
                return m;
            }
        }
        return new ModelJRecord();
    }

    public static void setAlreadySync() {
        sm_bIsAlreadySync = true;
    }

    public static boolean isSync() {
        return (sm_bIsAlreadySync == true);
    }

}
