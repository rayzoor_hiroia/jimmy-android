package com.hiroia.jimmy.manager.old;

import com.hiroia.jimmy.model.ModelEModeRecord;

import java.util.ArrayList;

/**
 * Create By Ray on 2018/12/24
 */
public class _EModelDataManager {

    private static boolean sm_bIsQuery = false;
    private static ArrayList<ModelEModeRecord> sm_modelBrewsRecords = new ArrayList<>();

    //------------------------------------------------------------
    // Getter and Setter
    //------------------------------------------------------------

    public static boolean isEModelDataRecordEmpty(){
        return (getModelBrewsRecords().size() == 0 && sm_bIsQuery == false);
    }

    public static void setIsAlreadyQuery(){
        sm_bIsQuery = true;
    }

    public static ArrayList<ModelEModeRecord> getModelBrewsRecords (){
        return sm_modelBrewsRecords;
    }

    public static void add(ModelEModeRecord m){
        sm_modelBrewsRecords.add(m);
    }

    public static void setModelBrewsRecords(ArrayList<ModelEModeRecord> ms){
        _EModelDataManager.sm_modelBrewsRecords = ms;
    }


}
