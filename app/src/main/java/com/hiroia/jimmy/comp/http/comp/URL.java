package com.hiroia.jimmy.comp.http.comp;

import com.library.android_common.component.common.Opt;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.JavaTools;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

/**
 * Create By Ray on 2018/7/23
 * To Parsing Uri rules
 * Sample : https://shop.hiroia.com/recipe/qrcode/2209
 *          https://[host]/[param1]/[param2]/[param3]...
 */


public class URL {

    public static final String HTTP_SYMBOL = "://";
    public static final String HTTPS = "https";
    public static final String HTTP = "http";

    private String m_url = StrUtil.EMPTY;
    private String m_host = StrUtil.EMPTY;
    private ArrayList<String> m_params = new ArrayList<>();

    //----------------------------------------------
    // Construct
    //----------------------------------------------
    public URL() {
        // do nothing
    }

    public URL(String sUrl) {
        set(sUrl);
    }

    public Parser createParser(String sUrl) {
        return new Parser(sUrl);
    }

    //----------------------------------------------
    // Set
    //----------------------------------------------
    public void set(String sUrl) {
        Parser parser = createParser(sUrl);
        setUrl(sUrl);
        setHost(parser.getHost());
        setParams(parser.getParams());
    }

    //----------------------------------------------
    // Class Parser
    //----------------------------------------------
    public class Parser {

        private String m_sUrl = StrUtil.EMPTY;
        private ArrayList<String> m_params; // 取得所有的 paths
        private boolean m_bIsHttps = false;

        //-------------------------------------------------------
        // Construct
        //-------------------------------------------------------
        public Parser(String sUrl) {
            m_sUrl = Opt.of(sUrl).get();
            m_params = new ArrayList<>();
            parsing();
        }

        //-------------------------------------------------------
        // Public Method
        //-------------------------------------------------------
        public boolean isHttps() {
            return m_bIsHttps;
        }

        public String getUrl() {
            return m_sUrl;
        }

        public String getHost() {
            return Lst.of(m_params).first();
        }

        public ArrayList<String> getParams() {
            return Lst.of(m_params).removeFirst().toList(); // 第一筆是 host
        }

        //-------------------------------------------------------
        // Private Preference
        //-------------------------------------------------------
        private void parsing() {
            // check is Https ?//
            m_bIsHttps = Opt.of(m_sUrl)
                    .subLastIndexOf(HTTP_SYMBOL)
                    .get()
                    .contains(HTTPS);

            // parsing all //
            m_params = Opt.of(m_sUrl)
                    .subIndexOf(HTTP_SYMBOL)
                    .replaceToStrEmpty(HTTP_SYMBOL)
                    .splitToPureLst(StrUtil.SLASH)
                    .toList();
        }
    }

    //----------------------------------------------
    // Getter & Setter
    //----------------------------------------------
    public String getUrl() { return m_url; }

    public void setUrl(String url) {
        this.m_url = url;
    }

    public String getHost() {
        return m_host;
    }

    public void setHost(String m_host) {
        this.m_host = m_host;
    }

    public ArrayList<String> getParams() {
        return m_params;
    }

    public Lst<String> getLstParams() { return Lst.of(m_params); }

    public void setParams(ArrayList<String> m_params) {
        this.m_params = m_params;
    }

    //-------------------------------------------------------
    // Tester
    //-------------------------------------------------------
    public static void main(String[] args) {

        String sUrl = "https://shop.hiroia.com/recipe/qrcode/2209";
        URL.Parser urlParser = new URL().createParser(sUrl);
        JavaTools.println(StrUtil.DIVIDER);
        JavaTools.println(" is https = " + urlParser.isHttps());
        JavaTools.println(" getURL = " + urlParser.getUrl());
        JavaTools.println(" getHost = " + urlParser.getHost());
        JavaTools.println(" getParams = " + urlParser.getParams().toString());

    }

}
