package com.hiroia.jimmy.comp.view.dialog;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.hiroia.jimmy.R;

/**
 * Created by Bruce on 2018/12/25.
 * 請暫勿使用
 */
public class IOSPopupDialog implements View.OnClickListener {

    private Activity m_ctx;
    private PopupWindow m_popupWindow;
    private View m_vParent;
    private IOSPopupDialog.ViewHolder m_viewHolder;
    private IOSPopupDialog.OnTouchListener m_listener;

    public IOSPopupDialog(Activity activity, View vParent, String firstItem, String secondItem) {
        init(activity, vParent, firstItem, secondItem);
    }

    private void init(Activity activity, View vParent, String firstItem, String secondItem) {
        this.m_ctx = activity;
        this.m_vParent = vParent;

        //-----------------------------------------------------------------
        // root view
        View view = LayoutInflater.from(activity).inflate(R.layout._test_dialog_popup, null);

        m_popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        m_popupWindow.setAnimationStyle(R.style.BottomFade);

        m_viewHolder = new IOSPopupDialog.ViewHolder(
                (TextView) view.findViewById(R.id.dialog_popup_ios_style_design_recipe_tv),
                (TextView) view.findViewById(R.id.dialog_popup_ios_style_free_hand_tv)
        );

        m_viewHolder.m_tvRecipe.setText(firstItem);
        m_viewHolder.m_tvFreeHand.setText(secondItem);
        m_viewHolder.m_tvRecipe.setOnClickListener(this);
        m_viewHolder.m_tvFreeHand.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_popup_ios_style_design_recipe_tv:

                m_popupWindow.dismiss();
                break;

            case R.id.dialog_popup_ios_style_free_hand_tv:

                m_popupWindow.dismiss();
                break;
        }
    }

    //-------------------------------------------------------------
    // Public Method
    //-------------------------------------------------------------
    public void show(IOSPopupDialog.OnTouchListener listener) {
        m_listener = listener;
        m_popupWindow.showAtLocation(m_vParent, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    public void dismiss() {
        m_popupWindow.dismiss();
    }

    public PopupWindow getInstance() {
        return m_popupWindow;
    }

    //------------------------------------------------------------
    // Listener
    //------------------------------------------------------------
    public interface OnTouchListener {
        void firstItemTouch(String title);

        void secondItemTouch(String title);
    }

    //------------------------------------------------------------
    // ViewHolder
    //------------------------------------------------------------
    private class ViewHolder {

        private TextView m_tvRecipe, m_tvFreeHand;

        public ViewHolder(TextView recipe, TextView freeHand) {
            this.m_tvRecipe = recipe;
            this.m_tvFreeHand = freeHand;
        }
    }
}
