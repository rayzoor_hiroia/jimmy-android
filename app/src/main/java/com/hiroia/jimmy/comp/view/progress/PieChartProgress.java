package com.hiroia.jimmy.comp.view.progress;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

/**
 * Create By Ray on 2018/12/4
 */
public class PieChartProgress extends PieChart {

    private int m_nProgressVal = 0;
    private int m_nBarViewVal = 100;

    private int m_nProgressColor = Color.BLUE;
    private int m_nBarColor = Color.TRANSPARENT;

    //------------------------------------------------------
    // Construct
    //------------------------------------------------------

    public PieChartProgress(Context context) {
        super(context);
        initView();
    }

    public PieChartProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public PieChartProgress(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    private void initView() {

        setUsePercentValues(false);              //使用百分比显示
        getDescription().setEnabled(false);     //设置PieChart图表的描述 取消右下角描述        setDrawHoleEnabled(true);               //是否显示PieChart内部圆环(true:下面属性才有意义)
        setHoleColor(Color.TRANSPARENT);              //设置PieChart内部圆的颜色
        setBackgroundColor(Color.TRANSPARENT);
        setExtraOffsets(5, 5, 5, 5); //设置PieChart图表上下左右的偏移，类似于外边距
        setTransparentCircleColor(Color.TRANSPARENT);   //设置PieChart内部透明圆与内部圆间距(31f-28f)填充颜色
        setTransparentCircleAlpha(0);         //设置PieChart内部透明圆与内部圆间距(31f-28f)透明度[0~255]数值越小越透明
        setHoleRadius(58f);                     //设置PieChart内部圆的半径(这里设置28.0f)
        setTransparentCircleRadius(61f);        //设置PieChart内部透明圆的半径(这里设置31.0f)

        // 關閉圖
        Legend l = getLegend();
        l.setEnabled(false);

        // init view //
        updateData(m_nProgressVal, m_nBarViewVal);
    }

    //------------------------------------------------------
    // Private Preference
    //------------------------------------------------------

    private void updateData(int progressVal, int barViewVal) {

        // Data //
        ArrayList<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(progressVal, ""));
        entries.add(new PieEntry(barViewVal, ""));

        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setColors(Lst.of(m_nProgressColor, m_nBarColor).toList());

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(0f);
        data.setValueTextColor(Color.TRANSPARENT);
        setData(data);
        highlightValues(null);
        invalidate(); // refresh
    }

    //------------------------------------------------------
    // Public Method
    //------------------------------------------------------
    public void addProgress() {
        addProgress(1);
    }

    public void addProgress(int val) {
        updateData(m_nProgressVal += val, m_nBarViewVal -= val);
    }

    public void setProgressVal(int val){
        val = val > 100 ? 100 : val;
        updateData((m_nProgressVal = val), (m_nBarViewVal - val));
    }

    public void setBarColor(int color) {
        m_nBarColor = color;
    }

    public void setProgressColor(int color) {
        m_nProgressColor = color;
    }
}
