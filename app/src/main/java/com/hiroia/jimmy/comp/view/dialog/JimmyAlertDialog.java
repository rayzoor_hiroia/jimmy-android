package com.hiroia.jimmy.comp.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;

/**
 * Create By Ray on 2018/10/25
 */
public class JimmyAlertDialog implements View.OnClickListener {

    // View //
    private Dialog m_dialog;
    private LinearLayout m_llvCancelBtn;
    private TextView m_tvTitle, m_tvContent;
    private Button m_btnConfirm;

    // Listener //
    private AlertDialogListener m_alertDialogListener;

    //-----------------------------------------------------
    // Construct
    //-----------------------------------------------------

    public JimmyAlertDialog(Activity activity, String title, String content) {

        //-----------------------------------------------------
        // init view //
        m_dialog = new Dialog(activity);
        m_dialog.setContentView(R.layout.dialog_alert_jimmy_style);
        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        m_tvTitle = m_dialog.findViewById(R.id.dialog_alert_jimmy_style_title_tv);
        m_tvContent = m_dialog.findViewById(R.id.dialog_alert_jimmy_style_content_tv);

        m_btnConfirm = m_dialog.findViewById(R.id.dialog_alert_jimmy_style_confirm_btn);
        m_llvCancelBtn = m_dialog.findViewById(R.id.dialog_alert_jimmy_style_cancel_btn_llv);

        //-----------------------------------------------------
        // set data //
        m_btnConfirm.setOnClickListener(this);
        m_llvCancelBtn.setOnClickListener(this);

        m_tvTitle.setText(title);
        m_tvContent.setText(content);

    }

    //-----------------------------------------------------
    // Public Method
    //-----------------------------------------------------
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.dialog_alert_jimmy_style_confirm_btn: // Confirm
                m_alertDialogListener.onConfirm();
                dismiss();
                break;
            case R.id.dialog_alert_jimmy_style_cancel_btn_llv: // Cancel
                m_alertDialogListener.onCancel();
                dismiss();
                break;
        }
    }

    public void show(AlertDialogListener listener) {
        m_alertDialogListener = listener;
        m_dialog.show();
    }

    public void dismiss() {
        m_dialog.dismiss();
    }

    public boolean isShowing() {
        return m_dialog.isShowing();
    }

    public Dialog getDialog() {
        return m_dialog;
    }

    public void setTitleText(String msg) {
        m_tvTitle.setText(msg);
    }

    public void setContentText(String msg) {
        m_tvContent.setText(msg);
    }

    public void setButtonText(String msg) {
        m_btnConfirm.setText(msg);
    }

    //-----------------------------------------------------
    // Interface
    //-----------------------------------------------------
    public interface AlertDialogListener {
        void onConfirm();
        void onCancel();
    }

}
