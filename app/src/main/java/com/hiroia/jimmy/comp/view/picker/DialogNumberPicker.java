package com.hiroia.jimmy.comp.view.picker;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.StrUtil;
import com.shawnlin.numberpicker.NumberPicker;

/**
 * Create By Ray on 2018/11/29
 * Dialog Number Picker
 */
public class DialogNumberPicker {

    // View //
    private Dialog m_dialog;
    private Activity m_act;
    private LinearLayout m_llvPickerLayout;

    // Comp //
    private DecimalPlace m_currdpp = DecimalPlace.NONE;
    private OnConfirmFlowRateListener m_onConfirmFlowRateListener;

    //--------------------------------------------------------
    // Construct
    //--------------------------------------------------------

    public DialogNumberPicker(Activity activity) {
        m_act = activity;

        // init dialog //
        m_dialog = new Dialog(activity);
        m_dialog.setContentView(R.layout.dialog_picker_alert_jimmy_style);
        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        m_dialog.setCancelable(false);

        // init view //
        m_llvPickerLayout = m_dialog.findViewById(R.id.dialog_picker_alert_jimmy_style_picker_llv);
        Button btnConfirm = m_dialog.findViewById(R.id.dialog_picker_alert_jimmy_style_set_btn);

        // set data //
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_onConfirmFlowRateListener.onConfirm(getResult());
            }
        });
    }

    //--------------------------------------------------------
    // Private Preference
    //--------------------------------------------------------

    private View numberPickerView(boolean hasDot) {
        View v = LayoutInflater.from(m_act).inflate(R.layout.comp_pmode_picker, null);
        TextView tvDot = v.findViewById(R.id.comp_pmode_picker_dot_tv);
        tvDot.setVisibility(hasDot ? View.VISIBLE : View.INVISIBLE);
        return v;
    }

    private String getResult() {
        final StringBuilder sb = new StringBuilder();

        for (int i = 0; i < m_llvPickerLayout.getChildCount(); i++) {
            View v = m_llvPickerLayout.getChildAt(i);
            sb.append(((NumberPicker) v.findViewById(R.id.comp_pmode_picker_num_picker)).getValue() + "");
        }
        return sb.insert(sb.length() - m_currdpp.pos(), StrUtil.DOT).toString();
    }

    //--------------------------------------------------------
    // Public Method
    //--------------------------------------------------------

    public void setView(int size) {
        setView(size, DecimalPlace.NONE);
    }

    public void setView(int size, final DecimalPlace dp) {
        m_currdpp = dp;
        Lst.accumulateOf(size).backForEach(new Lst.IConsumer<Integer>() {
            @Override
            public void runEach(int i, Integer pos) {
                View v = numberPickerView(i == dp.pos());
                m_llvPickerLayout.addView(v);
            }
        });
    }

    public void show(OnConfirmFlowRateListener listener) {
        m_onConfirmFlowRateListener = listener;
        show();
    }

    public void show() {
        if (m_dialog != null)
            m_dialog.show();
    }

    public void dismiss() {
        if (m_dialog != null && m_dialog.isShowing())
            m_dialog.dismiss();
    }

    //--------------------------------------------------------
    // Enum
    //--------------------------------------------------------

    public enum DecimalPlace {
        NONE(-1), FIRST_DIGITAL(1), SECOND_DIGITAL(2), THIRD_DIGITAL(3);
        //---------------------------------------------
        private int m_pos = 0;

        DecimalPlace(int pos) {
            m_pos = pos;
        }

        public int pos() {
            return m_pos;
        }
    }

    //--------------------------------------------------------
    // Interface
    //--------------------------------------------------------

    public interface OnConfirmFlowRateListener {
        void onConfirm(String val);
    }

}
