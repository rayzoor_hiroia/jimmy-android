package com.hiroia.jimmy.comp.comp;

import com.library.android_common.component.common.Opt;
import com.library.android_common.component.common.Pair;
import com.library.android_common.component.common.tuple.Tuple;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.common.tuple.Tuple3;
import com.library.android_common.util.DoubleUtil;
import com.library.android_common.util.IntUtil;
import com.library.android_common.util.JavaTools;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Create By Ray on 2019/1/29
 */
public class TrainingData {


    // Global Constant //
    public static final String START_TIME = "start_time";
    public static final String END_TIME = "end_time";
    public static final String FLOW_RATE = "flow_rate";

    // Comp //
    private Lst<Tuple<Integer, Integer, Double, Void, Void, Void, Void, Void, Void, Void>>
            m_params = Lst.of();

    //-----------------------------------------------------------
    // Construct //
    //-----------------------------------------------------------

    public TrainingData() {

    }

    public static TrainingData build() {
        return new TrainingData();
    }

    public static TrainingData parse(String data) {
        TrainingData td = new TrainingData();
        try {
            JSONArray jsArr = new JSONArray(data);
            for (int i = 0; i < jsArr.length(); i++) {
                JSONObject jsObj = jsArr.getJSONObject(i);
                td.add(jsObj.optInt(START_TIME), jsObj.optInt(END_TIME), jsObj.optDouble(FLOW_RATE));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return td;
    }

    //-----------------------------------------------------------
    // Public Method
    //-----------------------------------------------------------

    public static TrainingData testData() {
        TrainingData td = new TrainingData();
        for (int i = 0; i < 3; i++) {
            td.add(i + 1, i + 3, IntUtil.rangeRandom(1, 5));
        }
        return td;
    }

    public void add(int startTime, int endTime, double flowRate) {
        m_params.add(Tuple3.of(startTime, endTime, flowRate));
    }

    // 轉換成 View Chart 所需要的白線 data .
    public Lst<Integer> toFlowRateLine(int totalTimeSec){
        Lst<Integer> vals = Lst.of();
        for (int i = 0; i < m_params.size(); i++){
         vals.cloneAdd(DoubleUtil.of(m_params.get(i).v3()).intValue(), totalTimeSec / m_params.size());
        }
        return vals;
    }

    public String datas() {

        StringBuilder sb = new StringBuilder();
        sb.append(StrUtil.LEFT_BRACKET);

        // add params //
        for (int i = 0; i < m_params.size(); i++) {

            // ( //
            sb.append(StrUtil.LEFT_CURLY_BRACKET);

            // "start_time" : 1 //
            sb.append(StrUtil.QUOTE);
            sb.append(START_TIME);
            sb.append(StrUtil.QUOTE);
            sb.append(StrUtil.COLON);
            sb.append(m_params.get(i).v1());

            // , //
            sb.append(StrUtil.COMMA);

            // "end_time" : 10 //
            sb.append(StrUtil.QUOTE);
            sb.append(END_TIME);
            sb.append(StrUtil.QUOTE);
            sb.append(StrUtil.COLON);
            sb.append(m_params.get(i).v2());

            // , //
            sb.append(StrUtil.COMMA);

            // "flow_rate" : 10 //
            sb.append(StrUtil.QUOTE);
            sb.append(FLOW_RATE);
            sb.append(StrUtil.QUOTE);
            sb.append(StrUtil.COLON);
            sb.append(m_params.get(i).v3());

            sb.append(StrUtil.RIGHT_CURLY_BRACKET);
            sb.append(StrUtil.COMMA);

        }

        if (m_params.size() > 0)
            sb.setLength(sb.length() - 1);
        sb.append(StrUtil.RIGHT_BRACKET);

        return sb.toString();
    }

    // Println //
    public void println_d() {
        println_d(StrUtil.EMPTY);
    }

    public void println_d(final String filter) {
        m_params.forEach(new Lst.IConsumer<Tuple<Integer, Integer, Double, Void, Void, Void, Void, Void, Void, Void>>() {
            @Override
            public void runEach(int i, Tuple<Integer, Integer, Double, Void, Void, Void, Void, Void, Void, Void> tup) {
                LogUtil.d(TrainingData.class, filter +
                        StrUtil.spaceOf(START_TIME) + StrUtil.SPACE_EQUAL + tup.v1() + StrUtil.COMMA +
                        StrUtil.spaceOf(END_TIME) + StrUtil.SPACE_EQUAL + tup.v2() + StrUtil.COMMA +
                        StrUtil.spaceOf(FLOW_RATE) + StrUtil.SPACE_EQUAL + tup.v3()
                );
            }
        });
    }

    public void println() {
        m_params.forEach(new Lst.IConsumer<Tuple<Integer, Integer, Double, Void, Void, Void, Void, Void, Void, Void>>() {
            @Override
            public void runEach(int i, Tuple<Integer, Integer, Double, Void, Void, Void, Void, Void, Void, Void> tup) {
                JavaTools.println(StrUtil.spaceOf(START_TIME) + StrUtil.SPACE_EQUAL + tup.v1() + StrUtil.COMMA +
                        StrUtil.leftSpaceOf(END_TIME) + StrUtil.SPACE_EQUAL + tup.v2() + StrUtil.COMMA +
                        StrUtil.leftSpaceOf(FLOW_RATE) + StrUtil.SPACE_EQUAL + tup.v3());
            }
        });
    }

    //-------------------------------------------------------
    // Test //
    //-------------------------------------------------------
    public static void main(String[] args) {

    }
}
