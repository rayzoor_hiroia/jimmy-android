package com.hiroia.jimmy.comp.api;

import android.app.Activity;
import android.widget.Toast;

import com.hiroia.jimmy.cs.HttpCs;
import com.hiroia.jimmy.view.login.LoginActivity;
import com.library.android_common.component.view.dialog.PTDialog;
import com.library.android_common.enums.HttpDef;
import com.library.android_common.util.LogUtil;

import org.json.JSONObject;

/**
 * Created by Bruce on 2019/1/17.
 * Login
 */
public class ApiLogin {

    static public void login(final Activity activity, String email, String password) {

        HttpDef.LOGIN.post()
                .addParam(HttpCs.EMAIL, email)
                .addParam(HttpCs.PASSWORD, password)
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject rootObj) {
                        LogUtil.d(ApiLogin.class, " response = ".concat(response));
//                        if (response.isEmpty()) return;
//                        //----------------------------------------------------------------
//
//                        try {
//                            JSONArray jsArr = rootObj.getJSONObject("result").optJSONArray("shops");
//                            List<ModelBindBean> list = ModelBindBean.getModels(jsArr);
//                            ApiManager.setModelBindBeanList(list);
//
//                            listener.result(list);
//
//                            if (jsObj.getBoolean(HttpCs.SUCCESS)) {
//                                onLoginSucc(ModelUserLogin.decodeJSON(jsObj.getJSONObject(HttpCs.RESULT)));
//                            } else
//                                onLoginFail(); // 當登入失敗時
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }
                });
    }


//    private void doAfterLogin(ModelUserLogin m) { // 當登入成功後
//
//        AccountManager.setLogin(emailET.getText().toString(), passwordET.getText().toString());
//        AccountManager.setToken(m.getToken()); // Normal Token
//        AccountManager.setUserName(m.getUserName());
//        AccountManager.setUserSn(Integer.parseInt(m.getUserSn()));
//        AccountManager.setEmail(m.getEmail());
//        AccountManager.setLoginStatus(GlobalConstant.LOGIN_STATUS_SUCCESS, 0);
//        AccountManager.setAvatar(m.getFbPhoto());
//
//        //--------------------------------------------------
//        // after login
//        LoginActivity.getInstance().onLoginFinish();
//    }
//
//    private void onLoginSucc(final ModelUserLogin m) { // 當登入成功時
//
//        LoginActivity.getInstance().runOnUiThread(new Runnable() { // 為了更新ui 必須跳回 run on ui thread
//            @Override
//            public void run() {
//                PTDialog.setMessage(MutiMsg.DATA_INIT.msg());
//            }
//        });
//
//        ThreadUtil.createThread(new ThreadUtil.OnRunInNewThread() { // do in background
//            @Override
//            public void run() {
//                doAfterLogin(m);
//            }
//        });
//
//    }

    private static void onLoginFail(final Activity activity) {
        LoginActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, "Login Fail", Toast.LENGTH_SHORT).show(); // 改多國語言
                if (PTDialog.getInstance() != null) PTDialog.dismiss();
            }
        });
    }
}
