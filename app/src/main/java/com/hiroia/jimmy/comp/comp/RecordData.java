package com.hiroia.jimmy.comp.comp;

import com.hiroia.jimmy.model.record.ModelJRecord;
import com.library.android_common.component.common.Pair;
import com.library.android_common.component.common.Pairs;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.DoubleUtil;
import com.library.android_common.util.IntUtil;
import com.library.android_common.util.JavaTools;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Create By Ray on 2019/1/28
 */
public class RecordData {


    // Constant Member //
    public static final String TIME = "t";
    public static final String WEIGHT = "w";

    // Comp //
    private Lst<Pair<Double, Double>> m_params = Lst.of();

    //-------------------------------------------------------
    // Construct //
    //-------------------------------------------------------

    public RecordData() {

    }

    public static RecordData build() {
        return new RecordData();
    }

    //-------------------------------------------------------
    // Public Method //
    //-------------------------------------------------------

    public static RecordData testData() {
        RecordData rd = new RecordData();
        for (double i = 0.2; i <= 30; i += 0.2) {
            rd.add(DoubleUtil.of(i).reSizeDecimal(1), IntUtil.rangeRandom(1, 9) + DoubleUtil.of(i).reSizeDecimal(1));
        }
        return rd;
    }

    public static RecordData parse(String data) {
        RecordData rd = new RecordData();
        try {
            JSONArray jsArr = new JSONArray(data);
            for (int i = 0; i < jsArr.length(); i++) {
                JSONObject jsObj = jsArr.getJSONObject(i);
                rd.add(jsObj.optDouble(TIME), jsObj.optDouble(WEIGHT));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rd;
    }

    public void add(double t, double w) {
        m_params.add(Pair.of(t, w));
    }

    public String datas() {

        // init comp //
        StringBuilder sb = new StringBuilder();
        sb.append(StrUtil.LEFT_BRACKET);

        // add params //
        for (Pair<Double, Double> p : m_params.toList()) {

            // ( //
            sb.append(StrUtil.LEFT_CURLY_BRACKET);

            // "t" : 1 //
            sb.append(StrUtil.QUOTE);
            sb.append(TIME);
            sb.append(StrUtil.QUOTE);
            sb.append(StrUtil.COLON);
            sb.append(p.k());

            // , //
            sb.append(StrUtil.COMMA);

            // "w" : 100 //
            sb.append(StrUtil.QUOTE);
            sb.append(WEIGHT);
            sb.append(StrUtil.QUOTE);
            sb.append(StrUtil.COLON);
            sb.append(p.v());

            // ) //
            sb.append(StrUtil.RIGHT_CURLY_BRACKET);
            sb.append(StrUtil.COMMA);
        }

        if (m_params.size() > 0)
            sb.setLength(sb.length() - 1);
        sb.append(StrUtil.RIGHT_BRACKET);

        return sb.toString();
    }

    public Lst<Pair<Double, Double>> values() {
        return m_params;
    }

    // 取得總水量 //
    public double getWater() {
        return Pairs.of(m_params.toList()).toVLst().doubleTotal();
    }

    // 在 EMode 是累加類型的 data 所以要取最後一筆 //
    public double getEModeWater() {
        return Pairs.of(m_params.toList()).toVLst().last();
    }

    // 取得總時長 (秒) //
    public int getTotalTimeSec() {
        if (m_params.size() == 0)
            return 0;
        return DoubleUtil.of(m_params.last().k()).intValue();
    }

    // 清除 //
    public void clear() {
        m_params.clear();
    }

    public boolean isWaterVolOverThan2000() {
        return getEModeWater() >= 2000.0;
    }

    public void println_d() {
        println_d(StrUtil.EMPTY);
    }

    public void println_d(String filter) {
        m_params.forEach(new Lst.IConsumer<Pair<Double, Double>>() {
            @Override
            public void runEach(int i, Pair<Double, Double> p) {
                LogUtil.d(RecordData.class, " index = " + i + " sec = " + p.k() + StrUtil.COMMA + " vol = " + p.v());
            }
        });
    }

    // for java //
    public void println() {
        m_params.forEach(new Lst.IConsumer<Pair<Double, Double>>() {
            @Override
            public void runEach(int i, Pair<Double, Double> p) {
                JavaTools.println(" index = " + i + StrUtil.COMMA + " sec = " + DoubleUtil.of(p.k()).reSizeDecimal(1) + StrUtil.COMMA + " vol = " + DoubleUtil.of(p.v()).reSizeDecimal(1));
            }
        });
    }

    public void println_data() {
        JavaTools.println(datas());
    }

    //-------------------------------------------------------
    // Test //
    //-------------------------------------------------------

    public static void main(String[] args) {

        RecordData rd = RecordData.testData();
        rd.println();
        rd.println_data();

    }


}
