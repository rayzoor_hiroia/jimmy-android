package com.hiroia.jimmy.comp.view.chart;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.EntryXComparator;
import com.hiroia.jimmy.R;
import com.hiroia.jimmy.comp.comp.WaterVolStep;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Bruce on 2019/1/10.
 */
public class LineChart {

    private LineChart m_lcResultChart;
    private WaterVolStep m_waterVol = new WaterVolStep();

    /**
     * 1. 顯示 LineChart 區塊
     *    LineChart Data、繪製區塊的大小縮放(上下左右)、可否點擊、顏色、透明度
     * 2. 顯示 中心線
     *    中心線的 Lst、顏色
     * 3. 顯示 Ｘ軸線 ＆ 顯示的內容 ＥＸ： 月份、秒數．．．等
     * 4. 顯示 Ｙ軸線 ＆ 顯示的內容 ＥＸ： 月份、秒數．．．等
     */
    public LineChart(Context c, WaterVolStep steps) {
        init(c);
    }

    private void init(Context c) {
        View v = LayoutInflater.from(c).inflate(R.layout.comp_linechart_view, null);

//        m_lcResultChart = v.findViewById(R.id.comp_line_chart_lc);

    }


    // 設定 參數至 Chart View //
//    private void setResultChartData(WaterVolStep steps) {
//
//        m_lcResultChart.getDescription().setEnabled(false);
//        m_lcResultChart.setPinchZoom(false);
//        m_lcResultChart.setDrawGridBackground(false);
//        m_lcResultChart.setTouchEnabled(true); // 設定 Chart 是否可以觸摸
////        m_lcResultChart.setViewPortOffsets(150, 150, 150, 150); // 控制上下左右座標軸顯示的距離，千萬不要全部 set 0 “ X & Y 軸會消失 ”
//
//        // 設定 X 軸 參數 //
//        XAxis x = m_lcResultChart.getXAxis();
//        x.setEnabled(true);
//        x.setDrawGridLines(false); // 設定 X軸 網格線
//        x.setValueFormatter(new IAxisValueFormatter() { // 設定 X軸 文本的格式
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                return String.valueOf((int) value).concat("s");
//            }
//        });
//        x.setSpaceMax(5f); // 設定 X軸 額外的最大空間
////        x.setGranularity(0.5f); // 設定 X軸 間隔大小
//        x.setAxisMaximum((float) getParentActivity().getTimeSec()); // 設定 X軸 最大值
//        x.setAxisMinimum(0f); // 設定 X軸 最小值
//        x.setTextColor(Color.WHITE); // 設定 X軸 文字顏色
//        x.setTextSize(12); // 設定 X軸 文字大小
//        x.setLabelCount(5); // 設定 X軸 標籤數量
//        x.setPosition(XAxis.XAxisPosition.BOTTOM);  // 設定 X軸 標籤在最下方 圖表外層顯示
//
//
//        // 設定 Y 軸參數 //
//        YAxis y = m_lcResultChart.getAxisLeft();
//        y.setEnabled(true);
//        y.setAxisMinimum(1f); // 設定 Y軸 最小值
//        y.setTextColor(Color.TRANSPARENT);
//        y.setDrawAxisLine(false);
//        y.setDrawGridLines(false); // 設定 Y軸 網格線
//
//
//        m_lcResultChart.getAxisRight().setEnabled(false);
//        m_lcResultChart.getLegend().setEnabled(false);
//        m_lcResultChart.animateXY(1000, 1000); // 設定動畫
//
//        // Set View //
//        m_lcResultChart.setNoDataText(StrUtil.EMPTY); // 當沒有 data text 設定為空
//        m_lcResultChart.setDescription(null); // close desc
//
//
//        List<Entry> entries = new ArrayList<>();
//        for (int i = steps.getStepsBySec().size() - 1; i >= 0; i--) {
//            entries.add(new Entry(i + 1, steps.getStepsBySec().get(i).v()));
//            Collections.sort(entries, new EntryXComparator()); // 設定 entries 的順序性
//        }
//
//        // 設定內部參數 //
//        LineDataSet dataSet = new LineDataSet(entries, StrUtil.EMPTY);
//        dataSet.setValueTextColor(Color.TRANSPARENT); // 線上的 text 顏色
//        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER); // CUBIC_BEZIER(立方曲線)，LINEAR(直線)，STEPPED(階梯)，HORIZONTAL_BEZIER(水平曲線)
//        dataSet.setCubicIntensity(0.2f); // 設定曲線的彎曲程度
//        dataSet.setDrawCircles(false);  // 在點上畫圓 默認true
//        dataSet.setDrawFilled(true);    // 設定是否填充
//        dataSet.setLineWidth(0);        // 設定 Line 寬度
//        dataSet.setHighLightColor(getResources().getColor(R.color.jimmyBlue)); // 設定 點選某個點時，橫豎兩條線的顏色
//        dataSet.setFillColor(getResources().getColor(R.color.jimmyBlue)); // 設定 填充 顏色
//        dataSet.setFillAlpha(5000);     // 設定 填充 透明度
//
//
//        //------------------------------------------------------------------
//        // 白色 BaseLine Line Chart //
//        int scoreModeTime = getParentActivity().getTimeSec() / getParentActivity().getRandomPoursCount();
//
//
//        Lst<Integer> vols = Lst.of();
//        for (int i = 0; i < getParentActivity().getRandomPoursCount(); i++) {
//            vols.cloneAdd(getParentActivity().getRateVolList().get(i), scoreModeTime);
//        }
//
//
//        List<Entry> entriesBaseLine = new ArrayList<>();
//        for (int i = 0; i < vols.toList().size(); i++) {
//            entriesBaseLine.add(new Entry(i + 1, vols.get(i)));
//            Collections.sort(entriesBaseLine, new EntryXComparator()); // 設定 entriesBaseLine 的順序性
//        }
//
//        LineDataSet whiteBaseDataSet = new LineDataSet(entriesBaseLine, StrUtil.EMPTY);
//        whiteBaseDataSet.setValueTextColor(Color.TRANSPARENT); // 線上的 text 顏色
//        whiteBaseDataSet.setMode(LineDataSet.Mode.LINEAR); // CUBIC_BEZIER(立方曲線)，LINEAR(直線)，STEPPED(階梯)，HORIZONTAL_BEZIER(水平曲線)
//        whiteBaseDataSet.setCubicIntensity(0.2f); // 設定曲線的彎曲程度
//        whiteBaseDataSet.setDrawCircles(false); // 在點上畫圓 默認true
//        whiteBaseDataSet.setDrawFilled(false);  // 設定是否填充
//        whiteBaseDataSet.setLineWidth(1f);      // 設定 Line 寬度
//        whiteBaseDataSet.setColor(Color.WHITE); // 設定 Line 顏色
//        whiteBaseDataSet.setHighLightColor(getResources().getColor(R.color.jimmyBlue)); // 設定 點選某個點時，橫豎兩條線的顏色
//        whiteBaseDataSet.setFillColor(getResources().getColor(R.color.jimmyBlue)); // 設定 填充 顏色
//        whiteBaseDataSet.setFillAlpha(0);       // 設定 填充 透明度
//
//
//        // TODO 要白色矩形 Line Chart 請打開下面這段
//        //------------------------------------------------------------------
//        // 白色矩形 Line Chart //
////        LineDataSet whiteDataSet = new LineDataSet(entries, StrUtil.EMPTY);
////        whiteDataSet.setValueTextColor(Color.TRANSPARENT); // 線上的 text 顏色
////        whiteDataSet.setMode(LineDataSet.Mode.STEPPED); // CUBIC_BEZIER(立方曲線)，LINEAR(直線)，STEPPED(階梯)，HORIZONTAL_BEZIER(水平曲線)
////        whiteDataSet.setCubicIntensity(0.2f); // 設定曲線的彎曲程度
////        whiteDataSet.setDrawCircles(false); // 在點上畫圓 默認true
////        whiteDataSet.setDrawFilled(false);  // 設定是否填充
////        whiteDataSet.setLineWidth(1.5f);    // 設定 Line 寬度
////        whiteDataSet.setColor(Color.WHITE); // 設定 Line 顏色
////        whiteDataSet.setFillAlpha(0);       // 設定 填充 透明度
//
//        LineData lineData = new LineData(dataSet, whiteBaseDataSet); // LineData 物件可放 多個 LineDataSet 物件，若需要添加其他 LineChart，記得要添加
//        m_lcResultChart.setData(lineData);
//        m_lcResultChart.invalidate();
//
//    }
}
