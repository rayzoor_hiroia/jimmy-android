package com.hiroia.jimmy.comp.cs;

/**
 * Create By Ray on 2019/1/21
 */
public class HiroiaCs {


    public static final String HIROIA_PACKAGE = "com.hiroia.hiroia";
    public static final String HIROIA_REDIRECT_ACTIVITY = "com.hiroia.cafe.activity.RedirectActivity";
    public static final String PLAY_VIDEO_PAGE = "play_video_page"; // hiroia 的產品詳細頁面

    //---------------------------------------------
    // key、val
    public static final String RECIPE_ID = "id";
    public static final String WHERE = "where";


}
