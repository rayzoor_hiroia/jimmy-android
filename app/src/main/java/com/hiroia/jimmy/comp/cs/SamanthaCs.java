package com.hiroia.jimmy.comp.cs;

/**
 * Create By Ray on 2019/1/21
 */
public class SamanthaCs {
    public static final String MAIN_PACKAGE = "com.hiroia.samantha";
//    public static final String JIMMY_OTA_REDIRECT_PACKAGE = "com.hiroia.samantha.activity.RedirectActivity";
    public static final String JIMMY_OTA_REDIRECT_PACKAGE = "com.hiroia.samantha.activity.v2.SamanthaMainActivity";
}
