package com.hiroia.jimmy.comp.comp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

import com.facebook.FacebookSdk;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * Create By Ray on 2018/11/1
 */
public class PermissionChecker {

    public static final int REQUEST_BLE_CODE = 1001;
    public static final int REQUEST_EXTERNAL_STROAGE = 1002;
    public static final int REQUEST_CAMERA = 1003;
    public static Context sm_ctx;

    //---------------------------------------------------------
    // Construct
    //---------------------------------------------------------
    public static boolean hasStroagePermission(Context ctx) {
        return ContextCompat.checkSelfPermission(ctx, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasLocationPermission(Context ctx) { // BLE need Location Permission
        return ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                &&
                ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasCameraPermission(Context ctx) {
        return ContextCompat.checkSelfPermission(ctx, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

}
