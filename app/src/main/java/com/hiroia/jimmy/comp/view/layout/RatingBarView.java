package com.hiroia.jimmy.comp.view.layout;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hiroia.jimmy.R;

/**
 * Created by Bruce on 2018/11/8.
 */

public class RatingBarView extends LinearLayout {

    private RatingBarView m_self;

    private boolean m_bIsClickable = true;
    private OnRatingListener m_ratingListener;
    private float m_fStarImageSize = 0f;
    private int m_defaultStartCount = 0;
    private int m_nStarCount = 0;
    private Drawable m_starEmptyDrawable;
    private Drawable m_starFillDrawable;
    private ImageView m_imgStar;
    private int m_nMaxCount = 5;

    // Enable //
    private boolean m_bIsScaleAnimOn = false;

    //------------------------------------------------------------------------
    // Construct
    //------------------------------------------------------------------------
    public RatingBarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RatingBarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public RatingBarView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }


    //------------------------------------------------------
    // Private Method
    //------------------------------------------------------
    private void init(Context context, AttributeSet attrs) {
        m_self = this;
        setOrientation(m_self.HORIZONTAL);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RatingBarView);
        m_fStarImageSize = typedArray.getDimension(R.styleable.RatingBarView_starImageSize, 28);
        m_defaultStartCount = typedArray.getInteger(R.styleable.RatingBarView_starCount, m_nMaxCount);
        m_starFillDrawable = typedArray.getDrawable(R.styleable.RatingBarView_starFill);
        m_starEmptyDrawable = typedArray.getDrawable(R.styleable.RatingBarView_starEmpty);
        typedArray.recycle();

        for (int i = 0; i < m_defaultStartCount; ++i) {
            ImageView imageView = getStarImageView(context);
            imageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (m_bIsClickable) {
                        m_nStarCount = indexOfChild(v) + 1;
                        setStar(m_nStarCount, m_bIsScaleAnimOn);
                        if (m_ratingListener != null) {
                            m_ratingListener.onRatingScore(m_nStarCount);
                        }
                    }
                }
            });
            addView(m_imgStar);
        }
    }


    private ImageView getStarImageView(Context context) {
        m_imgStar = new ImageView(context); // 要改 記憶體問題，不能這樣寫
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(Math.round(m_fStarImageSize), Math.round(m_fStarImageSize));
        m_imgStar.setLayoutParams(params);

        m_imgStar.setPadding(0, 0, 4, 0); // set Star 之間的間距
        m_imgStar.setImageDrawable(m_starFillDrawable);
        m_imgStar.setMaxWidth(28);
        m_imgStar.setMaxHeight(28);
        return m_imgStar;
    }

    //------------------------------------------------------
    // Public Method
    //------------------------------------------------------
    public int getMaxStarCount(){
        return m_nMaxCount;
    }

    public void setStar(int starCount, boolean isAnimOn) {
        // If set star size large than default size use default size. If size less than 0 use 0. //
        starCount = (starCount > m_defaultStartCount) ? m_defaultStartCount : ((starCount < 0) ? 0 : starCount);

        // Set Anim //
        if (isAnimOn) {
            for (int i = 0; i < starCount; i++) {
                ScaleAnimation sa = new ScaleAnimation(0, 0, 1, 1);
                getChildAt(i).startAnimation(sa);
            }
        }

        // Set Star //
        for (int i = 0; i < getChildCount(); i++) {
            ((ImageView) getChildAt(i)).setImageDrawable((i >= starCount) ? m_starEmptyDrawable : m_starFillDrawable);
        }
    }

    public void setScaleAminOn(boolean enable) {
        m_bIsScaleAnimOn = enable;
    }

    public boolean isClickable() {
        return m_bIsClickable;
    }

    public void setClickable(boolean isClickable) {
        this.m_bIsClickable = isClickable;
    }

    public float getStarImageSize() {
        return m_fStarImageSize;
    }

    public void setStarImageSize(float starImgSize) {
        this.m_fStarImageSize = starImgSize;
    }

    public int getDefaultStarCount() {
        return m_defaultStartCount;
    }

    public void setDefaultStarCount(int defaultStarCount) {
        this.m_defaultStartCount = defaultStarCount;
    }

    public Drawable getStarEmptyDrawable() {
        return m_starEmptyDrawable;
    }

    public void setStarEmptyDrawable(Drawable drawable) {
        this.m_starEmptyDrawable = drawable;
    }

    public Drawable getStarFillDrawable() {
        return m_starFillDrawable;
    }

    public void setStarFillDrawable(Drawable drawable) {
        this.m_starFillDrawable = drawable;
    }

    //------------------------------------------------------
    // Listener
    //------------------------------------------------------
    public void setOnRatingListener(OnRatingListener listener) {
        this.m_ratingListener = listener;
    }

    //------------------------------------------------------
    // Interface
    //------------------------------------------------------
    public interface OnRatingListener {
        void onRatingScore(int starScore);
    }
}
