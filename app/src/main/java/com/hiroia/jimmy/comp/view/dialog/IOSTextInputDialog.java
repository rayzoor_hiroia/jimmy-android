package com.hiroia.jimmy.comp.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.hiroia.jimmy.R;

/**
 * Create By Ray on 2018/11/26
 */
public class IOSTextInputDialog {

    // View //
    private Dialog m_dialog = null;
    private Context m_ctx = null;
    private TextView m_btnConfirm, m_btnCancel;
    private EditText m_edInputer;

    // Comp //
    private OnButtonClickListener m_btnClickListener;

    //-------------------------------------------------------------
    // Construct
    //-------------------------------------------------------------

    public IOSTextInputDialog(final Context context) {
        m_ctx = context;

        //---------------------------------------------------------
        // init dialog //
        m_dialog = new Dialog(context);
        m_dialog.setContentView(R.layout.comp_dialog_ios_input);
        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        m_dialog.setCancelable(false);

        // set data //
        m_btnConfirm = m_dialog.findViewById(R.id.comp_dialog_ios_input_name_dialog_confirm);
        m_btnCancel = m_dialog.findViewById(R.id.comp_dialog_ios_input_device_name_dialog_cancel);
        m_edInputer = m_dialog.findViewById(R.id.comp_dialog_ios_input_name_dialog_msg);

        //---------------------------------------------------------
        // set listener //
        // On Confirm //
        m_btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // 檢查是否有輸入 //
                if (m_edInputer.getText().toString().isEmpty()){
                    Toast.makeText(context, "Input is empty.", Toast.LENGTH_SHORT).show();
                    return;
                }

                m_btnClickListener.onConfirm(m_edInputer.getText().toString());
            }
        });

        // On Cancel //
        m_btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_btnClickListener.onCancel();
            }
        });
    }

    //-------------------------------------------------------------
    // Public Method
    //-------------------------------------------------------------
    // 數字輸入 //
    public void setNumberType() {
        m_edInputer.setInputType(InputType.TYPE_CLASS_NUMBER);
    }

    // Text 輸入 //
    public void setTextType(){
        m_edInputer.setInputType(InputType.TYPE_CLASS_TEXT);
    }

    // 設定 最大字數 //
    public void setMaxLength(int size) {
        m_edInputer.setFilters(new InputFilter[]{new InputFilter.LengthFilter(size)}); // 限制輸入長度, 密碼限定 4個
    }

    // 設定標題內文 //
    public void setMsg(String msg) {
        ((TextView) m_dialog.findViewById(R.id.comp_dialog_ios_input_device_name_dialog_title)).setText(msg);
    }

    // Show //
    public void show(OnButtonClickListener btnClickListener) {
        m_btnClickListener = btnClickListener;
        m_dialog.show();
    }

    // 設定能否點擊取消 //
    public void setCancelable(boolean enable) {
        m_dialog.setCancelable(enable);
    }

    // 設定 dismiss //
    public void dismiss() {
        if (m_dialog != null && m_dialog.isShowing()) {
            m_dialog.dismiss();
        }
    }

    // 取得 instance //
    public Dialog getInstance() {
        return m_dialog;
    }

    // set showing //
    public boolean isShowing() {
        return m_dialog.isShowing();
    }

    //-------------------------------------------------------------
    // Interface
    //-------------------------------------------------------------
    public interface OnButtonClickListener {
        void onConfirm(String inputMsg);
        void onCancel();
    }


}
