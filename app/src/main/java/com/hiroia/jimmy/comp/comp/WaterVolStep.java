package com.hiroia.jimmy.comp.comp;

import com.library.android_common.component.common.Pair;
import com.library.android_common.component.common.Pairs;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.IntUtil;
import com.library.android_common.util.JavaTools;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

/**
 * Create By Ray on 2018/11/15
 * 紀錄每秒出水量
 */
public class WaterVolStep {

    private Lst<Pair<Double, Integer>> m_halfSteps; // Sec, Vol
    private long id = 0;
    private WaterVolStep m_self;

    //---------------------------------------------------
    // Construct
    //---------------------------------------------------
    public WaterVolStep() {
        m_self = this;
        init();
    }

    private void init() {
        m_halfSteps = Lst.of();
    }

    //---------------------------------------------------
    // Public Method
    //---------------------------------------------------

    // 合併成 每秒的量
    public Lst<Pair<Integer, Integer>> getPerSecLst() {

        final Lst<Pair<Integer, Integer>> rlt = Lst.of();
        Pairs.of(m_halfSteps.toList()).toKLst().mergeDoublePer(2).forEach(new Lst.IConsumer<Double>() {
            @Override
            public void runEach(int i, Double d) {
                rlt.add(Pair.of(d.intValue(), Pairs.of(m_halfSteps.toList()).toVLst().mergeIntPer(2).get(i)));
            }
        });
        return rlt;
    }

    public WaterVolStep reverse() {
        m_halfSteps.reverse();
        return m_self;
    }

    public int size() {
        return getSteps().size();
    }

    public void clear() {
        m_halfSteps.clear();
    }

    // 添加步驟 //
    public void addStep(int sec, int vol) {
        double half = sec - 0.5;
        m_halfSteps.add(Pair.of(half, vol / 2));
        m_halfSteps.add(Pair.of(new Double(sec), vol / 2));

    }

    // 新增每 0.5 秒的變化量 //
    public void addHalfSecStep(double halfSec, int vol) {
        m_halfSteps.add(Pair.of(halfSec, vol));
    }


    // 取得 該秒數的出水量 //
    public int getVolBySec(int sec) {
        return Pairs.of(getPerSecLst().toList()).getValbyKey(sec);
    }

    // 取得平均值 //
    public int average() {
        Lst<Pair<Integer, Integer>> steps = getPerSecLst();
        return Pairs.of(steps.toList()).toVLst().total() / steps.size();
    }

    // 總出水量 //
    public int totalWaterVol() {
        Lst<Pair<Integer, Integer>> steps = getPerSecLst();
        return Pairs.of(steps.toList()).toVLst().total();
    }

    // 取得所有的步驟 // TODO 修復 此 function 得到的整數秒不正確，在 E Mode 需要修正 check by Ray.
    public Lst<Pair<Integer, Integer>> getSteps() {
        return getPerSecLst();
    }

    // 取得所有的正整數秒 steps //
    public Lst<Pair<Double, Integer>> getStepsBySec(){
        return m_halfSteps.oddGets();
    }

    // 取得所有的步驟 //
    public Lst<Pair<Double, Integer>> gethalfSecSteps() {
        return m_halfSteps;
    }


    public int getTotalWeight() {
        return Pairs.of(getSteps().toList()).toVLst().total();
    }

    public int getLastWeight() {
        return Pairs.of(getSteps().toList()).toVLst().last();
    }

    public int getTotalTime() {
        return Pairs.of(getSteps().toList()).toKLst().total();
    }

    public int getLastTime() {
        return Pairs.of(getSteps().toList()).toKLst().size();
    }

    // 隨機添加參數 [測試用] //
    public void setRandomStep(int min, int max) {
        for (int i = 0; i < 60; i++) {
            addStep(i, IntUtil.rangeRandom(min, max));
        }
    }

    public void swapKV() {
        m_halfSteps = Pairs.of(m_halfSteps.toList()).swapAll().toLst();
    }

    // test Line Chart 用的 不要的話可以刪除
    public void setFakeData() {
        addStep(1, 0);
        addStep(2, 0);
        addStep(3, 30);
        addStep(4, 30);
        addStep(5, 35);
        addStep(6, 35);
        addStep(7, 35);
        addStep(8, 35);
        addStep(9, 38);
        addStep(10, 40);
        addStep(11, 41);
        addStep(12, 41);
        addStep(13, 42);
        addStep(14, 42);
        addStep(15, 42);
        addStep(16, 45);
        addStep(17, 45);
        addStep(18, 45);
        addStep(19, 45);
        addStep(20, 50);
        addStep(21, 50);
        addStep(22, 50);
        addStep(23, 55);
        addStep(24, 55);
        addStep(25, 55);
        addStep(26, 56);
        addStep(27, 56);
        addStep(28, 58);
        addStep(29, 58);
        addStep(30, 58);
        addStep(31, 58);
        addStep(32, 60);
        addStep(33, 60);
        addStep(34, 60);
        addStep(35, 65);
        addStep(36, 65);
        addStep(37, 65);
        addStep(38, 70);
        addStep(39, 70);
        addStep(40, 70);
        addStep(41, 70);
        addStep(42, 70);
        addStep(43, 70);
        addStep(44, 70);
        addStep(45, 70);
        addStep(46, 80);
        addStep(47, 80);
        addStep(48, 80);
        addStep(49, 85);
        addStep(50, 85);
        addStep(51, 85);
        addStep(52, 90);
        addStep(53, 90);
        addStep(54, 100);
        addStep(55, 120);
        addStep(56, 120);
        addStep(57, 130);
        addStep(58, 150);
        addStep(59, 155);

    }


    public void setFakeData2() {
        // 順序 1m
//        addStep(0, 0);
//        addStep(1, 0);
//        addStep(2, 2);
//        addStep(3, 3);
//        addStep(4, 4);
//        addStep(5, 5);
//        addStep(6, 6);
//        addStep(7, 7);
//        addStep(8, 8);
//        addStep(9, 9);
//        addStep(10,10);
//        addStep(11,11);
//        addStep(12,12);
//        addStep(13,13);
//        addStep(14,14);
//        addStep(15,15);
//        addStep(16,16);
//        addStep(17,17);
//        addStep(18,18);
//        addStep(19,19);
//        addStep(20,20);
//        addStep(21,21);
//        addStep(22,22);
//        addStep(23,23);
//        addStep(24,24);
//        addStep(25,25);
//        addStep(26,26);
//        addStep(27,27);
//        addStep(28,28);
//        addStep(29,29);
//        addStep(30,30);
//        addStep(31,31);
//        addStep(32,32);
//        addStep(33,33);
//        addStep(34,34);
//        addStep(35,35);
//        addStep(36,36);
//        addStep(37,37);
//        addStep(38,38);
//        addStep(39,39);
//        addStep(40,40);
//        addStep(41,41);
//        addStep(42,42);
//        addStep(43,43);
//        addStep(44,44);
//        addStep(45,45);
//        addStep(46,46);
//        addStep(47,47);
//        addStep(48,48);
//        addStep(49,49);
//        addStep(50,50);
//        addStep(51,51);
//        addStep(52,52);
//        addStep(53,53);
//        addStep(54,54);
//        addStep(55,55);
//        addStep(56,56);
//        addStep(57,57);
//        addStep(58,58);
//        addStep(59,59);


        
        addStep(0, 5);
        addStep(1, 6);
        addStep(2, 7);
        addStep(3, 4);
        addStep(4, 5);
        addStep(5, 5);
        addStep(6, 6);
        addStep(7, 9);
        addStep(8, 5);
        addStep(9, 7);
        addStep(10,8);
        addStep(11,0);
        addStep(12,0);
        addStep(13,0);
        addStep(14,0);
        addStep(15,0);
        addStep(16,0);
        addStep(17,0);
        addStep(18,0);
        addStep(19,0);
        addStep(20,0);
        addStep(21,0);
        addStep(22,0);
        addStep(23,0);
        addStep(24,0);
        addStep(25,0);
        addStep(26,0);
        addStep(27,0);
        addStep(28,6);
        addStep(29,5);
        addStep(30,7);


    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    //----------------------------------------------------
    // Println //
    public void println_d(Class<?> cls) {
        println_d(cls, StrUtil.EMPTY);
    }

    public void println_d(Class<?> cls, String filter) {

        // Println Step //
        for (int i = 0; i < m_halfSteps.size(); i++) {
            Pair<Integer, Integer> p = getSteps().get(i);
            LogUtil.d(cls, filter + (i + 1) + ".  sec : " + (p.k()) // 秒數從 1 開始
                    + ", " + p.v());
        }
        // Print average //
        LogUtil.d(cls, filter + " average = " + average());

    }

    public static void println_random_item(int min, int max) {
        for (int i = 1; i < max; i++) {
            System.out.println("addStep(" + i + ", " + IntUtil.rangeRandom(min, max) + ");");
        }
    }


    public static void main(String[] args) {

        WaterVolStep wvs = new WaterVolStep();
        wvs.setFakeData();

        wvs.gethalfSecSteps().forEach(new Lst.IConsumer<Pair<Double, Integer>>() {
            @Override
            public void runEach(int i, Pair<Double, Integer> p) {
                JavaTools.println("k = " + p.k() + " ,  v = " + p.v());
            }
        });

    }


}
