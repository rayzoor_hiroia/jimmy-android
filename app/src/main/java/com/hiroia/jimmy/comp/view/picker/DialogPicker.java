package com.hiroia.jimmy.comp.view.picker;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.library.android_common.util.LogUtil;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.ArrayList;


/**
 * Created by Bruce on 2018/11/27.
 */
public class DialogPicker {

    private Dialog m_dialog;
    private OnClickEvent m_listener;
    private LinearLayout m_llvDialogPicker, m_llvUnit;

    // Picker
    private PickerView m_pickerView;
    private NumberPicker m_npRatePicker;
//    private TextView m_tvDot;
//    private ArrayList<View> m_pickerItems;

    //------------------------------------------------------------
    // Construct
    //------------------------------------------------------------
    public DialogPicker(Activity activity) {
        init(activity, "", "", "");
    }

    public DialogPicker(Context ctx, String title, String subTitle) {
        init(ctx, title, subTitle, "SET"); // TODO MultiMsg
    }

    public DialogPicker(Activity activity, String title, String subTitle) {
        init(activity, title, subTitle, "SET"); // TODO MultiMsg
    }

    public DialogPicker(Activity activity, String title, String subTitle, String strConfirm) {
        init(activity, title, subTitle, strConfirm);
    }

    //---------------------------------------------------------------
    // Private Method
    //---------------------------------------------------------------
    private void init(Context activity, String title, String subTitle, String strConfirm) {

        m_dialog = new Dialog(activity);
        m_dialog.setContentView(R.layout.dialog_picker_alert_jimmy_style);
        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        m_dialog.setCancelable(false);

        //------------------------------------------------------------
        // init view
        m_llvDialogPicker = m_dialog.findViewById(R.id.dialog_picker_alert_jimmy_style_picker_llv);
        ((TextView) m_dialog.findViewById(R.id.dialog_picker_alert_jimmy_style_title_tv)).setText(title);
        ((TextView) m_dialog.findViewById(R.id.dialog_picker_alert_jimmy_style_sub_title_tv)).setText(subTitle);
        m_llvUnit = m_dialog.findViewById(R.id.dialog_picker_alert_jimmy_style_unit_llv);
        LinearLayout btnCancel = m_dialog.findViewById(R.id.dialog_picker_alert_jimmy_style_cancel_btn_llv);
        Button btnConfirm = m_dialog.findViewById(R.id.dialog_picker_alert_jimmy_style_set_btn);

        //------------------------------------------------------------
        // set data
        btnConfirm.setText(strConfirm);
        btnCancel.setOnClickListener(clickListener);
        btnConfirm.setOnClickListener(clickListener);


        
        //------------------------------------------------------------
        // Picker View
//        PickerView pickerView = new PickerView(activity, 3, 1);
//
//        for (View item : pickerView.getItems()) {
//            m_llvDialogPicker.addView(item);
//        }


        m_pickerView = new PickerView(activity, 3, 1);

        for (View item : m_pickerView.getItems()) {
            m_llvDialogPicker.addView(item);
        }
    }

    //---------------------------------------------------------------
    // Public Method
    //---------------------------------------------------------------
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.dialog_picker_alert_jimmy_style_set_btn: // Confirm
                    m_listener.confirm(getPickerValue());
                    break;

                case R.id.dialog_picker_alert_jimmy_style_cancel_btn_llv: // Cancel
                    m_dialog.dismiss();
                    break;
            }
        }
    };

    //---------------------------------------------------------------
    // Public Method
    //---------------------------------------------------------------
    public void setContent(Activity activity, String title, String subTitle) {
        init(activity, title, subTitle, "SET"); // TODO MultiMsg
    }

    public void setContent(Activity activity, String title, String subTitle, String strConfirm) {
        init(activity, title, subTitle, strConfirm);
    }

    public void show(OnClickEvent listener) {
        m_listener = listener;
        m_dialog.show();
    }

    public void dismiss() {
        if (m_dialog != null && m_dialog.isShowing())
            m_dialog.dismiss();
    }

    public Dialog getInstance() {
        return m_dialog;
    }


    
    //---------------------------------------------------------------------------
    //---------------------------------------------------------------------------
    // PickerValue 接口 撰寫中 //
    public String getPickerValue() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < m_llvDialogPicker.getChildCount(); i ++){
            sb.append(((NumberPicker)m_llvDialogPicker.getChildAt(i)).getValue() + "");
        }
        return sb.toString();
    }

//    private String getPickerValueT(String symbol) {
////        String val = (m_npRatePicker.getValue() + "") + (m_npRatePicker2.getValue() + "");
//        String val = StrUtil.EMPTY;
//
//        switch (symbol) {
//            case StrUtil.DOT:
//                m_llvUnit.setVisibility(View.VISIBLE);
//                val = (m_npRatePicker.getValue() + StrUtil.DOT) + (m_npRatePicker2.getValue() + "") + (m_npRatePicker3.getValue() + "");
//                break;
//
//            case StrUtil.COLON:
//                val = (m_npRatePicker.getValue() + StrUtil.COLON) + (m_npRatePicker2.getValue() + "") + (m_npRatePicker3.getValue() + "");
//                break;
//
//            case StrUtil.EMPTY:
//                m_llvUnit.setVisibility(View.VISIBLE);
//                val = (m_npRatePicker.getValue() + StrUtil.EMPTY) + (m_npRatePicker2.getValue() + StrUtil.EMPTY);
//                break;
//        }
//
//        return val;
//    }

    
    //---------------------------------------------------------------------------
    //---------------------------------------------------------------------------
    // Picker View
    //---------------------------------------------------------------------------
    //---------------------------------------------------------------------------
    public class PickerView {

        private NumberPicker m_npRatePicker;
        private TextView m_tvDot;
        private ArrayList<View> m_pickerItems;
        private ArrayList<Integer> m_pickersValue;


        public PickerView(Context activity, int size, int index) {
            m_pickerItems = new ArrayList<>();
            m_pickersValue = new ArrayList<>();

            // 當有３組 picker 時 >> 12.3 、 1.23 、 .123
            // 當要小數點第N位時(不能是第０位)
            // 裡面一個 for 迴圈外面又一個導致他產生會有問題 picker
            // 回傳值的方法依樣寫在 dialog

            for (int i = 0; i < size; i++) {
                View view = LayoutInflater.from(activity).inflate(R.layout.comp_pmode_picker, null);
                m_npRatePicker = view.findViewById(R.id.comp_pmode_picker_num_picker);
                m_tvDot = view.findViewById(R.id.comp_pmode_picker_dot_tv);
                m_tvDot.setVisibility(i == index ? View.VISIBLE : View.GONE);
                m_pickerItems.add(view);
                m_pickersValue.add(i, m_npRatePicker.getValue());
                LogUtil.d(PickerView.class, "position = " + i + " value = " + m_npRatePicker.getValue());
            }
        }

        public ArrayList<View> getItems() {
            return m_pickerItems;
        }

        public ArrayList<Integer> getItemsValue() {
            return m_pickersValue;
        }
    }


    //---------------------------------------------------------------
    // interface
    //---------------------------------------------------------------
    public interface OnClickEvent {
        void confirm(String value);
    }
}
