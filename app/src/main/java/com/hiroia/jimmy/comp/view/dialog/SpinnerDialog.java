package com.hiroia.jimmy.comp.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.hiroia.jimmy.R;
import com.library.android_common.component.common.lst.Lst;

/**
 * Create By Ray on 2019/1/3
 */
public class SpinnerDialog {

    // View //
    private Context m_ctx = null;
    private Dialog m_dialog = null;
    private Spinner m_spinner = null;


    // Comp //
    private String[] m_contents = {};

    //-----------------------------------------------------------------
    // Construct
    //-----------------------------------------------------------------

    public SpinnerDialog(Context context) {
        m_ctx = context;

        //-------------------------------------------------------------
        // init dialog //
        m_dialog = new Dialog(m_ctx);
        m_dialog.setContentView(R.layout.dialog_spinner_jimmy_style);
        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        m_spinner = m_dialog.findViewById(R.id.dialog_spinner_jimmy_style);
    }

    //-----------------------------------------------------------------
    // Public Method
    //-----------------------------------------------------------------

    public void setContent(Lst<String> contents) {
        m_contents = contents.toArray();
    }

    public void show() {
        if (m_dialog == null) return;
        m_dialog.show();
    }

    public void dismiss() {
        m_dialog.dismiss();
    }

    //-----------------------------------------------------------------
    // Private Preference
    //-----------------------------------------------------------------

}
