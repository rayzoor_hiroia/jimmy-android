package com.hiroia.jimmy.comp.view.list_view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

/**
 * Create By Ray on 2018/11/1
 */
public class CustomListView extends ListView{

    private CustomListView m_self;
    private int m_res;
    private int m_count;
    private CustomListViewAdapter m_adapter;
    private OnGetViewListener m_getViewListener;

    //---------------------------------------------------
    // Construct
    //---------------------------------------------------

    public CustomListView(Context context) {
        super(context);
    }

    public CustomListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        m_adapter = new CustomListViewAdapter();
        m_self = this;
    }

    //---------------------------------------------------
    // Public Method
    //---------------------------------------------------

    public CustomListView setCount(int count){
        m_count = count;
        return m_self;
    }

    public CustomListView setItemLayout(int res){
        m_res = res;
        return m_self;
    }

    public CustomListView notifyDataSetChanged(){
        m_adapter.notifyDataSetChanged();
        return m_self;
    }

    public CustomListView setItem(OnGetViewListener listener){
        m_getViewListener = listener;
        return m_self;
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        m_self.setOnItemClickListener(listener);
    }

    public void launch(){
        m_self.setAdapter(m_adapter);
    }

    //---------------------------------------------------
    // Listener
    //---------------------------------------------------
    public interface OnGetViewListener{
        View getView(View v);
    }

    //---------------------------------------------------
    // Inner Class
    //---------------------------------------------------

    class CustomListViewAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return m_count;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View v, ViewGroup parent) {
            v = LayoutInflater.from(getContext()).inflate(m_res, null);
            return m_getViewListener.getView(v);
        }
    }

}
