package com.hiroia.jimmy.comp.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.hiroia.jimmy.R;


/**
 * Create By Ray on 2018/10/18
 * Ios Style dialog
 * <p>
 * reference : http://codesinandroid.blogspot.com/2016/10/showing-progress-dialog-same-as-ios-mb.html
 */
public class IOSProgressDialog {

    // View //
    private Dialog m_dialog = null;
    private Context m_ctx = null;
    private ImageView m_imgIcon;
    private TextView m_tvMsg;

    //-------------------------------------------------------------
    // Construct
    //-------------------------------------------------------------

    public IOSProgressDialog(Context context) {
        m_ctx = context;

        //---------------------------------------------------------
        // init dialog //
        m_dialog = new Dialog(m_ctx);
        m_dialog.setContentView(R.layout.dialog_progress_ios_style);
        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        m_dialog.getWindow().getAttributes().gravity = Gravity.CENTER;

        WindowManager.LayoutParams lp = m_dialog.getWindow().getAttributes();
        lp.dimAmount = 0.2f;
        m_dialog.getWindow().setAttributes(lp);

        //---------------------------------------------------------
        // init view //
        m_imgIcon = (ImageView) m_dialog.findViewById(R.id.dialog_progress_ios_style_icon_img);
        m_tvMsg = (TextView) m_dialog.findViewById(R.id.dialog_progress_ios_style_msg_tv);
    }

    //-------------------------------------------------------------
    // Public Method
    //-------------------------------------------------------------

    public void setMessage(String msg) {
        if (msg.isEmpty() || m_tvMsg == null) {
            m_tvMsg.setVisibility(View.GONE);
            return;
        }
        m_tvMsg.setVisibility(View.VISIBLE);
        m_tvMsg.setText(msg);
    }

    public void show(String msg){
        setMessage(msg);
        show();
    }

    public void show() {
        AnimationDrawable drawableAnim = (AnimationDrawable) m_imgIcon.getBackground();
        drawableAnim.start();
        m_dialog.show();
    }

    public void setCancelable(boolean enable){
        m_dialog.setCancelable(enable);
    }

    public void dismiss(){
        if (m_dialog != null && m_dialog.isShowing()){
            m_dialog.dismiss();
        }
    }

    public Dialog getInstance(){
        return m_dialog;
    }

    public boolean isShowing(){
        return m_dialog.isShowing();
    }

}
