package com.hiroia.jimmy.comp.callback;

/**
 * Create By Ray on 2019/2/18
 */
public interface OnPermissionCheckListener {
    void onUserConfirm();
    void onUserCancel();
}
