package com.hiroia.jimmy.comp.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.enums.MultiMsg;


/**
 * Create By Ray on 2018/6/1
 * 共用 Custom Alert Dialog, 如果換樣式就不用到處調整
 * 2018/06/25 更名為 IOSAlertDialog ，樣式已更換
 */
public class IOSAlertDialog {

    private Dialog m_dialog;
    private OnClickEvent m_listener;

    //---------------------------------------------------------------
    // Construct
    //---------------------------------------------------------------
    public IOSAlertDialog() {
    }

    public IOSAlertDialog(Context ctx, String title, String msg) {
        init(ctx, title, msg, MultiMsg.OK.msg(), MultiMsg.CANCEL.msg());
    }

    public IOSAlertDialog(Activity activity, String title, String msg) {
        init(activity, title, msg, MultiMsg.OK.msg(), MultiMsg.CANCEL.msg());
    }

    public IOSAlertDialog(Activity activity, String title, String msg, String strConfirm, String strCancel) {
        init(activity, title, msg, strConfirm, strCancel);
    }

    //---------------------------------------------------------------
    // Private Preference
    //---------------------------------------------------------------
    private void init(Context activity, String title, String msg, String strConfirm, String strCancel) {
        m_dialog = new Dialog(activity);
        m_dialog.setContentView(R.layout.comp_dialog_ios_style);
        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        m_dialog.setCancelable(false);

        //------------------------------------------------------------
        // init view
        ((TextView) m_dialog.findViewById(R.id.comp_alert_dialog_title)).setText(title);
        ((TextView) m_dialog.findViewById(R.id.comp_alert_dialog_msg)).setText(msg);
        Button btnConfirm = (Button) m_dialog.findViewById(R.id.comp_alert_dialog_confirm);
        Button btnCancel = (Button) m_dialog.findViewById(R.id.comp_alert_dialog_cancel);

        //------------------------------------------------------------
        // set data
        btnConfirm.setText(strConfirm);
        btnCancel.setText(strCancel);
        btnConfirm.setOnClickListener(clickListener);
        btnCancel.setOnClickListener(clickListener);
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.comp_alert_dialog_confirm: // confirm
                    m_listener.confirm();
                    break;

                case R.id.comp_alert_dialog_cancel: // cancel
                    m_dialog.dismiss();
                    m_listener.cancel();
                    break;
            }
        }
    };

    //---------------------------------------------------------------
    // Public Method
    //---------------------------------------------------------------
    public void setContent(Activity activity, String title, String msg) {
        init(activity, title, msg, MultiMsg.OK.msg(), MultiMsg.CANCEL.msg());
    }

    public void setContent(Activity activity, String title, String msg, String strConfirm, String strCancel) {
        init(activity, title, msg, strConfirm, strCancel);
    }

    public void show(OnClickEvent listener) {
        m_listener = listener;
        m_dialog.show();
    }

    public void dismiss() {
        if (m_dialog != null && m_dialog.isShowing())
            m_dialog.dismiss();
    }

    public Dialog getInstance() {
        return m_dialog;
    }

    //---------------------------------------------------------------
    // interface
    //---------------------------------------------------------------
    public interface OnClickEvent {
        void confirm();

        void cancel();
    }
}
