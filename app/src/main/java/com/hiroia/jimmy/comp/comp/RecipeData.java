package com.hiroia.jimmy.comp.comp;

import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.common.tuple.Tuple;
import com.library.android_common.component.common.tuple.Tuple3;
import com.library.android_common.util.StrUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bruce on 2019/1/31.
 */
public class RecipeData {

    public static final String RATIO = "ratio";
    public static final String FLOW_RATE = "flow_rate";
    public static final String PAUSE_TIME = "pause_time";

    private Lst<Tuple3<Integer, Double, Integer>> m_params = Lst.of();

    public RecipeData() {

    }

    public static RecipeData build() {
        return new RecipeData();
    }

    public static RecipeData parse(String data) {
        RecipeData recipeData = new RecipeData();

        try {
            JSONArray jsArr = new JSONArray(data);
            for (int i = 0; i < data.length(); i++) {
                JSONObject jsObj = jsArr.getJSONObject(i);
                recipeData.add(jsObj.optInt(RATIO), jsObj.optDouble(FLOW_RATE), jsObj.optInt(PAUSE_TIME));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return recipeData;
    }


    public void add(int ratio, double flowRate, int pauseTime) {
        m_params.add(Tuple3.of(ratio, flowRate, pauseTime));
    }

    public String datas() {
        StringBuilder sb = new StringBuilder();
        sb.append(StrUtil.LEFT_BRACKET);
        for (int i = 0; i < m_params.size(); i++) {

            // { //
            sb.append(StrUtil.LEFT_CURLY_BRACKET);

            // "ratio": 2 //
            sb.append(StrUtil.QUOTE);
            sb.append(RATIO);
            sb.append(StrUtil.QUOTE);
            sb.append(StrUtil.COLON);
            sb.append(m_params.get(i).v1());

            // , //
            sb.append(StrUtil.COMMA);

            // "flow_rate": 13 //
            sb.append(StrUtil.QUOTE);
            sb.append(FLOW_RATE);
            sb.append(StrUtil.QUOTE);
            sb.append(StrUtil.COLON);
            sb.append(m_params.get(i).v2());

            // , //
            sb.append(StrUtil.COMMA);

            // "pause_time": 15 //
            sb.append(StrUtil.QUOTE);
            sb.append(PAUSE_TIME);
            sb.append(StrUtil.QUOTE);
            sb.append(StrUtil.COLON);
            sb.append(m_params.get(i).v3());

            // }, //
            sb.append(StrUtil.RIGHT_CURLY_BRACKET);
            sb.append(StrUtil.COMMA);
        }
        if (m_params.size() > 0)
            sb.setLength(sb.length() - 1);
        sb.append(StrUtil.RIGHT_BRACKET);

        return sb.toString();
    }
}
