package com.hiroia.jimmy.comp.view.progress;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;

/**
 * Create By Ray on 2018/12/6
 * 這個 Lib 沒有做設定顏色的 Api，暫時不封裝
 * 請勿使用
 */
public class CircularDripProgress extends LinearLayout{

    // View //
    private LinearLayout m_self;
    private CircularProgressBar m_cpb;

    // Comp //
    private int m_nMax = 100;
    private int m_nDefaultPos = 0;

    //----------------------------------------------------------
    // Construct
    //----------------------------------------------------------

    public CircularDripProgress(Context context) {
        super(context);
        init(context, null);
    }

    public CircularDripProgress(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CircularDripProgress(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public CircularDripProgress(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context ctx, AttributeSet attrs){
        m_self = this;
        m_cpb = new CircularProgressBar(ctx, attrs);
        m_cpb.setLayoutParams(m_self.getLayoutParams());

        // set param //
        m_cpb.setProgressMax(m_nMax);
        m_cpb.setProgress(m_nDefaultPos);
    }

    //----------------------------------------------------------
    // Public Method
    //----------------------------------------------------------














}
