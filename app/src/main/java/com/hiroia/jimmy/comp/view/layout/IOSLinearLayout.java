package com.hiroia.jimmy.comp.view.layout;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.hiroia.jimmy.R;

/**
 * Created by Bruce on 2018/6/14.
 */
public class IOSLinearLayout extends LinearLayout {

    //------------------------------------------------------------------------
    // Construct
    //------------------------------------------------------------------------
    public IOSLinearLayout(Context context) {
        super(context);
        init();
    }

    public IOSLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public IOSLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public IOSLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setBackgroundResource(R.drawable.alertdialog_style);
    }

}
