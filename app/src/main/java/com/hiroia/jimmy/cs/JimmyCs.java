package com.hiroia.jimmy.cs;

/**
 * Create By Ray on 2019/2/21
 */
public class JimmyCs {

    // OTA 相關 //
    public static final String OTA_PACKAGE_PATH = "com.hiroia.ota.jimmy";
    public static final String OTA_PACKAGE_PATH_MAIN_ACTIVITY = OTA_PACKAGE_PATH.concat(".MainActivity");

    // Debug Mode Setting //
    public static final boolean IS_SETTING_MODE_ON = false;
}
