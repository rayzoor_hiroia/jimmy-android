package com.hiroia.jimmy.application;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.hiroia.jimmy.ble.BLEJimmy;
import com.hiroia.jimmy.comp.comp.PermissionChecker;
import com.hiroia.jimmy.db.sp.SpManager;
import com.hiroia.jimmy.manager.PhoneManager;
import com.hiroia.jimmy.manager.net.NetworkManager;

/**
 * Create By Ray on 2018/11/1
 */
public class JimmyApplication extends Application{

    public static Context sm_ctx;

    //--------------------------------------------------------------------------
    // Construct
    //--------------------------------------------------------------------------
    public JimmyApplication() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //-------------------------------------------
        // update comp
        sm_ctx = this.getApplicationContext();

        //-------------------------------------------
        // init
        SpManager.init(sm_ctx);
        NetworkManager.init(sm_ctx);

        // BLE //
        BLEJimmy bleJimmy = new BLEJimmy();
        bleJimmy.init(getApplicationContext());

        // device manager //
        PhoneManager.init(sm_ctx);
    }
}
