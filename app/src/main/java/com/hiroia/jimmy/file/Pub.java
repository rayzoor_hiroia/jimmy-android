package com.hiroia.jimmy.file;

/**
 * Created by bruce on 02/13.
 */

public class Pub {

    //-----------------------------------------------------
    // Content
    //-----------------------------------------------------
    public static final String RAYMOND_MUTIMSG_PAGE_PATH = "/Users/hiroia_20180511/Desktop/Raymond/Android/samantha/samantha-android/app/src/main/java/com/hiroia/samantha/enums/MutiMsg.java";
    public static final String RAYMOND_DESKTOP_PATH = "/Users/hiroia_20180511/Desktop/";
    public static final String BRUCE_MUTIMSG_PAGE_PATH = "/Users/hiroia-android/Desktop/bruce/android/samantha-android/app/src/main/java/com/hiroia/samantha/enums/MutiMsg.java";
    public static final String BRUCE_PATH = "/Users/hiroia-android/Desktop/";

    public static final String LOCAL_SERVICE_CHINESE = "file:///android_asset/service_ch.html";
    public static final String LOCAL_SERVICE_ENGLISH = "file:///android_asset/service_en.html";
    public static final String LOCAL_PRIVACY_CHINESE = "file:///android_asset/privacy_ch.html";
    public static final String LOCAL_PRIVACY_ENGLISH = "file:///android_asset/privacy_en.html";

    public static final String LOCAL_SERVICE_TERM_CHINESE = "file:///android_asset/services_term_ch.html";
    public static final String LOCAL_SERVICE_TERM_ENGLISH = "file:///android_asset/services_term_en.html";
    public static final String LOCAL_SERVICE_TERM_JAPENESE = "file:///android_asset/services_term_jp.html";
    public static final String LOCAL_PRIVACY_POLICY_CHINESE = "file:///android_asset/privacy_policy_ch.html";
    public static final String LOCAL_PRIVACY_POLICY_ENGLISH = "file:///android_asset/privacy_policy_en.html";
    public static final String LOCAL_PRIVACY_POLICY_JAPENESE = "file:///android_asset/privacy_policy_jp.html";


    public static void main(String[] args) {
//        JavaTools.println(desktop());
    }
}



