package com.hiroia.jimmy.view.mode_e;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.hiroia.jimmy.ble.module.BLEJimmyFragment;
import com.hiroia.jimmy.comp.comp.RecordData;
import com.hiroia.jimmy.comp.view.dialog.IOSAlertDialog;
import com.hiroia.jimmy.cs.HttpCs;
import com.hiroia.jimmy.db.sql.JRecordDBA;
import com.hiroia.jimmy.enums.HttpDef;
import com.hiroia.jimmy.manager.AccountManager;
import com.hiroia.jimmy.manager.JRecordEModeManager;
import com.hiroia.jimmy.manager.net.NetworkManager;
import com.hiroia.jimmy.model.record.ModelJRecord;
import com.library.android_common.component.common.Opt;
import com.library.android_common.component.date.YMDHMS;
import com.library.android_common.component.date.hms.MS;
import com.library.android_common.component.date.hms.S;
import com.library.android_common.util.LogUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.hiroia.jimmy.ble.comp.JimmyInfo.Mode.E1;
import static com.hiroia.jimmy.ble.comp.JimmyInfo.Mode.E3;

/**
 * Created by Bruce on 2018/11/19.
 */
public class EModeExtractionFragment_3 extends BLEJimmyFragment implements View.OnClickListener {

    // View //
    private LinearLayout m_llvBack, m_llvRecord, m_llvStatus, m_llvReset;
    private TextView m_tvRecord, m_tvTitle, m_tvTitleSmall, m_tvStatus, m_tvSubTitle, m_tvWeight, m_tvTimer, m_tvUnit;
    private ImageView m_imgStatus, m_imgWater, m_imgCupEmpty, m_imgCupFull, m_imgReset;
    private IOSAlertDialog m_dialog;

    // Comp //
    private JimmyInfo m_jimmyInfo = null;
    private MS m_currMs = MS.of(0, 0); // default ms
//    private WaterVolStep m_wvs = new WaterVolStep();

    private ModelJRecord m_modelJRecord = new ModelJRecord();
    private RecordData m_recordData = new RecordData();

    private double m_lastSecWeight = 0.0;
    private Timer m_timer = new Timer();
    private int m_sec = 0;
    private int m_t0 = 0; // t0 的時間
    private int m_staySec = 0; // 在相同重量下停止的時間
    private int m_nIsRecodingTimes = 0;
    private int m_nTZero = 0;
    private boolean m_bIsRecording = false; // 是否正在執行 recording , 若是則紀錄。並且 recording 完成後將所有 recording comp 歸零。

    //-----------------------------------------------------------------
    // Fragment Life Cycle
    //-----------------------------------------------------------------

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_emode_extraction, null);

        //-----------------------------------------------------------------
        // init
        m_llvBack = rootView.findViewById(R.id.fragment_emode_extraction_back_llv);
        m_llvRecord = rootView.findViewById(R.id.fragment_emode_extraction_record_llv);
        m_llvStatus = rootView.findViewById(R.id.fragment_emode_extraction_data_status_llv);
        m_llvReset = rootView.findViewById(R.id.fragment_emode_extraction_reset_llv);

        m_tvRecord = rootView.findViewById(R.id.fragment_emode_extraction_record_tv);
        m_tvTitle = rootView.findViewById(R.id.fragment_emode_extraction_title_tv);
        m_tvTitleSmall = rootView.findViewById(R.id.fragment_emode_extraction_title_small_tv);
        m_tvStatus = rootView.findViewById(R.id.fragment_emode_extraction_data_status_tv);
        m_tvSubTitle = rootView.findViewById(R.id.fragment_emode_extraction_sub_title_tv);
        m_tvUnit = rootView.findViewById(R.id.fragment_emode_espresso_unit_tv);

        m_tvWeight = rootView.findViewById(R.id.fragment_emode_espresso_weight_tv);
        m_tvTimer = rootView.findViewById(R.id.fragment_emode_espresso_timer_tv);

        m_imgWater = rootView.findViewById(R.id.fragment_emode_extraction_state_water_img);
        m_imgCupEmpty = rootView.findViewById(R.id.fragment_emode_extraction_state_cup_empty_img);
        m_imgCupFull = rootView.findViewById(R.id.fragment_emode_extraction_state_cup_full_img);
        m_imgReset = rootView.findViewById(R.id.fragment_emode_extraction_reset_img);
        m_imgStatus = rootView.findViewById(R.id.fragment_emode_extraction_data_status_img);

        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);
        m_llvRecord.setOnClickListener(this);
        m_imgReset.setOnClickListener(this);
        m_imgStatus.setOnClickListener(this);

        setEspressoState(getParentActivity().getCurrMode());
        return rootView;
    }

    @Override
    public void onResume() {
        registerTimer();
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_emode_extraction_back_llv: // Back
                goToBrewRecordPage();
                break;

            case R.id.fragment_emode_extraction_record_llv: // Record
                getParentActivity().switchStateView(EModeActivity.EModeState.BREW_RECORDS_STATE);
                getParentActivity().setIsExtractionGoTo(true);
                break;

            case R.id.fragment_emode_extraction_data_status_img: // Recording Mark
                goToBrewRecordPage();
                break;

            case R.id.fragment_emode_extraction_reset_img: // Reset
                resetBothTW();
                m_sec = 0;
                m_lastSecWeight = 0.0;
                break;
        }
    }


    //-----------------------------------------------------------------
    // Private Method
    //-----------------------------------------------------------------

    private EModeActivity getParentActivity() {
        return EModeActivity.getInstance();
    }

    private void goToBrewRecordPage() {
        m_dialog = new IOSAlertDialog(getParentActivity(), "Confirm Navigation", "Are you sure to leave Espresso Mode?", "Leave", "Stay"); // 改多國語言
        m_dialog.show(new IOSAlertDialog.OnClickEvent() {
            @Override
            public void confirm() {
                getParentActivity().switchStateView(EModeActivity.EModeState.BREW_RECORDS_STATE);
                m_dialog.dismiss();
            }

            @Override
            public void cancel() {
                m_dialog.dismiss();
            }
        });
    }

    // 改多國語言
    private void setEspressoState(JimmyInfo.Mode mode) {
        switch (mode) {
            case E1:
                m_tvTitleSmall.setText("State 1");
                m_imgWater.setVisibility(View.VISIBLE);
                m_imgCupEmpty.setVisibility(View.INVISIBLE);
                m_imgCupFull.setVisibility(View.INVISIBLE);
                break;

            case E2:
                m_tvTitleSmall.setText("State 2");
                m_imgWater.setVisibility(View.VISIBLE);
                m_imgCupEmpty.setVisibility(View.VISIBLE);
                m_imgCupFull.setVisibility(View.INVISIBLE);
                break;

            case E3:
                m_tvTitleSmall.setText("State 3");
                m_imgWater.setVisibility(View.INVISIBLE);
                m_imgCupEmpty.setVisibility(View.VISIBLE);
                m_imgCupFull.setVisibility(View.VISIBLE);
                break;
        }
    }

    // 更新 Sub Title //
    private void updateSubTitle(final JimmyInfo info) {
        getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // 紀錄中代表 Extraction 完成 //
                if (m_bIsRecording) {
                    m_tvSubTitle.setTextSize(12);
                    m_tvSubTitle.setText("Extraction Complete");
                    return;
                }

                // Tare or Extracting //
                m_tvSubTitle.setTextSize(20);
                m_tvSubTitle.setText(!isStateAfterTare(info) ? "Extracting" : (info.getCurrMode() == E1) ? "Manual Tare" : "Auto Tare");

                // check 是否為 E3 //
                if (info.getCurrMode() != E3 || info.getWeight() > 0.5)
                    return;

                // 如果重量小於 0.5g 代表為 Pre-Infusion //
                m_tvSubTitle.setText((info.getTime() > 5) ? "Pre-Infusion" : "Auto Tare");
            }
        });
    }


    // 監聽的事件 //
    private void registerTimer() {
        if (m_timer == null) {
            m_timer = new Timer();
        }
        m_timer.schedule(m_timeTask, S.ONE_SEC.millySec(), S.ONE_SEC.millySec());
    }

    // 執行 Recording data //
    private TimerTask m_timeTask = new TimerTask() {
        @Override
        public void run() {

            // 檢查有無連線中 //
            if (!isConnected()) return;

            // Check data //
            if (m_jimmyInfo == null) return;

            // Update Sub Title //
            updateSubTitle(m_jimmyInfo);

            // 紀錄中 //
            if (m_bIsRecording) {
                m_nIsRecodingTimes += 1;
                LogUtil.d(EModeExtractionFragment_3.class, getSimpleClassName() + " [WaterVolStep],  is Recording !! ,  times = " + m_nIsRecodingTimes);
                m_bIsRecording = (m_jimmyInfo.getWeight() <= 0.0 && m_jimmyInfo.getTime() <= 0) ? false : true;

                // 紀錄完成後三秒 //
                if (m_nIsRecodingTimes >= 3) {
                    // Refresh 狀態
                    m_sec = 0;
                    m_lastSecWeight = 0.0;
                    m_nIsRecodingTimes = 0;
                    m_t0 = 0;
                    m_recordData.clear();
                }
                return;
            }

            double weight = m_jimmyInfo.getWeight();
            int sec = m_jimmyInfo.getTime();

            // 監測 E2、E3 的 t0 時間 //
            if (m_jimmyInfo.getCurrMode() == JimmyInfo.Mode.E2 || m_jimmyInfo.getCurrMode() == JimmyInfo.Mode.E3) {
                // 當 display 沒有計時 && 重量開始變化
                if (sec < 1 && weight > 0.0) {
                    m_t0 += 1;
                    LogUtil.d(EModeExtractionFragment_3.class, getSimpleClassName() + " [WaterVolStep],  T0 = " + m_t0);
                }
            }

            // E3 的流程 //
            if (m_jimmyInfo.getCurrMode() == JimmyInfo.Mode.E3) {
                m_sec = sec;
                m_recordData.add(m_sec, weight);
            }

            LogUtil.d(EModeExtractionFragment_3.class, getSimpleClassName() + " [WaterVolStep],  Sec = " + sec + ",  mSec = " + m_sec + ",  Stay Sec = " + m_staySec + ",  current weight = " + weight + ",  last weight = " + m_lastSecWeight);

            // Reset 狀態 //
            if (weight <= 0.0 && m_jimmyInfo.getTime() <= 0) {
                m_sec = 0;
                m_lastSecWeight = 0.0;
            }

            // 重量低於 0.0 時不執行 +- range 0.2 //
            if (weight < 0.2 || sec <= 0) return;

            // E1, E2 的流程 //
            if (m_jimmyInfo.getCurrMode() != JimmyInfo.Mode.E3) {
                m_sec = sec;
                m_recordData.add(m_sec, weight);
            }

            // 當重量變化停止後，開始 Recording 前倒數計時 //
            if (m_lastSecWeight != weight) {
                m_lastSecWeight = weight;
                m_staySec = 0;
            } else {
                m_staySec += 1;
            }

            // 若是重量變化停止超過三秒，開始 Recording //
            if (m_staySec >= 2) {
                LogUtil.d(EModeExtractionFragment_3.class, getSimpleClassName() + "[WaterVolStep], Start to Recording data ");
                getParentActivity().showProgressDialog("Recording...");

                // Start to recording //
                syncJRecordsToCloud();

                // Setting recording comp //
                m_bIsRecording = true;
                m_staySec = 0;
            }
        }
    };

    // Current Weight - Last Weight //
    private double getDiffWeight(double currentWeight) {
        return currentWeight - m_lastSecWeight;
    }

    private boolean isStateAfterTare(JimmyInfo info) {
        return info.getTime() < 5 && info.getWeight() < 0.5;
    }

    private void syncJRecordsToCloud() {

        // Declare //
        m_modelJRecord.setRecordData(m_recordData);
        m_modelJRecord.setCreated_time(YMDHMS.nowYMDHMS());
        m_modelJRecord.setMode(ModelJRecord.JScaleMode.E_MODE);
        m_modelJRecord.setTime1(m_t0);
        m_modelJRecord.setTime2(m_recordData.getTotalTimeSec() - m_modelJRecord.getTime1());


        // Check 水量是否超過兩千，若是超過就不執行 //
        if (m_recordData.isWaterVolOverThan2000()) {
            LogUtil.e(EModeExtractionFragment_3.class, HttpDef.CREATE_J_RECORD.getTag() + " is over than 2000 ! ");
            getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
            return;
        }

        final JRecordDBA dba = JRecordDBA.open(getParentActivity());

        // 如果沒有網路 //
        if (NetworkManager.isOffLine()) {

            // insert to db //
            dba.insert(m_modelJRecord);
            getParentActivity().dismissProgressDialog(S.Positive.THREE_SEC);
            return;
        }

        // Start to sync to cloud //
        HttpDef.CREATE_J_RECORD.post()
                .addParam("token", AccountManager.getAccountModel().getToken())
                .addParam("water", m_modelJRecord.getRecordData().getEModeWater() + "")
                .addParam("jscale_id", "0")
                .addParam("jrecipe_id", "0")
                .addParam("score", m_modelJRecord.getScore() + "")
                .addParam("mode", m_modelJRecord.getMode().id() + "")
                .addParam("state", m_modelJRecord.getState().id() + "")
                .addParam("record_time", m_modelJRecord.getCreated_time().formatISO8601())
                .addParam("time1", m_modelJRecord.getTime1() + "")
                .addParam("time2", m_modelJRecord.getTime2() + "")
                .addParam("training_data", m_modelJRecord.getTrainingData().datas())
                .addParam("record_data", m_modelJRecord.getRecordData().datas())
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject jsObj) {
                        if (response.isEmpty() || response == null) {
                            LogUtil.e(EModeExtractionFragment_3.class, HttpDef.CREATE_J_RECORD.getTag() + " response == null or Empty");
                            getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                            return;
                        }

                        // Check 是否為 success //
                        if (!Opt.ofJson(jsObj, HttpCs.SUCCESS).parseToBoolean().get()) {
                            LogUtil.e(EModeExtractionFragment_3.class, HttpDef.CREATE_J_RECORD.getTag() + " Http Success = " + false);
                            getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                            return;
                        }

                        LogUtil.d(EModeExtractionFragment_3.class, HttpDef.CREATE_J_RECORD.getTag() + " response = " + response);

                        // decode JSON //
                        jsObj = Opt.ofJson(jsObj, "result").toJsonObj().get();
                        ModelJRecord m = ModelJRecord.decodeJSON(jsObj);

                        dba.insert(m); // Insert to db //
                        JRecordEModeManager.addModel(m); // Add to manager //

                        // dismiss progress dialog //
                        getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        m_timer.cancel();
        m_timer = null;
        m_timeTask.cancel();
        m_timeTask = null;
        super.onDestroy();
    }

    //-----------------------------------------------------------------
    // BLEAction
    //-----------------------------------------------------------------

    @Override
    public void onScanDevicesUpdate(ArrayList<BluetoothDevice> devices) {

    }

    @Override
    public void onConnectionStateUpdate(int state) {

    }

    @Override
    public void onJimmyInfoUpdate(JimmyInfo jimmyInfo) {
        m_jimmyInfo = jimmyInfo;

        //---------------------------------------------------

        m_modelJRecord.setState(jimmyInfo.getCurrMode());

        m_tvWeight.setText(jimmyInfo.getWeight() + "");
        m_tvTimer.setText(jimmyInfo.getMs().toColonString());
        m_tvUnit.setText(jimmyInfo.getCurrUnit().get());
        setEspressoState(jimmyInfo.getCurrMode());


        updateSubTitle(jimmyInfo);

    }

    @Override
    public void onBLEInit() {

    }

}
