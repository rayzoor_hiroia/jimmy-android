package com.hiroia.jimmy.view.mode_e;

import android.app.Fragment;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.hiroia.jimmy.ble.module.BLEJimmyActivity;
import com.hiroia.jimmy.comp.comp.WaterVolStep;
import com.hiroia.jimmy.db.sql.EModeChartDBA;
import com.hiroia.jimmy.db.sql.EModeRecordDBA;
import com.hiroia.jimmy.manager.old._EModelDataManager;
import com.hiroia.jimmy.model.ModelDateRecord;
import com.hiroia.jimmy.model.ModelEModeRecord;
import com.hiroia.jimmy.model.record.ModelJRecord;
import com.hiroia.jimmy.view.mode_e.record.EModeReportDailyContentFragment_2;
import com.hiroia.jimmy.view.mode_e.record.EModeReportDailyFragment_1;
import com.library.android_common.component.common.Pair;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.hms.MS;
import com.library.android_common.component.date.ymd.YMD;
import com.library.android_common.util.DateUtil;

import java.util.ArrayList;

/**
 * Created by Bruce on 2018/11/14.
 */
public class EModeActivity extends BLEJimmyActivity {

    // Fragment //
    private static EModeActivity sm_self;
    private EModeBrewRecordsFragment_1 m_brewRecordsFragment;
    private EModeExtractionFragment_3 m_extractionFragment;
    private EModeEspressoStateSelectFragment_2 m_stateSelectFragment;
    private EModeReportDailyContentFragment_2 m_dailyContentFragment;
    private EModeReportDailyFragment_1 m_dailyFragment;

    // Comp //
    private JimmyInfo.Mode m_currMode = JimmyInfo.Mode.T1;
//    private boolean m_bIsTopInfoShow = false;
    private boolean m_bIsRecordingMode = false;
    private boolean m_bIsAlreadyRecord = false;
    private boolean m_bIsReadyToRecording = false;

    private ModelDateRecord m_currentDateRecord = null;
    private ModelEModeRecord m_currentEmodeRecord = null;

    private MS m_currMS = MS.of(0, 0);
    private Lst<Pair<Integer, Double>> m_records = Lst.of();
    private boolean m_bIsExtractionGoTo = false;

    //-------------------------------------------------------------
    // Activity Life Cycle
    //-------------------------------------------------------------

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emode, false);
        sm_self = this;

        //------------------------------------------
        // init view
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        syncRecordToManager();
        switchStateView(EModeState.BREW_RECORDS_STATE);
    }

    //-------------------------------------------------------------
    // Private Preference
    //-------------------------------------------------------------

    private void init() {
        m_brewRecordsFragment = new EModeBrewRecordsFragment_1();
        m_dailyFragment = new EModeReportDailyFragment_1();
        m_stateSelectFragment = new EModeEspressoStateSelectFragment_2();
        m_dailyContentFragment = new EModeReportDailyContentFragment_2();
        m_extractionFragment = new EModeExtractionFragment_3();
    }

    private void switchFragment(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_emode_flt, fragment)
                .commit();
    }

    //-------------------------------------------------------------
    // Public Method
    //-------------------------------------------------------------

    public void notifyDataSetChanged(){
        m_dailyFragment.notifyDataSetChanged();
    }

    public JimmyInfo.Mode getCurrMode() {
        return m_currMode;
    }

    public void setCurrMode(JimmyInfo.Mode currMode) {
        this.m_currMode = currMode;
    }

//    public boolean getIsTopInfoShow() {
//        return m_bIsTopInfoShow;
//    }
//
//    public void setIsTopInfoShow(boolean isTopInfoShow) {
//        this.m_bIsTopInfoShow = isTopInfoShow;
//    }

    public boolean getIsExtractionGoTo() {
        return m_bIsExtractionGoTo;
    }

    public void setIsExtractionGoTo(boolean isExtractionGoTo) {
        this.m_bIsExtractionGoTo = isExtractionGoTo;
    }

    public EModeBrewRecordsFragment_1 getEModeBrewRecordsFragment() {
        return m_brewRecordsFragment;
    }

    public static EModeActivity getInstance() {
        return sm_self;
    }

    // 設定紀錄模式 : true || false //
    public void setRecodingMode(boolean enable) {
        m_bIsRecordingMode = enable;
    }

    // 是否為紀錄模式 //
    public boolean isRecodingMode() {
        return true;
    }

    // 若已經紀錄過相同的一筆便不會繼續紀錄，reset it.
    public void resetRecordGate() {
        m_bIsAlreadyRecord = false;
    }

    // 設定當前選擇的 date record //
    public void setCurrentDateRecord(ModelDateRecord m) {
        m_currentDateRecord = m;
    }

    // 取得當前選擇的 date record //
    public ModelDateRecord getCurrentDateRecord() {
        return m_currentDateRecord;
    }

    public void setCurrentModelEModeRecord(ModelEModeRecord m) {
        m_currentEmodeRecord = m;
    }

    public ModelEModeRecord getCurrentEModeRecord() {
        return m_currentEmodeRecord;
    }

    // Switch Fragment View //
    public void switchStateView(EModeState state) {
        switch (state) {
            case BREW_RECORDS_STATE:
                m_brewRecordsFragment = new EModeBrewRecordsFragment_1(); // refresh fragment
                switchFragment(m_brewRecordsFragment);
                break;

            case EXTRACTION_STATE:
                m_extractionFragment = new EModeExtractionFragment_3(); // refresh fragment
                switchFragment(m_extractionFragment);
                break;

            case STATE_SELECT_STATE:
                switchFragment(m_stateSelectFragment);
                break;

            case DAILY_CONTENT_STATE:
                switchFragment(m_dailyContentFragment);
                break;

            case DAILY_STATE:
                switchFragment(m_dailyFragment);
                break;
        }
    }

    public enum EModeState {
        BREW_RECORDS_STATE,
        EXTRACTION_STATE,
        STATE_SELECT_STATE,
        DAILY_CONTENT_STATE,
        DAILY_STATE,
    }

    //-----------------------------------------------------------
    // Interface
    //-----------------------------------------------------------
    public interface OnRecordingListener {
        void onFinish();
    }

    //-----------------------------------------------------------
    // BLE Action
    //-----------------------------------------------------------

    @Override
    public void onScanDevicesUpdate(ArrayList<BluetoothDevice> devices) {

    }

    @Override
    public void onConnectionStateUpdate(int state) {

    }

    @Override
    public void onJimmyInfoUpdate(JimmyInfo jimmyInfo) {

    }

    @Override
    public void onBLEInit() {

    }

}
