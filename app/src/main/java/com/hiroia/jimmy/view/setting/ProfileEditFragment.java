package com.hiroia.jimmy.view.setting;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy._test._ActionCallBack;
import com.hiroia.jimmy.cs.HttpCs;
import com.hiroia.jimmy.db.sp.SpAccount;
import com.hiroia.jimmy.enums.HttpDef;
import com.hiroia.jimmy.manager.AccountManager;
import com.hiroia.jimmy.model.ModelAccount;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.library.android_common.component.common.Pair;
import com.library.android_common.component.date.hms.S;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by Bruce on 2019/1/30.
 */
public class ProfileEditFragment extends BaseFragment implements View.OnClickListener {

    private LinearLayout m_llvCancel, m_llvSave, m_llvPhoto;
    private TextView m_tvSave, m_tvTitle, m_tvChangePhoto;
    private ImageView m_imgPhoto;
    private EditText m_etName;
    private Button m_btnChangePassword;

    private String m_sPicPath = StrUtil.EMPTY;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_profile_edit, null);

        //-----------------------------------------------------------------
        // init
        m_llvCancel = rootView.findViewById(R.id.fragment_profile_edit_cancel_llv);
        m_llvSave = rootView.findViewById(R.id.fragment_profile_edit_save_llv);
        m_llvPhoto = rootView.findViewById(R.id.fragment_profile_edit_portrait_llv);

        m_tvSave = rootView.findViewById(R.id.fragment_profile_edit_save_tv);
        m_tvTitle = rootView.findViewById(R.id.fragment_profile_edit_title_tv);
        m_tvChangePhoto = rootView.findViewById(R.id.fragment_profile_edit_change_portrait_tv);

        m_imgPhoto = rootView.findViewById(R.id.fragment_profile_edit_portrait_img);

        m_etName = rootView.findViewById(R.id.fragment_profile_edit_user_name_et);

        m_btnChangePassword = rootView.findViewById(R.id.fragment_profile_edit_cgange_password_btn);

        //-----------------------------------------------------------------
        //set data
        m_llvCancel.setOnClickListener(this);
        m_llvSave.setOnClickListener(this);
        m_llvPhoto.setOnClickListener(this);
        m_btnChangePassword.setOnClickListener(this);

        loadPic();
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_profile_edit_cancel_llv: // Cancel
                getParentActivity().switchFragment(SettingActivity.SettingState.SETTING_PAGE);
                break;

            case R.id.fragment_profile_edit_save_llv: // Save
                updateAccount(); // Call Upload Account API
                uploadPhoto(); // Call Upload Photo API
                getParentActivity().switchFragment(SettingActivity.SettingState.SETTING_PAGE);
                break;

            case R.id.fragment_profile_edit_portrait_llv: // Change Photo

                // Check Storage permission //
                if (!getParentActivity()._checkWritePermission())
                    return;

                getParentActivity().pickImage(new _ActionCallBack() {
                    @Override
                    public void action(String path) {
                        LogUtil.d(ProfileEditFragment.class, " Upload Profile Pic Path : " + path);
                        m_sPicPath = path;
                        m_imgPhoto.setImageBitmap(BitmapFactory.decodeFile(path));
                    }
                });
                break;

            case R.id.fragment_profile_edit_cgange_password_btn: // Change Password
                getParentActivity().switchFragment(SettingActivity.SettingState.CHANGE_PASSWORD);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        m_etName.setText(AccountManager.getAccountModel().getUserName());
    }

    //---------------------------------------------------------------
    // Public Method
    //---------------------------------------------------------------

    public SettingActivity getParentActivity() {
        return SettingActivity.getInstance();
    }

    //---------------------------------------------------------------
    // Private Method
    //---------------------------------------------------------------

    private void uploadPhoto() {

        // 若是照片沒有被選不執行 //
        if (LogUtil.Check.d(!isPhotoSelected(), ProfileEditFragment.class, " Photo is Empty."))
            return;

        HttpDef.UPLOAD_USER_PHOTO
                .form()
                .addFormData(HttpCs.TOKEN, AccountManager.getAccountModel().getToken())
                .addFormFile(HttpCs.PHOTO, new File(m_sPicPath))
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject jsObj) {
                        if (response == null || response.isEmpty()) {
                            LogUtil.d(ProfileEditFragment.class, HttpDef.UPLOAD_USER_PHOTO.getTag() + " response is Null or Empty.");
                            return;
                        }

                        LogUtil.d(ProfileEditFragment.class, HttpDef.UPLOAD_USER_PHOTO.getTag().concat(response));
                        getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);

                        // Start to decode JSON //
                        try {
                            if (!jsObj.getBoolean(HttpCs.SUCCESS)) return;

                            // decode JSON //
                            ModelAccount m = ModelAccount.decodeUploadPhotoJSON(jsObj);
                            m.setToken(AccountManager.getAccountModel().getToken());
                            m.setEmail(AccountManager.getAccountModel().getEmail());
                            m.setRecentToken(AccountManager.getAccountModel().getRecentToken());

                            // Save data //
                            SpAccount.put(m);

                            // Sync to Manager //
                            AccountManager.syncAccountModel(m);
                            getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);

                        } catch (JSONException e) {
                            getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                            e.printStackTrace();
                        }
                    }
                });

        getParentActivity().dismissProgressDialog(S.Positive.THREE_SEC);
    }

    private void updateAccount() {

        // 檢查姓名是否 Changed //
        if (!isNameChanged())
            return;

        Pair<String, String> namePair = Pair.of(HttpCs.USER_NAME, m_etName.getText().toString().trim());
        getParentActivity().showProgressDialog();

        HttpDef.UPLOAD_ACCOUNT.raw()
                .addRawObject(HttpCs.TOKEN, AccountManager.getAccountModel().getToken())
                .addRawArray(HttpCs.UPDATE, namePair)
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject jsObj) {
                        if (response == null || response.isEmpty()) {
                            LogUtil.d(ProfileEditFragment.class, HttpDef.UPLOAD_ACCOUNT.getTag() + " response is Null or Empty.");
                            return;
                        }

                        LogUtil.d(ProfileEditFragment.class, HttpDef.UPLOAD_ACCOUNT.getTag() + " response  =  " + response);
                        getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);

                        // Start To decode JSON //
                        try {
                            if (!jsObj.getBoolean(HttpCs.SUCCESS)) return;

                            // decode JSON //
                            ModelAccount m = ModelAccount.decodeUploadAccountJSON(jsObj);
                            m.setToken(AccountManager.getAccountModel().getToken());
                            m.setEmail(AccountManager.getAccountModel().getEmail());
                            m.setRecentToken(AccountManager.getAccountModel().getRecentToken());

                            // Save data //
                            SpAccount.put(m);

                            // Sync Manager //
                            AccountManager.syncAccountModel(m);
                            getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);

                        } catch (JSONException e) {
                            getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                            e.printStackTrace();
                        }
                    }
                });

        getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
    }

    // load pic into profile pic //
    private void loadPic() {
        LogUtil.d(ProfileEditFragment.class, "photo = " + AccountManager.getAccountModel().getFbPhoto());
        String sPicPath = AccountManager.getAccountModel().getFbPhoto();

        if (!sPicPath.isEmpty())
            Picasso.get().load(AccountManager.getAccountModel().getFbPhoto()).error(R.mipmap.ic_portrait_default).into(m_imgPhoto);
        else
            Picasso.get().load(R.mipmap.ic_portrait_default).into(m_imgPhoto);
    }

    // 若是為空代表沒有選擇 //
    private boolean isPhotoSelected() {
        return !m_sPicPath.isEmpty();
    }

    // AccountManager 只有在 Sync Api 後才會被 update, 所以若是 EditText' value 相同的話, 就代表 user 沒有更新名稱。 //
    private boolean isNameChanged() {
        return !AccountManager.getAccountModel().getUserName().trim().equals(m_etName.getText().toString().trim());
    }

}
