package com.hiroia.jimmy.view.mode_t;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.hiroia.jimmy.R;
import com.hiroia.jimmy.comp.view.layout.RatingBarView;
import com.hiroia.jimmy.db.sql.JRecordDBA;
import com.hiroia.jimmy.enums.HttpDef;
import com.hiroia.jimmy.manager.AccountManager;
import com.hiroia.jimmy.manager.JRecordTModelManager;
import com.hiroia.jimmy.manager.net.NetworkManager;
import com.hiroia.jimmy.model.record.ModelJRecord;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.library.android_common.component.common.Opt;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.DateUtil;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.json.JSONObject;

/**
 * Created by Bruce on 2018/11/1.
 */
public class TModeFinishedFragment_5 extends BaseFragment implements View.OnClickListener {

    // Final Member //
    public static final double sm_discountScore = 1;
    public static Lst<Integer> sm_scoreRules = Lst.of(801, 3001, 4501, 7501);

    // View //
    private TextView m_tvTotalScore;
    private Button m_btnTryAgain, m_btnLeaveTmode;
    private RatingBarView m_ratingBarView;
    private LinearLayout m_llvBackIcon;

    // Comp //
    private long m_id = 0;

    //---------------------------------------------------------
    // Fragment Life Cycle
    //---------------------------------------------------------
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_tmode_training_finished, null);

        //-----------------------------------------------------------------
        // init view
        m_btnLeaveTmode = rootView.findViewById(R.id.fragment_tmode_training_finished_leave_btn);
        m_llvBackIcon = rootView.findViewById(R.id.fragment_tmode_training_back_icon_llv);
        m_tvTotalScore = rootView.findViewById(R.id.fragment_tmode_training_finished_totalscore_tv);
        m_btnTryAgain = rootView.findViewById(R.id.fragment_tmode_training_finished_try_again_btn);
        m_ratingBarView = rootView.findViewById(R.id.fragment_tmode_star_rating_bar_view);

        //-----------------------------------------------------------------
        // set data
        m_id = DateUtil.timeStamp(); // set current time stamp.

        m_btnTryAgain.setOnClickListener(this);
        m_llvBackIcon.setOnClickListener(this);
        m_btnLeaveTmode.setOnClickListener(this);

        m_ratingBarView.setClickable(false);
        m_ratingBarView.setEnabled(false);
        m_tvTotalScore.setText(String.valueOf(getParentActivity().getTotalScore()));

        setStarRatingBar();

        // sync data //
        uploadRecipeToCloud(); // upload recipe to cloud.

        //-----------------------------------------------------------------
        // println
        getParentActivity().getRecordData().println_d(TModeFinishedFragment_5.class.getSimpleName());
        getParentActivity().getTrainingData().println_d(TModeFinishedFragment_5.class.getSimpleName());

        return rootView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_tmode_training_finished_try_again_btn:
                getParentActivity().switchStateView(TModeActivity.TModeState.FLOW_RATE_PAGE); // Switch To 選擇 Flow Rate 的頁面
                break;

            case R.id.fragment_tmode_training_back_icon_llv:
                getParentActivity().switchStateView(TModeActivity.TModeState.TRAINING_MODE);
                getParentActivity().refresh();
                break;

            case R.id.fragment_tmode_training_finished_leave_btn:
                getParentActivity().resetFragment(); // 更新所有 Fragment
                getParentActivity().switchStateView(TModeActivity.TModeState.TRAINING_MODE);
                getParentActivity().refresh();
                break;
        }
    }

    //---------------------------------------------------------
    // Private Preference
    //---------------------------------------------------------

    private TModeActivity getParentActivity() {
        return TModeActivity.getInstance();
    }

    // 上傳 Recipe to cloud //
    private void uploadRecipeToCloud() {

        // 離線狀態不執行 //
        if (!NetworkManager.isConnected()) {
            NetworkManager.println_offLine_e(TModeFinishedFragment_5.class.getSimpleName());
            insertJRecordToDB(getParentActivity().getModelJRecord()); // 這裡的 insert to db 是沒有跟 cloud sync 過的。
            return;
        }

        // Show Progress //
        getParentActivity().showProgressDialog();

        // Start To Upload Recipe //
        ModelJRecord m = getParentActivity().getModelJRecord();
        HttpDef.CREATE_J_RECORD.post()
                .addParam("token", AccountManager.getAccountModel().getToken())
                .addParam("water", m.getRecordData().getWater() + "")
                .addParam("jscale_id", "0")
                .addParam("jrecipe_id", "0")
                .addParam("score", m.getScore() + "")
                .addParam("mode", m.getMode().id() + "")
                .addParam("record_time", m.getCreated_time().formatISO8601())
                .addParam("time1", StrUtil.ZERO)
                .addParam("time2", StrUtil.ZERO)
                .addParam("training_data", m.getTrainingData().datas())
                .addParam("record_data", m.getRecordData().datas())
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject jsObj) {
                        if (response.isEmpty() || response == null) {
                            LogUtil.e(TModeFinishedFragment_5.class, HttpDef.CREATE_J_RECORD.getTag() + " response == null or Empty");

                            // dismiss progress dialog //
                            getParentActivity().dismissProgressDialog();
                            insertJRecordToDB(getParentActivity().getModelJRecord()); // 這裡的 insert to db 是沒有跟 cloud sync 過的。

                            return;
                        }

                        // decode JSON //
                        jsObj = Opt.ofJson(jsObj, "result").toJsonObj().get();
                        ModelJRecord m = ModelJRecord.decodeJSON(jsObj);
                        insertJRecordToDB(m); // Insert JRecord to db.

                        // dismiss progress dialog //
                        LogUtil.d(TModeFinishedFragment_5.class, HttpDef.CREATE_J_RECORD.getTag() + " response = " + response);
                        getParentActivity().dismissProgressDialog();

                    }
                });
    }

    // 將 JRecord data insert to DB //
    public void insertJRecordToDB(ModelJRecord m) {

        // Create Model //
        m.getMode();
        m.setId(m_id);
        m.println(TModeFinishedFragment_5.class.getSimpleName() + " [ ModelJRecord ] : ");

        // Open DB //
        JRecordDBA db = JRecordDBA.open(getParentActivity());
        db.insert(m);

        // Sync With Data Manager //
        JRecordTModelManager.addModel(m);
    }

    // 設定 Star 數 //
    private void setStarRatingBar() {
        // 設定 score rating star //
        m_ratingBarView.setStar(getParentActivity().getModelJRecord().getStars(), false);
    }

//    // 將分數 insert to db //
//    private void insertScoreToDB() {
//
//        // Create Model //
//        ModelTModeRecord m = new ModelTModeRecord();
//        m.setId(m_id);
//        m.setMode(getParentActivity().getMode().mode());
//        m.setStars(m.getStars());
//        m.setYmd(YMD.nowYMD());
//        m.setScore(getParentActivity().getTotalScore());
//        m.println(TModeFinishedFragment_5.class.getSimpleName());
//
//        // Sync With Data Manager //
//        _TModelDataManager.add(m);
//
//        // 連線狀態下不 insert to db //
//        if (NetworkManager.isConnected())
//            return;
//
//        // open db //
//        TModeRecordDBA db = TModeRecordDBA.open(getParentActivity());
//        db.insert(m);
//    }


//    // 取得星星數 //
//    private int getStars() {
//
//        int score = getParentActivity().getTotalScore();
//        int stars = 5;
//
//        for (int i = 0; i < sm_scoreRules.size(); i++) {
//            if (score < sm_scoreRules.get(i)) {
//                stars = i + 1;
//                break;
//            }
//        }
//        return stars;
//    }


    //--------------------------------------------------------------------------------------------------
//    // todo finish : 將每秒水量 insert to db //
//    private void insertWaterVolToDB() {
//
//        // 1. id -> 兩個 Table
//        // 2. 根據 A table 的 id query b table 對應的 id table TModeChartDBA 的 getStepsById();
//
//        TModeChartDBA dba = TModeChartDBA.open(getParentActivity());
//        WaterVolStep wvs = new WaterVolStep();
//        wvs.setFakeData(); // 假資料
////        Lst<Pair<Integer, Integer>> steps = wvs.getSteps();
////        Lst<Pair<Integer, Integer>> steps = getParentActivity().getWaterVolSteps().getSteps();
//        Lst<Pair<Integer, Integer>> steps = getParentActivity().getRecordData().values();
//
////        long id = DateUtil.timeStamp();
//        for (int i = 0; i < steps.size(); i++) {
//            dba.insert(m_id, steps.get(i).k(), steps.get(i).v()); // 作 insert DB
//        }
//
//        //-------------------------------------------------------
//        // logo
//        WaterVolStep step = dba.getStepsById(m_id);
//        for (int i = 0; i < 30; i++) {
//            LogUtil.d(this.getClass(), "id = " + m_id + " sec = " + step.getSteps().get(i).k() + ", vol = " + step.getSteps().get(i).v());
//        }
//
//    }


    // TODO Recover : 底下是舊的算法，暫時保留。因為後續可能會用到 check by Ray.
//    // 取得最大 Score //
//    private int getMaxScore() {
//        int basicVal = TModeFlowRateFragment_3.ScoreState.EXCELLENT.score() * getParentActivity().getTimeSec(); // Excellent 的基本分數
//        int comboVal = TModeActivity.COMBO_EXCELLENT.total() * (getParentActivity().getTimeSec() / 10); // Excellent 的 combo 分數的總分
//        int max = new Double((basicVal + comboVal * sm_discountScore)).intValue();
//        LogUtil.d(TModeFinishedFragment_5.class, "max = " + max + " basicVal = " + basicVal + " comboVal = " + comboVal);
//        return max;
//    }
//
//    // 取得星星數 //
//    private int getStars() {
//
//        int rangeIndex = 0; // index of star range
//        int partSize = 12; // partition size
////        Lst<Integer> ratio = Lst.of(1, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5); // 根據 index 給予的星數
//        Lst<Integer> ratio = Lst.of(1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 5, 5); // 根據 index 給予的星數
//
//        // 設定星級 range //
//        Lst<Lst<Integer>> ranges =
//                Lst.accumulateOf(getMaxScore())
//                .partition(partSize)
//                .reSize(partSize);
//        LogUtil.d(TModeFinishedFragment_5.class, "ranges = " + ranges);
//
//        //  range index //
//        for (int i = 0; i < ranges.size(); i++) {
//            rangeIndex = i;
//            if (ranges.get(i).contains(getParentActivity().getTotalScore()))
//                break;
//        }
//
//        return ratio.get(rangeIndex);
//    }

}
