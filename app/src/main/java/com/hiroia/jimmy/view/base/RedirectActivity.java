package com.hiroia.jimmy.view.base;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.hiroia.jimmy.comp.cs.HiroiaCs;
import com.hiroia.jimmy.comp.cs.SamanthaCs;
import com.library.android_common.enums.HttpDef;
import com.library.android_common.util.StrUtil;

/**
 * Create By Ray on 2019/1/21
 * 重新導向的 Activity
 */
public class RedirectActivity extends BaseActivity {


    //--------------------------------------------------------------
    // OnCreate
    //--------------------------------------------------------------
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        switchToAnotherApp();
    }

    private void switchToAnotherApp() {

//        getActManager()
//                .finish()
//                .retrieveAnotherApp(SamanthaCs.MAIN_PACKAGE, SamanthaCs.JIMMY_OTA_REDIRECT_PACKAGE);

//        boolean hasOTA = getActManager()
//                .retrieveToAnotherApp(SamanthaCs.MAIN_PACKAGE, SamanthaCs.JIMMY_OTA_REDIRECT_PACKAGE, Intent.ACTION_VIEW, StrUtil.EMPTY);

//        // 如果有就跳轉 //
//        if (hasOTA)
//            getActManager().finish();
//        else { // 如果沒有就導向 google play store app //
//            Intent it = new Intent(Intent.ACTION_VIEW, Uri.parse(HttpDef.PLAY_STORE_SAMANTHA_URI.notTest().getUrl()));
//            startActivity(it);
//            finish(); // 導向 google play 後強制關閉 App
//        }
    }


}
