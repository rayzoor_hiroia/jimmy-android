package com.hiroia.jimmy.view.mode_t;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.comp.comp.RecordData;
import com.hiroia.jimmy.comp.comp.TrainingData;
import com.hiroia.jimmy.comp.view.dialog.IOSAlertDialog;
import com.hiroia.jimmy.model.ModelTModeRecord;
import com.hiroia.jimmy.model.record.ModelJRecord;
import com.hiroia.jimmy.view.base.BaseActivity;
import com.hiroia.jimmy.view.mode_t.record.TModeRecordChartFragment_2;
import com.hiroia.jimmy.view.mode_t.record.TModeRulesRecordFragment_1;
import com.hiroia.jimmy.view.mode_t.rules.TModeTrainingRulesFragment;
import com.hiroia.jimmy.view.mode_t.test._TModeResultChartFragment;
import com.library.android_common.component.TimeCounter;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.YMDHMS;

import java.util.ArrayList;

import static com.hiroia.jimmy.view.mode_t.TModeActivity.TModeState.FLOW_RATE_PAGE;
import static com.hiroia.jimmy.view.mode_t.TModeActivity.TModeState.TRAINING_MODE;
import static com.hiroia.jimmy.view.mode_t.TModeActivity.TModeState.VELOCITY_MODE;

/**
 * Created by Bruce on 2018/11/1.
 */
public class TModeActivity extends BaseActivity {

    // TMode Game 的最大 combo //
    public static final Lst<Integer> COMBO_EXCELLENT = Lst.of(11, 15, 42, 75, 124, 200, 462, 766, 1024);
    public static final Lst<Integer> COMBO_GOOD = Lst.of(5, 8, 20, 31, 56, 72, 113, 200, 522);

    // Comp //
    private static TModeActivity sm_self;
    private RecordData m_recordData = new RecordData();
    private TrainingData m_trainingData = new TrainingData();
    private ModelJRecord m_modelJRecord = new ModelJRecord();

    private long m_recordId = -1;
    private Mode m_currMode = Mode.SINGLE;
    private YMDHMS m_ymdhms = YMDHMS.nowYMDHMS();
    private int m_nFlowRateVol = 0;
    private double m_dFlowRateVol = 0.0;
    private int m_nModeSec = 60;
    private int m_nTotalScore = 0;
    private int m_nRandomPoursCount = 1; //  Random 的 Pours
    private boolean m_bIsTrainingBaristaGoTo = false;
    private boolean m_bIsRulesRecordGoTo = false;
    private ModelTModeRecord m_currentTModeRecord = null;
    private ArrayList<Integer> m_rateVolList = new ArrayList<>();

    // Fragments //
    private TModeFinishedFragment_5 m_finishedFragment;
    private TModeFlowRateFragment_3 m_flowRateFragment;
    private TModeResultChartFragment_4 m_resultChartFragment;
    private TModeVelocityFragment_2 m_velocityFragment;
    private TModeTrainingBaristaFragment_1 m_trainingFragment;
    private TModeRulesRecordFragment_1 m_rulesRecordFragment;
    private TModeTrainingRulesFragment m_trainingRulesFragment;
    private TModeRecordChartFragment_2 m_recordChartFragment;
    private TModeState m_currFragState = TModeState.TRAINING_MODE;

    // Test Fragment //
    private _TModeResultChartFragment m_testResultChartFragment;

    //-------------------------------------------------------------
    // Activity Life Cycle
    //-------------------------------------------------------------

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tmode, true);
        sm_self = this;

        //------------------------------------------
        // init view
        init();

        //------------------------------------------
        // set view
        switchStateView(TRAINING_MODE);
//        switchStateView(RESULT_CHART);
//        switchStateView(VELOCITY_MODE);
//        switchStateView(FINISHED_PAGE);

        //------------------------------------------
        // test
//        switchStateView(_TEST_FLOW_RATE);
    }

    @Override
    public void onBackPressed() {
        if (m_currFragState != FLOW_RATE_PAGE)
            super.onBackPressed();
        else {
            // 當 Flow Rate Fragment 在遊戲中時，不可以立即關閉 需要 time out //
//            showProgressDialog("Stopping !!");
//            m_flowRateFragment.stopGaming();
//            new TimeCounter().setOnCountingListener(1000, new TimeCounter.OnCountingListener() {
//                @Override
//                public void onTick(long m) {
//
//                }
//
//                @Override
//                public void onFinish() {
//                    TModeActivity.super.onBackPressed();
//                    dismissProgressDialog();
//                }
//            });

            // 當 Flow Rate Fragment 在遊戲中時，不可以立即關閉 需要 time out //
//            final IOSAlertDialog dialog = new IOSAlertDialog(this, "提醒您", "你即將結束訓練模式，確定離開？", "離開", "取消"); // 改多國語言
            final IOSAlertDialog dialog = new IOSAlertDialog(this, "Confirm Navigation", "Are you sure to leave Barista Training Mode?", "Stay", "Leave"); // 改多國語言
            dialog.show(new IOSAlertDialog.OnClickEvent() {
                @Override
                public void confirm() {
                    m_flowRateFragment.stopGaming();
                    new TimeCounter().setOnCountingListener(1000, new TimeCounter.OnCountingListener() {
                        @Override
                        public void onTick(long m) {

                        }

                        @Override
                        public void onFinish() {
                            switchFragment(m_velocityFragment);
                            m_currFragState = VELOCITY_MODE;
                        }
                    });

                    dialog.dismiss();
                }

                @Override
                public void cancel() {
                    dialog.dismiss();
                }
            });
        }
    }

    //-------------------------------------------------------------
    // Private Preference
    //-------------------------------------------------------------
    private void switchFragment(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_tmode_flt, fragment)
                .commit();
    }

    //-------------------------------------------------------------
    // Public Method
    //-------------------------------------------------------------

    public void refresh() {
        m_currMode = Mode.SINGLE;
        m_nFlowRateVol = 0;
        m_dFlowRateVol = 0.0;
        m_nModeSec = 60;
        m_nTotalScore = 0;
        m_nRandomPoursCount = 1; //  Random 的 Pours
        m_bIsTrainingBaristaGoTo = false;
        m_bIsRulesRecordGoTo = false;
        m_currentTModeRecord = null;
        m_rateVolList = new ArrayList<>();
    }

    // Set Random Size //
    public void setRandomPoursCount(int count) {
        m_nRandomPoursCount = count;
    }

    // Get Random Size //
    public int getRandomPoursCount() {
        return m_nRandomPoursCount;
    }

    // 取得 Mode
    public Mode getMode() {
        return m_currMode;
    }

    // 設定 Mode //
    public void setTMode(Mode m) {
        m_currMode = m;
        m_modelJRecord.setMode(m.mode() == 0 ? ModelJRecord.JScaleMode.TS_MODE : ModelJRecord.JScaleMode.TR_MODE);
    }

    // 設定 TrainingData //
    public void setTrainingData(TrainingData data) {
        m_trainingData = data;
        m_modelJRecord.setTrainingData(data);
    }

    // 取得 TrainingData //
    public TrainingData getTrainingData() {
        return m_trainingData;
    }

    // 設定 RecordData //
    public void setRecordData(RecordData data) {
        m_recordData = data;
        m_modelJRecord.setRecordData(data);
    }

    // 取得 RecordData //
    public RecordData getRecordData() {
        return m_recordData;
    }

    // 設定 TMode Record 的 ID, 用在 Fragment 切頁後跟 Manager 拿 Recipe  //
    public void setTRecordId(long id){
        m_recordId = id;
    }

    // 取得 record id //
    public long getTRecordId(){
        return m_recordId;
    }

    // 設定 Training Flow Rate //
    public void setFlowRate(int rate) {
        m_nFlowRateVol = rate;
    }

    // 設定 Training Double Flow Rate //
    public void setDFlowRate(double rate) {
        m_dFlowRateVol = rate;
    }

    // 取得 Training Flow Rate //
    public int getFlowRateVol() {
        return m_nFlowRateVol;
    }

    // 取得 Training Double Flow Rate //
    public Double getDFlowRate() {
        return m_dFlowRateVol;
    }

    // 設定 練習時間 Mode。 1m、2m、3m //
    public void setTimeSec(int sec) {
        m_nModeSec = sec;
    }

    // 取得 Mode 時間 //
    public int getTimeSec() {
        return m_nModeSec;
    }

    // 取得 Mode Ms 時間 //
    public long getTimeMillySec() {
        return (getTimeSec() * 1000);
    }

    // 設定遊戲總分 //
    public void setTotalScore(int score) {
        m_nTotalScore = score;
        m_modelJRecord.setScore(score);
    }

    // 取得遊戲總分 //
    public int getTotalScore() {
        return m_nTotalScore;
    }

    // 取得現在時間 //
    public YMDHMS getNowYMDHMS(){
        return m_ymdhms;
    }

    // 設定現在時間 //
    public void setYMDHMS(YMDHMS ymdhms){
        m_ymdhms = ymdhms;
        m_modelJRecord.setCreated_time(ymdhms);
    }

    // 取得 Model Record //
    public ModelJRecord getModelJRecord() {
        return m_modelJRecord;
    }

    public ArrayList<Integer> getRateVolList() {
        return m_rateVolList;
    }

    public void addRateVol(int vol) {
        m_rateVolList.add(vol);
    }

    public void clearRateVol() {
        m_rateVolList.clear();
    }

    public boolean getIsTrainingBaristaGoTo() {
        return m_bIsTrainingBaristaGoTo;
    }

    public void setIsTrainingBaristaGoTo(boolean isTrainingBaristaGoTo) {
        this.m_bIsTrainingBaristaGoTo = isTrainingBaristaGoTo;
    }

    public boolean getIsRulesRecordGoTo() {
        return m_bIsRulesRecordGoTo;
    }

    public void setIsRulesRecordGoTo(boolean isRulesRecordGoTo) {
        this.m_bIsRulesRecordGoTo = isRulesRecordGoTo;
    }

    public ModelTModeRecord getCurrentTModeRecord() {
        return m_currentTModeRecord;
    }

    public void setCurrentTModeRecord(ModelTModeRecord m) {
        this.m_currentTModeRecord = m;
    }

    // 切頁用途 暫時的做法 //
    public TModeTrainingBaristaFragment_1 getTModeTrainigBaristaFragment() {
        return m_trainingFragment;
    }

    // Get Instance //
    public static TModeActivity getInstance() {
        return sm_self;
    }

    // Reset All Fragment Status //
    public void resetFragment() {
        init();
    }

    // Switch Fragment //
    public void switchStateView(TModeActivity.TModeState state) {
        m_currFragState = state;
        switch (state) {
            case FINISHED_PAGE:
                switchFragment(m_finishedFragment);
                break;
            case FLOW_RATE_PAGE:
                m_flowRateFragment = new TModeFlowRateFragment_3();
                switchFragment(m_flowRateFragment); // T mode Gaming 每次進去都要 refresh
                break;
            case RESULT_CHART:
                switchFragment(m_resultChartFragment);
                break;
            case VELOCITY_MODE:
                switchFragment(m_velocityFragment);
                break;
            case TRAINING_MODE:
                switchFragment(m_trainingFragment);
                break;
            case RULE_RECORD:
                switchFragment(m_rulesRecordFragment);
                break;
            case TRAINING_RESULT_PAGE:
                switchFragment(m_trainingRulesFragment);
                break;
            case _TEST_FLOW_RATE:
                switchFragment(m_testResultChartFragment);
                break;
            case RECORD_CHART_VIEW:
                switchFragment(m_recordChartFragment);
                break;
        }
    }

    //-------------------------------------------------------------
    // Private Preference
    //-------------------------------------------------------------
    private void init() {
        m_finishedFragment = new TModeFinishedFragment_5();
        m_flowRateFragment = new TModeFlowRateFragment_3();
        m_resultChartFragment = new TModeResultChartFragment_4();
        m_velocityFragment = new TModeVelocityFragment_2();
        m_trainingFragment = new TModeTrainingBaristaFragment_1();
        m_rulesRecordFragment = new TModeRulesRecordFragment_1();
        m_trainingRulesFragment = new TModeTrainingRulesFragment();
        m_recordChartFragment = new TModeRecordChartFragment_2();

        // test fragment //
        m_testResultChartFragment = new _TModeResultChartFragment();
    }

    //-------------------------------------------------------------
    // Enum
    //-------------------------------------------------------------
    public enum TModeState {
        FINISHED_PAGE, FLOW_RATE_PAGE, RESULT_CHART, VELOCITY_MODE, TRAINING_MODE, RULE_RECORD, TRAINING_RESULT_PAGE, _TEST_FLOW_RATE, RECORD_CHART_VIEW
    }

    public enum Mode {
        SINGLE("Single Flow Rate", 0),
        RANDOM("Random Flow Rate", 1);
        //--------------------------------------------------------
        private String m_msg;
        private int m_mode;

        Mode(String msg, int mode) {
            m_msg = msg;
            m_mode = mode;
        }

        public String getTitle() {
            return m_msg;
        }

        public int mode() {
            return m_mode;
        }

    }

}
