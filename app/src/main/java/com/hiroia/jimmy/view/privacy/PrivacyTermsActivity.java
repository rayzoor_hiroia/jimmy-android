package com.hiroia.jimmy.view.privacy;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.comp.view.viewpager.CustomViewPager;
import com.hiroia.jimmy.view.base.BaseActivity;
import com.library.android_common.util.LogUtil;

/**
 * Created by Bruce on 2019/2/13.
 */
public class PrivacyTermsActivity extends BaseActivity {

    private TabLayout m_Tabs = null;
    private CustomViewPager m_viewPager = null;

    private PrivacyFragment m_privacyFragment = new PrivacyFragment();
    private TermsFragment m_termsFragment = new TermsFragment();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_terms);
        init();
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    private void init() {
        m_Tabs = findViewById(R.id.activity_privacy_terms_tabs);
        m_viewPager = findViewById(R.id.activity_privacy_terms_view_pager);

        m_privacyFragment = new PrivacyFragment();
        m_termsFragment = new TermsFragment();


        // listener //
        m_viewPager.setPagingEnabled(false);
        m_viewPager.setAdapter(pagerAdapter());
        onTabChangeListener();
    }

    private FragmentPagerAdapter pagerAdapter() {
        final int TAB_PRIVACY = 0;
        final int TAB_TERMS = 1;

        FragmentPagerAdapter adapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case TAB_PRIVACY:
                        return m_privacyFragment;

                    case TAB_TERMS:
                        return m_termsFragment;

                    default:
                        return null;
                }
            }

            @Override
            public int getCount() {
                return m_Tabs.getTabCount();
            }
        };

        return adapter;
    }


    private void onTabChangeListener() {

        m_viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                m_Tabs.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // tab layout //
        m_Tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                LogUtil.d(PrivacyTermsActivity.class, "tab = " + tab.getPosition());
                m_viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
