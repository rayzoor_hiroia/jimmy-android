package com.hiroia.jimmy.view.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.cs.HttpCs;
import com.hiroia.jimmy.enums.HttpDef;
import com.hiroia.jimmy.manager.AccountManager;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.library.android_common.component.common.Opt;
import com.library.android_common.component.common.Pair;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.hms.S;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.json.JSONObject;

/**
 * Created by Bruce on 2019/1/30.
 */
public class ChangePasswordFragment extends BaseFragment implements View.OnClickListener {

    private LinearLayout m_llvBack, m_llvSave;
    private TextView m_tvSave, m_tvTitle, m_tvError;
    private EditText m_etOldPassword, m_etNewPassword, m_etConfirmPassword;

    private final String CHANGE_PASSWORD_SUCCESS = "true";
    private final String CHANGE_PASSWORD_FAIL = "false";
    private final int NEW_PASSWORD = 1;
    private final int CONFIRM_PASSWORD = 2;
    private final int PASSWORD_SIZE = 6;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_change_password, null);

        //-----------------------------------------------------------------
        // init
        m_llvBack = rootView.findViewById(R.id.fragment_change_password_back_llv);
        m_llvSave = rootView.findViewById(R.id.fragment_change_password_save_llv);

        m_tvSave = rootView.findViewById(R.id.fragment_change_password_save_tv);
        m_tvTitle = rootView.findViewById(R.id.fragment_change_password_title_tv);
        m_tvError = rootView.findViewById(R.id.fragment_change_password_error_text_tv);

        m_etOldPassword = rootView.findViewById(R.id.fragment_change_password_old_password_et);
        m_etNewPassword = rootView.findViewById(R.id.fragment_change_password_new_password_et);
        m_etConfirmPassword = rootView.findViewById(R.id.fragment_change_password_confirm_password_et);

        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);
        m_llvSave.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_change_password_back_llv:
                getParentActivity().switchFragment(SettingActivity.SettingState.PROFILE_EDIT);
                clearData();
                break;

            case R.id.fragment_change_password_save_llv:
                checkIsNotEmpty();
                break;
        }
    }


    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private SettingActivity getParentActivity() {
        return SettingActivity.getInstance();
    }

    private void clearData() {
        m_etOldPassword.setText(StrUtil.EMPTY);
        m_etNewPassword.setText(StrUtil.EMPTY);
        m_etConfirmPassword.setText(StrUtil.EMPTY);
        m_tvError.setText(StrUtil.EMPTY);
    }

    private boolean checkIsNotEmpty() {

        Lst<Pair<String, String >> ps = Lst.of(
                Pair.of(m_etOldPassword.getText().toString().trim(), "Please enter your original password."),
                Pair.of(m_etNewPassword.getText().toString().trim(), "Please enter your new password."),
                Pair.of(m_etConfirmPassword.getText().toString().trim(), "Please enter new password again."));

        for (int i = 0; i < ps.size(); i ++){

            // Declare //
            Pair<String, String> p = ps.get(i);

            // Check if is empty //
            if (p.k().isEmpty()) {
                m_tvError.setText(p.v());
                return false;
            }

            // Check new password size //
            if (i == 1 && p.k().length() < PASSWORD_SIZE) {
                m_tvError.setText("Password must be 6 or more characters");
                return false;
            }

            // Check password confirm is equal with new password //
            if (i == 2 && !p.k().equals(ps.get(1).k())){
                m_tvError.setText("Incorrect input please check the confirm password.");
                return false;
            }
        }

        // Show Dialog //
        getParentActivity().showProgressDialog();

        // Start to update password //
        changePassword();


        //---------------------------------------------------------------- old //

//        String sOldPassword = m_etOldPassword.getText().toString().trim();
//        String sNewPassword = m_etNewPassword.getText().toString().trim();
//        String sConfirmPassword = m_etConfirmPassword.getText().toString().trim();
//
//        if (sOldPassword.isEmpty()) {
//            m_tvError.setText("Please enter original password"); // TODO 多國語言
//            return false;
//        }
//
//        if (sNewPassword.isEmpty()) {
//            m_tvError.setText("Please enter new password"); // TODO 多國語言
//            return false;
//        }
//
//        if (sConfirmPassword.isEmpty()) {
//            m_tvError.setText("Please enter new password again"); // TODO 多國語言
//            return false;
//        }
//
//        if (sNewPassword.length() < PASSWORD_SIZE) {
//            m_tvError.setText("Password must be 6 or more characters"); // TODO 多國語言
//            return false;
//        }
//
//        if (!sNewPassword.equals(sConfirmPassword)) {
//            m_tvError.setText("incorrect password"); // TODO 多國語言
//            return false;
//        } else {
//            getParentActivity().showProgressDialog();
//            changePassword(); // Call Change Password API
//        }

        return true;
    }


    private void changePassword() {

        String oldPassword = m_etOldPassword.getText().toString().trim();
        String newPassword = m_etNewPassword.getText().toString().trim();
        String confirmPassword = m_etConfirmPassword.getText().toString().trim();

        HttpDef.CHANGE_PASSWORD.post()
                .addParam(HttpCs.TOKEN, AccountManager.getAccountModel().getToken())
                .addParam(HttpCs.OLD_PASSWORD, oldPassword)
                .addParam(HttpCs.NEW_PASSWORD, newPassword)
                .addParam(HttpCs.NEW_PASSWORD_CONFIRMATION, confirmPassword)
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject jsObj) {
                        if (response == null || response.isEmpty()){
                            LogUtil.e(ChangePasswordFragment.class, HttpDef.CHANGE_PASSWORD.getTag() +  " response is Null or Empty.");
                            return;
                        }

                        // Start to decode //
                        decodeChangePasswordJSON(jsObj);
                    }
                });
    }

    private void decodeChangePasswordJSON(JSONObject jsObj) {
        String succ = Opt.ofJson(jsObj, HttpCs.SUCCESS).get();

        switch (succ) {

            case CHANGE_PASSWORD_SUCCESS:
                clearData();
                getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                getParentActivity().showToast("Your password has been reset successfully.");

                // 更新成功後回到上一頁 //
                getParentActivity().switchFragment(SettingActivity.SettingState.SETTING_PAGE);
                break;

            case CHANGE_PASSWORD_FAIL:
                getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                getParentActivity().showToast("Password error.");
                break;
        }
    }

}
