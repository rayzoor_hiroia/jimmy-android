package com.hiroia.jimmy.view.login;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy._test._ActionCallBack;
import com.hiroia.jimmy.comp.view.dialog.IOSAlertDialog;
import com.hiroia.jimmy.cs.HttpCs;
import com.hiroia.jimmy.enums.HttpDef;
import com.hiroia.jimmy.model.ModelAccount;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.library.android_common.component.common.Opt;
import com.library.android_common.component.common.Pair;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.hms.S;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

/**
 * Create By Ray on 2018/10/26
 */
public class SignUpFragment extends BaseFragment implements View.OnClickListener {


    private LinearLayout m_llvCancel, m_llvPhotoAndName, m_llvEmail, m_llvPassword;
    private TextView m_tvTitle, m_tvSubTitle;
    private ImageView m_imgPhoto;
    private EditText m_etName, m_etEmail, m_etPassword, m_etConfirmPassword;
    private Button m_btnNext;

    private final String REGISTER_SUCCESS = "true";
    private final String REGISTER_FAIL = "false";
    private String m_sPicPath = StrUtil.EMPTY;
    private final int PASSWORD_SIZE = 6;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-------------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_signup, container, false);

        //-------------------------------------------------------------------
        // init view
        m_llvCancel = rootView.findViewById(R.id.fragment_signup_cancel_btn_llv);
        m_llvPhotoAndName = rootView.findViewById(R.id.fragment_signup_setting_portrait_name_llv);
        m_llvEmail = rootView.findViewById(R.id.fragment_signup_setting_email_llv);
        m_llvPassword = rootView.findViewById(R.id.fragment_signup_setting_password_llv);

        m_tvTitle = rootView.findViewById(R.id.fragment_signup_title_tv);
        m_tvSubTitle = rootView.findViewById(R.id.fragment_signup_sub_title_tv);

        m_imgPhoto = rootView.findViewById(R.id.fragment_signup_portrait_img);

        m_etName = rootView.findViewById(R.id.fragment_signup_name_et);
        m_etEmail = rootView.findViewById(R.id.fragment_signup_email_et);
        m_etPassword = rootView.findViewById(R.id.fragment_signup_password_et);
        m_etConfirmPassword = rootView.findViewById(R.id.fragment_signup_confirm_password_et);

        m_btnNext = rootView.findViewById(R.id.fragment_signup_next_btn);

        //-------------------------------------------------------------------
        // set data
        m_llvCancel.setOnClickListener(this);
        m_imgPhoto.setOnClickListener(this);
        m_btnNext.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_signup_cancel_btn_llv: // Cancel
                final IOSAlertDialog alert = new IOSAlertDialog(getParentActivity(), "Confirm Navigation", "Are your sure to leave register page.？", "Leave", "Stay"); //TODO 改多國語言
                alert.show(new IOSAlertDialog.OnClickEvent() {
                    @Override
                    public void confirm() {
                        clearData();
                        getParentActivity().switchFragment(LoginActivity.LogInStep.LOG_IN_STEP1);
                        alert.dismiss();
                    }

                    @Override
                    public void cancel() {
                        alert.dismiss();
                    }
                });
                break;

            case R.id.fragment_signup_portrait_img: // Photo img
                getParentActivity().pickImage(new _ActionCallBack() {
                    @Override
                    public void action(String path) {
                        LogUtil.d(SignUpFragment.class, " Upload Profile Pic Path : " + path);
                        m_sPicPath = path;
                        m_imgPhoto.setImageBitmap(BitmapFactory.decodeFile(path));
                    }
                });
                break;

            case R.id.fragment_signup_next_btn: // Next btn
                if (checkIsNotEmpty()) break;
                break;
        }
    }

    //---------------------------------------------------------------
    // Public Method
    //---------------------------------------------------------------

    public LoginActivity getParentActivity() {
        return LoginActivity.getInstance();
    }

    //---------------------------------------------------------------
    // Private Method
    //---------------------------------------------------------------

    private boolean checkIsNotEmpty() {

        // 定義 key & value， 為一組２維 Array 的概念 //
        ArrayList<Pair<String, EditText>> textInput = Lst.of(
                Pair.of(HttpCs.NAME, m_etName),
                Pair.of(HttpCs.EMAIL, m_etEmail),
                Pair.of(HttpCs.PASSWORD, m_etPassword),
                Pair.of(HttpCs.PASSWORD_CONFIRMATION, m_etConfirmPassword)).toList();


        // 1. 用 for 迴圈 得出每項２維的 Index //
        // 2. 再針對每組 Index 作 Event handle //
        for (int i = 0; i < textInput.size(); i++) {
            String sKey = textInput.get(i).k();
            String sValue = textInput.get(i).v().getText().toString().trim();


            if (sValue.isEmpty()) {
//                getParentActivity().showToast(Lst.of("請輸入您的暱稱", "請輸入您的信箱", "請輸入您的密碼", "請再次輸入您的密碼").get(i)); // 改多國語言
                getParentActivity().showToast(Lst.of("Please enter your name.", "Please enter your email.", "Please enter your password.", "Please enter your password again.").get(i)); // 改多國語言
                return false;
            }


            switch (sKey) {
                case HttpCs.NAME:
                    m_tvTitle.setText("SignUp(2/3)"); // 改多國語言
                    m_tvSubTitle.setText("Setting Account(Email)"); // 改多國語言
                    m_llvPhotoAndName.setVisibility(View.GONE);
                    m_llvEmail.setVisibility(View.VISIBLE);
                    m_llvPassword.setVisibility(View.GONE);
                    break;

                case HttpCs.EMAIL:
                    if (!m_etEmail.getText().toString().trim().contains(StrUtil.AT)) {
//                        getParentActivity().showToast("請輸入正確的電子郵件格式"); // 改多國語言
                        getParentActivity().showToast("Email format error."); // 改多國語言

                        m_etEmail.setTextColor(Color.RED);
                        m_etEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.jimmy_edittext_under_line_red);
                        return false;
                    } else {

                        m_tvTitle.setText("SignUp(3/3)"); // 改多國語言
                        m_tvSubTitle.setText("Setting Password"); // 改多國語言
                        m_btnNext.setText("SignUp Free"); // 改多國語言

                        m_llvPhotoAndName.setVisibility(View.GONE);
                        m_llvEmail.setVisibility(View.GONE);
                        m_llvPassword.setVisibility(View.VISIBLE);

                    }
                    break;

                case HttpCs.PASSWORD:
                    if (sValue.length() < PASSWORD_SIZE) {
                        getParentActivity().showToast("密碼需要6個字元以上(英文與數字至少各一碼)"); // 改多國語言
                        m_etPassword.setTextColor(Color.RED);
                        m_etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.jimmy_edittext_under_line_red);
                        return false;
                    }
                    break;

                case HttpCs.PASSWORD_CONFIRMATION:
                    if (!m_etPassword.getText().toString().trim().equals(sValue)) {
                        getParentActivity().showToast("再次輸入的密碼不相符"); // 改多國語言
                        m_etConfirmPassword.setTextColor(Color.RED);
                        m_etConfirmPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.jimmy_edittext_under_line_red);
                        return false;
                    } else {
                        getParentActivity().showProgressDialog();
                        createAccount(); // call api
                    }
                    break;
            }
        }

        // --------------------------------------------------------------


        return true;
    }


    private void clearData() {
        m_etName.setText(StrUtil.EMPTY);
        m_etEmail.setText(StrUtil.EMPTY);
        m_etPassword.setText(StrUtil.EMPTY);
        m_etConfirmPassword.setText(StrUtil.EMPTY);
    }

    // New Create Account API //
    private void createAccount() {

        // Declare //
        String name = m_etName.getText().toString().trim();
        String email = m_etEmail.getText().toString().trim();
        String password = m_etPassword.getText().toString().trim();
        String confirmPassword = m_etConfirmPassword.getText().toString().trim();
        // Start to register account. //
        HttpDef.REGISTER_ACCOUNT.form()
                .addFormData(HttpCs.NAME, name)
                .addFormData(HttpCs.EMAIL, email)
                .addFormData(HttpCs.PASSWORD, password)
                .addFormData(HttpCs.PASSWORD_CONFIRMATION, confirmPassword)
                .addFormFileIf(!StrUtil.EMPTY.equals(m_sPicPath), HttpCs.PHOTO, new File(m_sPicPath))
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject jsObj) {

                        // To avoid response null or empty //
                        if (response == null || response.isEmpty()) {
                            LogUtil.e(SignUpFragment.class, HttpDef.REGISTER_ACCOUNT.getTag() + " response is Null or Empty.");
                            return;
                        }

                        // Log response state //
                        LogUtil.d(SignUpFragment.class, HttpDef.REGISTER_ACCOUNT.getTag() + " response = " + response);
                        getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);

                        // Start to decode JSON //
                        try {
                            decodeRegisterJSON(jsObj);
                        } catch (JSONException e) {
                            getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                            e.printStackTrace();
                        }

                    }
                });
        getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
    }

    private void decodeRegisterJSON(JSONObject jsObj) throws JSONException {
        String succ = Opt.ofJson(jsObj, HttpCs.SUCCESS).get();

        switch (succ) {
            case REGISTER_SUCCESS:
                getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                clearData();
                getParentActivity().showToast("Register successfully. Please check email to complete the registration within 24 hours."); // 改多國語言
                getParentActivity().switchFragment(LoginActivity.LogInStep.SUCCESS_STEP1);
                break;

            case REGISTER_FAIL:
                getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                clearData();
                ModelAccount m = ModelAccount.decodeRegisterFailJSON(jsObj);
                getParentActivity().showToast(m.getMessage()); // 待確認
                break;

            default:
                getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                getParentActivity().showToast("Error");
                break;
        }
    }

}
