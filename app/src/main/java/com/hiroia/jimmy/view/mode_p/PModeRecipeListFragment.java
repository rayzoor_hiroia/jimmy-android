package com.hiroia.jimmy.view.mode_p;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.model.unuse.ModelRecipeList;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.ymd.YMD;

import java.util.ArrayList;

/**
 * Create By Ray on 2018/10/29
 */

public class PModeRecipeListFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    // View //
    private LinearLayout m_llvBack, m_llvBrew, m_llvRecipeList, m_llvBlank;
    private TextView m_tvBrew, m_tvTitle, m_tvTitleSmall, m_tvSubTitle, m_tvBlankTxt;
    private ImageView m_imgBlank;
    private ListView m_lvRecipeList;

    private PModeRecipeListAdapter m_recipeListAdapter;

    private ArrayList m_testData = Lst.of(
            ModelRecipeList.of("Andy", YMD.nowYMD(), 20.5, "1:15",200, 85),
            ModelRecipeList.of("Tony", YMD.nowYMD(), 21.5, "1:15",300, 86),
            ModelRecipeList.of("Jack", YMD.nowYMD(), 22.5, "1:15",400, 87)
    ).toList();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_pmode_recipe_list, null);

        //-----------------------------------------------------------------
        // init
        m_llvBack = rootView.findViewById(R.id.fragment_pmode_recipe_list_back_llv);
        m_llvBrew = rootView.findViewById(R.id.fragment_pmode_recipe_list_brew_llv);
        m_llvRecipeList = rootView.findViewById(R.id.fragment_pmode_recipe_list_llv);
        m_llvBlank = rootView.findViewById(R.id.fragment_pmode_recipe_list_blank_llv);

        m_tvBrew = rootView.findViewById(R.id.fragment_pmode_recipe_list_brew_tv);
        m_tvTitle = rootView.findViewById(R.id.fragment_pmode_recipe_list_title_tv);
        m_tvTitleSmall = rootView.findViewById(R.id.fragment_pmode_recipe_list_title_small_tv);
        m_tvSubTitle = rootView.findViewById(R.id.fragment_pmode_recipe_list_sub_title_tv);
        m_tvBlankTxt = rootView.findViewById(R.id.fragment_pmode_recipe_list_blank_txt_tv);

        m_imgBlank = rootView.findViewById(R.id.fragment_pmode_recipe_list_blank_img);

        m_lvRecipeList = rootView.findViewById(R.id.fragment_pmode_recipe_list_lv);

        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);
        m_llvBrew.setOnClickListener(this);
        m_lvRecipeList.setOnItemClickListener(this);

        //-----------------------------------------------------------------
        // set Adapter
        m_recipeListAdapter = new PModeRecipeListAdapter(getParentActivity(), m_testData);
        m_lvRecipeList.setAdapter(m_recipeListAdapter);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_pmode_recipe_list_back_llv: // Back
                getParentActivity().finish();
                break;

            case R.id.fragment_pmode_recipe_list_brew_llv: // Brew
                getParentActivity().switchStateView(PModeActivity.PModeState.POUR_MODE);
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getParentActivity().switchStateView(PModeActivity.PModeState.RECIPE_CONTENT);
    }

    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private PModeActivity getParentActivity() {
        return PModeActivity.getInstance();
    }
}
