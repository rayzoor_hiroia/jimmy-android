package com.hiroia.jimmy.view.mode_t.rules;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.hiroia.jimmy.view.mode_t.TModeActivity;

import static com.hiroia.jimmy.view.mode_t.TModeActivity.TModeState.TRAINING_MODE;

/**
 * Created by Bruce on 2018/11/1.
 */
public class TModeTrainingRulesFragment extends BaseFragment implements View.OnClickListener {

    // View //
    private LinearLayout m_llvBack;
    private TextView m_tvTitle, m_tvSubTitle, m_tvRulesTxt;
    private Button m_btnOk;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_tmode_training_rules, null);

        //-----------------------------------------------------------------
        // init
        m_llvBack = rootView.findViewById(R.id.fragment_tmode_training_rules_back_llv);
        m_tvTitle = rootView.findViewById(R.id.fragment_tmode_training_rules_title_tv);
        m_tvSubTitle = rootView.findViewById(R.id.fragment_tmode_training_rules_sub_title_tv);
        m_tvRulesTxt = rootView.findViewById(R.id.fragment_tmode_training_rules_txt_tv);
        m_btnOk = rootView.findViewById(R.id.fragment_tmode_training_rules_ok_btn);

        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);
        m_btnOk.setOnClickListener(this);

        return rootView;
    }

    //------------------------------------------------------------
    // implement method
    //------------------------------------------------------------
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_tmode_training_rules_back_llv:
            case R.id.fragment_tmode_training_rules_ok_btn:
                getParentActivity().switchStateView(getParentActivity().getIsTrainingBaristaGoTo() ?
                        TModeActivity.TModeState.TRAINING_MODE :
                        TModeActivity.TModeState.VELOCITY_MODE);
                break;
        }
    }

    //------------------------------------------------------------
    // Private
    //------------------------------------------------------------
    private TModeActivity getParentActivity() {
        return TModeActivity.getInstance();
    }

}
