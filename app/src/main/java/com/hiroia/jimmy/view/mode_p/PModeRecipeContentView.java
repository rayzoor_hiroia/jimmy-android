package com.hiroia.jimmy.view.mode_p;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.model.unuse.ModeRecipeContent;
import com.library.android_common.component.common.Pair;

import java.util.ArrayList;

/**
 * Created by Bruce on 2018/12/11.
 */
public class PModeRecipeContentView {

    private Activity m_ctx;
    private ArrayList<View> m_recipeContent;


    public class ItemContentView {

        public ItemContentView(Activity activity, ModeRecipeContent model) {
            m_recipeContent = new ArrayList<>();

            //-------------------------------------------
            model = ModeRecipeContent.getItemData(model);
            for (Pair<String, String> p : model.getpAllWithoutEmpty()) {
                View v = LayoutInflater.from(activity).inflate(R.layout.comp_pmode_recipe_item, null);
                ((TextView) v.findViewById(R.id.comp_pmode_recipe_item_title_tv)).setText(p.k());
                ((TextView) v.findViewById(R.id.comp_pmode_recipe_item_value_tv)).setText(p.v());
                m_recipeContent.add(v);
            }

        }

        public ArrayList<View> getItemContent() {
            return m_recipeContent;
        }
    }

}
