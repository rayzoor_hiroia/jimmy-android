package com.hiroia.jimmy.view.mode_p;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.view.base.BaseFragment;

/**
 * Created by Bruce on 2018/11/14.
 */
public class PModeProcessSettingFragment extends BaseFragment implements View.OnClickListener {

    // View //
    private LinearLayout m_llvBack, m_llvStart;
    private TextView m_tvStart, m_tvTitle, m_tvSubTitle;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_pmode_process_setting, null);

        //-----------------------------------------------------------------
        // init
        m_llvBack = rootView.findViewById(R.id.fragment_pmode_process_setting_back_llv);
        m_llvStart = rootView.findViewById(R.id.fragment_pmode_process_setting_start_llv);

        m_tvStart = rootView.findViewById(R.id.fragment_pmode_process_setting_start_tv);
        m_tvTitle = rootView.findViewById(R.id.fragment_pmode_process_setting_title_tv);
        m_tvSubTitle = rootView.findViewById(R.id.fragment_pmode_process_setting_sub_title_tv);


        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);
        m_llvStart.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_pmode_process_setting_back_llv: // Back
                getParentActivity().switchStateView(PModeActivity.PModeState.SET_RECIPE);
                break;

            case R.id.fragment_pmode_process_setting_start_llv: // Start
                // 流程待確認
                break;
        }
    }

    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private PModeActivity getParentActivity() {
        return PModeActivity.getInstance();
    }
}
