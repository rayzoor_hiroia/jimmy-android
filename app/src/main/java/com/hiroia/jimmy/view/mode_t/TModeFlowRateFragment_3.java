package com.hiroia.jimmy.view.mode_t;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.bluetooth.BluetoothDevice;
import android.graphics.Color;
import android.graphics.Path;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.hiroia.jimmy.R;
import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.hiroia.jimmy.ble.module.BLEJimmyFragment;
import com.hiroia.jimmy.comp.comp.RecordData;
import com.hiroia.jimmy.comp.comp.TrainingData;
import com.hiroia.jimmy.comp.view.anim.LVCustomCircularRing;
import com.hiroia.jimmy.comp.view.dialog.IOSAlertDialog;
import com.library.android_common.component.TimeCounter;
import com.library.android_common.component.common.Pairs;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.YMDHMS;
import com.library.android_common.util.DoubleUtil;
import com.library.android_common.util.IntUtil;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

import static com.hiroia.jimmy.view.mode_t.TModeActivity.COMBO_EXCELLENT;
import static com.hiroia.jimmy.view.mode_t.TModeActivity.COMBO_GOOD;
import static com.hiroia.jimmy.view.mode_t.TModeFlowRateFragment_3.ScoreState.EXCELLENT;
import static com.hiroia.jimmy.view.mode_t.TModeFlowRateFragment_3.ScoreState.GOOD;

/**
 * Created by Bruce on 2018/11/1.
 */
public class TModeFlowRateFragment_3 extends BLEJimmyFragment implements View.OnClickListener {

    // Final Member //
    private final long PREPARED_COUNT_DOWN_TIME = 4000;
    private final long SWITCHVIEW_COUNT_DOWN_TIME = 2000;
    private final long FREQENCY_UPDATE_SEEK_BAR = 500; // 更新 seek bar 的頻率

    private final int CIRCLE_RING_SPEED = 5000;
    private final int CIRCLE_RING_BAR_WIDTH = 12;
    private final Integer[] WATER_VOL_RANDOM = {2, 4, 8};

    private final String JIMMY_BLUE_COLOR_CODE = "#276fdf";
    private final String WHITE_COLOR_CODE = "#FFFFFF";

    // View //
    private TextView m_tvWaterSpeed, m_tvSpeedScore, m_tvRateVol, m_tvPreparedTimeCount, m_tvPourScore, m_tvTimeClock, m_tvPourTimes, m_tvTooSlow, m_tvTooFast;
    private SeekBar m_skbScoreProgress;
    private LVCustomCircularRing m_lvCircleRing;
    private RoundCornerProgressBar m_rcProgressBar;
    private LinearLayout m_llvBack, m_llvScoreLayout, m_llvCountDownLayout, m_llvTimerProgress, m_llvTimerClock;

    // Comp //
    private Lst<Lst<Integer>> m_lstScoreRanges = Lst.of();
    private ScoreCombo m_comboManager = new ScoreCombo();
    private RecordData m_recordData = new RecordData();
    private TrainingData m_trainingData = new TrainingData();

    //    private WaterVolStep m_waterVolStep = new WaterVolStep();
    private ScoreState m_currentState = EXCELLENT;
    private TimeCounter m_timeCounter, // 前三秒的倒數計時
            m_scoreTimeCounter, // 分數得倒數計時
            m_dataTimeCounter; // Data 的倒數計時

    private double m_dCurrWeight = 0.0; // 現在出水量
    private double m_dLastSecWeight = 0.0; // 前一秒出水量
    private double m_dDataSec = 0.0;

    private int m_nScoreModeTime = 60; // Mode Time
    private int m_nProgressVal = 0; // Pour n Progress left time
    private int m_nScoreCount = 0; // 計算得分
    private int m_nPoursLeftTimes = 0; // Random Mode Pours 剩餘的次數
    private int m_dTotalTime = 60000;

    private boolean m_bIsPoursModeFinish = false;
    private boolean m_bIsIntegerSec = false;

    //-------------------------------------------------------
    // Fragment Life Circle
    //-------------------------------------------------------
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_tmode_flow_rate, null);

        //-----------------------------------------------------------------
        // init view
        m_tvWaterSpeed = rootView.findViewById(R.id.fragment_tmode_flow_rate_water_speed_tv);
        m_tvSpeedScore = rootView.findViewById(R.id.fragment_tmode_flow_rate_score_state_tv);
        m_tvRateVol = rootView.findViewById(R.id.fragment_tmode_flow_rate_vol_tv);
        m_tvPreparedTimeCount = rootView.findViewById(R.id.fragment_tmode_flow_rate_time_count_tv);
        m_tvTimeClock = rootView.findViewById(R.id.fragment_tmode_flow_rate_time_clock_tv);
        m_tvPourScore = rootView.findViewById(R.id.fragment_tmode_flow_rate_score_tv);
        m_tvPourTimes = rootView.findViewById(R.id.fragment_tmode_flow_rate_pour_progress_tv);
        m_tvTooSlow = rootView.findViewById(R.id.fragment_tmode_flow_rate_slow_tv);
        m_tvTooFast = rootView.findViewById(R.id.fragment_tmode_flow_rate_fast_tv);

        m_rcProgressBar = rootView.findViewById(R.id.fragment_tmode_flow_rate_pour_progress_rc);
        m_skbScoreProgress = rootView.findViewById(R.id.fragment_tmode_flow_rate_score_state_progress_skb);
        m_lvCircleRing = rootView.findViewById(R.id.fragment_tmode_flow_rate_circle_ring);

        m_llvBack = rootView.findViewById(R.id.fragment_tmode_flow_rate_back_llv);

        m_llvScoreLayout = rootView.findViewById(R.id.fragment_tmode_flow_rate_score_layout_llv);
        m_llvCountDownLayout = rootView.findViewById(R.id.fragment_tmode_flow_rate_countdown_layout_llv);

        m_llvTimerClock = rootView.findViewById(R.id.fragment_tmode_flow_rate_clock_timer_llv);
        m_llvTimerProgress = rootView.findViewById(R.id.fragment_tmode_flow_rate_progress_llv);

        // Comp //
        m_timeCounter = new TimeCounter();

        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);

        // Score Seek Bar //
        m_skbScoreProgress.setClickable(false);
        m_skbScoreProgress.setEnabled(false);
        m_skbScoreProgress.setOnSeekBarChangeListener(onSeekBarChangeListener);

        // Ring Effect //
        m_lvCircleRing.setBarColor(Color.parseColor(WHITE_COLOR_CODE));
        m_lvCircleRing.setViewColor(Color.parseColor(WHITE_COLOR_CODE));
        m_lvCircleRing.setBarWidth(CIRCLE_RING_BAR_WIDTH);

        // Score Comp //
        setScoreModeTime();

        //-----------------------------------------------------------------
        // Game Prepared Init //
        getParentActivity().setYMDHMS(YMDHMS.nowYMDHMS()); // 設定 遊戲開始時間 //

        if (getParentActivity().getMode() == TModeActivity.Mode.RANDOM) {
            // View //
            m_llvTimerClock.setVisibility(View.GONE);
            m_llvTimerProgress.setVisibility(View.VISIBLE);

            // Comp //
            m_dTotalTime = getParentActivity().getTimeSec();
            m_nPoursLeftTimes = getParentActivity().getRandomPoursCount(); // random count

        } else {
            // Single Mode //
            m_llvTimerClock.setVisibility(View.VISIBLE);
            m_llvTimerProgress.setVisibility(View.GONE);

            // set recipe water vol //
            m_tvRateVol.setText(String.valueOf(getParentActivity().getFlowRateVol())); // User 自訂的 Flow Rate
            m_rcProgressBar.setMax(getParentActivity().getTimeSec()); // 根據 mode 來決定時間
        }

        // Start Game //
        runPoursSchedule();

        //-----------------------------------------------------------------
        // TODO TEST Count Down
        getParentActivity().getFab().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                m_waterVolAverage.add(Lst.of(5, 10, 15, 20, 25, 30).randomGet());
//                m_dCurrWeight += Lst.of(5.3, 10.1, 15.2, 20.0, 25.1, 30.2).randomGet();
                m_dCurrWeight += 10;
            }
        });

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_tmode_flow_rate_back_llv:
//                getParentActivity().showProgressDialog("Stopping...");
//                stopGaming();
//                m_timeCounter.setOnCountingListener(SWITCHVIEW_COUNT_DOWN_TIME, new TimeCounter.OnCountingListener() {
//                    @Override
//                    public void onTick(long m) {
//
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        getParentActivity().dismissProgressDialog();
//                        getParentActivity().switchStateView(TModeActivity.TModeState.VELOCITY_MODE);
//                    }
//                });

//                final IOSAlertDialog dialog = new IOSAlertDialog(getParentActivity(), "提醒您", "你即將結束訓練模式，確定離開？", "離開", "取消"); // 改多國語言
                final IOSAlertDialog dialog = new IOSAlertDialog(getParentActivity(), "Confirm Navigation", "Are you sure to leave Barista Training Mode?", "Leave", "Stay"); // 改多國語言
                dialog.show(new IOSAlertDialog.OnClickEvent() {
                    @Override
                    public void confirm() {
                        stopGaming();
                        m_timeCounter.setOnCountingListener(SWITCHVIEW_COUNT_DOWN_TIME, new TimeCounter.OnCountingListener() {
                            @Override
                            public void onTick(long m) {

                            }

                            @Override
                            public void onFinish() {
                                getParentActivity().switchStateView(TModeActivity.TModeState.VELOCITY_MODE);
                            }
                        });

                        dialog.dismiss();
                    }

                    @Override
                    public void cancel() {
                        dialog.dismiss();
                    }
                });

                break;
        }
    }

    //-------------------------------------------------------
    // Public Method
    //-------------------------------------------------------
    public void stopGaming() {
        // release view before back //
        if (m_timeCounter != null)
            m_timeCounter.cancel();

        if (m_scoreTimeCounter != null)
            m_scoreTimeCounter.cancel();

        if (m_dataTimeCounter != null)
            m_dataTimeCounter.cancel();
    }

    //-------------------------------------------------------
    // Private Preference
    //-------------------------------------------------------
    // 設定遊玩排程 //
    private void runPoursSchedule() {

        int flowRateVol = 0;

        //--------------------------------------------------
        // Single Mode //
        if (getParentActivity().getMode() == TModeActivity.Mode.SINGLE) {
            m_bIsPoursModeFinish = true;
            flowRateVol = getParentActivity().getFlowRateVol();

            LogUtil.d(TModeFlowRateFragment_3.class, "Single_flowRateVol = " + flowRateVol);
            getParentActivity().addRateVol(flowRateVol);

            // Training Data //
            m_trainingData.add(0, 30, flowRateVol); // start time = 啟動時間， end time = 結束時間
        }

        //--------------------------------------------------
        // Random Mode //
        if (getParentActivity().getMode() == TModeActivity.Mode.RANDOM) {

            flowRateVol = Lst.ofArray(WATER_VOL_RANDOM).randomGet();
            m_nScoreModeTime = getParentActivity().getTimeSec() / getParentActivity().getRandomPoursCount(); // 取得平均時間, Total Time / Random Pours Size
            m_tvPourTimes.setText(("Pour " + Lst.accumulateOf(getParentActivity().getRandomPoursCount()) // 設定 Pour Index
                    .reverse()
                    .get(m_nPoursLeftTimes - 1)));

            LogUtil.d(TModeFlowRateFragment_3.class, "Random_flowRateVol = " + flowRateVol);
            getParentActivity().addRateVol(flowRateVol);

            // Training Data //
            int rule = (3 - m_nPoursLeftTimes) * 10;
            m_trainingData.add(rule, rule + 10, flowRateVol);

            // Check if is finish //
            if ((m_nPoursLeftTimes -= 1) <= 0)
                m_bIsPoursModeFinish = true;

            m_tvRateVol.setText(String.valueOf(flowRateVol)); // User 自訂的 Flow Rate
            m_rcProgressBar.setMax(m_nScoreModeTime); // 設定時間
        }

        //---------------------------------------------------
        // 開始倒數 //
        m_skbScoreProgress.setMax(flowRateVol * 20); // 乘以 20 為了讓最低的 vol 2ml 也可以被分成五份
        m_skbScoreProgress.setProgress(IntUtil.of(m_skbScoreProgress.getMax()).halfVal());
        m_lstScoreRanges = Lst.accumulateOf(m_skbScoreProgress.getMax())
                .partition(5)
                .reSize(5); // 計算分數 range

        startPreparedCountDown();
    }

    private void updateScoreViewIf(boolean enable, ScoreState scoreState) {
        if (enable)
            updateScoreView(scoreState);
    }

    private void updateScoreView(ScoreState scoreState) {
        m_currentState = scoreState;
        m_tvSpeedScore.setText(scoreState.msg());
        switch (scoreState) {
            case GOOD:
                m_tvTooSlow.setTextColor(Color.WHITE);
                m_tvTooFast.setTextColor(Color.WHITE);
                m_tvWaterSpeed.setTextColor(getResources().getColor(R.color.tmode_score_mode_color_green));
                m_tvSpeedScore.setTextColor(getResources().getColor(R.color.tmode_score_mode_color_green));
                m_skbScoreProgress.setThumb(getResources().getDrawable(R.drawable.jimmy_seekbar_thumb_background_green));
                break;

            case NORMAL:
                m_tvTooSlow.setTextColor(Color.WHITE);
                m_tvTooFast.setTextColor(Color.WHITE);
                m_tvWaterSpeed.setTextColor(getResources().getColor(R.color.tmode_score_mode_color_light_blue));
                m_tvSpeedScore.setTextColor(getResources().getColor(R.color.tmode_score_mode_color_light_blue));
                m_skbScoreProgress.setThumb(getResources().getDrawable(R.drawable.jimmy_seekbar_thumb_background_light_blue));
                break;

            case TOO_FAST:
                m_tvTooSlow.setTextColor(Color.WHITE);
                m_tvTooFast.setTextColor(getResources().getColor(R.color.tmode_score_mode_color_red));
                setTextEnlargeAnim(m_tvTooFast, 1.5f, 500);
                m_tvWaterSpeed.setTextColor(getResources().getColor(R.color.tmode_score_mode_color_red));
                m_tvSpeedScore.setTextColor(getResources().getColor(R.color.tmode_score_mode_color_red));
                m_skbScoreProgress.setThumb(getResources().getDrawable(R.drawable.jimmy_seekbar_thumb_background_red));
                break;

            case TOO_SLOW:
                m_tvTooFast.setTextColor(Color.WHITE);
                m_tvTooSlow.setTextColor(getResources().getColor(R.color.tmode_score_mode_color_orange));
                setTextEnlargeAnim(m_tvTooSlow, 1.5f, 500);
                m_tvWaterSpeed.setTextColor(getResources().getColor(R.color.tmode_score_mode_color_orange));
                m_tvSpeedScore.setTextColor(getResources().getColor(R.color.tmode_score_mode_color_orange));
                m_skbScoreProgress.setThumb(getResources().getDrawable(R.drawable.jimmy_seekbar_thumb_background_orange));
                break;

            case EXCELLENT:
                m_tvTooFast.setTextColor(Color.WHITE);
                m_tvTooSlow.setTextColor(Color.WHITE);
                m_tvWaterSpeed.setTextColor(getResources().getColor(R.color.tmode_score_mode_color_blue));
                m_tvSpeedScore.setTextColor(getResources().getColor(R.color.tmode_score_mode_color_blue));
                m_skbScoreProgress.setThumb(getResources().getDrawable(R.drawable.jimmy_seekbar_thumb_background_blue));
                break;
        }
    }

    // Seek Listener //
    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, final int p, boolean fromUser) {
            // update score rate //
            m_lstScoreRanges.forEach(new Lst.IConsumer<Lst<Integer>>() {
                @Override
                public void runEach(int i, Lst<Integer> item) {
                    updateScoreViewIf(item.contains(p), ScoreState.class.getEnumConstants()[i]); // 此處的 p 乘以 2 是因為 Max 值是目標值的兩倍
                }
            });
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    // 準備前倒數 //
    private void startPreparedCountDown() {

        // Update Layout //
        m_llvScoreLayout.setVisibility(View.GONE);
        m_llvCountDownLayout.setVisibility(View.VISIBLE);
        m_llvBack.setVisibility(View.GONE);

        m_lvCircleRing.setBarColor(Color.parseColor(JIMMY_BLUE_COLOR_CODE));
        m_lvCircleRing.startAnim(CIRCLE_RING_SPEED);

        // Start Counting //
        m_timeCounter.setOnCountingListener(PREPARED_COUNT_DOWN_TIME, new TimeCounter.OnCountingListener() {
            @Override
            public void onTick(long m) {
                int sec = (int) (m / 1000);
                m_tvPreparedTimeCount.setText(String.valueOf(sec));
                updateSeekBarProgress();

//                m_waterVolStep.addStep(getParentActivity().getTimeSec() - (int) (m / 1000), getDrippingVol()); // 預備階段就會開始出水，所以需要此時就開始記錄水量
            }

            @Override
            public void onFinish() {

                // 開始沖水倒數 //
                m_tvPreparedTimeCount.setText(String.valueOf(0));
                m_llvScoreLayout.setVisibility(View.VISIBLE);
                m_llvCountDownLayout.setVisibility(View.GONE);
                m_llvBack.setVisibility(View.VISIBLE);

                startBrewCountDown();
            }
        });
    }


    private void startBrewCountDown() {

        final long startTime = System.currentTimeMillis();

        // 開始時間倒數 //
        m_timeCounter.setOnCountingListener((m_nScoreModeTime * 1000), new TimeCounter.OnCountingListener() {
            @Override
            public void onTick(long m) {
                // 剩餘時間 //
                m_tvPreparedTimeCount.setText(String.valueOf(m / 1000));
                m_tvTimeClock.setText(StrUtil.getMilliSecTimeLabel(getParentActivity().getTimeMillySec() - (System.currentTimeMillis() - startTime)));
                m_rcProgressBar.setProgress(m_nProgressVal += 1);
            }

            @Override
            public void onFinish() {
                // 檢查是否已經結束 //
                if (!m_bIsPoursModeFinish) {
                    resetAll();
                    runPoursSchedule();
                    return;
                }

                // 設定 Chart Page 所需要的參數 //
                getParentActivity().setTotalScore(m_nScoreCount); // 設定遊戲獲得分數
                getParentActivity().setRecordData(m_recordData); // 設定 Record Data
                getParentActivity().setTrainingData(m_trainingData); // 設定 Training Data u

                //--------------------------------------------
                // switch to next //
                m_timeCounter.setOnCountingListener(1000, new TimeCounter.OnCountingListener() {
                    @Override
                    public void onTick(long m) {

                    }

                    @Override
                    public void onFinish() {
                        getParentActivity().switchStateView(TModeActivity.TModeState.RESULT_CHART);
                        getParentActivity().setIsRulesRecordGoTo(false);
                    }
                });
                //--------------------------------------------
            }
        });

        //-----------------------------------------------------------------------------------------
        // Seek Bar tick per 500 ms.
        m_scoreTimeCounter = new TimeCounter();
        m_scoreTimeCounter.setOnCountingListener(500, (m_nScoreModeTime * 1000), new TimeCounter.OnCountingListener() {
            @Override
            public void onTick(long m) {

                // 更新分數 //
                updateGameScore();

                // 計算水量變化 //
                m_tvWaterSpeed.setText(String.valueOf(getDrippingVol()));
                updateSeekBarProgress();

                // 更新計算值 //
                m_dLastSecWeight = m_dCurrWeight;
            }

            @Override
            public void onFinish() {
                m_scoreTimeCounter = null;
            }
        });

        //----------------------------------------------------------------------------------------
        // Update data counter

        m_dataTimeCounter = new TimeCounter();
        m_dataTimeCounter.setOnCountingListener(200, (m_nScoreModeTime * 1000), new TimeCounter.OnCountingListener() {
            @Override
            public void onTick(long m) {
                // update bar, 因為每隔 0.2 秒 update一次，但是 Water 只需要每隔 1 秒更新ㄧ次 //
                m_recordData.add(DoubleUtil.of(m_dDataSec += 0.2).reSizeDecimal(1), getDrippingVol());
            }

            @Override
            public void onFinish() {
                m_dataTimeCounter = null;
            }
        });

    }

//    // Pour 倒數 //
//    private void old_startBrewCountDown() {
//
//        final long startTime = System.currentTimeMillis();
//
//        // 開始時間倒數 //
//        m_timeCounter.setOnCountingListener((m_nScoreModeTime * 1000), new TimeCounter.OnCountingListener() {
//            @Override
//            public void onTick(long m) {
//                // 更新分數 //
////                updateGameScore();
//
//                // 剩餘時間 //
//                m_tvPreparedTimeCount.setText(String.valueOf(m / 1000));
//                m_tvTimeClock.setText(StrUtil.getMilliSecTimeLabel(getParentActivity().getTimeMillySec() - (System.currentTimeMillis() - startTime)));
//                m_rcProgressBar.setProgress(m_nProgressVal += 1);
//
//                // update bar //
////                m_waterVolStep.addStep(getParentActivity().getTimeSec() - (int) (m / 1000), getDrippingVol());
//            }
//
//            @Override
//            public void onFinish() {
//                // 檢查是否已經結束 //
//                if (!m_bIsPoursModeFinish) {
//                    resetAll();
//                    runPoursSchedule();
//                    return;
//                }
//
//                // 設定 Chart Page 所需要的參數 //
//                getParentActivity().setTotalScore(m_nScoreCount); // 設定遊戲獲得分數
//                getParentActivity().setWaterSteps(m_waterVolStep); // 設定 Water Step
//
//                //--------------------------------------------
//                // switch to next //
//                m_timeCounter.setOnCountingListener(1000, new TimeCounter.OnCountingListener() {
//                    @Override
//                    public void onTick(long m) {
//
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        getParentActivity().switchStateView(TModeActivity.TModeState.RESULT_CHART);
//                        getParentActivity().setIsRulesRecordGoTo(false);
//                    }
//                });
//                //--------------------------------------------
//            }
//        });
//
//        //-----------------------------------------------------------------------------------------
//        // Seek Bar tick per 500 ms.
//        m_scoreTimeCounter = new TimeCounter();
//        m_scoreTimeCounter.setOnCountingListener(500, (m_nScoreModeTime * 1000), new TimeCounter.OnCountingListener() {
//            @Override
//            public void onTick(long m) {
//
//                // 更新分數 //
//                updateGameScore();
//
//                // 計算水量變化 //
//                m_tvWaterSpeed.setText(String.valueOf(getDrippingVol()));
//                updateSeekBarProgress();
//
//                // update bar, 因為每隔 0.5 秒 update一次，但是 Water 只需要每隔 1 秒更新ㄧ次 //
////                m_waterVolStep.addStep(getParentActivity().getTimeSec() - (int) (m / 1000), getDrippingVol());
//                double sec = new Double(
//                        Opt.of(String.valueOf(m / 100)
//                                .substring(String.valueOf(m / 100).length() - 1))
//                                .parseToInt().get() >= 5 ?
//                        getParentActivity().getTimeSec() - (double) (m / 1000) + 0.5 :
//                        getParentActivity().getTimeSec() - (double) (m / 1000));
//                LogUtil.d(TModeFlowRateFragment_3.class, "ms = " + m + ",  sec = " + sec + ",  vol = " + getDrippingVol());
//                m_waterVolStep.addHalfSecStep(new Double(getParentActivity().getTimeSec()) - sec, getDrippingVol());
//
//                // 更新計算值 //
//                m_dLastSecWeight = m_dCurrWeight;
//            }
//
//            @Override
//            public void onFinish() {
//                m_scoreTimeCounter = null;
//            }
//        });
//    }

    // 更新 Seek Progress 動畫效果 //
    private void updateSeekBarProgress() {
        ValueAnimator anim = ValueAnimator.ofInt(m_skbScoreProgress.getProgress(), getDrippingVol() < 1 ? 0 : (getDrippingVol() * 10)); // Thumb 從 A pos 到 B pos，此處乘以 10 是因為 max val 是原本的十倍
        anim.setDuration(FREQENCY_UPDATE_SEEK_BAR);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int pos = (Integer) animation.getAnimatedValue();
                m_skbScoreProgress.setProgress(pos);
            }
        });
        anim.start();
    }

    // Pour Progress Bar 結束時的動畫效果 //
    private void resetTimeProgress() {
        ValueAnimator anim = ValueAnimator.ofInt((int) m_rcProgressBar.getMax(), 0); // Thumb 從 A pos 到 B pos
        anim.setDuration(1000);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int pos = (Integer) animation.getAnimatedValue();
                m_rcProgressBar.setProgress(pos);
            }
        });
        anim.start();
    }

    // 還原所有狀態 //
    private void resetAll() {
        // View //
//        getParentActivity().clearRateVol();
        m_tvPreparedTimeCount.setText(String.valueOf(3));

        m_lvCircleRing.setBarColor(Color.WHITE);
        m_lvCircleRing.stopAnim();

        m_skbScoreProgress.setProgress(IntUtil.of(m_skbScoreProgress.getMax()).halfVal());
        resetTimeProgress();
        m_nProgressVal = 0;

        m_llvScoreLayout.setVisibility(View.GONE);
        m_llvCountDownLayout.setVisibility(View.VISIBLE);
    }

    // 取得實際出水量 //
    private int getDrippingVol() {
        int rlt = IntUtil.roundOf(m_dCurrWeight - m_dLastSecWeight); // 4 捨 5 入
        return rlt <= 0 ? 0 : rlt;
    }

    // 設定沖煮 Mode //
    private void setScoreModeTime() {
        m_nScoreModeTime = getParentActivity().getTimeSec();
    }

    // 計算遊戲分數 //
    private void updateGameScore() {
        if (getDrippingVol() > 0) { // 重量不變時不計分數。
            m_nScoreCount += m_currentState.score(); // Update 分數
            m_nScoreCount += m_comboManager.combo(m_currentState); // 計算有無 combo
        }
        m_tvPourScore.setText(String.valueOf(m_nScoreCount));
    }

    // 設定文字放大 Anim //
    private void setTextEnlargeAnim(TextView textView, float scale, int sec) {
        Path path = new Path();
        path.moveTo(1f, 1f);
        path.lineTo(scale, scale);
        path.lineTo(1f, 1f);
        ObjectAnimator.ofFloat(textView, "scaleX", "scaleY", path)
                .setDuration(sec)
                .start();
    }

    // Get Parent Activity //
    private TModeActivity getParentActivity() {
        return TModeActivity.getInstance();
    }

    //-------------------------------------------------------
    // Enum
    //-------------------------------------------------------
    // Score State //
    public enum ScoreState {
        TOO_SLOW("Too Slow", 2),
        GOOD("Good", 41),
        EXCELLENT("Excellent", 64),
        NORMAL("Normal", 23),
        TOO_FAST("Too Fast", 2);
        //---------------------------------------------------
        private String m_txt;
        private int m_score;

        ScoreState(String txt, int score) {
            m_txt = txt;
            m_score = score;
        }

        public String msg() {
            return m_txt;
        }

        public int score() {
            return m_score;
        }

    }

    // Mode //
    public enum ScoreMode {
        HALF_MIN, ONE_MIN, TWO_MIN
    }

    //-------------------------------------------------------
    // Inner Class
    //-------------------------------------------------------
    class ScoreCombo {

        private int m_nComboCount = 0;
        private ScoreState m_lastScore = ScoreState.EXCELLENT; // 初始狀態 是 EXCELLENT
        private ScoreState m_currScore;
        private Lst<ScoreState> m_combos = Lst.of(EXCELLENT, GOOD);

        // 用以計算 combo 值 //
        public int combo(ScoreState scoreState) {
            m_currScore = scoreState;

            // 檢測是否需要 combo, 只有 EXCELLENT & GOOD 需要 combo //
            if (!m_combos.contains(scoreState))
                return 0;

            // 若是狀態不同，終止 Score Combo //
            if (m_lastScore != m_currScore) {
                m_lastScore = m_currScore; // 更新 last score 值
                m_nComboCount = 0; // reset combo 值
                return 0;
            }

            // 若是相同，計算 combo 次數。從 2 開始而最多 10 combo，所以僅有9筆 item //
            m_nComboCount = m_nComboCount >= 9 ? 0 : (m_nComboCount += 1);

            // combo 到最大值 reset combo //
            if (m_nComboCount == 0)
                return 0;

            // 根據不同的 combo 給予不同的加總分數 //
            return Pairs
                    .of(m_combos.first(), COMBO_EXCELLENT)
                    .add(m_combos.get(1), COMBO_GOOD)
                    .getValbyKey(scoreState)
                    .get(m_nComboCount - 1); // pos -1
        }
    }

    //-------------------------------------------------------
    // BLE Action
    //-------------------------------------------------------

    @Override
    public void onScanDevicesUpdate(ArrayList<BluetoothDevice> devices) {

    }

    @Override
    public void onConnectionStateUpdate(int state) {

    }

    @Override
    public void onJimmyInfoUpdate(JimmyInfo jimmyInfo) {
        m_dCurrWeight = jimmyInfo.getWeight();


    }

    @Override
    public void onBLEInit() {

    }
}
