package com.hiroia.jimmy.view.mode_e.record;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.hiroia.jimmy.ble.module.BLEJimmyFragment;
import com.hiroia.jimmy.comp.view.dialog.IOSAlertDialog;
import com.hiroia.jimmy.manager.JRecordEModeManager;
import com.hiroia.jimmy.model.ModelDateRecord;
import com.hiroia.jimmy.model.ModelEModeRecord;
import com.hiroia.jimmy.view.device.DevicesActivity;
import com.hiroia.jimmy.view.mode_e.EModeActivity;
import com.hiroia.jimmy.view.mode_e.comp.EModeReportDailyAdapter;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.ymd.YMD;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.AFTER_CONNECTION_FINISH_PAGE;

/**
 * Created by Bruce on 2018/11/14.
 */
public class EModeReportDailyFragment_1 extends BLEJimmyFragment implements View.OnClickListener, AdapterView.OnItemClickListener {


    // View //
    private LinearLayout m_llvBack, m_llvStatus, m_llvDate;
    private TextView m_tvTitle, m_tvTitleSmall, m_tvChangeState, m_tvStatus, m_tvSubTitle, m_tvDate;
    private ImageView m_imgDateSelect, m_imgStatus;
    private ListView m_lvReportDaily;
    private IOSAlertDialog m_dialog;
    private EModeReportDailyAdapter m_reportDailyAdapter;

    // Comp //
    private Lst<ModelEModeRecord> m_records = Lst.of();
    private Lst<String> m_ymds = Lst.of();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_emode_report_daily, null);
        //-----------------------------------------------------------------
        // init
        m_llvBack = rootView.findViewById(R.id.fragment_emode_report_daily_back_llv);
        m_llvStatus = rootView.findViewById(R.id.fragment_emode_report_daily_data_status_llv);
        m_llvDate = rootView.findViewById(R.id.fragment_emode_report_daily_date_llv);

        m_tvTitle = rootView.findViewById(R.id.fragment_emode_report_daily_title_tv);
        m_tvTitleSmall = rootView.findViewById(R.id.fragment_emode_report_daily_title_small_tv);
        m_tvChangeState = rootView.findViewById(R.id.fragment_emode_report_daily_change_state_tv);
        m_tvStatus = rootView.findViewById(R.id.fragment_emode_report_daily_data_status_tv);
        m_tvSubTitle = rootView.findViewById(R.id.fragment_emode_report_daily_sub_title_tv);
        m_tvDate = rootView.findViewById(R.id.fragment_emode_report_daily_date_tv);

        m_imgDateSelect = rootView.findViewById(R.id.fragment_emode_report_daily_date_select_img);
        m_imgStatus = rootView.findViewById(R.id.fragment_emode_report_daily_data_status_img);
        m_lvReportDaily = rootView.findViewById(R.id.fragment_emode_report_daily_report_lv);

        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);
        m_llvDate.setOnClickListener(this);
        m_lvReportDaily.setOnItemClickListener(this);
        m_imgStatus.setOnClickListener(this);

        if (getParentActivity().getCurrentDateRecord() != null)
            m_records = Lst.of(JRecordEModeManager.getModelEModeRecords().toList())
                    .gets(getParentActivity().getCurrentDateRecord().getIndexes());

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        sortStandardToFirst();
        m_reportDailyAdapter = new EModeReportDailyAdapter(getParentActivity(), m_records.toList());
        m_lvReportDaily.setAdapter(m_reportDailyAdapter);
        showRecordMark();

        if (m_records != null && m_records.size() != 0) {
            m_tvSubTitle.setText(m_records.first().getDeviceName());
            m_tvDate.setText(m_records.first().getYMD().toSlashString());
        } else {
            m_tvDate.setText(YMD.nowYMD().toSlashString());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_emode_report_daily_back_llv: // Back
                getParentActivity().switchStateView(getParentActivity().getIsExtractionGoTo() ? EModeActivity.EModeState.EXTRACTION_STATE : EModeActivity.EModeState.BREW_RECORDS_STATE);
                break;

            case R.id.fragment_emode_report_daily_data_status_img: // Recording Mark
                m_dialog = new IOSAlertDialog(getParentActivity(), "Confirm Navigation", "Are you sure to leave Espresso Mode?", "Leave", "Stay"); // 改多國語言
                m_dialog.show(new IOSAlertDialog.OnClickEvent() {
                    @Override
                    public void confirm() {
                        getParentActivity().switchStateView(EModeActivity.EModeState.BREW_RECORDS_STATE);
                        m_dialog.dismiss();
                    }

                    @Override
                    public void cancel() {
                        m_dialog.dismiss();
                    }
                });
                break;

            case R.id.fragment_emode_report_daily_date_llv: // Date

                final SpinnerDialog spinnerDialog = new SpinnerDialog(getParentActivity(), getDisplayDates().toList(), StrUtil.EMPTY);
                spinnerDialog.setShowKeyboard(false); // 是否預設開啟 Virtual Keyboard //
                spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                    @Override
                    public void onClick(String date, int pos) {

                        m_tvDate.setText(date);

                        m_records.clear();
                        m_records.addAll(getReportDates(YMD.ofSlash(date)));

                        m_reportDailyAdapter.notifyDataSetChanged();

                        // dismiss dialog //
                        spinnerDialog.closeSpinerDialog();
                    }
                });
                spinnerDialog.showSpinerDialog();
                break;

            case R.id.fragment_emode_report_daily_change_state_llv: // Change State
                if (!isConnected()) {
                    getParentActivity().getActManager()
                            .putBooleanExtra(AFTER_CONNECTION_FINISH_PAGE, true)
                            .start(DevicesActivity.class)
                            .retrieve();
                    return;
                }
                getParentActivity().switchStateView(EModeActivity.EModeState.STATE_SELECT_STATE);
                break;
        }
    }

    public void notifyDataSetChanged() {
//        sortStandardToFirst();
        m_reportDailyAdapter.notifyDataSetChanged();
    }

    //-----------------------------------------------------------------
    //  點擊 Item 的 Listener 事件
    //-----------------------------------------------------------------
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getParentActivity().setCurrentModelEModeRecord(m_records.get(position));
        getParentActivity().switchStateView(EModeActivity.EModeState.DAILY_CONTENT_STATE);
    }

    //-----------------------------------------------------------------
    //  BLE Action
    //-----------------------------------------------------------------

    @Override
    public void onScanDevicesUpdate(ArrayList<BluetoothDevice> devices) {

    }

    @Override
    public void onConnectionStateUpdate(int state) {

    }

    @Override
    public void onJimmyInfoUpdate(JimmyInfo jimmyInfo) {

    }

    @Override
    public void onBLEInit() {

    }

    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private EModeActivity getParentActivity() {
        return EModeActivity.getInstance();
    }

    private void sortStandardToFirst() {

        for (int i = 0; i < m_records.size(); i++) {
            ModelEModeRecord m = m_records.get(i);
            if (m.isStd()) { // 將 std move to first.
                m_records.remove(i);
                m_records.addFirst(m);
                break;
            }
        }
    }

    private void showRecordMark() {
        m_llvStatus.setVisibility(getParentActivity().getIsExtractionGoTo() ? View.VISIBLE : View.GONE);
    }

    // 取得 Report 頁面的日期 //
    private Lst<ModelEModeRecord> getReportDates(YMD ymd) {
        Lst<ModelEModeRecord> ms = Lst.of();
        for (ModelEModeRecord m : JRecordEModeManager.getModelEModeRecords().toList())
            ms.addIf(m.getYMD().dateEquals(ymd), m);

        return ms;
    }

    // 取得顯示頁面的日期 //
    private Lst<String> getDisplayDates() {
        Lst<String> lst = Lst.of();
        Lst<ModelDateRecord> ms = ModelEModeRecord.toDateRecords(JRecordEModeManager.getModelEModeRecords().toList());
        for (ModelDateRecord m : ms.toList())
            lst.add(m.getYmd().toSlashString());
        return lst;
    }

    private String getCurrentDate(){
        return m_tvDate.getText().toString();
    }

}
