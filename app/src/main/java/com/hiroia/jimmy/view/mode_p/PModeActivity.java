package com.hiroia.jimmy.view.mode_p;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.view.base.BaseActivity;

/**
 * Create By Ray on 2018/10/29
 */
public class PModeActivity extends BaseActivity{

    //----------------------------------------------
    // Construct
    //----------------------------------------------
    private static PModeActivity sm_self;
    private PModePourFragment m_pourFragment;
    private PModeProcessSettingFragment m_processSettingFragment;
    private PModeRecipeContentFragment m_recipeContentFragment;
    private PModeRecipeFragment m_recipeFragment;
    private PModeRecipeListFragment m_recipeListFragment;

    //-------------------------------------------------------------
    // Activity Life Cycle
    //-------------------------------------------------------------
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pmode, false);
        sm_self = this;

        //------------------------------------------
        // init view
        init();

        //------------------------------------------
        // set view
        switchStateView(PModeState.RECIPE_LIST);
    }

    //-------------------------------------------------------------
    // Private Preference
    //-------------------------------------------------------------
    private void init() {
        m_pourFragment = new PModePourFragment();
        m_processSettingFragment = new PModeProcessSettingFragment();
        m_recipeContentFragment = new PModeRecipeContentFragment();
        m_recipeFragment = new PModeRecipeFragment();
        m_recipeListFragment = new PModeRecipeListFragment();
    }

    //-------------------------------------------------------------
    // Private Preference
    //-------------------------------------------------------------
    private void switchFragment(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_pmode_flt, fragment)
                .commit();
    }

    //-------------------------------------------------------------
    // Public Method
    //-------------------------------------------------------------
    public static PModeActivity getInstance() {
        return sm_self;
    }

    public void switchStateView(PModeState p){
        switch (p){
            case RECIPE_LIST:
                switchFragment(m_recipeListFragment);
                break;

            case SET_RECIPE:
                switchFragment(m_recipeFragment);
                break;

            case RECIPE_CONTENT:
                switchFragment(m_recipeContentFragment);
                break;

            case POUR_MODE:
                switchFragment(m_pourFragment);
                break;

            case PROCESS_SETTING:
                switchFragment(m_processSettingFragment);
                break;
        }
    }

    //-------------------------------------------------------------
    // Enum
    //-------------------------------------------------------------
    public enum PModeState{
        RECIPE_LIST,
        SET_RECIPE,
        RECIPE_CONTENT,
        POUR_MODE,
        PROCESS_SETTING,
    }

}
