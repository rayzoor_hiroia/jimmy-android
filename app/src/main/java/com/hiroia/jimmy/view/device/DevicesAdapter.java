package com.hiroia.jimmy.view.device;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hiroia.jimmy.BuildConfig;
import com.hiroia.jimmy.R;
import com.hiroia.jimmy.ble.comp.BLEDevices;
import com.hiroia.jimmy.db.sp.SpDeviceName;
import com.library.android_common.component.common.Pair;
import com.library.android_common.component.common.Pairs;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

/**
 * Create By Ray on 2018/10/29
 */

public class DevicesAdapter extends BaseAdapter {

    // Comp //
    private ArrayList<BluetoothDevice> m_devices;
    private Activity m_act;
    private boolean m_isBleConnected = false;

    private String m_sMacAddress = "DD:F1:28:68:B6:1C";
    private Pairs<String, String> m_ps = Pairs.of("DD:F1:28:68:B6:1C", "_2.1")
            .add("C0:C8:39:C4:7C:1B ", "_2.2");

    //---------------------------------------------------------
    //  Construct
    //---------------------------------------------------------

    public DevicesAdapter(Activity activity, ArrayList<BluetoothDevice> devices) { // 可能會改成 ArrayList<BluetoothDevice>
        m_devices = devices;
        m_act = activity;
    }

    @Override
    public int getCount() {
        return m_devices.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {

        v = LayoutInflater.from(m_act).inflate(R.layout.comp_select_device_view, null);

        // 如果是藍芽連線成功時，第一筆會是連線成功的 item. -> visible link icon .
        if (m_isBleConnected && position == 0)
            (v.findViewById(R.id.comp_select_device_connected_icon_img)).setVisibility(View.VISIBLE); // 連線後 顯示 icon.
        else
            (v.findViewById(R.id.comp_select_device_connected_icon_img)).setVisibility(View.INVISIBLE); // 斷線後 隱藏 icon


        // Debug
        if (m_devices.get(position).getName() != null)
            ((TextView) v.findViewById(R.id.comp_select_device_title_tv)).setText(getDeviceNameFromSp(m_devices.get(position)).concat(getVersion(m_devices.get(position))));

//        if (m_devices.get(position) == null) return v;
//            ((TextView) v.findViewById(R.id.comp_select_device_title_tv)).setText(m_devices.get(position).getName().concat(getVersion(m_devices.get(position))));

        //-------------------------------------------------
//        BluetoothDevice device = m_devices.get(position);
//        ((TextView) v.findViewById(R.id.comp_select_device_title_tv)).setText(device.getName() + StrUtil.UNDER_LINE + device.getAddress());



        return v;
    }

    private String getVersion(BluetoothDevice device) {
        if (!BuildConfig.DEBUG) return StrUtil.EMPTY; // 防止在 release mode 顯示
        return StrUtil.UNDER_LINE +  device.getAddress();
    }

    private String getDeviceNameFromSp(BluetoothDevice device){
        if (device == null) return StrUtil.EMPTY;
        return SpDeviceName.get(device.getAddress());
    }


    // 連線成功後的 Notify //
    public void notifyChangeWithConnected() {
        m_isBleConnected = true;
        super.notifyDataSetChanged();
    }

    // 斷線後的 Notify //
    public void notifyChangeWithDisconnected() {
        m_isBleConnected = false;
        super.notifyDataSetChanged();
    }


}
