package com.hiroia.jimmy.view.mode_p;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.model.unuse.ModelRecipeList;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

/**
 * Created by Bruce on 2018/12/11.
 */
public class PModeRecipeListAdapter extends BaseAdapter {

    // View //
    private Activity m_ctx;
    private ViewHolder m_viewHolder;

    // Comp //
    private ArrayList<ModelRecipeList> m_recipe;


    //---------------------------------------------------------
    //  Construct
    //---------------------------------------------------------
    public PModeRecipeListAdapter(Activity activity, ArrayList<ModelRecipeList> recipe) {
        m_ctx = activity;
        m_recipe = recipe;
    }


    @Override
    public int getCount() {
        return m_recipe.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        view = LayoutInflater.from(m_ctx).inflate(R.layout.comp_pmode_recipe_list, null);

        m_viewHolder = new ViewHolder(
                (TextView) view.findViewById(R.id.comp_pmode_recipe_name_tv),
                (TextView) view.findViewById(R.id.comp_pmode_recipe_date_tv),
                (TextView) view.findViewById(R.id.comp_pmode_recipe_bean_tv),
                (TextView) view.findViewById(R.id.comp_pmode_recipe_water_vol_tv),
                (TextView) view.findViewById(R.id.comp_pmode_recipe_temperature_tv)
        );

        //-----------------------------------------------------------------
        // set data
        ModelRecipeList model = m_recipe.get(position);

        m_viewHolder.m_tvName.setText(model.getRecipeName());
        m_viewHolder.m_tvDate.setText(model.getDate() + "");
        m_viewHolder.m_tvBean.setText(model.getBeanWeight() + "g");
        m_viewHolder.m_tvWaterVol.setText(model.getWaterVol() + "ml");
        m_viewHolder.m_tvTemperature.setText(model.getTemperature() + StrUtil.DEGREE_C);

        return view;
    }


    class ViewHolder {

        private TextView m_tvName, m_tvDate, m_tvBean, m_tvWaterVol, m_tvTemperature;

        ViewHolder(TextView name, TextView date, TextView bean, TextView waterVol, TextView temperature) {
            this.m_tvName = name;
            this.m_tvDate = date;
            this.m_tvBean = bean;
            this.m_tvWaterVol = waterVol;
            this.m_tvTemperature = temperature;
        }
    }
}
