package com.hiroia.jimmy.view.setting;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.BuildConfig;
import com.hiroia.jimmy.R;
import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.hiroia.jimmy.ble.module.BLEJimmyFragment;
import com.hiroia.jimmy.comp.view.dialog.IOSAlertDialog;
import com.hiroia.jimmy.db.sp.SpAccount;
import com.hiroia.jimmy.manager.AccountManager;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.hiroia.jimmy.view.device.DevicesActivity;
import com.hiroia.jimmy.view.home.HomeActivity;
import com.hiroia.jimmy.view.login.LoginActivity;
import com.hiroia.jimmy.view.privacy.PrivacyTermsActivity;
import com.library.android_common.util.ActivityUtil;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.hiroia.jimmy.manager.PhoneManager.getPhoneInfo;

/**
 * Created by Bruce on 2019/2/11.
 */
public class SettingFragment extends BLEJimmyFragment implements View.OnClickListener {

    private LinearLayout m_llvAppVer, m_llvFeedback, m_llvPrivacyTerms, m_llvHomeTab, m_llvJimmyTab, m_llvLogout;
    private TextView m_tvTitle, m_tvUserName, m_tvAppVerTitle, m_tvAppVer, m_tvFeedbackTitle, m_tvPrivacyTermsTitle, m_tvLogout;
    private ImageView m_imgPhoto;
    private Button m_btnEdit;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_setting, null);

        //-----------------------------------------------------------------
        // init view
        m_llvAppVer = rootView.findViewById(R.id.fragment_setting_app_version_llv);
        m_llvFeedback = rootView.findViewById(R.id.fragment_setting_feedback_llv);
        m_llvPrivacyTerms = rootView.findViewById(R.id.fragment_setting_privacy_and_terms_llv);
        m_llvHomeTab = rootView.findViewById(R.id.fragment_setting_home_llv);
        m_llvJimmyTab = rootView.findViewById(R.id.fragment_setting_jimmy_llv);
        m_llvLogout = rootView.findViewById(R.id.fragment_setting_account_logout_llv);

        m_tvTitle = rootView.findViewById(R.id.fragment_setting_title_tv);
        m_tvUserName = rootView.findViewById(R.id.fragment_setting_user_name_tv);
        m_tvAppVerTitle = rootView.findViewById(R.id.fragment_setting_app_version_title_tv);
        m_tvAppVer = rootView.findViewById(R.id.fragment_setting_app_version_tv);
        m_tvFeedbackTitle = rootView.findViewById(R.id.fragment_setting_feedback_title_tv);
        m_tvPrivacyTermsTitle = rootView.findViewById(R.id.fragment_setting_privacy_and_terms_title_tv);
        m_tvLogout = rootView.findViewById(R.id.fragment_setting_account_logout_tv);

        m_imgPhoto = rootView.findViewById(R.id.fragment_setting_portrait_img);
        m_btnEdit = rootView.findViewById(R.id.fragment_setting_account_edit_btn);


        //-----------------------------------------------------------------
        // set Data
        m_btnEdit.setOnClickListener(this);
        m_llvFeedback.setOnClickListener(this);
        m_llvPrivacyTerms.setOnClickListener(this);
        m_llvHomeTab.setOnClickListener(this);
        m_llvJimmyTab.setOnClickListener(this);
        m_llvLogout.setOnClickListener(this);


        m_tvAppVer.setText(BuildConfig.VERSION_CODE + StrUtil.EMPTY);
        m_tvUserName.setText(AccountManager.getAccountModel().getUserName());
        loadPic();

        return rootView;
    }

    //------------------------------------------------------------
    // BLE Action
    //------------------------------------------------------------

    @Override
    public void onScanDevicesUpdate(ArrayList<BluetoothDevice> devices) {

    }

    @Override
    public void onConnectionStateUpdate(int state) {

    }

    @Override
    public void onJimmyInfoUpdate(JimmyInfo jimmyInfo) {

    }

    @Override
    public void onBLEInit() {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_setting_account_edit_btn: // Edit
//                if (AccountManager.getAccountModel().getToken().isEmpty()) {
//                    final IOSAlertDialog dialog = new IOSAlertDialog(getParentActivity(), "Confirm Navigation", "Please login for full features", "Leave", "Stay"); // TODO 多國語言
//                    dialog.show(new IOSAlertDialog.OnClickEvent() {
//                        @Override
//                        public void confirm() {
//                            dialog.dismiss();
//                            ActivityUtil.of(getParentActivity())
//                                    .start(LoginActivity.class)
//                                    .retrieve();
//                        }
//
//                        @Override
//                        public void cancel() {
//                            dialog.dismiss();
//                        }
//                    });
//                } else
                getParentActivity().switchFragment(SettingActivity.SettingState.PROFILE_EDIT);
                break;

            case R.id.fragment_setting_feedback_llv: // Feedback
                feedBack();
                break;

            case R.id.fragment_setting_privacy_and_terms_llv: // Privacy & Terms
                ActivityUtil.of(getParentActivity())
                        .start(PrivacyTermsActivity.class)
                        .retrieve();
                break;

            case R.id.fragment_setting_account_logout_llv: // Logout

                // 清除後也要清除 manager //
                SpAccount.clear();
                AccountManager.clear();

                // Start to LoginActivity //
                ActivityUtil.of(getParentActivity())
                        .start(LoginActivity.class)
                        .finish()
                        .retrieve();
                break;

            case R.id.fragment_setting_home_llv: // Home TAB
                ActivityUtil.of(getParentActivity())
                        .start(HomeActivity.class)
                        .retrieve();
                break;

            case R.id.fragment_setting_jimmy_llv: // JIMMY TAB
                ActivityUtil.of(getParentActivity())
                        .start(DevicesActivity.class)
                        .retrieve();
                break;
        }
    }

    //-------------------------------------------------------------
    // Private Preference
    //-------------------------------------------------------------

    private SettingActivity getParentActivity() {
        return SettingActivity.getInstance();
    }

    // load pic into profile pic //
    private void loadPic() {
        LogUtil.d(SettingFragment.class, "photo = " + AccountManager.getAccountModel().getFbPhoto());
        String sPicPath = AccountManager.getAccountModel().getFbPhoto();

        if (!sPicPath.isEmpty())
            Picasso.get().load(AccountManager.getAccountModel().getFbPhoto()).error(R.mipmap.ic_portrait_default).into(m_imgPhoto);
        else
            Picasso.get().load(R.mipmap.ic_portrait_default).into(m_imgPhoto);
    }

    // Send FeedBack Email //
    private void feedBack() {
        Intent mailIntent = new Intent(Intent.ACTION_SEND);
        mailIntent.setType("plain/text");
        mailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@gmail.com"});
        mailIntent.putExtra(Intent.EXTRA_SUBJECT, StrUtil.EMPTY);
        mailIntent.putExtra(Intent.EXTRA_TEXT, "Please describe your question in the space above."
                + StrUtil.WRAP + "To assist with your issue，you will need to provide relevant information，so please do not delete or modify the following"
                + StrUtil.WRAP + StrUtil.DIVIDER + getPhoneInfo(isBluetoothAdapaterOn())); // TODO 多國語言 ; isBLEOn 暫時先代 false
        startActivity(Intent.createChooser(mailIntent, "Feedback Email"));
    }
}
