package com.hiroia.jimmy.view.mode_e.record;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.EntryXComparator;
import com.hiroia.jimmy.R;
import com.hiroia.jimmy.comp.comp.RecordData;
import com.hiroia.jimmy.comp.comp.TrainingData;
import com.hiroia.jimmy.comp.comp.WaterVolStep;
import com.hiroia.jimmy.comp.view.dialog.IOSAlertDialog;
import com.hiroia.jimmy.cs.HttpCs;
import com.hiroia.jimmy.db.sql.EModeChartDBA;
import com.hiroia.jimmy.db.sql.JRecordDBA;
import com.hiroia.jimmy.enums.HttpDef;
import com.hiroia.jimmy.manager.AccountManager;
import com.hiroia.jimmy.manager.JRecordEModeManager;
import com.hiroia.jimmy.manager.net.NetworkManager;
import com.hiroia.jimmy.model.ModelEModeRecord;
import com.hiroia.jimmy.model.record.ModelJRecord;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.hiroia.jimmy.view.mode_e.EModeActivity;
import com.library.android_common.component.TimeCounter;
import com.library.android_common.component.common.Pair;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.hms.MS;
import com.library.android_common.component.date.hms.S;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Bruce on 2018/11/20.
 */
public class EModeReportDailyContentFragment_2 extends BaseFragment implements View.OnClickListener {

    // View //
    private LinearLayout m_llvBack, m_llvStatus;
    private TextView m_tvTitle, m_tvTitleSmall, m_tvStatus, m_tvSubTitle, m_tvOrderTitle, m_tvExtractionTitle, m_tvTimeTitle, m_tvOrderValue, m_tvExtractionValue, m_tvTimeValue, m_tvCreationTimeValue;
    private ImageView m_imgStatus;
    private LineChart m_lcChart;
    private Button m_btnDailyRecipe;

    // Comp //
    private ModelEModeRecord m_currModel = new ModelEModeRecord();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_emode_report_daily_content, null);

        //-----------------------------------------------------------------
        // init
        m_llvBack = rootView.findViewById(R.id.fragment_emode_report_daily_content_back_llv);
        m_llvStatus = rootView.findViewById(R.id.fragment_emode_report_daily_content_data_status_llv);

        m_tvTitle = rootView.findViewById(R.id.fragment_emode_report_daily_content_title_tv);
        m_tvTitleSmall = rootView.findViewById(R.id.fragment_emode_report_daily_content_title_small_tv);
        m_tvStatus = rootView.findViewById(R.id.fragment_emode_report_daily_content_status_tv);
        m_tvSubTitle = rootView.findViewById(R.id.fragment_emode_report_daily_content_sub_title_tv);
        m_tvOrderTitle = rootView.findViewById(R.id.fragment_emode_report_daily_content_order_title_tv);
        m_tvExtractionTitle = rootView.findViewById(R.id.fragment_emode_report_daily_content_extraction_title_tv);
        m_tvTimeTitle = rootView.findViewById(R.id.fragment_emode_report_daily_content_time_title_tv);
        m_tvOrderValue = rootView.findViewById(R.id.fragment_emode_report_daily_content_order_value_tv);
        m_tvExtractionValue = rootView.findViewById(R.id.fragment_emode_report_daily_content_extraction_value_tv);
        m_tvTimeValue = rootView.findViewById(R.id.fragment_emode_report_daily_content_time_value_tv);
        m_tvCreationTimeValue = rootView.findViewById(R.id.fragment_emode_report_daily_content_creation_time_value_tv);

        m_imgStatus = rootView.findViewById(R.id.fragment_emode_report_daily_content_status_img);
        m_lcChart = rootView.findViewById(R.id.fragment_emode_daily_content_chart_lc);
        m_btnDailyRecipe = rootView.findViewById(R.id.fragment_emode_report_daily_content_set_daily_recipe_btn);

        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);
        m_imgStatus.setOnClickListener(this);
        m_btnDailyRecipe.setOnClickListener(this);

        //-----------------------------------------------------------------
        // set model

        m_currModel = getParentActivity().getCurrentEModeRecord();
        m_tvExtractionValue.setText(m_currModel.getWeight() + "g");
        m_tvTimeValue.setText(m_currModel.getTtime() + "s");
        m_tvCreationTimeValue.setText(MS.of((int) m_currModel.getTime()).toString());
        setResultChartData(JRecordEModeManager.getModelById(m_currModel.getId())); // 設定 Result Chart

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_emode_report_daily_content_back_llv: // Back
                getParentActivity().switchStateView(EModeActivity.EModeState.DAILY_STATE);
                break;

            case R.id.fragment_emode_report_daily_content_status_img: // Recording Mark
                final IOSAlertDialog dialog = new IOSAlertDialog(getParentActivity(), "Confirm Navigation", "Are you sure to leave Espresso Mode?", "Leave", "Stay"); // 改多國語言
                dialog.show(new IOSAlertDialog.OnClickEvent() {
                    @Override
                    public void confirm() {
                        getParentActivity().switchStateView(EModeActivity.EModeState.BREW_RECORDS_STATE);
                        dialog.dismiss();
                    }

                    @Override
                    public void cancel() {
                        dialog.dismiss();
                    }
                });
                break;

            case R.id.fragment_emode_report_daily_content_set_daily_recipe_btn: // Set as Daily Recipe

                setAsStd();

                break;
        }
    }

    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private EModeActivity getParentActivity() {
        return EModeActivity.getInstance();
    }

    // 設定為 std . //
    private void setAsStd() {

        if (NetworkManager.isOffLineLog_e(getSimpleClassName() + " , setAsStd(), ")) {
            getParentActivity().switchStateView(EModeActivity.EModeState.DAILY_STATE);
            getParentActivity().notifyDataSetChanged();
            return;
        }

        // Start to Launch ------------------------------------ //
        // Show Progress Dialog //
        getParentActivity().showAutoDismissProgressDialog(S.Positive.FIVE_SEC);

        TimeCounter tc = new TimeCounter();
        tc.setOnCountingListener(S.ONE_SEC.millySec(), new TimeCounter.OnCountingListener() {
            @Override
            public void onTick(long m) {

            }

            @Override
            public void onFinish() {
                // Declare //
                long id = m_currModel.getId();

                // Start to set JRecord as Daily //
                HttpDef.SET_AS_J_RECORD_DAILY.post()
                        .addParam("token", AccountManager.getAccountModel().getToken())
                        .addParam("id", id + "")
                        .trigger(new HttpDef.HttpDefResponse() {
                            @Override
                            public void response(String response, JSONObject jsObj) {
                                if (response == null || response.isEmpty()) {
                                    LogUtil.e(EModeReportDailyContentFragment_2.class, " response is null or empty.");

                                    getParentActivity().dismissProgressDialog();
                                    getParentActivity().notifyDataSetChanged();
                                    getParentActivity().switchStateView(EModeActivity.EModeState.DAILY_STATE);
                                    return;
                                }

                                // Start to decode JSON //
                                boolean isSucc = jsObj.optBoolean(HttpCs.SUCCESS);
                                if (!isSucc) {
                                    LogUtil.e(EModeReportDailyContentFragment_2.class, " Success = " + isSucc);
                                    return;
                                }

                                LogUtil.d(EModeReportDailyContentFragment_2.class, HttpDef.SET_AS_J_RECORD_DAILY.getTag() + " response : " + response);
                                ModelJRecord m = ModelJRecord.decodeJSONStd(jsObj);
                                m.println(getSimpleClassName() + HttpDef.SET_AS_J_RECORD_DAILY.getTag() + " ModelJRecord : ");

                                // Update to Manager //
                                JRecordEModeManager.updateModel(m);

                                // Update to Local Database //
                                JRecordDBA.open(getParentActivity()).update(m);

                                // Switch Page //
                                getParentActivity().dismissProgressDialog();
                                getParentActivity().notifyDataSetChanged();
                                getParentActivity().switchStateView(EModeActivity.EModeState.DAILY_STATE);
                            }
                        });
            }
        });


    }

    private void showRecordMark() {
        m_llvStatus.setVisibility(getParentActivity().getIsExtractionGoTo() ? View.VISIBLE : View.GONE);
    }


    private void setResultChartData(ModelJRecord m) {

        // Declare //
        RecordData recordData = m.getRecordData();
        TrainingData trainingData = m.getTrainingData();

        // Start to Drawing Chart //
        m_lcChart.getDescription().setEnabled(false);
        m_lcChart.setPinchZoom(false);
        m_lcChart.setDrawGridBackground(false);
        m_lcChart.setTouchEnabled(true); // 設定 Chart 是否可以觸摸
//        m_lcResultChart.setViewPortOffsets(150, 150, 150, 150); // 控制上下左右座標軸顯示的距離，千萬不要全部 set 0 “ X & Y 軸會消失 ”

        // 設定 X 軸 參數 //
        XAxis x = m_lcChart.getXAxis();
        x.setEnabled(true);
        x.setDrawGridLines(false); // 設定 X軸 網格線
        x.setValueFormatter(new IAxisValueFormatter() { // 設定 X軸 文本的格式
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return String.valueOf((int) value).concat("s");
            }
        });
        x.setSpaceMax(5f); // 設定 X軸 額外的最大空間
//        x.setGranularity(0.5f); // 設定 X軸 間隔大小
        x.setAxisMaximum((float) m.getRecordData().getTotalTimeSec() + 1); // 設定 X軸 最大值
        x.setAxisMinimum(0f); // 設定 X軸 最小值
        x.setTextColor(Color.WHITE); // 設定 X軸 文字顏色
        x.setTextSize(12); // 設定 X軸 文字大小
        x.setLabelCount(5); // 設定 X軸 標籤數量
        x.setPosition(XAxis.XAxisPosition.BOTTOM);  // 設定 X軸 標籤在最下方 圖表外層顯示

//        // 設定 Y 軸參數 //
//        YAxis y = m_lcChart.getAxisLeft();
//        y.setEnabled(true);
//        y.setAxisMinimum(1f); // 設定 Y軸 最小值
//        y.setTextColor(Color.TRANSPARENT);
//        y.setDrawAxisLine(false);
//        y.setDrawGridLines(false); // 設定 Y軸 網格線

        // 設定 Y 軸參數 //
        YAxis y = m_lcChart.getAxisLeft();
        y.setEnabled(true);
        y.setDrawGridLines(false); // 設定 Y軸 網格線
        y.setAxisMinimum(1f); // 設定 Y軸 最小值
        y.setTextColor(Color.WHITE); // 設定 Y軸 文字顏色
        y.setTextSize(12); // 設定 Y軸 文字大小
        y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        y.setValueFormatter(new IAxisValueFormatter() { // 設定 Y軸 文本的格式
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return String.valueOf((int) value).concat("ml");
            }
        });

        m_lcChart.getAxisRight().setEnabled(false);
        m_lcChart.getLegend().setEnabled(false);
        m_lcChart.animateXY(1000, 1000); // 設定動畫

        // Set View //
        m_lcChart.setNoDataText(StrUtil.EMPTY); // 當沒有 data text 設定為空
        m_lcChart.setDescription(null); // close desc


        List<Entry> entries = new ArrayList<>();
        for (int i = recordData.values().size() - 1; i >= 0; i--) {
            entries.add(new Entry(i + 1, new Double(recordData.values().get(i).v()).intValue()));
            Collections.sort(entries, new EntryXComparator()); // 設定 entries 的順序性
        }

//        List<Entry> entries = new ArrayList<>();
//        for (int i = steps.getStepsBySec().size() - 1; i >= 0; i--) {
//            entries.add(new Entry(i + 1, steps.getStepsBySec().get(i).v()));
//            Collections.sort(entries, new EntryXComparator()); // 設定 entries 的順序性
//        }

        // 設定內部參數 //
        LineDataSet dataSet = new LineDataSet(entries, StrUtil.EMPTY);
        dataSet.setValueTextColor(Color.TRANSPARENT); // 線上的 text 顏色
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER); // CUBIC_BEZIER(立方曲線)，LINEAR(直線)，STEPPED(階梯)，HORIZONTAL_BEZIER(水平曲線)
        dataSet.setCubicIntensity(0.2f); // 設定曲線的彎曲程度
        dataSet.setDrawCircles(false);  // 在點上畫圓 默認true
        dataSet.setDrawFilled(true);    // 設定是否填充
        dataSet.setLineWidth(0);        // 設定 Line 寬度
        dataSet.setHighLightColor(getResources().getColor(R.color.jimmyBlue)); // 設定 點選某個點時，橫豎兩條線的顏色
        dataSet.setFillColor(getResources().getColor(R.color.jimmyBlue)); // 設定 填充 顏色
        dataSet.setFillAlpha(5000);     // 設定 填充 透明度


        //------------------------------------------------------------------
        // 白色 BaseLine Line Chart //
        Lst<Integer> vols = trainingData.toFlowRateLine(recordData.getTotalTimeSec());

        List<Entry> entriesBaseLine = new ArrayList<>();
        for (int i = 0; i < vols.toList().size(); i++) {
            entriesBaseLine.add(new Entry(i + 1, vols.get(i)));
            Collections.sort(entriesBaseLine, new EntryXComparator()); // 設定 entriesBaseLine 的順序性
        }

        LineDataSet whiteBaseDataSet = new LineDataSet(entriesBaseLine, StrUtil.EMPTY);
        whiteBaseDataSet.setValueTextColor(Color.TRANSPARENT); // 線上的 text 顏色
        whiteBaseDataSet.setMode(LineDataSet.Mode.LINEAR); // CUBIC_BEZIER(立方曲線)，LINEAR(直線)，STEPPED(階梯)，HORIZONTAL_BEZIER(水平曲線)
        whiteBaseDataSet.setCubicIntensity(0.2f); // 設定曲線的彎曲程度
        whiteBaseDataSet.setDrawCircles(false); // 在點上畫圓 默認true
        whiteBaseDataSet.setDrawFilled(false);  // 設定是否填充
        whiteBaseDataSet.setLineWidth(1f);      // 設定 Line 寬度
        whiteBaseDataSet.setColor(Color.WHITE); // 設定 Line 顏色
        whiteBaseDataSet.setHighLightColor(getResources().getColor(R.color.jimmyBlue)); // 設定 點選某個點時，橫豎兩條線的顏色
        whiteBaseDataSet.setFillColor(getResources().getColor(R.color.jimmyBlue)); // 設定 填充 顏色
        whiteBaseDataSet.setFillAlpha(0);       // 設定 填充 透明度


        // TODO 要白色矩形 Line Chart 請打開下面這段
        //------------------------------------------------------------------
        // 白色矩形 Line Chart //
//        LineDataSet whiteDataSet = new LineDataSet(entries, StrUtil.EMPTY);
//        whiteDataSet.setValueTextColor(Color.TRANSPARENT); // 線上的 text 顏色
//        whiteDataSet.setMode(LineDataSet.Mode.STEPPED); // CUBIC_BEZIER(立方曲線)，LINEAR(直線)，STEPPED(階梯)，HORIZONTAL_BEZIER(水平曲線)
//        whiteDataSet.setCubicIntensity(0.2f); // 設定曲線的彎曲程度
//        whiteDataSet.setDrawCircles(false); // 在點上畫圓 默認true
//        whiteDataSet.setDrawFilled(false);  // 設定是否填充
//        whiteDataSet.setLineWidth(1.5f);    // 設定 Line 寬度
//        whiteDataSet.setColor(Color.WHITE); // 設定 Line 顏色
//        whiteDataSet.setFillAlpha(0);       // 設定 填充 透明度

        LineData lineData = new LineData(dataSet, whiteBaseDataSet); // LineData 物件可放 多個 LineDataSet 物件，若需要添加其他 LineChart，記得要添加
        m_lcChart.setData(lineData);
        m_lcChart.invalidate();

    }


    // Old --------------------------------------------------------------------------------------------------------------
    // 設定 參數至 Chart View //
//    private void setChartData(WaterVolStep steps) {
//
//        m_lcChart.getDescription().setEnabled(false);
//        m_lcChart.setPinchZoom(false);
//        m_lcChart.setDrawGridBackground(false);
//        m_lcChart.setTouchEnabled(true); // 設定 Chart 是否可以觸摸
////        m_lcChart.setViewPortOffsets(150, 150, 150, 150); // 控制上下左右座標軸顯示的距離，千萬不要全部 set 0 “ X & Y 軸會消失 ”
//
//        // 設定 X 軸 參數 //
//        XAxis x = m_lcChart.getXAxis();
//        x.setEnabled(true);
//        x.setDrawGridLines(false); // 設定 X軸 網格線
//        x.setValueFormatter(new IAxisValueFormatter() { // 設定 X軸 文本的格式
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                return String.valueOf((int) value).concat("s");
//            }
//        });
//        x.setSpaceMax(5f); // 設定 X軸 額外的最大空間
////        x.setGranularity(0.5f); // 設定 X軸 間隔大小
////        x.setAxisMaximum(steps.getSteps().size()); // 設定 X軸 最大值
////        x.setAxisMinimum(1f); // 設定 X軸 最小值
//        x.setAxisMaximum(steps.getStepsBySec().size()); // 設定 X軸 最大值
//        x.setAxisMinimum(1f); // 設定 X軸 最小值
//        x.setTextColor(Color.WHITE); // 設定 X軸 文字顏色
//        x.setTextSize(12); // 設定 X軸 文字大小
//        x.setLabelCount(5); // 設定 X軸 標籤數量
//        x.setPosition(XAxis.XAxisPosition.BOTTOM);  // 設定 X軸 標籤在最下方 圖表外層顯示
//
//
//        // 設定 Y 軸參數 //
//        YAxis y = m_lcChart.getAxisLeft();
//        y.setEnabled(true);
//        y.setDrawGridLines(false); // 設定 Y軸 網格線
//        y.setAxisMinimum(1f); // 設定 Y軸 最小值
//        y.setTextColor(Color.WHITE); // 設定 Y軸 文字顏色
//        y.setTextSize(12); // 設定 Y軸 文字大小
//        y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
//        y.setValueFormatter(new IAxisValueFormatter() { // 設定 Y軸 文本的格式
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                return String.valueOf((int) value).concat("ml");
//            }
//        });
//
//        m_lcChart.getAxisRight().setEnabled(false);
//        m_lcChart.getLegend().setEnabled(false);
//        m_lcChart.animateXY(1000, 1000); // 設定動畫
//
//        // Set View //
//        m_lcChart.setNoDataText(StrUtil.EMPTY); // 當沒有 data text 設定為空
//        m_lcChart.setDescription(null); // close desc
//
//
//        List<Entry> entries = new ArrayList<>();
//        for (int i = steps.getStepsBySec().size() - 1; i >= 0; i--) {
//            entries.add(new Entry(i + 1, steps.getStepsBySec().get(i).v()));
//            Collections.sort(entries, new EntryXComparator()); // 設定 entries 的順序性
//        }
//
//
//        // 設定內部參數 //
//        LineDataSet dataSet = new LineDataSet(entries, StrUtil.EMPTY);
//        dataSet.setValueTextColor(Color.TRANSPARENT); // 線上的 text 顏色
//        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER); // CUBIC_BEZIER(立方曲線)，LINEAR(直線)，STEPPED(階梯)，HORIZONTAL_BEZIER(水平曲線)
//        dataSet.setCubicIntensity(0.2f); // 設定曲線的彎曲程度
//        dataSet.setDrawCircles(false);  // 在點上畫圓 默認true
//        dataSet.setDrawFilled(true);    // 設定是否填充
//        dataSet.setLineWidth(0);        // 設定 Line 寬度
//        dataSet.setHighLightColor(getResources().getColor(R.color.jimmyBlue)); // 設定 點選某個點時，橫豎兩條線的顏色
//        dataSet.setFillColor(getResources().getColor(R.color.jimmyBlue)); // 設定 填充 顏色
//        dataSet.setFillAlpha(5000);     // 設定 填充 透明度
//
//
//        // TODO 要白色矩形 Line Chart 請打開下面這段
//        // 白色矩形 Line Chart
////        LineDataSet whiteDataSet = new LineDataSet(entries, StrUtil.EMPTY);
////        whiteDataSet.setValueTextColor(Color.TRANSPARENT); // 線上的 text 顏色
////        whiteDataSet.setMode(LineDataSet.Mode.STEPPED); // CUBIC_BEZIER(立方曲線)，LINEAR(直線)，STEPPED(階梯)，HORIZONTAL_BEZIER(水平曲線)
////        whiteDataSet.setCubicIntensity(0.2f); // 設定曲線的彎曲程度
////        whiteDataSet.setDrawCircles(false); // 在點上畫圓 默認true
////        whiteDataSet.setDrawFilled(false);  // 設定是否填充
////        whiteDataSet.setLineWidth(1.5f);    // 設定 Line 寬度
////        whiteDataSet.setColor(Color.WHITE); // 設定 Line 顏色
////        whiteDataSet.setFillAlpha(0);       // 設定 填充 透明度
////
////
////        LineData lineData = new LineData(dataSet, whiteDataSet);
//        LineData lineData = new LineData(dataSet);
//        m_lcChart.setData(lineData);
//        m_lcChart.invalidate();
//
//    }
}
