package com.hiroia.jimmy.view.mode_t;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.library.android_common.component.view.ios_style.IOSTheme;

import static com.hiroia.jimmy.view.mode_t.TModeActivity.Mode.RANDOM;
import static com.hiroia.jimmy.view.mode_t.TModeActivity.Mode.SINGLE;

/**
 * Created by Bruce on 2018/11/1.
 */
public class TModeTrainingBaristaFragment_1 extends BaseFragment implements View.OnClickListener {

    // View //
    private LinearLayout m_llvBack, m_llvRecord;
    private FrameLayout m_flvSingleBtn, m_flvRandomBtn;
    private ImageView m_imgSingleIcon, m_imgRandomIcon, m_imgBackIcon;
    private TextView m_tvSingleTxt, m_tvRandomTxt;
    private Button m_btnNext, m_btnRules;

    // Comp //
    private TModeActivity.Mode m_currMode = TModeActivity.Mode.SINGLE;

    //-------------------------------------------------------------
    // Fragment Life Cycle
    //-------------------------------------------------------------

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_tmode_training_barista, null);

        //-----------------------------------------------------------------
        // init view
        m_llvBack = rootView.findViewById(R.id.fragment_tmode_training_back_llv);
        m_llvRecord = rootView.findViewById(R.id.fragment_tmode_training_record_llv);

        m_flvSingleBtn = rootView.findViewById(R.id.fragment_tmode_training_single_mode_btn_flv);
        m_flvRandomBtn = rootView.findViewById(R.id.fragment_tmode_training_random_mode_btn_flv);

        m_tvSingleTxt = rootView.findViewById(R.id.fragment_tmode_training_single_mode_tv);
        m_tvRandomTxt = rootView.findViewById(R.id.fragment_tmode_training_random_mode_tv);

        m_imgSingleIcon = rootView.findViewById(R.id.fragment_tmode_finish_icon_single_mode_img);
        m_imgRandomIcon = rootView.findViewById(R.id.fragment_tmode_finish_icon_random_mode_img);
        m_imgBackIcon = rootView.findViewById(R.id.fragment_tmode_training_back_icon_img);
        m_btnNext = rootView.findViewById(R.id.fragment_tmode_training_next_btn);
        m_btnRules = rootView.findViewById(R.id.fragment_tmode_training_rules_btn);

        //-----------------------------------------------------------------
        // init view
//        m_imgBackIcon.setOnClickListener(this);
        m_llvBack.setOnClickListener(this);
        m_llvRecord.setOnClickListener(this);

        m_flvRandomBtn.setOnClickListener(this);
        m_flvSingleBtn.setOnClickListener(this);
        m_btnNext.setOnClickListener(this);
        m_btnRules.setOnClickListener(this);

        //-----------------------------------------------------------------
        // set data
        setMode(m_currMode);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_tmode_training_single_mode_btn_flv: // Single
                setMode(SINGLE);
                break;

            case R.id.fragment_tmode_training_random_mode_btn_flv: // Random
                setMode(RANDOM);
                break;

            case R.id.fragment_tmode_training_next_btn: // Next
                getParentActivity().switchStateView(TModeActivity.TModeState.VELOCITY_MODE);
                break;

            case R.id.fragment_tmode_training_rules_btn: // Rules
                getParentActivity().switchStateView(TModeActivity.TModeState.TRAINING_RESULT_PAGE);
                getParentActivity().setIsTrainingBaristaGoTo(true);
                break;

            case R.id.fragment_tmode_training_record_llv: // Record
                getParentActivity().switchStateView(TModeActivity.TModeState.RULE_RECORD);
                break;

            case R.id.fragment_tmode_training_back_llv: // Back
                getParentActivity().finish();
                break;
        }
    }

    //-------------------------------------------------------------
    // Private Preference
    //-------------------------------------------------------------

    private TModeActivity getParentActivity() {
        return TModeActivity.getInstance();
    }

    private void setMode(TModeActivity.Mode mode) {
        switch (mode) {
            case RANDOM:
                // View //
                m_flvRandomBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.jimmy_button_background_blue));
                m_imgRandomIcon.setVisibility(View.VISIBLE);

                m_flvSingleBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.jimmy_common_btn_background));
                m_imgSingleIcon.setVisibility(View.INVISIBLE);

                m_tvRandomTxt.setTextColor(Color.WHITE);
                m_tvSingleTxt.setTextColor(Color.GRAY);

                // Comp //
                m_currMode = RANDOM;
                getParentActivity().setTMode(RANDOM);
                break;

            case SINGLE:
                // View //
                m_flvSingleBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.jimmy_button_background_blue));
                m_imgSingleIcon.setVisibility(View.VISIBLE);

                m_flvRandomBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.jimmy_common_btn_background));
                m_imgRandomIcon.setVisibility(View.INVISIBLE);

                m_tvSingleTxt.setTextColor(Color.WHITE);
                m_tvRandomTxt.setTextColor(Color.GRAY);

                // Comp //
                m_currMode = SINGLE;
                getParentActivity().setTMode(SINGLE);
                break;
        }
    }

}
