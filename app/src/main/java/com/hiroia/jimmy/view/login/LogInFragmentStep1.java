package com.hiroia.jimmy.view.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.hiroia.jimmy.view.privacy.PrivacyTermsActivity;
import com.library.android_common.util.ActivityUtil;

/**
 * Create By Ray on 2018/10/25
 */

public class LogInFragmentStep1 extends BaseFragment implements View.OnClickListener {

    private FrameLayout m_flvFBLogIn;
    private TextView m_tvSubTitle, m_tvPrivacyTerms, m_tvCreateAccount;
    private Button m_btnLogIn;

    //---------------------------------------------------------------
    // Fragment Life Cycle
    //---------------------------------------------------------------
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-------------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_login_step1, container, false);

        //-------------------------------------------------------------------
        // init view
        m_flvFBLogIn = rootView.findViewById(R.id.fragment_login_step1_fb_flv);

        m_tvSubTitle = rootView.findViewById(R.id.fragment_login_step1_sub_title_tv);
        m_tvPrivacyTerms = rootView.findViewById(R.id.fragment_login_step1_privacy_terms_tv);
        m_tvCreateAccount = rootView.findViewById(R.id.fragment_login_step1_create_account_tv);

        m_btnLogIn = rootView.findViewById(R.id.fragment_login_step1_login_btn);


        //-------------------------------------------------------------------
        // set data
        m_btnLogIn.setOnClickListener(this);
        m_flvFBLogIn.setOnClickListener(this);
        m_tvPrivacyTerms.setOnClickListener(this);
        m_tvCreateAccount.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            // 要調整 //
            case R.id.fragment_login_step1_login_btn: // LogIn Button //
                getParentActivity().switchFragment(LoginActivity.LogInStep.LOG_IN_STEP2);
                break;

            case R.id.fragment_login_step1_fb_flv: // FB LogIn //
                getParentActivity().showProgressDialog();
                getParentActivity().fbLogin();
                break;

            case R.id.fragment_login_step1_create_account_tv: // Create Account //
                getParentActivity().switchFragment(LoginActivity.LogInStep.SIGN_UP_STEP1);
                break;

            case R.id.fragment_login_step1_privacy_terms_tv: // Privacy & Terms //
                ActivityUtil.of(getParentActivity())
                        .start(PrivacyTermsActivity.class)
                        .retrieve();
                break;
        }
    }

    //---------------------------------------------------------------
    // Public Method
    //---------------------------------------------------------------

    public LoginActivity getParentActivity() {
        return LoginActivity.getInstance();
    }

}
