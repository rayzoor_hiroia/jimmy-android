package com.hiroia.jimmy.view.mode_p;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.view.base.BaseFragment;

/**
 * Created by Bruce on 2018/11/14.
 */
public class PModeRecipeFragment extends BaseFragment implements View.OnClickListener {

    // View //
    private LinearLayout m_llvBack;
    private TextView m_tvTitle, m_tvBeanWeight, m_tvRatio, m_tvWaterVol, m_tvTemperature, m_tvErrorMsg;
    private EditText m_etRecipeName, m_etBeanWeight, m_etRatio, m_etWaterVol, m_etTemperature;
    private Button m_btnNext;
    private ImageView m_imgLock, m_imgFilter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_pmode_recipe, null);

        //-----------------------------------------------------------------
        // init
        m_llvBack = rootView.findViewById(R.id.fragment_pmode_recipe_back_llv);

        m_tvTitle = rootView.findViewById(R.id.fragment_pmode_recipe_title_tv);
        m_tvBeanWeight = rootView.findViewById(R.id.fragment_pmode_recipe_bean_title_tv);
        m_tvRatio = rootView.findViewById(R.id.fragment_pmode_recipe_ratio_title_tv);
        m_tvWaterVol = rootView.findViewById(R.id.fragment_pmode_recipe_water_title_tv);
        m_tvTemperature = rootView.findViewById(R.id.fragment_pmode_recipe_temperature_tv);
        m_tvErrorMsg = rootView.findViewById(R.id.fragment_pmode_recipe_error_msg_tv);

        m_etRecipeName = rootView.findViewById(R.id.fragment_pmode_recipe_name_et);
        m_etBeanWeight = rootView.findViewById(R.id.fragment_pmode_recipe_bean_et);
        m_etRatio = rootView.findViewById(R.id.fragment_pmode_recipe_ratio_et);
        m_etWaterVol = rootView.findViewById(R.id.fragment_pmode_recipe_water_et);
        m_etTemperature = rootView.findViewById(R.id.fragment_pmode_recipe_temperature_et);

        m_btnNext = rootView.findViewById(R.id.fragment_pmode_recipe_next_btn);

        m_imgLock = rootView.findViewById(R.id.fragment_pmode_recipe_bean_lock_img);
        m_imgFilter = rootView.findViewById(R.id.fragment_pmode_recipe_filter_img);


        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);
        m_btnNext.setOnClickListener(this);
        m_imgFilter.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_pmode_recipe_back_llv: // Back
                getParentActivity().switchStateView(PModeActivity.PModeState.RECIPE_LIST);
                break;

            case R.id.fragment_pmode_recipe_next_btn: // Next
                // 去兩個不同的頁面，由前一頁點入不同作為判斷
                break;

            case R.id.fragment_pmode_recipe_filter_img: // Filter
                clearData();
                break;
        }
    }

    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private PModeActivity getParentActivity() {
        return PModeActivity.getInstance();
    }

    private void clearData() {
        m_etBeanWeight.setHint("0.0g");
        m_etRatio.setHint("1:15");
        m_etWaterVol.setHint("0.0ml");
        m_etTemperature.setHint("90℃");
    }
}
