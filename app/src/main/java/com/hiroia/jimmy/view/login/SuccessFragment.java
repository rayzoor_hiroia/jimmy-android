package com.hiroia.jimmy.view.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.view.base.BaseFragment;

/**
 * Create By Ray on 2018/10/26
 */
public class SuccessFragment extends BaseFragment implements View.OnClickListener {

    private Button m_btnOk;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-------------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_success, container, false);

        //-------------------------------------------------------------------
        // init view
        m_btnOk = rootView.findViewById(R.id.fragment_success_ok_btn);

        //-------------------------------------------------------------------
        // set data

        m_btnOk.setOnClickListener(this);

        return rootView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_success_ok_btn:
                getParentActivity().switchFragment(LoginActivity.LogInStep.LOG_IN_STEP1);
                break;
        }
    }

    //---------------------------------------------------------------
    // Public Method
    //---------------------------------------------------------------

    public LoginActivity getParentActivity() {
        return LoginActivity.getInstance();
    }

}
