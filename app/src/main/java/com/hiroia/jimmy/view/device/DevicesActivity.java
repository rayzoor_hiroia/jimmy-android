package com.hiroia.jimmy.view.device;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.ble.BLEJimmy;
import com.hiroia.jimmy.ble.BLEJimmyGattAttributes;
import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.hiroia.jimmy.ble.module.BLEJimmyActivity;
import com.hiroia.jimmy.comp.cs.SamanthaCs;
import com.hiroia.jimmy.comp.view.anim.LVCustomCircularRing;
import com.hiroia.jimmy.comp.view.dialog.IOSAlertDialog;
import com.hiroia.jimmy.comp.view.dialog.IOSTextInputDialog;
import com.hiroia.jimmy.db.sp.SpDeviceName;
import com.hiroia.jimmy.view.base.RedirectActivity;
import com.hiroia.jimmy.view.home.HomeActivity;
import com.hiroia.jimmy.view.mode_t.TModeActivity;
import com.hiroia.jimmy.view.setting.SettingActivity;
import com.library.android_common.component.TimeCounter;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.view.ios_style.IOSItemDialog;
import com.library.android_common.enums.HttpDef;
import com.library.android_common.util.ActivityUtil;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;
import com.library.android_common.util.ThreadUtil;

import java.util.ArrayList;

import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.AFTER_CONNECTION_FINISH_PAGE;

/**
 * Create By Ray on 2018/10/29
 * 連線頁面
 */
public class DevicesActivity extends BLEJimmyActivity implements View.OnClickListener, ListView.OnItemClickListener, ListView.OnItemLongClickListener {

    // Final Member //
    private final int CONNECTED_ITEM_POSITION = 0;
    private final int CIRCLE_RING_SPEED = 5000;
    private final int CIRCLE_RING_BAR_WIDTH = 12;

    private final String CIRCLE_RING_BAR_COLOR = "#276fdf";
    private final String CIRCLE_RING_VIEW_COLOR = "#FFFFFF";
    private final String SELECT_JIMMY_TITLE = "Select a JIMMY";

    // View //
    private ListView m_lvDeviceListView;
    private DevicesAdapter m_devicesAdapter;
    private ImageView m_imgBluetoothIcon;
    private TextView m_tvSubTitle, m_tvMacAddress;
    private LinearLayout m_llvDeviceListInfo, m_llvBleConnIcon, m_llvHomeTab, m_llvJimmyTab, m_llvSetTab;
    private LVCustomCircularRing m_lvCircleRing;

    // Comp //
    private Lst<BluetoothDevice> m_devices = Lst.of();
    private int m_nCurrSelectDevicePos = 0;
    private boolean m_bFinishAfterConnectionSucc = false;
    private boolean m_bIsConnectState = false;

    //-------------------------------------------------------
    // Activity Life Cycle
    //-------------------------------------------------------
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices, true);
        //---------------------------------------------------
        // Check Service
        _checkBTPermissions();
        checkBluetoothService();
        checkLocationService();

        //---------------------------------------------------
        // init view
        m_tvSubTitle = findViewById(R.id.activity_device_sub_title);
        m_llvDeviceListInfo = findViewById(R.id.activity_devices_ble_device_list_info_llv);
        m_llvBleConnIcon = findViewById(R.id.activity_devices_ble_connection_icon_llv);
        m_llvHomeTab = findViewById(R.id.activity_devices_home_llv);
        m_llvSetTab = findViewById(R.id.activity_devices_setting_llv);
        m_lvDeviceListView = findViewById(R.id.activity_devices_list_lv);
        m_imgBluetoothIcon = findViewById(R.id.activity_devices_blue_img);
        m_lvCircleRing = findViewById(R.id.activity_devices_circle_ring);
        m_tvMacAddress = findViewById(R.id.activity_devices_mac_address_tv);

        //---------------------------------------------------
        // set view
        m_lvCircleRing.setBarColor(Color.parseColor(CIRCLE_RING_BAR_COLOR));
        m_lvCircleRing.setViewColor(Color.parseColor(CIRCLE_RING_VIEW_COLOR));
        m_lvCircleRing.setBarWidth(CIRCLE_RING_BAR_WIDTH);

        m_devicesAdapter = new DevicesAdapter(getActManager().getActivity(), m_devices.toList());
        m_lvDeviceListView.setAdapter(m_devicesAdapter);
        m_lvDeviceListView.setOnItemClickListener(this);

        // Listener //
        m_tvSubTitle.setText(SELECT_JIMMY_TITLE);
        m_imgBluetoothIcon.setOnClickListener(this);
        m_llvHomeTab.setOnClickListener(this);
        m_llvSetTab.setOnClickListener(this);
        m_llvBleConnIcon.setOnClickListener(this);
        m_imgBluetoothIcon.setOnClickListener(this);


//      //TODO switch another app code
//        getFab().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                startActivity(new Intent(DevicesActivity.this, RedirectActivity.class));
//                getActManager().retrieveAnotherApp(SamanthaCs.MAIN_PACKAGE, SamanthaCs.JIMMY_OTA_REDIRECT_PACKAGE, new ActivityUtil.OnSwitchAnotherAppListener() {
//                            @Override
//                            public void onAppNotFound() {
//                                Intent it = new Intent(Intent.ACTION_VIEW, Uri.parse(HttpDef.PLAY_STORE_SAMANTHA_URI.notTest().getUrl()));
//                                startActivity(it);
//                                finish(); // 導向 google play 後強制關閉 App
//                            }
//                        });
//            }
//        });

        getFab().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyJimmyPower();
            }
        });


    }

    @Override
    protected void onDestroy() {
        stopScan(); // 離開前，確保已經 stop scan.
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_devices_blue_img:
                stopConnectAction();
                setAutoScan(true);
                break;

            case R.id.activity_devices_home_llv:
                ActivityUtil.of(DevicesActivity.this)
                        .start(HomeActivity.class)
                        .finish()
                        .retrieve();
                break;

            case R.id.activity_devices_setting_llv:
                ActivityUtil.of(DevicesActivity.this)
                        .start(SettingActivity.class)
                        .finish()
                        .retrieve();
                break;
        }
    }

    @Override
    public void onBLEInit() {

        // 若是沒有連線 //
        if (!isConnected()) {
            setAutoScan(true);
            return;
        }

        // 若是連線中 //
        BluetoothDevice device = getConnectedDevice();
        if (device == null) return;

        m_devices.clear().add(getConnectedDevice());
        m_devicesAdapter.notifyChangeWithConnected();
        m_lvDeviceListView.setOnItemLongClickListener(this);
    }


    @Override
    protected void onResume() {
        super.onResume();

        // 監測 是否有開始執行掃描 // todo 暫時先不啟用
//        registerConnectCheck();

        // check if need finish this activity when connected //
        boolean b = getIntent().getBooleanExtra(BLEJimmyGattAttributes.AFTER_CONNECTION_FINISH_PAGE, false);
        m_bFinishAfterConnectionSucc = b;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

        // 若是第一筆為連線中，就斷線 or 改名字 //
        if (isConnected() && pos == 0) {
            final IOSItemDialog dialog = new IOSItemDialog(this, Lst.of("Disconnect", "Change Device Name").toList());
            dialog.show(new IOSItemDialog.OnItemClicklistener() {
                @Override
                public void onItemClick(int pos, View view) {
                    switch (pos) {
                        case 0:
                            closeConnect();
                            startScan();
                            break;

                        case 1:
                            final IOSTextInputDialog dialog = new IOSTextInputDialog(DevicesActivity.this);
                            dialog.setMsg("Change Device Name.");
                            dialog.setTextType();
                            dialog.show(new IOSTextInputDialog.OnButtonClickListener() {
                                @Override
                                public void onConfirm(String updateName) {
                                    if (getConnectedDevice() != null)
                                        SpDeviceName.put(updateName, getConnectedDevice().getAddress());
                                    dialog.dismiss();
                                }

                                @Override
                                public void onCancel() {
                                    dialog.dismiss();
                                }
                            });

                            break;
                    }
                    dialog.dismiss();
                }
            });
            return;
        }

        if (isConnected()) {
            closeConnect();
            startScan();
        }

        // Comp //
        m_nCurrSelectDevicePos = pos;
        updateView(CONNECTION_STATE_CONNECTING);

        // Start to connect //
        setReconnect(true); // 開啟 reconnect
        connect(m_devices.get(pos)); // connect to..
    }


    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int pos, long id) {


//        final IOSTextInputDialog dialog = new IOSTextInputDialog(this);
//        dialog.setMsg("Change Device Name.");
//        dialog.setTextType();
//        dialog.show(new IOSTextInputDialog.OnButtonClickListener() {
//            @Override
//            public void onConfirm(String updateName) {
//                if (getConnectedDevice() != null)
//                    SpDeviceName.put(updateName, getConnectedDevice().getAddress());
//                dialog.dismiss();
//            }
//
//            @Override
//            public void onCancel() {
//                dialog.dismiss();
//            }
//        });


//        // 連線成功的 Device //
//        if (pos == CONNECTED_ITEM_POSITION) {
//            // TODO 切換多國語言
////            showAlertDialog("Disconnect with current JIMMY", " 切斷與 " + m_devices.first().getName() + " 連線", new IOSAlertDialog.OnClickEvent() {
//            showAlertDialog("Disconnect from current JIMMY", " Disconnect form  " + m_devices.first().getName(), new IOSAlertDialog.OnClickEvent() {
//                @Override
//                public void confirm() {
//                    stopConnectAction();
//                    dismissAlertDialog();
//                }
//
//                @Override
//                public void cancel() {
//                    dismissAlertDialog();
//                }
//            });
//
//        } else { // 其他 Device //
//
//            // TODO 切換多國語言
////            showAlertDialog("更換 JIMMY", "是否切換連線到" + m_devices.get(pos).getName() + StrUtil.QUESTION_MARK, new IOSAlertDialog.OnClickEvent() {
//            showAlertDialog("Switch JIMMY", "Are you sure to switch to " + m_devices.get(pos).getName() + StrUtil.QUESTION_MARK, new IOSAlertDialog.OnClickEvent() {
//                @Override
//                public void confirm() {
//                    // TODO 等待新增 切換
//                    dismissAlertDialog();
//                }
//
//                @Override
//                public void cancel() {
//                    dismissAlertDialog();
//                }
//            });
//        }
        return false;
    }

    //-------------------------------------------------------
    // Private Preference
    //-------------------------------------------------------
//
//    // 每五秒 check 一次，是否有 // TODO 暫時不啟用
//    private void registerConnectCheck() {
//
//        // 若是連線中不執行 //
//        if (m_bIsConnectState) return;
//
//        // 若有掃描到不執行 //
//        if (m_devices.size() != 0) return;
//
//        // init time counter //
//        if (m_scanCheckerCounter == null)
//            m_scanCheckerCounter = new TimeCounter();
//
//        // 執行 //
//        if (m_scanCheckerCounter == null)
//            return;
//
//        m_scanCheckerCounter.setOnCountingListener(5000, new TimeCounter.OnCountingListener() {
//            @Override
//            public void onTick(long m) {
//
//            }
//
//            @Override
//            public void onFinish() {
//                stopScan();
//                startScan();
//            }
//        });
//
//        // Time Out //
//        ThreadUtil.sleep(200);
//        registerConnectCheck();
//    }

    // After Action update View //
    private void updateView(int state) {
        switch (state) {
            case CONNECTION_STATE_CONNECTED: // 連線成功

                // 如果連線成功，要 finish Activity  //
                if (m_bFinishAfterConnectionSucc) {
                    this.finish();
                }

                m_lvCircleRing.stopAnim();

                // 連線後才註冊 long click listener //
//                m_lvDeviceListView.setOnItemLongClickListener(this);

                // Update layout //
                m_lvDeviceListView.setVisibility(View.VISIBLE);
                m_llvBleConnIcon.setVisibility(View.GONE);

                // Update ListView item //
                refreshListViewItem();
                break;

            case CONNECTION_STATE_DISCONNECTED:

                m_lvCircleRing.stopAnim();

                // 斷線後 反註冊 long click listener //
//                m_lvDeviceListView.setOnItemLongClickListener(null);

                // Update layout //
                m_llvDeviceListInfo.setVisibility(View.GONE);
                m_llvBleConnIcon.setVisibility(View.GONE);
                m_lvDeviceListView.setVisibility(View.VISIBLE);

                m_devicesAdapter.notifyChangeWithDisconnected();

                // update sub title //
                m_tvSubTitle.setText(SELECT_JIMMY_TITLE);

                break;


            case CONNECTION_STATE_CONNECTING:

                m_lvCircleRing.startAnim(CIRCLE_RING_SPEED);

                // Update layout //
                m_llvDeviceListInfo.setVisibility(View.GONE);
                m_lvDeviceListView.setVisibility(View.GONE);
                m_llvBleConnIcon.setVisibility(View.VISIBLE);

                m_devicesAdapter.notifyChangeWithDisconnected();

                break;

        }
    }

    // refresh ListView item //
    private void refreshListViewItem() {
        m_devices.moveToFirst(m_nCurrSelectDevicePos); // 將 連線上的 item switch 到 first position
        m_devicesAdapter.notifyChangeWithConnected(); // 顯示 連線成功的 item link icon
    }

    // stop all in connect action //
    private void stopConnectAction() {
        setReconnect(false);
        updateView(CONNECTION_STATE_DISCONNECTED);
        if (isConnected())
            disconnect();
    }

    //-------------------------------------------------------
    // BLE Action
    //-------------------------------------------------------
    @Override
    public void onScanDevicesUpdate(ArrayList<BluetoothDevice> devices) {
        // Update Devices //
        m_devices.clear();
        m_devices.addAll(devices);

        // Update Adapter //
        m_devicesAdapter.notifyDataSetChanged();
    }

    @Override
    public void onConnectionStateUpdate(int state) {

        switch (state) {
            case CONNECTION_STATE_CONNECTED: // 連線成功
                LogUtil.d(DevicesActivity.class, " Connection State : Connected !!");
                updateView(CONNECTION_STATE_CONNECTED);

                m_bIsConnectState = true;

                m_llvDeviceListInfo.setVisibility(View.VISIBLE);
                if (getConnectedDevice() != null)
                    m_tvMacAddress.setText(getConnectedDevice().getAddress());

                break;

            case CONNECTION_STATE_DISCONNECTED: // 斷線
                LogUtil.e(DevicesActivity.class, "  Connection State : Disconnected !!");
                updateView(CONNECTION_STATE_DISCONNECTED);

                // 斷線後，開始掃描 //
                startScan();
                setAutoScan(true);
                m_bIsConnectState = false;

                m_llvDeviceListInfo.setVisibility(View.GONE);
                break;

            case CONNECTION_STATE_DISCONNECTING: // 斷線中
                break;

            case CONNECTION_STATE_CONNECTING: // 連線中
                updateView(CONNECTION_STATE_CONNECTING);
                m_bIsConnectState = true;
                break;
        }
    }

    @Override
    public void onJimmyInfoUpdate(JimmyInfo jimmyInfo) {
        jimmyInfo.println_d(this.getClass(), " PRINT_JIMMY_INFO");
        m_tvSubTitle.setText(jimmyInfo.getDeviceName()); // 設定 device name
    }
}
