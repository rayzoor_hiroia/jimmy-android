package com.hiroia.jimmy.view.login;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.hiroia.jimmy.R;
import com.hiroia.jimmy._test._ActionCallBack;
import com.hiroia.jimmy.cs.HttpCs;
import com.hiroia.jimmy.db.sp.SpAccount;
import com.hiroia.jimmy.enums.HttpDef;
import com.hiroia.jimmy.manager.AccountManager;
import com.hiroia.jimmy.model.ModelAccount;
import com.hiroia.jimmy.view.base.BaseActivity;
import com.hiroia.jimmy.view.home.HomeActivity;
import com.library.android_common.component.common.Opt;

import com.library.android_common.util.ActivityUtil;
import com.library.android_common.util.LogUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import static com.hiroia.jimmy.view.login.LoginActivity.LogInStep.GOTO_HOME_ACTIVITY;
import static com.hiroia.jimmy.view.login.LoginActivity.LogInStep.LOG_IN_STEP1;

/**
 * Create By Ray on 2018/10/25
 */
public class LoginActivity extends BaseActivity {

    private static LoginActivity sm_self;
    private final int PICK_PHOTO_FOR_AVATAR = 1;

    // Comp //
    private ActivityUtil m_act = ActivityUtil.of(this);
    public CallbackManager callbackManager = null;
    private _ActionCallBack m_callBack;

    // Fragment //
    private LogInFragmentStep1 m_logInFragmentStep1;
    private LogInFragmentStep2 m_logInFragmentStep2;
    private HelperFragment m_helperFragment;
    private SignUpFragment m_signUpFragment;
    private SuccessFragment m_successFragment;

    //------------------------------------------------------------
    // Activity LifeCycle
    //------------------------------------------------------------
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login, true);
        sm_self = this;

        //--------------------------------------------------------
        // init
        m_logInFragmentStep1 = new LogInFragmentStep1();
        m_logInFragmentStep2 = new LogInFragmentStep2();
        m_helperFragment = new HelperFragment();
        m_signUpFragment = new SignUpFragment();
        m_successFragment = new SuccessFragment();

        //--------------------------------------------------------
        // set data
//        switchFragment(LOG_IN_STEP1);

        // 預留的可能需求，自動登入應該會做一個開關
        if (SpAccount.isAlreadyLogin())
            AccountManager.syncAccountModel(SpAccount.get());

        if (AccountManager.isAlreadyLogin()) {
            LogUtil.d(LoginActivity.class," Login Success = " + AccountManager.getAccountModel().getUserName());
            switchFragment(GOTO_HOME_ACTIVITY);
        } else
            switchFragment(LOG_IN_STEP1);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    //------------------------------------------------------------
    // Public Method
    //------------------------------------------------------------

    // Get Instance //
    public static LoginActivity getInstance() {
        return sm_self;
    }

    public void switchFragment(LogInStep logInStep) {
        switch (logInStep) {
            case LOG_IN_STEP1:
                beginFragmentTrans(m_logInFragmentStep1);
                break;

            case LOG_IN_STEP2:
                beginFragmentTrans(m_logInFragmentStep2);
                break;

            case HELPER_STEP1:
                beginFragmentTrans(m_helperFragment);
                break;

            case SIGN_UP_STEP1:
                SignUpFragment signUpFragment = new SignUpFragment();
                beginFragmentTrans(signUpFragment); // 先暫時用new Fragment ，這是依需求情況判斷而定，不是每個 Fragment 都要做這件事情
                break;

            case SUCCESS_STEP1:
                beginFragmentTrans(m_successFragment);
                break;

            case GOTO_HOME_ACTIVITY:
                m_act.start(HomeActivity.class)
                        .finish()
                        .retrieve();
                break;

        }
    }


    //------------------------------------------------------------
    // Private Method
    //------------------------------------------------------------


    private void beginFragmentTrans(Fragment fragment) {
        getFragmentManager().beginTransaction().replace(R.id.activity_login_main_framelayout, fragment).commit();
    }

    // 當登入結束後，必須在登入頁面完成的 事情 //
    public void onLoginFinish(final String msg) {
        dismissProgressDialog();
        showToast(msg);
        switchFragment(GOTO_HOME_ACTIVITY); // Go to HomeActivity
    }

    // FB Login //
    public void fbLogin() { // facebook 登入從 LogInFragmentStep1.class 呼叫，寫在 Activity 是因為 Fragment 無法被 fb server 認證, 會抓不到 full path
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile")); // 如果這裡添加需求，在 ModuleFBLogin的 JSON Schema 會需要新增
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                LogUtil.d(LoginActivity.class, " login result user id : ".concat(loginResult.getAccessToken().getUserId()));
                //------------------------------------------------------
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject jsObj, GraphResponse response) {
                        LogUtil.d(LoginActivity.class, " fb login success " + jsObj.toString());
                        if (jsObj == null) return;
                        //------------------------------------------------

                        // FB 官方 decode JSON //
                        ModelAccount m = ModelAccount.decodeFBJSON(jsObj);
                        final String sEmail = m.getFb_Email();
                        final String sFbId = m.getFb_ID();
                        final String sFbToken = loginResult.getAccessToken().getToken();
                        LogUtil.d(LoginActivity.class, " fb email : ".concat(sEmail) + "\n fb id : ".concat(sFbId) + "\n fb token : ".concat(sFbToken));

                        //------------------------------------------------
                        showProgressDialog(); // 改多國語言
                        callJimmyFBLogin(sEmail, sFbId, sFbToken); // fb 認證成功後 call jimmy fb login api
                    }
                });
                //------------------------------------------------------
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, email, gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                dismissProgressDialog();
                showToast("Login Fail");
            }

            @Override
            public void onError(FacebookException error) {
                dismissProgressDialog();
                showToast("Login Fail");
            }
        });
    }

    private void callJimmyFBLogin(final String sEmail, String sFbId, String sFbToken) {

        HttpDef.FB_LOGIN.post()
                .addParam(HttpCs.EMAIL, sEmail)
                .addParam(HttpCs.FB_ID, sFbId)
                .addParam(HttpCs.FB_TOKEN, sFbToken)
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject jsObj) {
                        LogUtil.d(LoginActivity.class, " jimmy fb login api : ".concat(response));
                        if (response.isEmpty()) return;
                        //------------------------------------

                        try {
                            if (jsObj.getBoolean(HttpCs.SUCCESS)) { // 登入成功

                                // decode JSON //
                                ModelAccount m = ModelAccount.decodeFBJimmyJSON(jsObj);
                                m.setEmail(sEmail);

                                // put Sp
                                SpAccount.put(m);

                                // Sync Manager
                                AccountManager.syncAccountModel(m);

                                //------------------------------------
                                onLoginFinish("Login Success"); // 改多國語言
                            } else {
                                dismissProgressDialog();
                                showToast("Login Fail");
                            }

                        } catch (JSONException e) {
                            showToast("Login Fail");
                            e.printStackTrace();
                        }
                    }
                });
    }


    // Profile ImageView Picker //
    public void pickImage(_ActionCallBack callback) {
        this.m_callBack = callback;
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager = Opt.of(callbackManager).getOr(CallbackManager.Factory.create());
        callbackManager.onActivityResult(requestCode, resultCode, data);

        //-----picker old function-----
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            if (m_callBack != null) {
                m_callBack.action(picturePath);
            }
        }
        //-----picker old function-----

    }

    @Override
    protected void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }

    //------------------------------------------------------------
    // Enum
    //------------------------------------------------------------

    // 每個 member 都是一個 Object 跟其他(switch..等)的是不一樣的 //
    public enum LogInStep {
        LOG_IN_STEP1,
        LOG_IN_STEP2,
        HELPER_STEP1,
        SIGN_UP_STEP1,
        SUCCESS_STEP1,
        GOTO_HOME_ACTIVITY
    }
}
