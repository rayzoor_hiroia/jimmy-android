package com.hiroia.jimmy.view.mode_e;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.ble.BLEJimmy;
import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.hiroia.jimmy.ble.module.BLEJimmyFragment;

import java.util.ArrayList;

import static com.hiroia.jimmy.ble.comp.JimmyInfo.Mode.E1;
import static com.hiroia.jimmy.ble.comp.JimmyInfo.Mode.E2;
import static com.hiroia.jimmy.ble.comp.JimmyInfo.Mode.E3;

/**
 * Created by Bruce on 2018/11/19.
 */
public class EModeEspressoStateSelectFragment_2 extends BLEJimmyFragment implements View.OnClickListener {

    // View //
    private LinearLayout m_llvBack, m_llvOneState, m_llvTwoState, m_llvThreeState;
    private TextView m_tvTitle, m_tvTitleSmall, m_tvSubTitle, m_tvEspressoTxt;
    private Button m_btnOneState, m_btnTwoState, m_btnThreeState, m_btnNext;

    // Comp //
    private JimmyInfo m_jimmyInfo = null;
    private JimmyInfo.Mode m_targetMode = null;
    private boolean m_bIsSelectedByPhone = false;

    //-----------------------------------------------------------------------
    // Fragment Life Cycle
    //-----------------------------------------------------------------------

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_emode_espresso_state_select, null);

        //-----------------------------------------------------------------
        // init
        m_llvBack = rootView.findViewById(R.id.fragment_emode_espresso_state_select_back_llv);
        m_llvOneState = rootView.findViewById(R.id.fragment_emode_espresso_state_select_espresso_one_state_llv);
        m_llvTwoState = rootView.findViewById(R.id.fragment_emode_espresso_state_select_espresso_two_state_llv);
        m_llvThreeState = rootView.findViewById(R.id.fragment_emode_espresso_state_select_espresso_three_state_llv);

        m_tvTitle = rootView.findViewById(R.id.fragment_emode_espresso_state_select_title_tv);
        m_tvTitleSmall = rootView.findViewById(R.id.fragment_emode_espresso_state_select_title_small_tv);
        m_tvSubTitle = rootView.findViewById(R.id.fragment_emode_espresso_state_select_sub_title_tv);
        m_tvEspressoTxt = rootView.findViewById(R.id.fragment_emode_espresso_state_select_espresso_txt_tv);

        m_btnOneState = rootView.findViewById(R.id.fragment_emode_espresso_state_select_espresso_one_state_btn);
        m_btnTwoState = rootView.findViewById(R.id.fragment_emode_espresso_state_select_espresso_two_state_btn);
        m_btnThreeState = rootView.findViewById(R.id.fragment_emode_espresso_state_select_espresso_three_state_btn);
        m_btnNext = rootView.findViewById(R.id.fragment_emode_espresso_state_select_next_btn);

        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);
        m_btnOneState.setOnClickListener(this);
        m_btnTwoState.setOnClickListener(this);
        m_btnThreeState.setOnClickListener(this);
        m_btnNext.setOnClickListener(this);

        resetBothTW();

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_emode_espresso_state_select_back_llv: // Back
                getParentActivity().switchStateView(getParentActivity().getEModeBrewRecordsFragment().isFromBrewPage() ? EModeActivity.EModeState.BREW_RECORDS_STATE : EModeActivity.EModeState.DAILY_STATE);
                break;

            case R.id.fragment_emode_espresso_state_select_next_btn: // Next
                getParentActivity().switchStateView(EModeActivity.EModeState.EXTRACTION_STATE);
                break;

            case R.id.fragment_emode_espresso_state_select_espresso_one_state_btn: // E1
                if (m_jimmyInfo == null || m_jimmyInfo.getCurrMode() == E1) return;
                getParentActivity().showProgressDialog();

                m_targetMode = E1;
                m_bIsSelectedByPhone = true;
                switchJimmyMode(m_jimmyInfo.switchModeTo(E1), m_onJimmySwitchModeListener);
                m_tvEspressoTxt.setText("Manual-Tare / Auto-Start & Stop from/after Espresso brew");
                break;

            case R.id.fragment_emode_espresso_state_select_espresso_two_state_btn: // E2
                if (m_jimmyInfo == null || m_jimmyInfo.getCurrMode() == E2) return;
                getParentActivity().showProgressDialog();

                m_targetMode = E2;
                m_bIsSelectedByPhone = true;
                switchJimmyMode(m_jimmyInfo.switchModeTo(E2), m_onJimmySwitchModeListener);
                m_tvEspressoTxt.setText("Auto-Tare / Auto-Start & Stop from/after Espresso brew");
                break;

            case R.id.fragment_emode_espresso_state_select_espresso_three_state_btn: // E3
                if (m_jimmyInfo == null || m_jimmyInfo.getCurrMode() == E3) return;
                getParentActivity().showProgressDialog();

                m_targetMode = E3;
                m_bIsSelectedByPhone = true;
                switchJimmyMode(m_jimmyInfo.switchModeTo(E3), m_onJimmySwitchModeListener);
                m_tvEspressoTxt.setText("Auto-Tare / Auto-Start Timer after Auto-Tare / Auto-Stop after Espresso brew");
                break;
        }
    }

    @Override
    public void onScanDevicesUpdate(ArrayList<BluetoothDevice> devices) {

    }

    @Override
    public void onConnectionStateUpdate(int state) {

    }

    @Override
    public void onJimmyInfoUpdate(JimmyInfo jimmyInfo) {
        m_jimmyInfo = jimmyInfo;

        // Update Scale Mode //
        setScaleMode(jimmyInfo.getCurrMode());
    }

    @Override
    public void onBLEInit() {

    }

    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    // 完成切換模式後 //
    private BLEJimmy.OnJimmySwitchModeListener m_onJimmySwitchModeListener = new BLEJimmy.OnJimmySwitchModeListener() {
        @Override
        public void onSwitchFinish() {
            getParentActivity().dismissProgressDialog();
        }
    };

    private EModeActivity getParentActivity() {
        return EModeActivity.getInstance();
    }

    private void setScaleMode(JimmyInfo.Mode m){
        switch (m){
            default:
            case T1:
            case T2:
            case T3:
                m_llvOneState.setBackgroundResource(R.drawable.jimmy_common_btn_background);
                m_llvTwoState.setBackgroundResource(R.drawable.jimmy_common_btn_background);
                m_llvThreeState.setBackgroundResource(R.drawable.jimmy_common_btn_background);
                break;
            //---------------------------------------
            case E1:
                if (m_targetMode != E1 && m_bIsSelectedByPhone) break;
                setStep(E1);
                getParentActivity().setCurrMode(E1);

                m_targetMode = null;
                m_bIsSelectedByPhone = false;
                break;
            case E2:
                if (m_targetMode != E2 && m_bIsSelectedByPhone) break;
                setStep(E2);
                getParentActivity().setCurrMode(E2);

                m_targetMode = null;
                m_bIsSelectedByPhone = false;
                break;
            case E3:
                if (m_targetMode != E3 && m_bIsSelectedByPhone) break;
                setStep(E3);
                getParentActivity().setCurrMode(E3);

                m_targetMode = null;
                m_bIsSelectedByPhone = false;
                break;
            //---------------------------------------
        }

    }

    private void setStep(JimmyInfo.Mode mode) {
        switch (mode) {
            case E1:
                m_llvOneState.setBackgroundResource(R.drawable.jimmy_button_background_blue);
                m_llvTwoState.setBackgroundResource(R.drawable.jimmy_common_btn_background);
                m_llvThreeState.setBackgroundResource(R.drawable.jimmy_common_btn_background);
                break;

            case E2:
                m_llvOneState.setBackgroundResource(R.drawable.jimmy_common_btn_background);
                m_llvTwoState.setBackgroundResource(R.drawable.jimmy_button_background_blue);
                m_llvThreeState.setBackgroundResource(R.drawable.jimmy_common_btn_background);
                break;

            case E3:
                m_llvOneState.setBackgroundResource(R.drawable.jimmy_common_btn_background);
                m_llvTwoState.setBackgroundResource(R.drawable.jimmy_common_btn_background);
                m_llvThreeState.setBackgroundResource(R.drawable.jimmy_button_background_blue);
                break;
        }
    }

}
