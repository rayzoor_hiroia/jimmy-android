package com.hiroia.jimmy.view.mode_p;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.model.unuse.ModeRecipeContent;
import com.hiroia.jimmy.model.unuse.ModelRecipeList;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.library.android_common.component.date.ymd.YMD;

/**
 * Created by Bruce on 2018/11/14.
 */
public class PModeRecipeContentFragment extends BaseFragment implements View.OnClickListener {

    // View //
    private LinearLayout m_llvBack, m_llvMore, m_llvItem, m_llvPourItem;
    private TextView m_tvMore, m_tvTitle, m_tvSubTitle, m_tvNote;
    private ListView m_lvPourItem;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_pmode_recipe_content, null);

        //-----------------------------------------------------------------
        // init
        m_llvBack = rootView.findViewById(R.id.fragment_pmode_content_back_llv);
        m_llvMore = rootView.findViewById(R.id.fragment_pmode_content_more_llv);
        m_llvItem = rootView.findViewById(R.id.fragment_pmode_recipe_content_item_llv);
        m_llvPourItem = rootView.findViewById(R.id.fragment_pmode_recipe_content_pour_item_llv);

        m_tvMore = rootView.findViewById(R.id.fragment_pmode_content_more_tv);
        m_tvTitle = rootView.findViewById(R.id.fragment_pmode_content_title_tv);
        m_tvSubTitle = rootView.findViewById(R.id.fragment_pmode_content_sub_title_tv);
        m_tvNote = rootView.findViewById(R.id.fragment_pmode_recipe_content_note_tv); // 待確認

        m_lvPourItem = rootView.findViewById(R.id.fragment_pmode_recipe_content_pour_item_lv);

        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);
        m_llvMore.setOnClickListener(this);

        // TODO test
        setRecipeContentlView(ModeRecipeContent.getItemDataToRecipeList(ModelRecipeList.of("Andy", YMD.nowYMD(), 20.5, "1:15", 200, 85)));

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_pmode_content_back_llv: // Back
                // 流程待確認
                break;

            case R.id.fragment_pmode_content_more_llv: // More
                // 流程待確認
                break;
        }
    }

    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private PModeActivity getParentActivity() {
        return PModeActivity.getInstance();
    }

    // TODO test
    private void setRecipeContentlView(ModeRecipeContent m) {

        PModeRecipeContentView recipeDetailView = new PModeRecipeContentView();
        PModeRecipeContentView.ItemContentView contentView = recipeDetailView.new ItemContentView(getParentActivity(), m);

        for (View item : contentView.getItemContent())
            m_llvItem.addView(item);
    }
}
