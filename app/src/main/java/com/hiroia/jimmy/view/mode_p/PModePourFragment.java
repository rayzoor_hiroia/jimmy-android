package com.hiroia.jimmy.view.mode_p;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.hiroia.jimmy.R;
import com.hiroia.jimmy.comp.comp.WaterVolStep;
import com.hiroia.jimmy.view.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bruce on 2018/11/14.
 */
public class PModePourFragment extends BaseFragment implements View.OnClickListener {

    // View //
    private LinearLayout m_llvBack, m_llvDone, m_llvWaterProgress, m_llvScheduledSec;
    private TextView m_tvDone, m_tvTitle, m_tvTitleSmall, m_tvSubTitle, m_tvScheduledSec, m_tvCumulativeWater, m_tvWaterSpeed, m_tvCurrentFlow, m_tvTargetFlow;

    private PieChart m_pcPieChart;
    private WaterVolStep m_waterVolStep = new WaterVolStep();
    protected String[] mParties = new String[] {
            "Party A", "Party B", "Party C", "Party D", "Party E", "Party F", "Party G", "Party H",
            "Party I", "Party J", "Party K", "Party L", "Party M", "Party N", "Party O", "Party P",
            "Party Q", "Party R", "Party S", "Party T", "Party U", "Party V", "Party W", "Party X",
            "Party Y", "Party Z"
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_pmode_pour, null);

        //-----------------------------------------------------------------
        // init
        m_pcPieChart = rootView.findViewById(R.id.fragment_pmode_pour_pie_chart_pc);

        m_llvBack = rootView.findViewById(R.id.fragment_pmode_pour_back_llv);
        m_llvDone = rootView.findViewById(R.id.fragment_pmode_pour_done_llv);
        m_llvWaterProgress = rootView.findViewById(R.id.fragment_pmode_pour_water_progress_llv);
        m_llvScheduledSec = rootView.findViewById(R.id.fragment_pmode_pour_scheduled_sec_llv);


        m_tvDone = rootView.findViewById(R.id.fragment_pmode_pour_done_tv);
        m_tvTitle = rootView.findViewById(R.id.fragment_pmode_pour_title_tv);
        m_tvTitleSmall = rootView.findViewById(R.id.fragment_pmode_pour_title_small_tv);
        m_tvSubTitle = rootView.findViewById(R.id.fragment_pmode_pour_sub_title_tv);
        m_tvCumulativeWater = rootView.findViewById(R.id.fragment_pmode_pour_cumulative_water_tv);
        m_tvScheduledSec = rootView.findViewById(R.id.fragment_pmode_pour_scheduled_sec_tv);
        m_tvWaterSpeed = rootView.findViewById(R.id.fragment_pmode_pour_water_speed_tv);
        m_tvCurrentFlow = rootView.findViewById(R.id.fragment_pmode_pour_current_flow_rate_tv);
        m_tvTargetFlow = rootView.findViewById(R.id.fragment_pmode_pour_target_flow_rate_tv);



        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);
        m_llvDone.setOnClickListener(this);


        setPieChartData();
        return rootView;
    }


    // 設定 結果參數至 Chart View //
    private void setPieChartData() {


        ArrayList<Entry> yVals1 = new ArrayList<>();

        for (int i = 0; i < 3 + 1; i++) {
            yVals1.add(new Entry((float) (Math.random() * 100) + 100 / 5, i));
        }

        ArrayList<String> xVals = new ArrayList<>();

        for (int i = 0; i < 3 + 1; i++)
            xVals.add(mParties[i % mParties.length]);


        List<PieEntry> pieEntryList = new ArrayList<>();
        pieEntryList.add(new PieEntry(30f,"aaa"));
        pieEntryList.add(new PieEntry(70f,"bbb"));
//        for (Pair<Integer, Integer> p : steps.getSteps().toList()) {
//            pieEntryList.add(new PieEntry(p.k(), p.v()));
//        }

        PieDataSet dataSet = new PieDataSet(pieEntryList, "Label");

        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(getResources().getColor(R.color.jimmyBlue));
        colors.add(getResources().getColor(R.color.buttonGray));
        dataSet.setColors(colors);

        PieData pieData = new PieData(dataSet);
        pieData.setDrawValues(true);
        m_pcPieChart.setDrawHoleEnabled(true);
        m_pcPieChart.setHoleColor(Color.WHITE);
        m_pcPieChart.setHoleRadius(60f);
        m_pcPieChart.setData(pieData);
        m_pcPieChart.invalidate();

        //--------------------------------------------------
//        List<Entry> entries = new ArrayList<>();
//        for (Pair<Integer, Integer> p : steps.getSteps().toList()) {
//            entries.add(new Entry(p.k(), p.v()));
//        }

        // 設定內部參數 //
//        LineDataSet dataSet = new LineDataSet(entries, StrUtil.EMPTY);
//        dataSet.setValueTextColor(Color.TRANSPARENT);
//        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        dataSet.setCubicIntensity(0.2f);
//        dataSet.setDrawFilled(true);
//        dataSet.setDrawCircles(false);
//        dataSet.setLineWidth(0);
//        dataSet.setCircleRadius(4f);
//        dataSet.setCircleColor(getResources().getColor(R.color.jimmyBlue));
//        dataSet.setHighLightColor(getResources().getColor(R.color.jimmyBlue));
//        dataSet.setColor(getResources().getColor(R.color.jimmyBlue));
//        dataSet.setFillColor(getResources().getColor(R.color.jimmyBlue));
//        dataSet.setFillAlpha(500);

//        LineData lineData = new LineData(dataSet);
//        m_lcResultChart.setData(lineData);
//        m_lcResultChart.invalidate();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_pmode_pour_back_llv: // Back
                // 流程待確認
                break;

            case R.id.fragment_pmode_pour_done_llv: // Done
                getParentActivity().switchStateView(PModeActivity.PModeState.RECIPE_CONTENT);
                break;
        }
    }

    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private PModeActivity getParentActivity() {
        return PModeActivity.getInstance();
    }
}
