package com.hiroia.jimmy.view.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.cs.HttpCs;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.library.android_common.component.common.Opt;
import com.library.android_common.component.date.hms.S;
import com.library.android_common.enums.HttpDef;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.json.JSONObject;

import static com.hiroia.jimmy.cs.HttpCs.APPLELANGUAGE;
import static com.library.android_common.constant.HttpCs.EMAIL;


/**
 * Create By Ray on 2018/10/26
 */
public class HelperFragment extends BaseFragment implements View.OnClickListener {

    private LinearLayout m_llvCancel, m_llvEmail, m_llvEmailInfo;
    private TextView m_tvTitle, m_tvSubTitle, m_tvText, m_tvPromptText, m_tvDisplayEmail, m_tvAfterText;
    private EditText m_etEmail;
    private Button m_btnSend, m_btnOk;

    private final String FORGOT_PASSWORD_SUCCESS = "true";
    private final String FORGOT_PASSWORD_FAIL = "false";
    private String m_content = StrUtil.EMPTY;

    //-----------------------------------------------------------
    // Fragment Life Cycle
    //-----------------------------------------------------------

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-------------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_helper, container, false);

        //-------------------------------------------------------------------
        // init view
        m_llvCancel = rootView.findViewById(R.id.fragment_helper_cancel_btn_llv);
        m_llvEmail = rootView.findViewById(R.id.fragment_helper_email_llv);
        m_llvEmailInfo = rootView.findViewById(R.id.fragment_helper_email_info_llv);

        m_tvTitle = rootView.findViewById(R.id.fragment_helper_title_tv);
        m_tvSubTitle = rootView.findViewById(R.id.fragment_helper_sub_title_tv);
        m_tvText = rootView.findViewById(R.id.fragment_helper_text_tv);
        m_tvPromptText = rootView.findViewById(R.id.fragment_helper_prompt_text_tv);
        m_tvDisplayEmail = rootView.findViewById(R.id.fragment_helper_display_email_tv);
        m_tvAfterText = rootView.findViewById(R.id.fragment_helper_after_text_tv);

        m_etEmail = rootView.findViewById(R.id.fragment_helper_email_et);


        m_btnSend = rootView.findViewById(R.id.fragment_helper_send_btn);
        m_btnOk = rootView.findViewById(R.id.fragment_helper_ok_btn);

        //-------------------------------------------------------------------
        // set data
        m_llvCancel.setOnClickListener(this);
        m_btnSend.setOnClickListener(this);
        m_btnOk.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_helper_cancel_btn_llv: // Cancel btn
                clearTextInput();
                getParentActivity().switchFragment(LoginActivity.LogInStep.LOG_IN_STEP2);
                break;

            case R.id.fragment_helper_send_btn: // Send btn
                resetPassword();
                break;

            case R.id.fragment_helper_ok_btn: // OK btn
                clearTextInput();
                getParentActivity().switchFragment(LoginActivity.LogInStep.LOG_IN_STEP1);
                break;
        }
    }


    //-----------------------------------------------------------
    // Private Preference
    //-----------------------------------------------------------

    private void resetPassword() {

        m_content = m_etEmail.getText().toString().trim();

        if (m_content.isEmpty()) {
            getParentActivity().showToast("Please enter your email"); // 改多國語言
            return;
        }

        getParentActivity().showProgressDialog();

        HttpDef.RESET_PASSWORD.post()
                .addParam(EMAIL, m_content)
                .addParam(APPLELANGUAGE, HttpCs.APPLELANGUAGE_ZH_TW)
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject jsObj) {

                        // To avoid response is null or empty //
                        if (response == null || response.isEmpty()){
                            LogUtil.e(HelperFragment.class, HttpDef.RESET_PASSWORD.getTag() + " response is Null or Empty.");
                            return;
                        }

                        // Start to decode JSON //
                        decodeJSON(jsObj);
                    }
                });
    }

    //---------------------------------------------------------------
    // Public Method
    //---------------------------------------------------------------

    public LoginActivity getParentActivity() {
        return LoginActivity.getInstance();
    }

    //---------------------------------------------------------------
    // Private Method
    //---------------------------------------------------------------
    private void clearTextInput() {
        m_etEmail.setText(StrUtil.EMPTY);
    }

    private void decodeJSON(JSONObject jsObj) {
        String success = Opt.ofJson(jsObj, HttpCs.SUCCESS).get();

        switch (success) {
            case FORGOT_PASSWORD_SUCCESS:
                getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                m_tvDisplayEmail.setText(m_content);

                m_llvEmail.setVisibility(View.GONE);
                m_llvEmailInfo.setVisibility(View.VISIBLE);
                m_btnSend.setVisibility(View.GONE);
                m_btnOk.setVisibility(View.VISIBLE);
                break;

            case FORGOT_PASSWORD_FAIL:
                getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                getParentActivity().showToast("Fail");
                break;

            default:
                getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                getParentActivity().showToast("Error");
                break;
        }
    }
}