package com.hiroia.jimmy.view.home;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.hiroia.jimmy.ble.module.BLEJimmyActivity;

import com.hiroia.jimmy.cs.JimmyCs;
import com.hiroia.jimmy.view.device.DevicesActivity;
import com.hiroia.jimmy.view.mode_e.EModeActivity;
import com.hiroia.jimmy.view.mode_j.JModeActivity;
import com.hiroia.jimmy.view.mode_p.PModeActivity;
import com.hiroia.jimmy.view.mode_t.TModeActivity;
import com.hiroia.jimmy.view.setting.SettingActivity;
import com.library.android_common.util.ActivityUtil;
import com.library.android_common.util.LogUtil;


import java.util.ArrayList;

/**
 * Create By Ray on 2018/10/26
 */
public class HomeActivity extends BLEJimmyActivity implements View.OnClickListener {

    private LinearLayout m_llvEMode, m_llvPMode, m_llvTMode, m_llvJMode, m_llvJimmyTab, m_llvSetTab;

    //-------------------------------------------------------------
    // Activity Life Cycle
    //-------------------------------------------------------------
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_only_e_t_home, true); // P、J Mode 要啟用時，請更換成 activity_home

        //------------------------------------------
        // init view
        m_llvEMode = findViewById(R.id.activity_home_e_mode_llv);
        m_llvPMode = findViewById(R.id.activity_home_p_mode_llv);
        m_llvTMode = findViewById(R.id.activity_home_t_mode_llv);
        m_llvJMode = findViewById(R.id.activity_home_j_mode_llv);

        m_llvJimmyTab = findViewById(R.id.activity_home_jimmy_devices_llv);
        m_llvSetTab = findViewById(R.id.activity_home_setting_llv);

        //-------------------------------------------------
        // set view
        m_llvEMode.setOnClickListener(this);
        m_llvPMode.setOnClickListener(this);
        m_llvTMode.setOnClickListener(this);
        m_llvJMode.setOnClickListener(this);
        m_llvJimmyTab.setOnClickListener(this);
        m_llvSetTab.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_home_e_mode_llv: // EMode
                ActivityUtil.of(HomeActivity.this)
                        .start(EModeActivity.class)
                        .retrieve();
                break;

            case R.id.activity_home_p_mode_llv: // PMode
                ActivityUtil.of(HomeActivity.this)
                        .start(PModeActivity.class)
                        .retrieve();
                break;

            case R.id.activity_home_t_mode_llv: // TMode
                ActivityUtil.of(HomeActivity.this)
                        .start(TModeActivity.class)
                        .retrieve();
                break;

            case R.id.activity_home_j_mode_llv: // JMode
                ActivityUtil.of(HomeActivity.this)
                        .start(JModeActivity.class)
                        .retrieve();
                break;

            case R.id.activity_home_jimmy_devices_llv: // Jimmy Tab
                ActivityUtil.of(HomeActivity.this)
                        .start(DevicesActivity.class) // Jimmy Tab
                        .retrieve();
                break;

            case R.id.activity_home_setting_llv: // Setting Tba
                ActivityUtil.of(HomeActivity.this)
                        .start(SettingActivity.class)
                        .retrieve();
                break;
        }
    }

    @Override
    public void onScanDevicesUpdate(ArrayList<BluetoothDevice> devices) {

    }

    @Override
    public void onConnectionStateUpdate(int state) {

    }

    @Override
    public void onJimmyInfoUpdate(JimmyInfo jimmyInfo) {

    }

    @Override
    public void onBLEInit() {

    }
}
