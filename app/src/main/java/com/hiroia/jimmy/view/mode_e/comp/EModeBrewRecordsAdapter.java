package com.hiroia.jimmy.view.mode_e.comp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.model.ModelDateRecord;
import com.hiroia.jimmy.model.ModelEModeRecord;
import com.hiroia.jimmy.model.unuse.ModelBrewRecord;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

/**
 * Created by Bruce on 2018/11/22.
 */
public class EModeBrewRecordsAdapter extends BaseAdapter {

    // View //
    private Activity m_ctx;
    private ViewHolder m_viewHolder;

    // Comp //
    private ArrayList<ModelDateRecord> m_records;
    private final String STR_RECORDS = "Records";

    //---------------------------------------------------------
    //  Construct
    //---------------------------------------------------------
    public EModeBrewRecordsAdapter(Activity activity, ArrayList<ModelDateRecord> records) {
        m_ctx = activity;
        m_records = records;
    }

    @Override
    public int getCount() {
        return m_records.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        view = LayoutInflater.from(m_ctx).inflate(R.layout.comp_emode_brew_records_list, null);

        m_viewHolder = new ViewHolder(
                (LinearLayout) view.findViewById(R.id.comp_emode_brew_records_list_item_llv),
                (TextView) view.findViewById(R.id.comp_emode_brew_records_list_item_name_tv),
                (TextView) view.findViewById(R.id.comp_emode_brew_records_list_item_records_tv),
                (TextView) view.findViewById(R.id.comp_emode_brew_records_list_item_date_tv));

        //-----------------------------------------------------------------
        // set data
//        ModelEModeRecord m = m_records.get(position);
//        m_viewHolder.m_tvName.setText(m.getDeviceName());
//        m_viewHolder.m_tvRecords.setText(m_records.size() + STR_RECORDS);
//        m_viewHolder.m_tvDate.setText(m.getYMD().toSlashString());

        ModelDateRecord m = m_records.get(position);
        m_viewHolder.m_tvDate.setText(m.getYmd().toSlashString());
        m_viewHolder.m_tvRecords.setText(m.getSize() + StrUtil.spaceOf(STR_RECORDS));
        m_viewHolder.m_tvName.setText(m.getDeviceName());

        return view;
    }


    //---------------------------------------------------------
    // ViewHolder
    //---------------------------------------------------------

    class ViewHolder {

        private LinearLayout m_llvItem;
        private TextView m_tvName, m_tvRecords, m_tvDate;

        ViewHolder(LinearLayout item, TextView name, TextView records, TextView date) {
            this.m_llvItem = item;
            this.m_tvName = name;
            this.m_tvRecords = records;
            this.m_tvDate = date;
        }
    }
}
