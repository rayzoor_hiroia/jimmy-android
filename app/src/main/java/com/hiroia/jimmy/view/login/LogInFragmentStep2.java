package com.hiroia.jimmy.view.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.BuildConfig;
import com.hiroia.jimmy.R;
import com.hiroia.jimmy.cs.HttpCs;
import com.hiroia.jimmy.db.sp.SpAccount;
import com.hiroia.jimmy.manager.AccountManager;
import com.hiroia.jimmy.model.ModelAccount;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.library.android_common.component.common.Pair;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.hms.S;
import com.library.android_common.enums.HttpDef;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Create By Ray on 2018/10/25
 */
public class LogInFragmentStep2 extends BaseFragment implements View.OnClickListener {


    private LinearLayout m_llvCancelBtn;
    private TextView m_tvTitle, m_tvSubTitle, m_tvForgotPassword, m_tvErrorMsg;
    private EditText m_etEmail, m_etPassword;
    private Button m_btnLogin;

    //-----------------------------------------------------
    // Fragment Life Cycle
    //-----------------------------------------------------

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-------------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_login_step2, container, false);

        //-------------------------------------------------------------------
        // init view
        m_llvCancelBtn = rootView.findViewById(R.id.fragment_login_step2_cancel_btn_llv);

        m_tvTitle = rootView.findViewById(R.id.fragment_login_step2_title_tv);
        m_tvSubTitle = rootView.findViewById(R.id.fragment_login_step2_sub_title_tv);
        m_tvForgotPassword = rootView.findViewById(R.id.fragment_login_step2_forgot_password_tv);
        m_tvErrorMsg = rootView.findViewById(R.id.fragment_login_step2_error_msg_tv);

        m_etEmail = rootView.findViewById(R.id.fragment_login_step2_email_et);
        m_etPassword = rootView.findViewById(R.id.fragment_login_step2_password_et);

        m_btnLogin = rootView.findViewById(R.id.fragment_login_step2_login_btn);

        //-------------------------------------------------------------------
        // init view
        m_llvCancelBtn.setOnClickListener(this);
        m_tvForgotPassword.setOnClickListener(this);
        m_btnLogin.setOnClickListener(this);

        //-------------------------------------------------------------------
        // Debug mode //

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
//        // 測試模式下，才會執行 //
//        if (BuildConfig.DEBUG){
//            m_etEmail.setText("raymond@Hiroia.com");
//            m_etPassword.setText("bbbbbb");
//        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_login_step2_cancel_btn_llv: // Cancel
                clearData();
                getParentActivity().switchFragment(LoginActivity.LogInStep.LOG_IN_STEP1);
                break;

            case R.id.fragment_login_step2_forgot_password_tv: // Forgot Password
                clearData();
                getParentActivity().switchFragment(LoginActivity.LogInStep.HELPER_STEP1);
                break;

            case R.id.fragment_login_step2_login_btn: // Login
                // 確認是否有輸入值 //
                if (checkIsNotEmpty())
                    return;

                getParentActivity().showProgressDialog();
                emailLogin();
                break;

        }
    }

    //---------------------------------------------------------------
    // Public Method
    //---------------------------------------------------------------
    public LoginActivity getParentActivity() {
        return LoginActivity.getInstance();
    }

    //---------------------------------------------------------------
    // Private Method
    //---------------------------------------------------------------

    private void clearData() {
        m_etEmail.setText(StrUtil.EMPTY);
        m_etPassword.setText(StrUtil.EMPTY);
        m_tvErrorMsg.setText(StrUtil.EMPTY);
    }

    private boolean checkIsNotEmpty() {
        Lst<Pair<String, String>> ps = Lst.of(
                        Pair.of(m_etEmail.getText().toString().trim(), "Please enter your email."),
                        Pair.of(m_etPassword.getText().toString().trim(), "Please enter your password."));

        for (Pair<String, String> p : ps.toList())
            if (p.k().isEmpty()){
                m_tvErrorMsg.setText(p.v());
                return true;
            }
        return false;
    }


    private void emailLogin() {
        String email = m_etEmail.getText().toString().trim();
        String password = m_etPassword.getText().toString().trim();

        // 1. 判斷帳密是否都不為空（都有輸入值）
        // 帳密沒填寫，是要跳 showToast or set ErrorTextView

        // 2. 按下登入 >> showProgressDialog >> Call API
        // 成功： 儲存資料 put Sp、sync Manager，View 層：dismissProgressDialog & showToast & 跳轉頁面(Home 頁)
        // 失敗： 登入失敗，View 層：dismissProgressDialog & set ErrorTexView

        HttpDef.LOGIN.post()
                .addParam(HttpCs.EMAIL, email)
                .addParam(HttpCs.PASSWORD, password)
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject jsObj) {
                        if (response == null || response.isEmpty()){
                            LogUtil.e(LogInFragmentStep2.class, HttpDef.LOGIN.getTag() + " response is Null or Empty.");
                            return;
                        }

                        // Log response content //
                        LogUtil.d(LogInFragmentStep2.class, "Login API = ".concat(response));
                        getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);

                        // Start to decode JSON //
                        try {
                            if (jsObj.getBoolean(HttpCs.SUCCESS)) {

                                // decode JSON //
                                ModelAccount m = ModelAccount.decodeEmailJSON(jsObj);

                                // 儲存資料 put Sp
                                SpAccount.put(m);

                                // Sync Manager
                                AccountManager.syncAccountModel(m);

                                // View 層 dismissProgressDialog & showToast & 跳轉頁面(Home 頁)
                                getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                                getParentActivity().onLoginFinish("Login Success"); //TODO 改多國語言

                            } else {
                                // View 層 dismissProgressDialog & set ErrorText
                                getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                                m_tvErrorMsg.setText("Login Fail"); //TODO 改多國語言
                            }

                        }catch (JSONException e) {
                            getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
                            e.getMessage();
                        }
                    }
                });
        getParentActivity().dismissProgressDialog(S.Positive.THREE_SEC);
    }
}
