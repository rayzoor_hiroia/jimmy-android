package com.hiroia.jimmy.view.base;

import android.app.Fragment;

/**
 * Create By Ray on 2018/10/24
 */
public class BaseFragment extends Fragment{
    public String getSimpleClassName(){
        return getClass().getSimpleName();
    }
}
