package com.hiroia.jimmy.view.base;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RawRes;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.hiroia.jimmy.BuildConfig;
import com.hiroia.jimmy.R;
import com.hiroia.jimmy.comp.callback.OnPermissionCheckListener;
import com.hiroia.jimmy.comp.comp.PermissionChecker;
import com.hiroia.jimmy.comp.view.dialog.IOSAlertDialog;
import com.hiroia.jimmy.comp.view.dialog.IOSProgressDialog;
import com.hiroia.jimmy.cs.JimmyCs;
import com.hiroia.jimmy.enums.MultiMsg;
import com.library.android_common.component.TimeCounter;
import com.library.android_common.component.date.hms.S;
import com.library.android_common.util.ActivityUtil;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;
import com.library.android_common.util.ToastUtil;

import java.sql.Time;

/**
 * Create By Ray on 2018/10/24
 */
public class BaseActivity extends AppCompatActivity {

    // Final Member //
    public final int GRAVITY_FAB_TOP = Gravity.TOP;
    public final int GRAVITY_FAB_BOTTOM = Gravity.BOTTOM;

    // View //
    private FloatingActionButton m_fab, m_fab2;
    private EditText m_edNumInput;
    private FrameLayout m_baseLayout;
    private IOSProgressDialog m_progressDialog;
    private IOSAlertDialog m_alertDialog;
    private LinearLayout m_fabLayout;

    // Comp //
    private ActivityUtil m_actManager = ActivityUtil.of(this);
    private OnPermissionCheckListener m_onPermissionCheckListner = null;

    //------------------------------------------------------------
    // Activity Life Cycle
    //------------------------------------------------------------
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // default setting //
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);// 保持手機螢幕清醒
        requestWindowFeature(Window.FEATURE_NO_TITLE); // full screen

        // init view //
        View fabView = LayoutInflater.from(this).inflate(R.layout.comp_debug_fab, null);
        m_baseLayout = fabView.findViewById(R.id.comp_debug_fab_base_view_flt);
        m_fab = fabView.findViewById(R.id.comp_debug_fab_base_fab);
        m_fab2 = fabView.findViewById(R.id.comp_debug_fab_base_fab2);
        m_edNumInput = fabView.findViewById(R.id.comp_debug_fab_base_inputView_ed);
        m_fabLayout = fabView.findViewById(R.id.comp_debug_fab_view_llv);

        m_progressDialog = new IOSProgressDialog(this);
        m_alertDialog = new IOSAlertDialog();
    }

    //-----------------------------------------------------------
    // ActivityUtil
    //-----------------------------------------------------------
    public ActivityUtil getActManager() {
        if (m_actManager == null)
            return ActivityUtil.of(this);
        return m_actManager;
    }

    //------------------------------------------------------------
    // Set Content View
    //------------------------------------------------------------

    public void setContentView(int layout, boolean enableDebugFab) {
        View baseView = LayoutInflater.from(this).inflate(layout, null);
        setContentView(baseView, enableDebugFab);
    }

    public void setContentView(View baseView, boolean enableDebugFab) {

        // Floating Action Button Mode //
        if (enableDebugFab && BuildConfig.DEBUG && JimmyCs.IS_SETTING_MODE_ON) {
            m_baseLayout.addView(baseView, 0);
            super.setContentView(m_baseLayout);
            return;
        }

        // Normal Mode //
        super.setContentView(baseView);
    }

    public FloatingActionButton getFab() { // Get Fab Instance
        return m_fab;
    }

    public FloatingActionButton getFab2() { // Get Fab2 Instance
        return m_fab2;
    }

    public EditText getNumInput() {
        return m_edNumInput;
    }

    public String getSimpleClassName(){
        return getClass().getSimpleName();
    }

    //-----------------------------------------------------------
    // Toast
    //-----------------------------------------------------------
    public void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void showToastLong(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    // Only show on debug mode. //
    public void showDebugToast(String msg) {
        if (BuildConfig.DEBUG)
            showToast(msg);
    }

    //-----------------------------------------------------------
    // Dialog
    //-----------------------------------------------------------

    // Progress Dialog //
    public void showProgressDialog(S.Positive delaySec) {
        TimeCounter tc = new TimeCounter();
        tc.setOnCountingListener(delaySec.sec().millySec(), new TimeCounter.OnCountingListener() {
            @Override
            public void onTick(long m) {

            }

            @Override
            public void onFinish() {
                showProgressDialog(StrUtil.EMPTY);
            }
        });
    }

    public void showProgressDialog() {
        showProgressDialog(StrUtil.EMPTY);
    }

    public void showProgressDialog(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressDialogMsg(msg);
                m_progressDialog.show();
            }
        });
    }

    public void dismissProgressDialog(S.Positive delaySec) {
        dismissProgressDialog(delaySec.sec().millySec());
    }

    public void showAutoDismissProgressDialog(String msg, S.Positive delaySec) {
        showProgressDialog(msg);
        dismissProgressDialog(delaySec.sec().millySec());
    }

    public void showAutoDismissProgressDialog(S.Positive delaySec) {
        showProgressDialog();
        dismissProgressDialog(delaySec.sec().millySec());
    }

    public void setProgressDialogMsg(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                m_progressDialog.setMessage(msg);
            }
        });
    }

    private void dismissProgressDialog(final long dismissTime) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TimeCounter tc = new TimeCounter();
                tc.setOnCountingListener(dismissTime, new TimeCounter.OnCountingListener() {
                    @Override
                    public void onTick(long m) {

                    }

                    @Override
                    public void onFinish() {
                        dismissProgressDialog();
                    }
                });
            }
        });
    }

    public void dismissProgressDialog() {
        if (m_progressDialog == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_progressDialog.isShowing())
                    m_progressDialog.dismiss();
            }
        });
    }

    public boolean isProgressDialogShowing() {
        return m_progressDialog != null && m_progressDialog.isShowing();
    }

    public IOSProgressDialog getProgressDialog() {
        return m_progressDialog;
    }

    //------------------------------------------------------------
    // IOSAlertDialog Dialog //
    public void showAlertDialog(String title, String msg, IOSAlertDialog.OnClickEvent clickEventListener) {
        m_alertDialog.setContent(this, title, msg, "Confirm", "Cancel"); // 改多國語言
        m_alertDialog.show(clickEventListener);
    }

    public void dismissAlertDialog() {
        m_alertDialog.dismiss();
    }

    public IOSAlertDialog getAlertDialog() {
        return m_alertDialog;
    }

    //----------------------------------------------------------
    // Service Checker
    //----------------------------------------------------------
    protected void checkLocationService() { // 檢查定位 Service 是否開啟

        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        //------------------------------------------------------
        // init
        boolean isGPSEnable = false;
        boolean isNetWorkEnable = false;

        try {
            isGPSEnable = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            LogUtil.e(BaseActivity.class, " checkLocationService(), Error isGPSEnable ");
        }

        try {
            isNetWorkEnable = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            LogUtil.e(BaseActivity.class, " checkLocationService(), Error isNetWorkEnable ");
        }

        //------------------------------------------------------
        // alert user
        if (!isGPSEnable && !isNetWorkEnable) {
            final IOSAlertDialog dialog = new IOSAlertDialog(this, MultiMsg.CHECK_LOCATION_SERVICE_TITLE.msg(), MultiMsg.CHECK_LOCATION_SERVICE_MSG.msg());
            dialog.show(new IOSAlertDialog.OnClickEvent() {
                @Override
                public void confirm() {
                    Intent it = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    BaseActivity.this.startActivity(it);
                    dialog.dismiss();
                }

                @Override
                public void cancel() {
                    dialog.dismiss();
                }
            });
        }
    }


    protected void checkBluetoothService() { // 檢查藍牙 Service 是否開啟
        boolean hasBLE = getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
        BluetoothAdapter bleAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!hasBLE) {
            // 本裝置不支援 藍牙
            ToastUtil.show(MultiMsg.DEVICE_BLE_NOT_SUPPORT.msg());
        }

        if (!bleAdapter.isEnabled()) {

            final IOSAlertDialog dialog = new IOSAlertDialog(this, MultiMsg.DEVICE_BLE_IS_TURN_OFF_TITLE.msg(), MultiMsg.DEVICE_BLE_IS_TURN_OFF_MSG.msg());
            dialog.show(new IOSAlertDialog.OnClickEvent() {
                @Override
                public void confirm() {
                    Intent it = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
                    BaseActivity.this.startActivity(it);
                    dialog.dismiss();
                }

                @Override
                public void cancel() {
                    dialog.dismiss();
                }
            });
        }
    }

    //-----------------------------------------------------------
    // Permission
    //-----------------------------------------------------------

    public boolean _checkWritePermission() {

        boolean hasPermission = PermissionChecker.hasStroagePermission(this);
        if (!hasPermission) {
            this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PermissionChecker.REQUEST_EXTERNAL_STROAGE); //Any number
        }

        return hasPermission;
    }

    // Location Permission //
    public void _checkBTPermissions() {
        if (!PermissionChecker.hasLocationPermission(this)) {
            this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    PermissionChecker.REQUEST_BLE_CODE); //Any number
        }
    }

    // Camera Permission //
    public void _checkQRPermission() {
        if (!PermissionChecker.hasCameraPermission(this)) {
            this.requestPermissions(new String[]{Manifest.permission.CAMERA}, PermissionChecker.REQUEST_CAMERA);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PermissionChecker.REQUEST_BLE_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    return;

                if (!this.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    final IOSAlertDialog dialog = new IOSAlertDialog();
                    dialog.setContent(this, MultiMsg.PERMISSION_CHECK_LOCATION_TITLE.msg(), MultiMsg.PERMISSION_CHECK_LOCATION_MSG.msg());
                    dialog.show(new IOSAlertDialog.OnClickEvent() {
                        @Override
                        public void confirm() {
                            // 前往系統開啟 權限 頁面
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", getPackageName(), null));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            dialog.dismiss();
                        }

                        @Override
                        public void cancel() {
                            dialog.dismiss();
                        }
                    });
                }
                break;


            case PermissionChecker.REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) // 代表 user 取消
                    return;

                if (!this.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    final IOSAlertDialog dialog = new IOSAlertDialog();
                    dialog.setContent(this, MultiMsg.CAMERA_PERMISSIONS.msg(), MultiMsg.CAMERA_PERMISSIONS_MSG.msg());
                    dialog.show(new IOSAlertDialog.OnClickEvent() {
                        @Override
                        public void confirm() {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", getPackageName(), null));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            dialog.dismiss();
                        }

                        @Override
                        public void cancel() {
                            dialog.dismiss();
                        }
                    });
                }


            case PermissionChecker.REQUEST_EXTERNAL_STROAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    return;

                if (!this.shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    final IOSAlertDialog dialog = new IOSAlertDialog();
                    dialog.setContent(this, MultiMsg.STORAGE_PERMISSION.msg(), MultiMsg.STORAGE_PERMISSION_MSG.msg());
                    dialog.show(new IOSAlertDialog.OnClickEvent() {
                        @Override
                        public void confirm() {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", getPackageName(), null));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            dialog.dismiss();
                        }

                        @Override
                        public void cancel() {
                            dialog.dismiss();
                        }
                    });
                }
                break;
        }
    }


}
