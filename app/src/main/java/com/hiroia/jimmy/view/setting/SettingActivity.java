package com.hiroia.jimmy.view.setting;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.hiroia.jimmy.R;
import com.hiroia.jimmy._test._ActionCallBack;
import com.hiroia.jimmy.view.base.BaseActivity;
import com.library.android_common.component.common.Opt;

import static com.hiroia.jimmy.view.setting.SettingActivity.SettingState.SETTING_PAGE;

/**
 * Create By Ray on 2018/10/29
 */
public class SettingActivity extends BaseActivity {

    private static SettingActivity sm_self;
    private SettingFragment m_settingFragment;
    private ProfileEditFragment m_profileEditFragment;
    private ChangePasswordFragment m_changePasswordFragment;

    private _ActionCallBack m_callBack;
    private final int PICK_PHOTO_FOR_AVATAR = 1;

    //--------------------------------------------------------
    // Construct
    //--------------------------------------------------------

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings, false);
        sm_self = this;

        m_settingFragment = new SettingFragment();
        m_profileEditFragment = new ProfileEditFragment();
        m_changePasswordFragment = new ChangePasswordFragment();

        switchFragment(SETTING_PAGE);
    }

    //------------------------------------------------------------
    // Private Method
    //------------------------------------------------------------

    private void beginFragmentTrans(Fragment fragment){
        getFragmentManager().beginTransaction().replace(R.id.activity_setting_flt, fragment).commit();
    }

    //------------------------------------------------------------
    // Public Method
    //------------------------------------------------------------

    public static SettingActivity getInstance() {
        return sm_self;
    }

    public void switchFragment(SettingState settingState) {
        switch (settingState) {
            case SETTING_PAGE:
                beginFragmentTrans(m_settingFragment);
                break;

            case PROFILE_EDIT:
                beginFragmentTrans(m_profileEditFragment);
                break;

            case CHANGE_PASSWORD:
                beginFragmentTrans(m_changePasswordFragment);
                break;
        }
    }


    // Profile ImageView Picker //
    public void pickImage(_ActionCallBack callback) {
        this.m_callBack = callback;
        Intent intent = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //-----picker old function-----
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            if(m_callBack != null) {
                m_callBack.action(picturePath);
            }
        }
        //-----picker old function-----

    }



    //------------------------------------------------------------
    // Enum
    //------------------------------------------------------------

    public enum SettingState{
        SETTING_PAGE,
        PROFILE_EDIT,
        CHANGE_PASSWORD
    }

}
