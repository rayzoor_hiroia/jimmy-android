package com.hiroia.jimmy.view.privacy;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.hiroia.jimmy.file.Pub;
import com.library.android_common.component.common.Opt;

/**
 * Created by Bruce on 2019/2/13.
 */
public class TermsFragment extends Fragment {

    private final int DEFAULT_MARGIN = 8;
    private WebView m_webView = null;
    private String m_sHtmlPath;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        m_sHtmlPath = Pub.LOCAL_SERVICE_ENGLISH; // 目前僅英文版


        // init layout params //
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.setMargins(DEFAULT_MARGIN, 0, DEFAULT_MARGIN, 0);

        // init web view //
        m_webView = Opt.of(m_webView).getOr(new WebView(getActivity()));
        m_webView.setLayoutParams(lp);
        m_webView.loadUrl(m_sHtmlPath);

        return m_webView;
    }
}
