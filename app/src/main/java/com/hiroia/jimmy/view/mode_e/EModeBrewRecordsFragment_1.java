package com.hiroia.jimmy.view.mode_e;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.hiroia.jimmy.ble.module.BLEJimmyFragment;
import com.hiroia.jimmy.cs.HttpCs;
import com.hiroia.jimmy.db.sql.JRecordDBA;
import com.hiroia.jimmy.enums.HttpDef;
import com.hiroia.jimmy.manager.AccountManager;
import com.hiroia.jimmy.manager.JRecordEModeManager;
import com.hiroia.jimmy.manager.net.NetworkManager;
import com.hiroia.jimmy.manager.old._EModelDataManager;
import com.hiroia.jimmy.model.ModelDateRecord;
import com.hiroia.jimmy.model.ModelEModeRecord;
import com.hiroia.jimmy.model.record.ModelJRecord;
import com.hiroia.jimmy.view.device.DevicesActivity;
import com.hiroia.jimmy.view.mode_e.comp.EModeBrewRecordsAdapter;
import com.hiroia.jimmy.view.mode_t.record.TModeRulesRecordFragment_1;
import com.library.android_common.component.TimeCounter;
import com.library.android_common.component.common.Opt;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.hms.S;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.AFTER_CONNECTION_FINISH_PAGE;

/**
 * Created by Bruce on 2018/11/20.
 */
public class EModeBrewRecordsFragment_1 extends BLEJimmyFragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    // View //
    private LinearLayout m_llvBack, m_llvBrew, m_llvBrewRecords, m_llvBlank;
    private TextView m_tvTitle, m_tvTitleSmall, m_tvBrew, m_tvBlankTxt;
    private ListView m_lvBrewRecords;
    private ImageView m_imgBlank;

    // Comp //
    private static EModeBrewRecordsFragment_1 sm_self;
    private EModeBrewRecordsAdapter m_brewRecordsAdapter;
    private boolean m_bIsFromBrewRecordsPage = false;
    private ArrayList<ModelDateRecord> m_records = new ArrayList<>();
    private Lst<ModelJRecord> m_emodeRecords = Lst.of();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        sm_self = this;

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_emode_brew_records, null);

        //-----------------------------------------------------------------
        // init
        m_llvBack = rootView.findViewById(R.id.fragment_emode_brew_records_back_llv);
        m_llvBrew = rootView.findViewById(R.id.fragment_emode_brew_records_brew_llv);
        m_llvBrewRecords = rootView.findViewById(R.id.fragment_emode_brew_records_brew_records_llv);
        m_llvBlank = rootView.findViewById(R.id.fragment_emode_brew_records_blank_llv);

        m_tvTitle = rootView.findViewById(R.id.fragment_emode_brew_records_title_tv);
        m_tvTitleSmall = rootView.findViewById(R.id.fragment_emode_brew_records_title_small_tv);
        m_tvBrew = rootView.findViewById(R.id.fragment_emode_brew_records_brew_tv);
        m_tvBlankTxt = rootView.findViewById(R.id.fragment_emode_brew_records_blank_txt_tv);

        m_lvBrewRecords = rootView.findViewById(R.id.fragment_emode_brew_records_brew_records_lv);

        m_imgBlank = rootView.findViewById(R.id.fragment_emode_brew_records_blank_img);

        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);
        m_llvBrew.setOnClickListener(this);
        m_lvBrewRecords.setOnItemClickListener(this);

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();

        // 檢測是否 sync 到 Manager 過 //
        if (JRecordEModeManager.isSync()){
            m_emodeRecords = JRecordEModeManager.getModels();
            updateAdapter();
            return;
        }

        // Show Progress Dialog //
        getParentActivity().showProgressDialog();

        // 這裡的 Time Counter 只是為了確保 View 已經被 Launch //
        TimeCounter tc = new TimeCounter();
        tc.setOnCountingListener(S.TWO_SEC.millySec(), new TimeCounter.OnCountingListener() {
            @Override
            public void onTick(long m) {

            }

            @Override
            public void onFinish() {
                // sync data //
                syncEModeJRecordsToManager();
                JRecordEModeManager.getModels().forEach(new Lst.IConsumer<ModelJRecord>() {
                    @Override
                    public void runEach(int i, ModelJRecord m) {
                        m.println(" ModelJRecord form Manager = ");
                    }
                });
                updateAdapter();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_emode_brew_records_back_llv: // Back
                getParentActivity().finish();
                getParentActivity().setRecodingMode(true); // Start to recording data.
                break;

            case R.id.fragment_emode_brew_records_brew_llv: // Brew
                if (!isConnected()) {
                    getParentActivity().getActManager()
                            .putBooleanExtra(AFTER_CONNECTION_FINISH_PAGE, true)
                            .start(DevicesActivity.class)
                            .retrieve();
                    return;
                }
                getParentActivity().switchStateView(EModeActivity.EModeState.STATE_SELECT_STATE);
                m_bIsFromBrewRecordsPage = true;
                break;
        }
    }

    //-----------------------------------------------------------------
    //  點擊 Item 的 Listener 事件
    //-----------------------------------------------------------------

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getParentActivity().setCurrentDateRecord(m_records.get(position));
        getParentActivity().switchStateView(EModeActivity.EModeState.DAILY_STATE);
        getParentActivity().setIsExtractionGoTo(false);
    }

    //-----------------------------------------------------------------
    // Private Method
    //-----------------------------------------------------------------

    private EModeActivity getParentActivity() {
        return EModeActivity.getInstance();
    }

    // 把 Data sync to manager //
    private void syncEModeJRecordsToManager() {

        // Declare //
        m_emodeRecords.clear();

        // Start to sync data from cloud ------------------------ //
        // 連線狀態時 //
        if (NetworkManager.isConnected()) {

            // Check dirty records //
            final JRecordDBA dba = JRecordDBA.open(getParentActivity());
            dba.getDirtyJRecords(ModelJRecord.JScaleMode.E_MODE)
                    .forEach(new Lst.IConsumer<ModelJRecord>() {
                        @Override
                        public void runEach(int i, ModelJRecord dirtyModel) {
                            LogUtil.d(TModeRulesRecordFragment_1.class, " Dirty Model Id = " + dirtyModel.getId());
                            syncDirtyModelToCloud(dba, dirtyModel);
                        }
                    });

            // 取得 Records from cloud, 並 sync 至 manager //
            JRecordEModeManager.syncModels(getEModeJRecordsFromCloud());
        }

        // 離線狀態時，從 DB 抓 //
        if (NetworkManager.isOffLine()) {
            JRecordDBA dba = JRecordDBA.open(getParentActivity());
            JRecordEModeManager.syncModels(dba.getTable(ModelJRecord.JScaleMode.E_MODE));
        }

        // Sync to manager //
        m_emodeRecords = JRecordEModeManager.getModels();

        // 設定 Sync 完畢 //
        JRecordEModeManager.setAlreadySync();

        // Dismiss Dialog //
        getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
    }

    // 取得 E mode ModelJRecord from cloud //
    private Lst<ModelJRecord> getEModeJRecordsFromCloud() {

        final Lst<ModelJRecord> ms = Lst.of();
        HttpDef.LIST_JRECORDS_OF_USER.post()
                .addParam("token", AccountManager.getAccountModel().getToken())
                .addParam("mode", ModelJRecord.JScaleMode.E_MODE.strId())
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject jsObj) {
                        if (response == null || response.isEmpty()) {
                            LogUtil.e(EModeBrewRecordsFragment_1.class, HttpDef.LIST_JRECORDS_OF_USER.get() + " response is null reference !! ");
                            return;
                        }

                        LogUtil.d(TModeRulesRecordFragment_1.class, HttpDef.LIST_JRECORDS_OF_USER.getTag() + " [ Successful ], response = " + response);

                        // Start to decode JSONArray //
                        JSONArray jsArr = Opt.ofJson(jsObj, HttpCs.RESULT).toJsonArray().get();
                        if (LogUtil.Check.e(jsArr == null, TModeRulesRecordFragment_1.class, " getEModeJRecordsFromCloud(), JSONArray is null "))
                            return;

                        ms.addAll(ModelJRecord.decodeJSONs(jsArr));
                    }
                });

        return ms;
    }


    private void syncDirtyModelToCloud(final JRecordDBA dba, final ModelJRecord dirtyModel) {

        HttpDef.CREATE_J_RECORD.post()
                .addParam("token", AccountManager.getAccountModel().getToken())
                .addParam("water", dirtyModel.getRecordData().getWater() + "")
                .addParam("jscale_id",  StrUtil.ZERO)
                .addParam("jrecipe_id",  StrUtil.ZERO)
                .addParam("score", dirtyModel.getScore() + "")
                .addParam("mode", dirtyModel.getMode().id() + "")
                .addParam("state", dirtyModel.getState() + "")
                .addParam("record_time", dirtyModel.getCreated_time().formatISO8601())
                .addParam("time1", StrUtil.ZERO)
                .addParam("time2", StrUtil.ZERO)
                .addParam("training_data", dirtyModel.getTrainingData().datas())
                .addParam("record_data", dirtyModel.getRecordData().datas())
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject jsObj) {
                        if (response.isEmpty() || response == null) {
                            LogUtil.e(TModeRulesRecordFragment_1.class, HttpDef.CREATE_J_RECORD.getTag() + " response == null or Empty");
                            return;
                        }

                        // decode JSON //
                        jsObj = Opt.ofJson(jsObj, "result").toJsonObj().get();
                        ModelJRecord m = ModelJRecord.decodeJSON(jsObj);
                        dba.delete(dirtyModel); // Sync 成功後刪除 dba 裡面的 dirty model
                        dba.insert(m); // insert clean model //

                        // dismiss progress dialog //
                        LogUtil.d(EModeBrewRecordsFragment_1.class, HttpDef.CREATE_J_RECORD.getTag() + " response = " + response);
                    }
                });
    }

    private void updateAdapter(){
        // update Adapter，這裡的 ModelEModeRecord 只是為了 display 用。而正確的作法應該要用 ModelJRecord 替代 //
        m_records = ModelEModeRecord.toDateRecords(JRecordEModeManager.getModelEModeRecords().toList()).toList();
        m_brewRecordsAdapter = new EModeBrewRecordsAdapter(getParentActivity(), m_records);
        m_lvBrewRecords.setAdapter(m_brewRecordsAdapter);

        // update View //
        m_llvBlank.setVisibility(m_records.isEmpty() ? View.VISIBLE : View.GONE);
        m_llvBrewRecords.setVisibility(m_records.isEmpty() ? View.GONE : View.VISIBLE);
    }


    //-----------------------------------------------------------------
    // Public Method
    //-----------------------------------------------------------------

    public boolean isFromBrewPage() {
        return m_bIsFromBrewRecordsPage;
    }

    public void notifyListViewAdapter() {
//        m_records = _EModelDataManager.getModelBrewsRecords();
        m_records = ModelEModeRecord.toDateRecords(_EModelDataManager.getModelBrewsRecords()).toList();
        m_brewRecordsAdapter.notifyDataSetChanged();
    }

    public static EModeBrewRecordsFragment_1 getInstance() {
        return sm_self;
    }

    //-----------------------------------------------------------------
    // BLEAction
    //-----------------------------------------------------------------

    @Override
    public void onScanDevicesUpdate(ArrayList<BluetoothDevice> devices) {

    }

    @Override
    public void onConnectionStateUpdate(int state) {

    }

    @Override
    public void onJimmyInfoUpdate(JimmyInfo jimmyInfo) {

    }

    @Override
    public void onBLEInit() {

    }

}
