package com.hiroia.jimmy.view.mode_e.comp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.model.ModelEModeRecord;
import com.library.android_common.component.date.hms.MS;

import java.util.ArrayList;

/**
 * Created by Bruce on 2018/11/23.
 */
public class EModeReportDailyAdapter extends BaseAdapter {

    // Final Member //
    private final int ITEM_FIRST_POS = 0;

    // Comp //
    private boolean m_bSetInfoToTop = false;

    // View //
    private Activity m_ctx;
    private ViewHolder m_viewHolder;
    private ArrayList<ModelEModeRecord> m_records = new ArrayList<>();

    //---------------------------------------------------------
    //  Construct
    //---------------------------------------------------------
    public EModeReportDailyAdapter(Activity activity, ArrayList<ModelEModeRecord> dailies) {
        m_ctx = activity;
        m_records = dailies;
    }

    @Override
    public int getCount() {
        return m_records.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        view = LayoutInflater.from(m_ctx).inflate(R.layout.comp_emode_report_list, null);

        m_viewHolder = new EModeReportDailyAdapter.ViewHolder(
                (LinearLayout) view.findViewById(R.id.comp_emode_report_list_item_llv),
                (LinearLayout) view.findViewById(R.id.comp_emode_report_list_all_title_llv),
                (TextView) view.findViewById(R.id.comp_emode_report_list_order_title_tv),
                (TextView) view.findViewById(R.id.comp_emode_report_list_extraction_title_tv),
                (TextView) view.findViewById(R.id.comp_emode_report_list_time_title_tv),
                (TextView) view.findViewById(R.id.comp_emode_report_list_order_value_tv),
                (TextView) view.findViewById(R.id.comp_emode_report_list_extraction_value_tv),
                (TextView) view.findViewById(R.id.comp_emode_report_list_time_value_tv),
                (TextView) view.findViewById(R.id.comp_emode_report_list_creation_time_value_tv),
                view.findViewById(R.id.comp_emode_report_list_divider_v));

        // TODO 改多國語言
        m_viewHolder.m_tvOrderTitle.setText("Order");
        m_viewHolder.m_tvExtractionTitle.setText("Extraction");
        m_viewHolder.m_tvTimeTitle.setText("Time");

        if (position == 0){
            m_viewHolder.m_llvAllTitle.setVisibility(View.VISIBLE);
            m_viewHolder.m_vDivider.setVisibility(View.VISIBLE);
        }

        if (hasStandard() && position == 0) {
            m_viewHolder.m_tvOrder.setTextColor(m_ctx.getColor(R.color.jimmyBlue));
            m_viewHolder.m_tvExtraction.setTextColor(m_ctx.getColor(R.color.jimmyBlue));
            m_viewHolder.m_tvTime.setTextColor(m_ctx.getColor(R.color.jimmyBlue));
            m_viewHolder.m_tvCreationTime.setVisibility(View.INVISIBLE);
            m_viewHolder.m_tvOrder.setText("Std.");
        } else if (hasStandard() && position != 0) {
            m_viewHolder.m_tvOrder.setTextColor(m_ctx.getColor(R.color.WHITE));
            m_viewHolder.m_tvExtraction.setTextColor(m_ctx.getColor(R.color.WHITE));
            m_viewHolder.m_tvTime.setTextColor(m_ctx.getColor(R.color.WHITE));
            m_viewHolder.m_tvCreationTime.setVisibility(View.INVISIBLE);
            m_viewHolder.m_tvOrder.setText(String.valueOf(position));
        }

        if (!hasStandard()) {
            m_viewHolder.m_tvOrder.setTextColor(m_ctx.getColor(R.color.WHITE));
            m_viewHolder.m_tvExtraction.setTextColor(m_ctx.getColor(R.color.WHITE));
            m_viewHolder.m_tvTime.setTextColor(m_ctx.getColor(R.color.WHITE));
            m_viewHolder.m_tvCreationTime.setVisibility(View.INVISIBLE);
            m_viewHolder.m_tvOrder.setText(String.valueOf(position + 1));
        }


        m_viewHolder.m_tvExtraction.setText(m_records.get(position).getWeight() + "g");
        m_viewHolder.m_tvTime.setText(m_records.get(position).getTtime() + "s");
        m_viewHolder.m_tvCreationTime.setText(MS.of((int) m_records.get(position).getTime()).toString());

        return view;
    }


    private boolean hasStandard() {
        if (m_records.size() <= 0)
            return false;
        return m_records.get(0).isStd();
    }

    // 被點擊後顯示的藍色 title //
    private void showTopInfo(int pos) {

        if (pos != ITEM_FIRST_POS) return;

        m_viewHolder.m_llvAllTitle.setVisibility(View.VISIBLE);
        m_viewHolder.m_vDivider.setVisibility(View.VISIBLE);

        if (!m_bSetInfoToTop) return;

        m_viewHolder.m_tvOrder.setTextColor(m_ctx.getColor(R.color.jimmyBlue));
        m_viewHolder.m_tvOrder.setText("Std.");
        m_viewHolder.m_tvExtraction.setTextColor(m_ctx.getColor(R.color.jimmyBlue));
        m_viewHolder.m_tvTime.setTextColor(m_ctx.getColor(R.color.jimmyBlue));
        m_viewHolder.m_tvCreationTime.setVisibility(View.INVISIBLE);
    }

    public void setInfoTop(boolean enable) {
        m_bSetInfoToTop = enable;
        notifyDataSetChanged();
    }

    class ViewHolder {

        private LinearLayout m_llvItem, m_llvAllTitle;
        private TextView m_tvOrderTitle, m_tvExtractionTitle, m_tvTimeTitle, m_tvOrder, m_tvExtraction, m_tvTime, m_tvCreationTime;
        private View m_vDivider;

        ViewHolder(LinearLayout item, LinearLayout allTitle, TextView orderTitle, TextView extracTitle, TextView timeTitle, TextView order, TextView extrac, TextView time, TextView creationTime, View divider) {
            this.m_llvItem = item;
            this.m_llvAllTitle = allTitle;
            this.m_tvOrderTitle = orderTitle;
            this.m_tvExtractionTitle = extracTitle;
            this.m_tvTimeTitle = timeTitle;
            this.m_tvOrder = order;
            this.m_tvExtraction = extrac;
            this.m_tvTime = time;
            this.m_tvCreationTime = creationTime;
            this.m_vDivider = divider;
        }
    }
}
