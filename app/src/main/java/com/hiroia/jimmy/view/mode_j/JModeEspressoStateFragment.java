package com.hiroia.jimmy.view.mode_j;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.view.base.BaseFragment;

/**
 * Create By Ray on 2018/10/29
 */
public class JModeEspressoStateFragment extends BaseFragment implements View.OnClickListener {

    // View //
    private LinearLayout m_llvBack;
    private TextView m_tvTitle, m_tvTitleSmall, m_tvSubTitle, m_tvWeight, m_tvUnit, m_tvTime;
    private ImageView m_imgWater, m_imgCupEmpty, m_imgCupFull, m_imgSwitch, m_imgTimer, m_imgTare;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_jmode_espresso_state, null);

        //-----------------------------------------------------------------
        // init
//        m_llvBack = rootView.findViewById(R.id.fragment_login_step2_cancel_btn_llv);

        m_tvTitle = rootView.findViewById(R.id.fragment_jmode_espresso_title_tv);
        m_tvTitleSmall = rootView.findViewById(R.id.fragment_jmode_espresso_title_small_tv);
        m_tvSubTitle = rootView.findViewById(R.id.fragment_jmode_espresso_sub_title_tv);
        m_tvWeight = rootView.findViewById(R.id.fragment_jmode_espresso_weight_tv);
        m_tvUnit = rootView.findViewById(R.id.fragment_jmode_espresso_unit_tv);
        m_tvTime = rootView.findViewById(R.id.fragment_jmode_espresso_time_tv);

        m_imgWater = rootView.findViewById(R.id.fragment_jmode_espresso_state_water_img);
        m_imgCupEmpty = rootView.findViewById(R.id.fragment_jmode_espresso_state_cup_empty_img);
        m_imgCupFull = rootView.findViewById(R.id.fragment_jmode_espresso_state_cup_full_img);
        m_imgSwitch = rootView.findViewById(R.id.fragment_jmode_espresso_switch_img);
        m_imgTimer = rootView.findViewById(R.id.fragment_jmode_espresso_timer_img);
        m_imgTare = rootView.findViewById(R.id.fragment_jmode_espresso_tare_img);

        //-----------------------------------------------------------------
        // set data
//        m_llvBack.setOnClickListener(this);
        m_imgSwitch.setOnClickListener(this);
        m_imgTimer.setOnClickListener(this);
        m_imgTare.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.fragment_login_step2_cancel_btn_llv: // Back
//                break;

            case R.id.fragment_jmode_espresso_switch_img: // Switch
                break;

            case R.id.fragment_jmode_espresso_timer_img: // Timer
                break;

            case R.id.fragment_jmode_espresso_tare_img: // Tare
                break;
        }
    }

    //----------------------------------------------
    // Private Method
    //----------------------------------------------
    private JModeActivity getParentActivity() {
        return JModeActivity.getInstance();
    }
}
