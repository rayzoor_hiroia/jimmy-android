package com.hiroia.jimmy.view.mode_j;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.view.base.BaseFragment;

/**
 * Create By Ray on 2018/10/29
 */
public class JModeScaleStateFragment extends BaseFragment{


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_jmode_scale_state, null);
        return rootView;
    }
}
