package com.hiroia.jimmy.view.mode_t;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.ble.BLEJimmy;
import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.hiroia.jimmy.ble.module.BLEJimmyFragment;
import com.hiroia.jimmy.view.device.DevicesActivity;
import com.library.android_common.component.common.Opt;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.ArrayList;

import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.AFTER_CONNECTION_FINISH_PAGE;
import static com.hiroia.jimmy.ble.comp.JimmyInfo.Mode.T1;

/**
 * Created by Bruce on 2018/11/1.
 */
public class TModeVelocityFragment_2 extends BLEJimmyFragment implements View.OnClickListener {

    // Final Member //
    private final int ONE_MIN = 60;
    private final int TWO_MIN = 120;
    private final int HALF_MIN = 30;
    private final int POURS_COUNT = 3;

    // View //
    private NumberPicker m_npRatePicker0, m_npRatePicker1, m_npRatePicker2;
    private Button m_btnStartTraning, m_btnRules;

    private TextView m_tvSubTitle, m_tvRandomModeSubTitle, m_tvMLSymbol, m_tvTimeOneMin, m_tvTimeHalfMin, m_tvTimeTwoMin;
    private LinearLayout m_llvBack, m_llvSecSubLayout;

    // Comp //
    private int m_nCurrRateTime = HALF_MIN; // this is default time ;
    private String m_sRandomSubTitile;
    private String m_sRandomPoursContent = "Random Flow Rate Rate of _ Pours"; // under line 是用來替換數字

    private JimmyInfo m_jimmyInfo = null;
    private JimmyInfo.Mode m_targetMode = null;

    //-----------------------------------------------------
    // Fragment
    //-----------------------------------------------------

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_tmode_velocity, null);

        //-----------------------------------------------------------------
        // init view
        m_tvTimeHalfMin = rootView.findViewById(R.id.fragment_tmode_velocity_30sec_tv);
        m_tvTimeOneMin = rootView.findViewById(R.id.fragment_tmode_velocity_1min_tv);
        m_tvTimeTwoMin = rootView.findViewById(R.id.fragment_tmode_velocity_2min_tv);

//        m_npRatePicker0 = rootView.findViewById(R.id.fragment_tmode_velocity_num_picker_0);
        m_npRatePicker1 = rootView.findViewById(R.id.fragment_tmode_velocity_num_picker_1);
        m_npRatePicker2 = rootView.findViewById(R.id.fragment_tmode_velocity_num_picker_2);

        m_tvSubTitle = rootView.findViewById(R.id.fragment_tmode_velocity_subtitle_tv);
        m_tvMLSymbol = rootView.findViewById(R.id.fragment_tmode_velocity_ml_symbol);
        m_tvRandomModeSubTitle = rootView.findViewById(R.id.fragment_tmode_velocity_sec_sub_title_tv);

        m_btnStartTraning = rootView.findViewById(R.id.fragment_tmode_velocity_start_training_btn);
        m_btnRules = rootView.findViewById(R.id.fragment_tmode_velocity_rules_btn);

        m_llvBack = rootView.findViewById(R.id.fragment_tmode_velocity_back_llv);
        m_llvSecSubLayout = rootView.findViewById(R.id.fragment_tmode_velocity_sec_subtitle_llv);

        initViewByMode();

        //-----------------------------------------------------------------
        // set data
        m_tvTimeHalfMin.setOnClickListener(this);
        m_tvTimeOneMin.setOnClickListener(this);
        m_tvTimeTwoMin.setOnClickListener(this);

        m_btnStartTraning.setOnClickListener(this);
        m_btnRules.setOnClickListener(this);
        m_llvBack.setOnClickListener(this);
        m_tvSubTitle.setText(getParentActivity().getMode().getTitle());

        m_npRatePicker1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                m_npRatePicker2.setMinValue(newVal == 0 ? 1 : 0);
                m_npRatePicker2.setMaxValue(newVal == 1 ? 0 : 9);
            }
        });

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_tmode_velocity_start_training_btn: // Start Training
                // Check is already connected to JIMMY device. //
                if (!isConnected()) {
                    getParentActivity().getActManager()
                            .putBooleanExtra(AFTER_CONNECTION_FINISH_PAGE, true)
                            .start(DevicesActivity.class)
                            .retrieve();
                    return;
                }

                // Check JIMMY is T1 Mode. //
                if (m_jimmyInfo == null) return;

                m_targetMode = T1;
                if (m_jimmyInfo.getCurrMode() != T1) {
                    switchJimmyMode(m_jimmyInfo.switchModeTo(T1), m_onJimmySwitchModeListener);
                }

                // Set Gaming Param //
                setFlowRate();
                setTimeMode();
                switchToNextStep();
                break;

            case R.id.fragment_tmode_velocity_rules_btn: // Rules
                getParentActivity().switchStateView(TModeActivity.TModeState.TRAINING_RESULT_PAGE);
                getParentActivity().setIsTrainingBaristaGoTo(false);
                break;

            case R.id.fragment_tmode_velocity_back_llv: // Back
                getParentActivity().switchStateView(TModeActivity.TModeState.TRAINING_MODE);
                break;

            //----------------------------------------------------------------
            // Time Section
            case R.id.fragment_tmode_velocity_30sec_tv: // 30 sec Item
                m_nCurrRateTime = HALF_MIN;
                //-------------------------
                m_tvTimeHalfMin.setBackground(getParentActivity().getDrawable(R.drawable.jimmy_textview_background_radius_blue));

                m_tvTimeOneMin.setBackground(getParentActivity().getDrawable(R.drawable.jimmy_textview_background_radius_transparent));
                m_tvTimeTwoMin.setBackground(getParentActivity().getDrawable(R.drawable.jimmy_textview_background_radius_transparent));

                break;

            case R.id.fragment_tmode_velocity_1min_tv: // 1 min Item
                m_nCurrRateTime = ONE_MIN;
                //-------------------------
                m_tvTimeOneMin.setBackground(getParentActivity().getDrawable(R.drawable.jimmy_textview_background_radius_blue));

                m_tvTimeHalfMin.setBackground(getParentActivity().getDrawable(R.drawable.jimmy_textview_background_radius_transparent));
                m_tvTimeTwoMin.setBackground(getParentActivity().getDrawable(R.drawable.jimmy_textview_background_radius_transparent));

                break;

            case R.id.fragment_tmode_velocity_2min_tv: // 2 min Item
                m_nCurrRateTime = TWO_MIN;
                //-------------------------
                m_tvTimeTwoMin.setBackground(getParentActivity().getDrawable(R.drawable.jimmy_textview_background_radius_blue));

                m_tvTimeOneMin.setBackground(getParentActivity().getDrawable(R.drawable.jimmy_textview_background_radius_transparent));
                m_tvTimeHalfMin.setBackground(getParentActivity().getDrawable(R.drawable.jimmy_textview_background_radius_transparent));

                break;
        }
    }

    //-----------------------------------------------------
    // Private Preference
    //-----------------------------------------------------

    private TModeActivity getParentActivity() {
        return TModeActivity.getInstance();
    }

    // 完成切換模式後 //
    private BLEJimmy.OnJimmySwitchModeListener m_onJimmySwitchModeListener = new BLEJimmy.OnJimmySwitchModeListener() {
        @Override
        public void onSwitchFinish() {
            getParentActivity().dismissProgressDialog();
        }
    };

    // init view //
    private void initViewByMode() {
        TModeActivity.Mode mode = getParentActivity().getMode();
        switch (mode) {
            case SINGLE:
                getParentActivity().setRandomPoursCount(1); // 這裡這樣做，是為了不影響 Single Mode 後續邏輯

                m_tvRandomModeSubTitle.setVisibility(View.GONE);
                m_tvMLSymbol.setVisibility(View.VISIBLE);
                m_npRatePicker1.setVisibility(View.VISIBLE);
                m_npRatePicker2.setVisibility(View.VISIBLE);
                m_llvSecSubLayout.setVisibility(View.VISIBLE);
                m_nCurrRateTime = HALF_MIN;
                break;

            case RANDOM:
                // Comp //
//                int randomPours = IntUtil.rangeRandom(1, 3); // 隨機給一個 Pour 數 // 後續可能會改回 random
                int randomPours = POURS_COUNT;
                getParentActivity().setRandomPoursCount(randomPours);

                // View //
                m_tvMLSymbol.setVisibility(View.GONE);
                m_npRatePicker1.setVisibility(View.GONE);
                m_npRatePicker2.setVisibility(View.GONE);
                m_llvSecSubLayout.setVisibility(View.GONE);
                m_tvTimeHalfMin.setVisibility(View.GONE);
                m_tvTimeOneMin.setVisibility(View.VISIBLE);
                m_nCurrRateTime = ONE_MIN;

                m_tvRandomModeSubTitle.setVisibility(View.VISIBLE);
                m_tvRandomModeSubTitle.setText(m_sRandomPoursContent.replace(StrUtil.UNDER_LINE, randomPours + ""));

                break;
        }
    }

    // 設定時間  mode //
    private void setTimeMode() {
        getParentActivity().setTimeSec(m_nCurrRateTime);
    }

    // 設定 Rate Mode //
    private void setFlowRate() {

        // Parsing Val //
        String val = (m_npRatePicker1.getValue() + "") + (m_npRatePicker2.getValue() + "");
        getParentActivity().setFlowRate(Opt.of(val).parseToInt().get());

        //---------------------------------------------------------------------
        // 底下為 double type 的 Flow Rate Vol ，暫時勿移除
        // parsing val //
//        String val = (m_npRatePicker1.getValue() + "") + StrUtil.DOT + (m_npRatePicker2.getValue() + "");

        // counting //
//        int afterRoundOffVal = IntUtil.roundOf(val);
//        double dVal = Opt.of(val).parseToDouble().get();

        // set result //
//        getParentActivity().setDFlowRate(dVal);
//        getParentActivity().setFlowRate(afterRoundOffVal);
    }

    // Go To Next Page //
    private void switchToNextStep() {
        getParentActivity().switchStateView(TModeActivity.TModeState.FLOW_RATE_PAGE);
    }

    private void setScaleMode(JimmyInfo.Mode m) {
        switch (m) {
            default:
            case T1:
                if (m_targetMode != T1) break;
                m_targetMode = null;
            case T2:
            case T3:
            case E1:
            case E2:
            case E3:
                break;
        }
    }


    //-----------------------------------------------------
    // BLE Action
    //-----------------------------------------------------

    @Override
    public void onScanDevicesUpdate(ArrayList<BluetoothDevice> devices) {

    }

    @Override
    public void onConnectionStateUpdate(int state) {

    }

    @Override
    public void onJimmyInfoUpdate(JimmyInfo jimmyInfo) {
        m_jimmyInfo = jimmyInfo;

        // Update Scale Mode //
        setScaleMode(jimmyInfo.getCurrMode());
    }

    @Override
    public void onBLEInit() {

    }

}