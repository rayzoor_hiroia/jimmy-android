package com.hiroia.jimmy.view.mode_j;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.view.base.BaseActivity;

/**
 * Create By Ray on 2018/10/29
 */
public class JModeActivity extends BaseActivity {

    //----------------------------------------------
    // Construct
    //----------------------------------------------
    public static JModeActivity sm_self;
    private JModeScaleStateFragment m_scaleFragment;
    private JModeEspressoStateFragment m_espressoFragment;

    //-------------------------------------------------------------
    // Activity Life Cycle
    //-------------------------------------------------------------
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jmode, false);
        sm_self = this;

        //------------------------------------------
        // init view
        init();

        //------------------------------------------
        // set view
        switchStateView(JModeState.ESPRESSO_STATE);

    }

    //-------------------------------------------------------------
    // Private Preference
    //-------------------------------------------------------------
    private void init() {
        m_scaleFragment = new JModeScaleStateFragment();
        m_espressoFragment = new JModeEspressoStateFragment();
    }

    private void switchFragment(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_jmode_flt, fragment)
                .commit();
    }


    //----------------------------------------------
    // Public Method
    //----------------------------------------------

    // Get Instance //
    public static JModeActivity getInstance() {
        return sm_self;
    }


    public void switchStateView(JModeState state){
        switch (state){
            case ESPRESSO_STATE:
                switchFragment(m_espressoFragment);
                break;

            case SCALE_DATA_STATE:
                switchFragment(m_scaleFragment);
                break;
        }
    }


    //----------------------------------------------
    // Enum
    //----------------------------------------------

    public enum JModeState{
        SCALE_DATA_STATE, ESPRESSO_STATE
    }



}
