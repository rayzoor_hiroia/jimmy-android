package com.hiroia.jimmy.view.mode_t.record;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.hiroia.jimmy.ble.module.BLEJimmyFragment;
import com.hiroia.jimmy.comp.view.layout.RatingBarView;
import com.hiroia.jimmy.cs.HttpCs;
import com.hiroia.jimmy.db.sql.JRecordDBA;
import com.hiroia.jimmy.enums.HttpDef;
import com.hiroia.jimmy.manager.AccountManager;
import com.hiroia.jimmy.manager.JRecordTModelManager;
import com.hiroia.jimmy.manager.net.NetworkManager;
import com.hiroia.jimmy.model.record.ModelJRecord;
import com.hiroia.jimmy.view.mode_t.TModeActivity;
import com.library.android_common.component.common.Opt;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.hms.S;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Bruce on 2018/11/1.
 */
public class TModeRulesRecordFragment_1 extends BLEJimmyFragment implements View.OnClickListener, ListView.OnItemClickListener {

    // Final Member //
    private final int SINGLE_RECORD = 0;
    private final int RANDOM_RECORD = 1;
    private final int SINGLE_MODE = 1001;
    private final int RANDOM_MODE = 1002;

    // View //
    private LinearLayout m_llvBack, m_llvlist, m_llvBlank;
    private TextView m_tvTitle, m_tvSingle, m_tvRandom, m_tvUserName, m_tvBlankTxt;
    private ImageView m_imgSticker, m_imgBlank;
    private ListView m_lvRecord;

    // Comp //
    private RecordAdapter m_recordAdapter;
    private Lst<ModelJRecord> m_singleRecords = Lst.of();
    private Lst<ModelJRecord> m_randomRecords = Lst.of();
    private Lst<ModelJRecord> m_records = Lst.of();
    private int m_nCurrentMode = SINGLE_MODE;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //-----------------------------------------------------------------
        // root view
        View rootView = inflater.inflate(R.layout.fragment_tmode_rules_record, null);

        //-----------------------------------------------------------------
        // init
        m_llvBack = rootView.findViewById(R.id.fragment_tmode_rules_record_back_llv);
        m_llvlist = rootView.findViewById(R.id.fragment_tmode_rules_record_list_llv);
        m_llvBlank = rootView.findViewById(R.id.fragment_tmode_rules_record_blank_llv);

        m_tvTitle = rootView.findViewById(R.id.fragment_tmode_rules_record_title_tv);
        m_tvSingle = rootView.findViewById(R.id.fragment_tmode_rules_record_single_tv);
        m_tvRandom = rootView.findViewById(R.id.fragment_tmode_rules_record_random_tv);
        m_tvUserName = rootView.findViewById(R.id.fragment_tmode_rules_record_user_name_tv);
        m_tvBlankTxt = rootView.findViewById(R.id.fragment_tmode_rules_record_blank_txt_tv);

        m_imgSticker = rootView.findViewById(R.id.fragment_tmode_rules_record_sticker_img);
        m_imgBlank = rootView.findViewById(R.id.fragment_tmode_rules_record_blank_img);

        m_lvRecord = rootView.findViewById(R.id.fragment_tmode_rules_record_list_lv);

        m_recordAdapter = new RecordAdapter();

        //-----------------------------------------------------------------
        // set data
        m_llvBack.setOnClickListener(this);
        m_tvSingle.setOnClickListener(this);
        m_tvRandom.setOnClickListener(this);
        m_lvRecord.setOnItemClickListener(this);

        //-----------------------------------------------------------------
        // set adapter
        m_lvRecord.setAdapter(m_recordAdapter);


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        syncJRecordsToManager();
        updateRecordView();

        // sort by date //
    }

    private void syncJRecordsToManager() {

        // Declare //
        m_singleRecords.clear();
        m_randomRecords.clear();

        // 如果 Sync 過了就 return ; //
        if (!JRecordTModelManager.isSync())
            getParentActivity().showProgressDialog();
        else {
            m_singleRecords = JRecordTModelManager.getSingleModels();
            m_randomRecords = JRecordTModelManager.getRandomModels();
            m_records = m_singleRecords;
            return;
        }

        // Start to sync cloud data //
        if (NetworkManager.isConnected()) { // 若是 有 網路

            // 檢查有沒有 dirty recipe ( 離線狀態時新增而沒有跟 Cloud Sync 過的 JRecord ) //
            final JRecordDBA dba = JRecordDBA.open(getParentActivity());
            dba.getDirtyJRecords(ModelJRecord.JScaleMode.TS_MODE, ModelJRecord.JScaleMode.TR_MODE)
                    .forEach(new Lst.IConsumer<ModelJRecord>() {
                @Override
                public void runEach(int i, ModelJRecord dirtyModel) {
                    LogUtil.d(TModeRulesRecordFragment_1.class, " Dirty Model Id = " + dirtyModel.getId());
                    syncDirtyModelToCloud(dba, dirtyModel);
                }
            });

            // 取得 Recipe form cloud //
            Lst<ModelJRecord> ms = Lst.of();
            ms.addAll(getRecordsFormCloud(ModelJRecord.JScaleMode.TS_MODE));
            ms.addAll(getRecordsFormCloud(ModelJRecord.JScaleMode.TR_MODE));

            // Sync to manager //
            JRecordTModelManager.syncModels(ms);
        }

        // 若是沒有網路，從 DB 抓 //
        if (NetworkManager.isOffLine()) {
            JRecordDBA dba = JRecordDBA.open(getParentActivity());
            JRecordTModelManager.syncModels(dba.getTable(ModelJRecord.JScaleMode.TS_MODE, ModelJRecord.JScaleMode.TR_MODE));
        }

        // Assign Data 完畢 //
        m_singleRecords = JRecordTModelManager.getSingleModels();
        m_randomRecords = JRecordTModelManager.getRandomModels();
        m_records = m_singleRecords;

        // Sync 完畢 //
        JRecordTModelManager.setAlreadySync();

        // Dismiss Dialog //
        getParentActivity().dismissProgressDialog(S.Positive.ONE_SEC);
    }

    //------------------------------------------------------------
    // implement method
    //------------------------------------------------------------
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_tmode_rules_record_back_llv: // Back
                getParentActivity().switchStateView(TModeActivity.TModeState.TRAINING_MODE);
                break;

            case R.id.fragment_tmode_rules_record_single_tv: // Single Item
                m_tvSingle.setBackground(getParentActivity().getResources().getDrawable(R.drawable.jimmy_textview_background_radius_blue));
                m_tvRandom.setBackground(getParentActivity().getResources().getDrawable(R.drawable.jimmy_textview_background_radius_gray));
                m_nCurrentMode = SINGLE_MODE;
                updateRecordView();

                // update record view //
                m_records = m_singleRecords;
                m_recordAdapter.notifyDataSetChanged();
                break;

            case R.id.fragment_tmode_rules_record_random_tv: // Random Item
                m_tvRandom.setBackground(getParentActivity().getResources().getDrawable(R.drawable.jimmy_textview_background_radius_blue));
                m_tvSingle.setBackground(getParentActivity().getResources().getDrawable(R.drawable.jimmy_textview_background_radius_gray));
                m_nCurrentMode = RANDOM_MODE;
                updateRecordView();

                // update record view //
                m_records = m_randomRecords;
                m_recordAdapter.notifyDataSetChanged();
                break;
        }
    }

    //------------------------------------------------------------
    // Private Preference
    //------------------------------------------------------------

    private void updateRecordView() {
        // Update by any mode which single or random is empty size
        if (m_nCurrentMode == SINGLE_MODE && m_singleRecords.size() == 0) {
            m_llvlist.setVisibility(View.GONE);
            m_llvBlank.setVisibility(View.VISIBLE);
        } else if (m_nCurrentMode == RANDOM_MODE && m_randomRecords.size() == 0) {
            m_llvlist.setVisibility(View.GONE);
            m_llvBlank.setVisibility(View.VISIBLE);
        } else {
            m_records = m_singleRecords;
            m_llvlist.setVisibility(View.VISIBLE);
            m_llvBlank.setVisibility(View.GONE);
        }
    }

    private TModeActivity getParentActivity() {
        return TModeActivity.getInstance();
    }

    // 將 Dirty Model 與 Cloud Sync //
    private void syncDirtyModelToCloud(final JRecordDBA dba, final ModelJRecord dirtyModel) {

        HttpDef.CREATE_J_RECORD.post()
                .addParam("token", AccountManager.getAccountModel().getToken())
                .addParam("water", dirtyModel.getRecordData().getWater() + "")
                .addParam("jscale_id", "0")
                .addParam("jrecipe_id", "0")
                .addParam("score", dirtyModel.getScore() + "")
                .addParam("mode", dirtyModel.getMode().id() + "")
                .addParam("record_time", dirtyModel.getCreated_time().formatISO8601())
                .addParam("time1", StrUtil.ZERO)
                .addParam("time2", StrUtil.ZERO)
                .addParam("training_data", dirtyModel.getTrainingData().datas())
                .addParam("record_data", dirtyModel.getRecordData().datas())
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject jsObj) {
                        if (response.isEmpty() || response == null) {
                            LogUtil.e(TModeRulesRecordFragment_1.class, HttpDef.CREATE_J_RECORD.getTag() + " response == null or Empty");
                            return;
                        }

                        // decode JSON //
                        jsObj = Opt.ofJson(jsObj, "result").toJsonObj().get();
                        ModelJRecord m = ModelJRecord.decodeJSON(jsObj);
                        dba.delete(dirtyModel); // Sync 成功後刪除 dba 裡面的 dirty model
                        dba.insert(m); // insert clean model //

                        // dismiss progress dialog //
                        LogUtil.d(TModeRulesRecordFragment_1.class, HttpDef.CREATE_J_RECORD.getTag() + " response = " + response);

                    }
                });
    }

    // 取得 ModelTModelRecord //
//    private Lst<ModelTModeRecord> getTables() {
//        // set random or single data //
//        if (_TModelDataManager.isModelBrewRecordEmpty()) {
//            getParentActivity().showAutoDismissProgressDialog(1000); // show progress dialog
//
//            // Open DB //
//            TModeRecordDBA dba = new TModeRecordDBA(getParentActivity());
//            Lst<ModelTModeRecord> records = dba.getTable();
//
//            // Set Data Manager //
//            _TModelDataManager.setIsAlreadyQuery(); // 當 db 的 data 數為 0時，只要 query 過便不再 query db;
//            _TModelDataManager.setModelBrewRecords(records.toList());
//            return records;
//        }
//        return Lst.of(_TModelDataManager.getModelBrewRecords());
//    }

    // Draw Item //
    private View recordItem(int pos) {
        View v = getParentActivity().getLayoutInflater().inflate(R.layout.comp_tmode_record_list, null);
        ((TextView) v.findViewById(R.id.comp_tmode_record_list_index_tv)).setText(String.valueOf(pos + 1));
        ((TextView) v.findViewById(R.id.comp_tmode_record_list_score_tv)).setText(m_records.get(pos).getStrScore());
        ((TextView) v.findViewById(R.id.comp_tmode_record_list_date_tv)).setText(m_records.get(pos).getCreated_time().ymd().toSlashString());
        RatingBarView m_ratingBarView = v.findViewById(R.id.comp_tmode_record_list_score_rating_rbv);
        m_ratingBarView.setStar(m_records.get(pos).getStars(), false);
        m_ratingBarView.setClickable(false);
        return v;
    }

    private Lst<ModelJRecord> getRecordsFormCloud(ModelJRecord.JScaleMode mode) {

        final Lst<ModelJRecord> ms = Lst.of();
        HttpDef.LIST_JRECORDS_OF_USER.post()
                .addParam("token", AccountManager.getAccountModel().getToken())
                .addParam("mode", mode.strId())
                .trigger(new HttpDef.HttpDefResponse() {
                    @Override
                    public void response(String response, JSONObject jsObj) {
                        if (response == null || response.isEmpty()) {
                            LogUtil.e(TModeRulesRecordFragment_1.class, HttpDef.LIST_JRECORDS_OF_USER.getTag() + " response is null reference !!");
                            return;
                        }
                        //----------------------------------------------------
                        // Log //
                        LogUtil.d(TModeRulesRecordFragment_1.class, HttpDef.LIST_JRECORDS_OF_USER.getTag() + " [ Successful ], response = " + response);

                        // Start to decode JSONArray //
                        JSONArray jsArr = Opt.ofJson(jsObj, HttpCs.RESULT).toJsonArray().get();
                        if (LogUtil.Check.e(jsArr == null, TModeRulesRecordFragment_1.class, " getRecordsFormCloud(), JSONArray is null "))
                            return;

                        ms.addAll(ModelJRecord.decodeJSONs(jsArr));
                    }
                });

        return ms;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getParentActivity().setTRecordId(m_records.get(position).getId());
        getParentActivity().switchStateView(TModeActivity.TModeState.RECORD_CHART_VIEW);
    }

    //------------------------------------------------------------
    // Inner Class
    //------------------------------------------------------------

    public class RecordAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return m_records.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return recordItem(position);
        }
    }


    //------------------------------------------------------------
    // BLE Action
    //------------------------------------------------------------

    @Override
    public void onScanDevicesUpdate(ArrayList<BluetoothDevice> devices) {

    }

    @Override
    public void onConnectionStateUpdate(int state) {

    }

    @Override
    public void onJimmyInfoUpdate(JimmyInfo jimmyInfo) {

    }

    @Override
    public void onBLEInit() {

    }

}
