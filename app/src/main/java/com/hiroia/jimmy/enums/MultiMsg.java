package com.hiroia.jimmy.enums;

/**
 * Create By Ray on 2018/10/25
 * Multiple Language
 *
 */
public enum MultiMsg {


    CAMERA_PERMISSIONS("相機權限", "Camera permissions", "相机权限", "カメラ権限", "สิทธิ์ของกล้อง"),
    CAMERA_PERMISSIONS_MSG("此服務需要開啟相機權限，是否前往開啟?", "This service requires camera access, is it turned on?", "此服务需要开启相机权限，是否前往开启?", "このサービスはカメラへのアクセスが必要です。電源が入っていますか？", "บริการนี้ต้องการการเข้าถึงกล้องถ่ายรูปหรือไม่?"),
    STORAGE_PERMISSION("儲存空間權限", "Storage permission", "储存空间权限", "ストレージのアクセス許可", "สิทธิ์การจัดเก็บข้อมูล"),
    STORAGE_PERMISSION_MSG("此服務需要開啟儲存空間權限，是否前往開啟?", "This service needs to open the storage permission. Is it going to open?", "此服务需要开启储存空间权限，是否前往开启?", "このサービスはストレージ権限を開く必要があります。オープンする予定ですか？", "บริการนี้จำเป็นต้องเปิดการอนุญาตพื้นที่เก็บข้อมูลจะเปิดขึ้นหรือไม่?"),
    PERMISSION_CHECK_LOCATION_TITLE("位置權限", "Location Permission", "位置权限", "権限を検索する", "การอนุญาตสถานที่"),
    PERMISSION_CHECK_LOCATION_MSG("此服務需要位置權限，是否前往開啟?", "This service require location permission. Do you want to turn it on", "此服务需要位置权限，是否前往开启?", "このサービスにはロケーションの権限が必要です。あなたはそれに行きたいですか？", "บริการนี้ต้องการสิทธิ์ในตำแหน่งคุณต้องการไปหรือไม่?"),
    CHECK_LOCATION_SERVICE_TITLE("偵測到定位服務未開啟", "The Location Service is not enable.", "侦测到定位服务未开启", "ロケーションサービスが有効になっていません", "บริการตำแหน่งไม่ได้เปิดใช้งาน"),
    CHECK_LOCATION_SERVICE_MSG("是否前往開啟?", "Turn on Location Service?", "是否前往开启", "位置情報サービスを有効にする", "เปิดบริการตำแหน่ง"),
    DEVICE_BLE_NOT_SUPPORT("偵測到本裝置不支援藍芽BLE", "Device does not support BLE", "侦测到本装置不支援蓝芽BLE", "ブルートゥース BLE はサポートされない。", "ไม่พบอุปกรณ์นี้เพื่อสนับสนุน Bluetooth BLE"),
    DEVICE_BLE_IS_TURN_OFF_TITLE("偵測到藍芽裝置未開啟", "Bluetooth disabled", "侦测到蓝芽装置未开启", "ブルートゥース機能オフ", "ไม่พบอุปกรณ์บลูทู ธ"),
    DEVICE_BLE_IS_TURN_OFF_MSG("是否前往開啟?", "Turn on Bluetooth Service?", "是否前往开启", "位置情報サービスを有効にする", "เปิดบริการตำแหน่ง"),
    OK("確認", "OK", "确认", "確認", "ตกลง"),
    CANCEL("取消", "Cancel", "取消", "キャンセル", "ยกเลิก"),


    //-------------------------------------------------------------------------------------------------
    ;
    private String m_ch;
    private String m_eg;
    private String m_cn;
    private String m_jp;

    //------------------------------------------------------
    // Construct
    //------------------------------------------------------

    MultiMsg(String ch, String eg, String cn, String jp, String thi){
        m_ch = ch;
        m_eg = eg;
        m_cn = cn;
        m_jp = jp;
    }

    public String msg(){
        return m_eg;
    }





}
