package com.hiroia.jimmy.enums;

import android.util.Log;

import com.hiroia.jimmy.BuildConfig;
import com.hiroia.jimmy.comp.http.HttpFiles;
import com.hiroia.jimmy.comp.http.HttpGets;
import com.hiroia.jimmy.comp.http.HttpPosts;
import com.hiroia.jimmy.view.base.BaseActivity;
import com.hiroia.jimmy.view.home.HomeActivity;
import com.library.android_common.component.common.Pair;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.http.HttpFormData;
import com.library.android_common.component.http.HttpRaw;
import com.library.android_common.component.http.comp.Raw;
import com.library.android_common.constant.HttpCs;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by Raymond Zorro on 19/04/2018.
 * 1. 用於呼叫 Samantha Api
 * 2. Api 需要將其定義在此
 * 3. 以定義好 Get 與 Post method
 * 4. 內部使用 Multiple Thread
 */

public enum HttpDef {

    //----------------------------------------------------------------
    // COMMON
    TEST("測試用", "http://headers.jsontest.com/"),
    TEST_PIC("測試用的圖片", "https://image.freepik.com/free-photo/cute-cat-picture_1122-449.jpg"),
    TEST_PIC_1("測試用的圖片", "https://image.freepik.com/free-photo/cute-cat-picture_1122-500.jpg"),
    TEST_PIC_2("測試用的圖片", "https://image.freepik.com/free-photo/cute-cat-picture_1122-501.jpg"),
    TEST_PIC_3("測試用的圖片", "https://image.freepik.com/free-photo/cute-cat-picture_1122-502.jpg"),
    PLAY_STORE_SAMANTHA_URI("Samantha 上架網址", "https://play.google.com/store/apps/details?id=com.hiroia.samantha"),

    //----------------------------------------------------------------
    // USER //
    LOGIN("登入", "https://api3.hiroia.com/api/v3/user/login"),
    FB_LOGIN("FB 登入", "https://api3.hiroia.com/api/v3/user/jimmyfblogin"),
    REGISTER_ACCOUNT("建立帳號", "https://api3.hiroia.com/api/v3/user/create"),
    RESET_PASSWORD("重置密碼", "https://api3.hiroia.com/api/v3/user/resetpassword"),
    UPDATE("更新帳號資料", "https://api3.hiroia.com/api/v3/user/update"),
    GET_USER_INFO("Get user info", "https://api3.hiroia.com/api/v3/user/get"),
    CHANGE_PASSWORD("Change password", "https://api3.hiroia.com/api/v3/user/changepassword"),
    UPLOAD_USER_PHOTO("Upload user photo", "https://api3.hiroia.com/api/v3/user/photo/add"),
    GET_TARGET_USER_PHOTO("Get target user photo", "https://api3.hiroia.com/api/v3/photo/get"),
    UPLOAD_ACCOUNT("Upload Account", "https://api3.hiroia.com/api/v3/user/update"),

    // RECORD、RECIPE //
    LIST_JRECORDS_OF_USER("List records by user", "https://jimmy.hiroia.com/api/v1/jrecord/list/user"),
    CREATE_J_RECORD("Create J Record", "https://jimmy.hiroia.com/api/v1/jrecord/create"),
    SET_AS_J_RECORD_DAILY("Set as JRecord daily", "https://jimmy.hiroia.com/api/v1/jrecord/set/daily/recipe"),


    HIROIA_FB_LOGIN("HIROIA FB 登入", "https://api3.hiroia.com/api/v3/user/hiroiafblogin"), // HIROIA FB 登入
    SAMANTHA_FB_LOGIN("SAMANTHA FB 登入", "https://api3.hiroia.com/api/v3/user/samanthafblogin"), // SAMANTHA FB 登入
    ;

    //--------------------------------------------------------------------------
    // declare
    //--------------------------------------------------------------------------
    private static final String POST_METHOD = "POST";
    private static final String GET_METHOD = "GET";
    private static final String FORM_DATA = "FORM_DATA";
    private static final String RAW_DATA = "RAW_DATA";

    private HttpDef m_self = null;
    private String m_sDesc = StrUtil.EMPTY;
    private String m_sUrl = StrUtil.EMPTY;
    private String m_sCookie = StrUtil.EMPTY;
    private String m_sCLength = StrUtil.EMPTY;
    private String m_sCType = StrUtil.EMPTY;
    private String m_sConnType = StrUtil.EMPTY;
    private String m_sTag = StrUtil.EMPTY;
    //---------------------------------------

    private Raw m_raw = Raw.create();
    private Lst<Pair<String, String>> m_formParams = Lst.of();
    private Lst<Pair<String, File>> m_formFiles = Lst.of();
    private Map<String, String> m_Map = null;
    private ArrayList<?> m_Lst = null;
    private HttpDefResponse m_Res;
    private Class<?> m_Cls = HttpDef.class;
    private boolean m_isDevMode;

    //--------------------------------------------------------------------------
    // construct
    //--------------------------------------------------------------------------

    HttpDef(String sDesc, String sUrl) { // Basic
        m_sDesc = sDesc;
        m_sUrl = sUrl;
        m_Map = new LinkedHashMap<>();
        m_self = this;
        m_sTag = m_self.name();
        dev(); // 直接在 init 呼叫 dev mode ，就不需要動到外部的 code

    }

    HttpDef(String sDesc, String sUrl, Class<?>... ca) { // 加上有使用到的 class
        m_sDesc = sDesc;
        m_sUrl = sUrl;
        m_Lst = Lst.of(ca).toList();
        m_Map = new LinkedHashMap<>();
        m_self = this;
        m_sTag = m_self.name();
        dev(); // 直接在 init 呼叫 dev mode ，就不需要動到外部的 code
    }

    //--------------------------------------------------------------------------
    // public method
    //--------------------------------------------------------------------------
    public String getUrl() {
        return m_sUrl;
    }

    public String getsDesc() {
        return m_sDesc;
    }

    public ArrayList<?> getPairs() {
        return m_Lst;
    }

    public String getCookie() {
        return m_sCookie;
    }

    public String getCType() {
        return m_sCType;
    }

    public String getCLength() {
        return m_sCLength;
    }

    public String getTag() {
        return StrUtil.bracketsOf(StrUtil.spaceOf(m_sTag));
    }

    //--------------------------------------------------------------------------
    // http connection
    //--------------------------------------------------------------------------
    // Http Method //
    public HttpDef post() {
        m_sConnType = POST_METHOD;
        return m_self;
    }

    public HttpDef get() {
        m_sConnType = GET_METHOD;
        return m_self;
    }

    public HttpDef form() {
        m_sConnType = FORM_DATA;
        return m_self;
    }

    public HttpDef raw() {
        m_sConnType = RAW_DATA;
        return m_self;
    }

    //---------------------------------------------------
    // Add Param //
    // Post //
    public HttpDef addParam(String sKey, String sVal) {
        m_Map.put(sKey, sVal);
        return m_self;
    }

    public HttpDef addParamIf(boolean enable, String sKey, String sVal) {
        if (enable)
            addParam(sKey, sVal);
        return m_self;
    }

    // Form Data //
    public HttpDef addFormData(String sKey, String sVal) {
        m_formParams.add(Pair.of(sKey, sVal));
        return m_self;
    }

    public HttpDef addFormFile(String sKey, File f) {
        m_formFiles.add(Pair.of(sKey, f));
        return m_self;
    }

    public HttpDef addFormFileIf(boolean enable, String sKey, File f) {
        if (enable)
            addFormFile(sKey, f);
        return m_self;
    }

    public HttpDef addFormDataIf(boolean enable, String sKey, String sVal) {
        if (enable)
            addFormData(sKey, sVal);
        return m_self;
    }

    // Raw Data //
    public HttpDef addRaw(Raw raw) {
        m_raw = raw;
        return m_self;
    }

    public HttpDef addRawObject(String key, String val) {
        m_raw.addObject(key, val);
        return m_self;
    }

    public HttpDef addRawArray(String key, Pair<String, String>... ps) {
        m_raw.addArray(key, ps);
        return m_self;
    }

    public HttpDef addRawObjectIf(boolean enable, String key, String val) {
        if (enable)
            addRawObject(key, val);
        return m_self;
    }

    public HttpDef addRawArrayIf(boolean enable, String key, Pair<String, String>... ps) {
        if (enable)
            addRawArray(key, ps);
        return m_self;
    }

    public HttpDef addRawIf(boolean enable, Raw raw) {
        if (enable)
            addRaw(raw);
        return m_self;
    }

    //-----------------------------------------
    // Comp //
    private HttpDef dev() { // develop api

        if (!BuildConfig.DEBUG) return m_self; // 若是 Release 版本不執行
        if (m_isDevMode) return m_self; // 若是 已執行過 不再執行
        //-----------------------------
        m_sUrl = HttpCs.HTTPS.concat(HttpCs.TEST_API)
                .concat(m_sUrl.substring(HttpCs.HTTPS.length(), m_sUrl.length()));
        m_isDevMode = true;

        return m_self;
    }

    public HttpDef notTest() {
        m_sUrl = m_sUrl.replace(HttpCs.TEST_API, StrUtil.EMPTY);
        return m_self;
    }

    //--------------------------------------------------------------------------
    // trigger
    //--------------------------------------------------------------------------

    public void println_d(Class<?>... cls) { // d 代表 log.d ， 輸入class 是 filter 的 key word
        m_Res = null;
        m_Cls = cls[0];
        //--------------------------

        launchConn();
    }

    public void trigger(HttpDef.HttpDefResponse response) {
        m_Res = response;
        //--------------------------
        launchConn();
    }

    public void trigger_f(List<Pair<String, String>> params, Pair<String, File> fileParam, HttpDef.HttpDefResponse response) { // f 代表 file loader
        m_Res = response;
        //--------------------------
        Log.d(HttpDef.class.getSimpleName(), StrUtil.bracketsOf(m_self.name()) + " URL : ".concat(m_sUrl));

        try {

            Pair<String, Pair<List<Pair<String, String>>, Pair<String, File>>> ps =
                    Pair.of(m_self.getUrl(), Pair.of(params, fileParam));

            HttpFiles httpUploader = new HttpFiles();
            Pair<String, JSONObject> pair = httpUploader.execute(ps).get();

            Log.d(HttpCs.class.getSimpleName(), StrUtil.bracketsOf(m_self.name()) + " Http File Loader : ".concat(pair.k()));

            if (m_Res != null)
                m_Res.response(pair.k(), pair.v());

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    //--------------------------------------------------------
    // private preferences
    //--------------------------------------------------------
    private void launchConn() {

        Log.d(HttpDef.class.getSimpleName(), StrUtil.bracketsOf(m_self.name()) + " URL : ".concat(m_sUrl));
        checkStatus(); // 用以 check connection method
        //--------------------------------------------------------

        switch (m_sConnType) {
            case GET_METHOD:    // GET Method
                try {

                    HttpGets httpGet = new HttpGets();
                    Pair<String, JSONObject> pair = httpGet.execute(Pair.of("URL", m_self.getUrl())).get();

                    Log.d(m_Cls.getName(), StrUtil.bracketsOf(m_self.name()) + " [ GET Method ] " + " result : " + pair.k());

                    if (m_Res != null)
                        m_Res.response(pair.k(), pair.v());

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;

            //--------------------------------------------------------
            case POST_METHOD:   // POST method
                try {

                    List<NameValuePair> lst = new ArrayList<>();
                    for (Map.Entry<String, String> map : m_Map.entrySet())
                        lst.add(new BasicNameValuePair(map.getKey(), map.getValue()));

                    HttpPosts httpPost = new HttpPosts();
                    Pair<String, JSONObject> pair = httpPost.execute(Pair.of(lst, m_self.getUrl())).get();

                    Log.d(m_Cls.getName(), StrUtil.bracketsOf(m_self.name()) + " [ POST Method ] " + " result : " + pair.k());
                    if (m_Res != null)
                        m_Res.response(pair.k(), pair.v());

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;

            //--------------------------------------------------------
            case FORM_DATA: // Form Data
                String url = m_self.getUrl();
                try {

                    HttpFormData httpFormData = new HttpFormData();
                    Pair<String, JSONObject> pair = httpFormData.execute(Pair.of(url, Pair.of(m_formParams, m_formFiles))).get();

                    Log.d(m_Cls.getName(), StrUtil.bracketsOf(m_self.name()) + "[ FORM DATA ] " + " result : " + url);

                    if (m_Res != null)
                        m_Res.response(pair.k(), pair.v());

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;

            //--------------------------------------------------------
            case RAW_DATA: // Raw data
                try {

                    HttpRaw httpRaw = new HttpRaw();
                    Pair<String, JSONObject> pair = httpRaw.execute(Pair.of(m_raw, m_self.getUrl())).get();

                    Log.d(m_Cls.getName(), StrUtil.bracketsOf(m_self.name()) + "[ RAW DATA ] " + " result : " + m_self.getUrl());

                    if (m_Res != null)
                        m_Res.response(pair.k(), pair.v());

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                m_raw = new Raw();

                m_raw = new Raw();
                break;

        } // switch

    }

    private void checkStatus() { // 檢查 type 是否正確
        if (m_sConnType.isEmpty()) {
            Log.e(HttpDef.class.getName(), HttpDef.class.getName().concat("Connection method please set [ Get ] or [ Post ] or [ Form ] or [ Raw ]."));
            return;
        }
    }

    //--------------------------------------------------------
    // Listener
    //--------------------------------------------------------
    public interface HttpDefResponse {
        void response(String response, JSONObject jsObj);
    }

}

