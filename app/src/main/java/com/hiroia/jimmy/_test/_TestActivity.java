package com.hiroia.jimmy._test;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.view.View;

import com.hiroia.jimmy.R;
import com.hiroia.jimmy.comp.view.dialog.SpinnerDialog;
import com.hiroia.jimmy.comp.view.picker.DialogNumberPicker;
import com.hiroia.jimmy.cs.JimmyCs;
import com.hiroia.jimmy.view.base.BaseActivity;
import com.library.android_common.component.TimeCounter;
import com.library.android_common.component.date.hms.HMS;
import com.library.android_common.util.ActivityUtil;
import com.library.android_common.util.LogUtil;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import static com.hiroia.jimmy.comp.view.picker.DialogNumberPicker.DecimalPlace.SECOND_DIGITAL;

/**
 * Create By Ray on 2018/10/25
 * Just for test
 */
public class _TestActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout._activity_test);


        SpinnerDialog dialog = new SpinnerDialog(this);
        dialog.show();
    }


    private void toOpenAnotherApp(){
        //------------------------------------------------------------------------------------
        // 用以測試 開啟 OTA App //
        getFab().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActManager().retrieveAnotherApp(
                        JimmyCs.OTA_PACKAGE_PATH,
                        JimmyCs.OTA_PACKAGE_PATH_MAIN_ACTIVITY,
                        new ActivityUtil.OnSwitchAnotherAppListener() {
                            @Override
                            public void onAppNotFound() {
                                // do on app not found
                                showToast("App not found. ");
                            }
                        });
            }
        });
        //------------------------------------------------------------------------------------
    }

    //--------------------------------------------------------------------------------------
    private void setCircularProgress(){

        final CircularProgressBar cpb = findViewById(R.id._test_circular_progressbar);
        cpb.setProgressMax(100);
        cpb.setProgress(0);

        TimeCounter tc = new TimeCounter(); // test anim
        tc.setOnCountingListener(200, TimeCounter.ONE_HUNDRED_SEC, new TimeCounter.OnCountingListener() {
            @Override
            public void onTick(long m) {
//                float val =  cpb.getProgress() + 1;
//                cpb.setProgress(val);
                updateProgress(cpb);
            }

            @Override
            public void onFinish() {

            }
        });

    }

    // Pour Progress Bar 結束時的動畫效果 //
    private void updateProgress(final CircularProgressBar cpb) {
        ValueAnimator anim = ValueAnimator.ofInt((int) cpb.getProgress(), (int) cpb.getProgress() + 2); // Thumb 從 A pos 到 B pos
        anim.setDuration(200);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int pos = (Integer) animation.getAnimatedValue();
                cpb.setProgress(pos);
            }
        });
        anim.start();
    }


    private void initPieChartProgress() {
//
//        final  PieChartProgress pp = (PieChartProgress) findViewById(R.id._activity_pie_chart_progress);
//        pp.setBarColor(getColor(R.color.tmode_score_mode_color_orange));
//        pp.setProgressColor(getColor(R.color.jimmyBlue));
//
//        TimeCounter tc = new TimeCounter();
//        tc.setOnCountingListener(TimeCounter.ONE_HUNDRED_SEC, new TimeCounter.OnCountingListener() {
//            @Override
//            public void onTick(long m) {
//                pp.addProgress();
//            }
//
//            @Override
//            public void onFinish() {
//
//            }
//        });

    }

    private void showDialogPicker() {

//        final DialogPicker dialogPicker = new DialogPicker(this);
//        dialogPicker.show(new DialogPicker.OnClickEvent() {
//            @Override
//            public void confirm(String value) {
//                showToast(dialogPicker.getPickerValue());
//            }
//        });


        DialogNumberPicker dialog = new DialogNumberPicker(this);
        dialog.setView(3, SECOND_DIGITAL);
        dialog.show(new DialogNumberPicker.OnConfirmFlowRateListener() {
            @Override
            public void onConfirm(String val) {
                showToast(" Val = " + val);
            }
        });

    }

}
