package com.hiroia.jimmy._test;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.hiroia.jimmy.R;
import com.library.android_common.component.TimeCounter;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

/**
 * Created by Bruce on 2018/11/30.
 */
public class _TestPieChartActivity extends Activity implements OnChartValueSelectedListener, View.OnClickListener {

    private int m_nProgressVal = 0;
    private int m_nBackgroungVal = 100;
    private PieChart mPieChart;

    //显示百分比
    private Button btn_show_percentage;
    //显示类型
    private Button btn_show_type;
    //x轴动画
    private Button btn_anim_x;
    //y轴动画
    private Button btn_anim_y;
    //xy轴动画
    private Button btn_anim_xy;
    //保存到sd卡
    private Button btn_save_pic;
    //显示中间文字
    private Button btn_show_center_text;
    //旋转动画
    private Button btn_anim_rotating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pie_chart_test);
        initView();
    }

    //初始化View
    private void initView() {

        btn_show_percentage = (Button) findViewById(R.id.btn_show_percentage);
        btn_show_percentage.setOnClickListener(this);
        btn_show_type = (Button) findViewById(R.id.btn_show_type);
        btn_show_type.setOnClickListener(this);
        btn_anim_x = (Button) findViewById(R.id.btn_anim_x);
        btn_anim_x.setOnClickListener(this);
        btn_anim_y = (Button) findViewById(R.id.btn_anim_y);
        btn_anim_y.setOnClickListener(this);
        btn_anim_xy = (Button) findViewById(R.id.btn_anim_xy);
        btn_anim_xy.setOnClickListener(this);
        btn_save_pic = (Button) findViewById(R.id.btn_save_pic);
        btn_save_pic.setOnClickListener(this);
        btn_show_center_text = (Button) findViewById(R.id.btn_show_center_text);
        btn_show_center_text.setOnClickListener(this);
        btn_anim_rotating = (Button) findViewById(R.id.btn_anim_rotating);
        btn_anim_rotating.setOnClickListener(this);

        // 這塊是整理過的 ------------------------------------------------------------------
        //饼状图
        mPieChart = (PieChart) findViewById(R.id.mPieChart);
        mPieChart.setUsePercentValues(true);              //使用百分比显示
        mPieChart.getDescription().setEnabled(false);     //设置PieChart图表的描述 取消右下角描述
        mPieChart.setExtraOffsets(5, 5, 5, 5); //设置PieChart图表上下左右的偏移，类似于外边距
//        mPieChart.setBackgroundColor(Color.BLUE);         //设置PieChart图表背景色
        mPieChart.setDragDecelerationFrictionCoef(0.95f); //设置PieChart图表转动阻力摩擦系数[0,1]
//        mPieChart.setRotationAngle(0);                    //设置PieChart图表起始角度

        // 触摸旋转
        mPieChart.setRotationEnabled(true);               //设置PieChart图表是否可以手动旋转
        mPieChart.setHighlightPerTapEnabled(true);        //设置PieChart图表点击Item高亮是否可用

        // 內圈
        mPieChart.setDrawHoleEnabled(true);               //是否显示PieChart内部圆环(true:下面属性才有意义)
        mPieChart.setHoleColor(Color.TRANSPARENT);              //设置PieChart内部圆的颜色
        mPieChart.setBackgroundColor(Color.TRANSPARENT);
        mPieChart.setTransparentCircleColor(Color.TRANSPARENT);   //设置PieChart内部透明圆与内部圆间距(31f-28f)填充颜色
        mPieChart.setTransparentCircleAlpha(0);         //设置PieChart内部透明圆与内部圆间距(31f-28f)透明度[0~255]数值越小越透明
        mPieChart.setHoleRadius(58f);                     //设置PieChart内部圆的半径(这里设置28.0f)
        mPieChart.setTransparentCircleRadius(61f);        //设置PieChart内部透明圆的半径(这里设置31.0f)

        // 轉動區塊的文字設定
//        mPieChart.setEntryLabelColor(Color.TRANSPARENT);        //设置pieChart图表文本字体颜色
//        mPieChart.setEntryLabelTextSize(12f);             //设置pieChart图表文本字体大小

//        mPieChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);// 设置pieChart图表展示动画效果
        mPieChart.animateY(1400, Easing.EasingOption.EaseInBounce);// 设置pieChart图表展示动画效果

        // Listener
//        mPieChart.setOnChartValueSelectedListener(this);

        // 關掉 圖例
        Legend l = mPieChart.getLegend();
        l.setEnabled(false);

        //设置数据
        setData(0, 100);
        // 截止線 ------------------------------------------------------------------

//        ArrayList<PieEntry> entries = new ArrayList<>();
//        entries.add(new PieEntry(25, "优秀"));
//        entries.add(new PieEntry(35, "满分"));
//
//        //设置数据
//        setData(entries);


//        Legend l = mPieChart.getLegend();
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
//        l.setOrientation(Legend.LegendOrientation.VERTICAL);
//        l.setDrawInside(false);
//        l.setXEntrySpace(7f);
//        l.setYEntrySpace(0f);
//        l.setYOffset(0f);
    }

    //设置中间文字
//    private SpannableString generateCenterSpannableText() {
//        //原文：MPAndroidChart\ndeveloped by Philipp Jahoda
//        SpannableString s = new SpannableString("刘某人程序员\n我仿佛听到有人说我帅");
//        //s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
//        //s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
//        // s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
//        //s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
//        // s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
//        // s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
//        return s;
//    }

    // 做成參照 Seek bar 的設定方式，最大、最小、設定值，做成一個接口，讓其內部的數值等比例添加

    //设置数据
    private void setData(int progressVal, int backgroungVal) {
        ArrayList<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(progressVal, ""));
        entries.add(new PieEntry(backgroungVal, ""));

        PieDataSet dataSet = new PieDataSet(entries, "");
//        dataSet.setSliceSpace(3f);
//        dataSet.setSelectionShift(5f); //设置饼状Item被选中时变化的距离

//        //数据和颜色
//        ArrayList<Integer> colors = new ArrayList<>();
//        for (int c : ColorTemplate.VORDIPLOM_COLORS)
//            colors.add(c);
//        for (int c : ColorTemplate.JOYFUL_COLORS)
//            colors.add(c);
//        for (int c : ColorTemplate.COLORFUL_COLORS)
//            colors.add(c);
//        for (int c : ColorTemplate.LIBERTY_COLORS)
//            colors.add(c);
//        for (int c : ColorTemplate.PASTEL_COLORS)
//            colors.add(c);

//        colors.add(ColorTemplate.getHoloBlue());
//        dataSet.setColors(Lst.of(getColor(R.color.jimmyBlue), getColor(R.color.tmode_score_mode_color_orange)).toList());
        dataSet.setColors(Lst.of(getColor(R.color.jimmyBlue), Color.TRANSPARENT).toList());

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.TRANSPARENT);
        mPieChart.setData(data);
        mPieChart.highlightValues(null);
        //刷新
        mPieChart.invalidate();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //显示百分比
            case R.id.btn_show_percentage:

                TimeCounter timeCounter = new TimeCounter();
                timeCounter.setOnCountingListener(50000, new TimeCounter.OnCountingListener() {
                    @Override
                    public void onTick(long m) {
                        setData(m_nProgressVal += 2, m_nBackgroungVal -= 2);
                    }

                    @Override
                    public void onFinish() {

                    }
                });

//                for (IDataSet<?> set : mPieChart.getData().getDataSets())
//                    set.setDrawValues(!set.isDrawValuesEnabled());
//
//                mPieChart.invalidate();
                break;
            //显示类型
            case R.id.btn_show_type:
                if (mPieChart.isDrawHoleEnabled())
                    mPieChart.setDrawHoleEnabled(false);
                else
                    mPieChart.setDrawHoleEnabled(true);
                mPieChart.invalidate();
                break;
            //x轴动画
            case R.id.btn_anim_x:
                mPieChart.animateX(1400);
                break;
            //y轴动画
            case R.id.btn_anim_y:
                mPieChart.animateY(1400);
                break;
            //xy轴动画
            case R.id.btn_anim_xy:
                mPieChart.animateXY(1400, 1400);
                break;
            //保存到sd卡
            case R.id.btn_save_pic:
                mPieChart.saveToPath("title" + System.currentTimeMillis(), "");
                break;
            //显示中间文字
            case R.id.btn_show_center_text:
                if (mPieChart.isDrawCenterTextEnabled())
                    mPieChart.setDrawCenterText(false);
                else
                    mPieChart.setDrawCenterText(true);
                mPieChart.invalidate();
                break;
            //旋转动画
            case R.id.btn_anim_rotating:
//                mPieChart.spin(1000, mPieChart.getRotationAngle(), mPieChart.getRotationAngle() + 360, Easing.EasingOption.EaseInCubic);
                break;
        }
    }


    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
