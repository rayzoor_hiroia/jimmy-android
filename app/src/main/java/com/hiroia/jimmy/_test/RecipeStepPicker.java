package com.hiroia.jimmy._test;

/**
 * Create By Ray on 2018/10/31
 */
public class RecipeStepPicker {



    //---------------------------------------
    // Construct
    //---------------------------------------
    public RecipeStepPicker(){

        init();
    }

    private void init(){

    }


    //---------------------------------------
    // Public Method
    //---------------------------------------

//    public void setModel(ForumalStepModel... model){
//
//    }

    public void setOnPcikerFinishListener(OnRecipePickerFinishListener listener){

    }


    //---------------------------------------
    // Listener
    //---------------------------------------
    public interface OnRecipePickerFinishListener{
        void onFirstWaterFinish();
        void onFinish();
    }


    public static void main(String[] args){

        RecipeStepPicker picker = new RecipeStepPicker();
//        picker.setModel();
        picker.setOnPcikerFinishListener(new OnRecipePickerFinishListener() {
            @Override
            public void onFirstWaterFinish() {

            }

            @Override
            public void onFinish() {

            }
        });




    }

}
