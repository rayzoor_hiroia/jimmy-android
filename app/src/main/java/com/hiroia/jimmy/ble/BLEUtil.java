package com.hiroia.jimmy.ble;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.AdvertiseData;
import android.content.IntentFilter;
import android.os.ParcelUuid;
import android.text.TextUtils;
import android.util.Log;

import com.hiroia.jimmy.ble.comp.BLEAdvertiseRecord;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.ByteUtil;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.JIMMY_SCAN_SERVICE_UUID;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.JIMMY_SCAN_SERVICE_UUID_LATEST_VERSION;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.JIMMY_SCAN_SERVICE_UUID_LATEST_VERSION_2_2;

/**
 * Create By Ray on 2018/11/1
 */
public class BLEUtil {

    // BLEService Intent filter //
    public static IntentFilter bleIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        BLEService.sm_actionList.forEach(new Lst.IConsumer<String>() {
            @Override
            public void runEach(int i, String action) {
                intentFilter.addAction(action);
            }
        });
        return intentFilter;
    }


    public static boolean isJimmyDevice(BluetoothDevice device, byte[] data) {

        // 檢查是否為空 //
        if (device == null) return false;

        // 檢查是否 Name 不存在 //
        if (device.getName() == null) return false;

        // 檢查 Name 是否為 Null String //
        if (device.getName().equals(StrUtil.STR_NULL)) return false;

        //-----------------------------------------------------------------------

        final String deviceName = device.getName();
        final String macAddress = device.getAddress();

        LogUtil.d(BLEUtil.class, "deviceName = " + deviceName + " Mac Address = " + macAddress + " data = " + ByteUtil.of(data).toByteArrayStr());
        LogUtil.d(BLEUtil.class, "deviceName = " + deviceName + " Mac Address = " + macAddress + ", data[25] = " + data[25] + ", data[26] = " + data[26] + ", data[27] = " + data[27] + ", data[28] = " + data[28] + ", data[29] = " + data[29] + ", data[30] = " + data[30]);

        if (data[25] == 32 && data[26] == 74 && data[27] == 73 && data[28] == 77 && data[29] == 77 && data[30] == 89) {
            return true;
        }

        // MP2 版本後的 Mac Address 前 8 碼 98:F9:C7
        if (macAddress.substring(0, 8).equals("98:F9:C7")) {
            return true;
        }

        // Parsing Data //
        AdvertiseData ad = new AdvertiseData.Builder()
                .addServiceUuid(ParcelUuid.fromString(UUID.nameUUIDFromBytes(data).toString()))
                .build();


        // To find uuid //
        List<ParcelUuid> pUuids = ad.getServiceUuids();
        final List<String> uuids = new ArrayList<>();

        Lst.of(pUuids).forEach(new Lst.IConsumer<ParcelUuid>() {
            @Override
            public void runEach(int i, ParcelUuid parcelUuid) {
                LogUtil.d(BLEUtil.class, "ParcelUuid -   i : " + i + " Device = " + deviceName + " , Mac Address = " + macAddress + " UUID = " + parcelUuid.getUuid().toString());
                uuids.add(parcelUuid.getUuid().toString());
            }
        });

        // 這是針對特定機種 DVT 版本所做的 work.
        boolean isContain = Lst.of(uuids).containsAnyOf(JIMMY_SCAN_SERVICE_UUID, JIMMY_SCAN_SERVICE_UUID_LATEST_VERSION, JIMMY_SCAN_SERVICE_UUID_LATEST_VERSION_2_2, "884a7a62-f0f0-3f74-b6a9-7d5325a44fe6", "ebe0f6c4-ef6b-3818-88e8-d4ffb2bbb95b", "dc45512b-ccb3-34e7-b42d-b5825fd64128");
        return isContain;
    }


//    public static boolean isJimmyDevice(BluetoothDevice device, byte[] scanRecord) {
//
//        // 檢查是否為空 //
//        if (device == null) return false;
//
//        // 檢查是否 Name 不存在 //
//        if (device.getName() == null) return false;
//
//        // 檢查 Name 是否為 Null String //
//        if (device.getName().equals(StrUtil.STR_NULL)) return false;
//
//        //-----------------------------------------------------------------------
//
//        final String deviceName = device.getName();
//        final String macAddress = device.getAddress();
//
//        // Parsing Data //
//        AdvertiseData data = new AdvertiseData.Builder()
//                .addServiceUuid(ParcelUuid.fromString(UUID.nameUUIDFromBytes(scanRecord).toString()))
//                .build();
//
//
//        // To find uuid //
//        List<ParcelUuid> pUuids = data.getServiceUuids();
//        final List<String> uuids = new ArrayList<>();
//
//        Lst.of(pUuids).forEach(new Lst.IConsumer<ParcelUuid>() {
//            @Override
//            public void runEach(int i, ParcelUuid parcelUuid) {
//                LogUtil.d(BLEUtil.class, "ParcelUuid -   i :" + i + " Device = " + deviceName + " , Mac Address = " + macAddress + " UUID = " + parcelUuid.getUuid().toString());
//                uuids.add(parcelUuid.getUuid().toString());
//            }
//        });
//
//        return Lst.of(uuids).containsAnyOf(JIMMY_SCAN_SERVICE_UUID, JIMMY_SCAN_SERVICE_UUID_LATEST_VERSION, JIMMY_SCAN_SERVICE_UUID_LATEST_VERSION_2_2, "884a7a62-f0f0-3f74-b6a9-7d5325a44fe6", "ebe0f6c4-ef6b-3818-88e8-d4ffb2bbb95b", "dc45512b-ccb3-34e7-b42d-b5825fd64128");
//    }

    // Reference : https://stackoverflow.com/questions/22016224/ble-obtain-uuid-encoded-in-advertising-packet/49106868
    public static void println_record(BluetoothDevice device, byte[] scanRecord) {

        final String deviceName = device.getName();
        final String macAddress = device.getAddress();

        AdvertiseData data = new AdvertiseData.Builder()
                .addServiceUuid(ParcelUuid.fromString(UUID.nameUUIDFromBytes(scanRecord).toString()))
                .build();

        List<ParcelUuid> uuids = data.getServiceUuids();
        Lst.of(uuids).forEach(new Lst.IConsumer<ParcelUuid>() {
            @Override
            public void runEach(int i, ParcelUuid parcelUuid) {
                LogUtil.d(BLEUtil.class, " Device = " + deviceName + " , Mac Address = " + macAddress + " UUID = " + parcelUuid.getUuid().toString());
            }
        });
    }


}
