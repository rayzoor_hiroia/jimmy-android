package com.hiroia.jimmy.ble;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.library.android_common.component.ThreadPool;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.ActivityUtil;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.ThreadUtil;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.BLE_TAG_CONNECT;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.BLE_TAG_FIRMWARE_VERSION;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.BLE_TAG_HANDLE_JIMMY_INFO_DATA;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.BLE_TAG_RECEIVER;

/**
 * Create By Ray on 2018/11/1
 */
public class BLEJimmy {

    private static BLEJimmy sm_self = null;

    // Comp //
    private Context m_ctx = null;
    private Activity m_currActivity;
    private BLEService m_bleService = null;
    private ThreadPool m_tp;
    private Lst<BluetoothDevice> m_devices = Lst.of();
    private BluetoothDevice m_currBleDevice;
    private BLECmd m_bleCmd = new BLECmd();

    // Listener //
    private ScanDeviceListener m_scanDeviceListener;
    private ConnectStateListener m_connChgListener;
    private JimmyStatusInfoListener m_jimmyInfoListener;

    // Checker //
    private boolean m_bIsScanning = false; // 是否掃描中
    private boolean m_bIsConnected = false; // 是否連線中

    //---------------------------------------------------------------
    // Construct
    //---------------------------------------------------------------
    public boolean init(Context c) {
        JimmyReceiver receiver = new JimmyReceiver();

        if (m_ctx != null) { // check init status !
            LogUtil.d(this.getClass(), " releasing context ");
            release();
        }

        m_ctx = c;
        m_tp = new ThreadPool(ThreadPool.CORE_THREAD_SIZE_FIVE);

        Intent bleServiceIntent = new Intent(m_ctx, BLEService.class);
        m_ctx.bindService(bleServiceIntent, m_bleServiceConnection, Context.BIND_AUTO_CREATE);
        m_ctx.registerReceiver(receiver, BLEUtil.bleIntentFilter());

        sm_self = this;
        return m_ctx != null;
    }

    public boolean release() {
        m_ctx.unbindService(m_bleServiceConnection);
        m_ctx = null;
        return m_ctx == null;
    }

    public static BLEJimmy getInstance() {
        return sm_self;
    }

    //---------------------------------------------------------------
    // Service Handle
    //---------------------------------------------------------------
    private final ServiceConnection m_bleServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            LogUtil.d(this.getClass(), "onServiceConnected");
            m_bleService = ((BLEService.LocalBinder) service).getService();

            if (!m_bleService.init()) {
                LogUtil.e(this.getClass(), "BLEService init failed!");
                return;
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            m_bleService = null;
        }
    };

    //---------------------------------------------------------------
    // Broadcast Receiver
    //---------------------------------------------------------------
    private class JimmyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {

                // 更新裝置被掃瞄 //
                case BLEService.ACTION_DEVICE_FOUND:

                    BluetoothDevice bleDevice = intent.getParcelableExtra(BLEService.EXTRA_DEVICE);
                    m_devices.addIf(!m_devices.contains(bleDevice), bleDevice);

                    if (bleDevice != null) {
                        LogUtil.d(BLEJimmy.class, BLEJimmyGattAttributes.BLE_TAG_SCAN
                                + " device name : " + bleDevice.getName()
                                + ", device mac address : " + bleDevice.getAddress()
                        );

                    } else
                        LogUtil.d(BLEJimmy.class, " ble device is null ");

                    //------------------------------------------------
                    // send to UI layer //
                    ActivityUtil.of(m_currActivity).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            m_scanDeviceListener.scanDeviceList(m_devices.toList());
                        }
                    });

                    break;

                // 更新連線狀態：連線、斷線、連線中、斷線中 //
                case BLEService.ACTION_CONNECTION_STATE:

                    final int connectionState = intent.getIntExtra(BLEService.EXTRA_CONNECTION_STATE, -1);
                    LogUtil.d(BLEJimmy.class, BLE_TAG_RECEIVER + " Receiver is Connected " + (connectionState == BLEService.CONNECTION_STATE_CONNECTED));

                    m_bIsConnected = connectionState == BLEService.CONNECTION_STATE_CONNECTED; // update current connection state
                    LogUtil.d(BLEJimmy.class, BLE_TAG_RECEIVER + " Receiver m_bIsConnected = " + m_bIsConnected);

                    //-------------------------------------------------
                    // send to UI layer //
                    ActivityUtil.of(m_currActivity).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            m_connChgListener.onConnectionStateChange(connectionState); // to listener connection state.
                        }
                    });

                    break;

                // JIMMY 裝置 Info. //
                case BLEService.ACTION_UPDATE_JIMMY_INFO:

                    // Jimmy Info //
                    final byte[] data = intent.getByteArrayExtra(BLEService.EXTRA_JIMMY_INFO);
                    m_tp.setAction(this.getClass().getSimpleName() + " handleData() ");
                    m_tp.execute(new Runnable() {
                        @Override
                        public void run() {
                            handleData(data);
                        }
                    });
                    break;

                // Update JIMMY FirmWare Version
                case BLEService.ACTION_FIRMWARE_VERSION:

                    // Jimmy FirmWare Version //
                    boolean isOld = intent.getBooleanExtra(BLEService.EXTRA_JIMMY_IS_OLD_VERSION, false);
                    LogUtil.d(BLEJimmy.class, BLE_TAG_FIRMWARE_VERSION + " JIMMY firmware is old Version = " + isOld);
                    m_bleCmd.setFMVersion(isOld);

                    break;
            }
        }
    }

    //---------------------------------------------------------------
    // BLE Action
    //---------------------------------------------------------------
    // 開始掃描 //
    public boolean startScan() {
        if (LogUtil.Check.e(m_bleService == null, BLEJimmy.class, "startScan(), m_bleService is null "))
            return false;
        m_bleService.startScan();
        m_bIsScanning = true;
        return true;
    }

    // 停止掃描 //
    public boolean stopScan() {
        if (LogUtil.Check.e(m_bleService == null, BLEJimmy.class, "stopScan(), m_bleService is null "))
            return false;
        m_bleService.stopScan();
        m_bIsScanning = false;
        return true;
    }

    // 是否為掃描中 //
    public boolean isScanning() {
        return m_bIsScanning;
    }

    //---------------------------------------------------------------
    // 連線至 //
    public boolean connect(BluetoothDevice bleDevice) {
        if (LogUtil.Check.e(m_bleService == null, " BleService is Null "))
            return false;

        // trying to connect //
        LogUtil.d(BLEJimmy.class, BLE_TAG_CONNECT + " Trying to connect device Name = " + bleDevice.getName() + ", Address = " + bleDevice.getAddress());
        boolean isConnected = (m_bleService != null) ? m_bleService.connect(bleDevice.getAddress()) : false;

        // 若是連線成功，就儲存 curr device //
        if (isConnected)
            m_currBleDevice = bleDevice;
        return isConnected;
    }

    // 執行斷線 //
    public boolean disconnect() {
        if (m_bleService == null)
            return false;

        m_bleService.disconnect();
        m_currBleDevice = null; // 斷線後 clear current device .
        return true;
    }

    // 是否為連線中 //
    public boolean isConnected() {
        return m_bIsConnected;
    }

    // 設定是否持續連線 //
    public void setReconnect(boolean enable) {
        if (m_bleService == null)
            return;
        m_bleService.setReconnect(enable);
    }

    // 檢測是否藍牙 Adapter 已經開啟 //
    public boolean isBluetoothAdpaterOn(){
        return m_bleService.isBluetoothAdapterOn();
    }

    // 取得正在連線中的藍牙 裝置 //
    public BluetoothDevice getConnectedDevice() {
        return m_currBleDevice;
    }

    //---------------------------------------------------------------
    // JimmyAction
    //---------------------------------------------------------------
    // 取得 JIMMY Info : Status , Time, Weight。
    public void notifyJimmyInfo() {
        m_bleService.sendData(BLEService.CMD.NOTIFY_INFO);
    }

    public void notifyJimmyPower(){
        m_bleService.sendData(BLEService.CMD.NOTIFY_BATTERY);
    }

    public void tareWeight() {
        m_bleService.sendData(m_bleCmd.cmdTareWeight());
    }

    public void resetTime() {
        m_bleService.sendData(m_bleCmd.cmdResetTime());
    }

    public void switchMode(int times, BLEJimmy.OnJimmySwitchModeListener onJimmySwitchModeListener){
        while (times != 0){
            switchMode();
            ThreadUtil.sleep(BLEJimmyGattAttributes.TIME_OUT_SWITCH_JIMMY_MODE);
            times --;
        }

        onJimmySwitchModeListener.onSwitchFinish();
    }

    public void switchMode(){
        m_bleService.sendData(m_bleCmd.cmdSwitchMode());
    }

    public void resetBothTW(){
        m_bleService.sendData(m_bleCmd.cmdResetBoth());
    }

    //---------------------------------------------------------------
    // HandleData
    //---------------------------------------------------------------
    // scale will send 7 byte each notify, 1 for status, 3 for time and 3 for weight. data_array = {status, time[0], time[1], time[2], weight[0], weight[1], weight[2]}
    private void handleData(byte[] data) {

        final JimmyInfo jimmyInfo = new JimmyInfo(data);
        jimmyInfo.println_d(this.getClass(), BLE_TAG_HANDLE_JIMMY_INFO_DATA);

        // Send to UI layer //
        ActivityUtil.of(m_currActivity).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                m_jimmyInfoListener.onInfoUpdate(jimmyInfo);
            }
        });
    }

    //---------------------------------------------------------------
    // Interface
    //---------------------------------------------------------------
    // On Device Scanning Listener //
    public void onDeviceScan(Activity activity, ScanDeviceListener scanDeviceListener) {
        m_currActivity = activity;
        m_scanDeviceListener = scanDeviceListener;
    }

    // On Connection Status Update Listener //
    public void onUpdateStatus(Activity activity, ConnectStateListener connectStateListener) {
        m_currActivity = activity;
        m_connChgListener = connectStateListener;
    }

    // On Jimmy Info Notify Listener //
    public void onJimmyInfoUpdate(Activity activity, JimmyStatusInfoListener jimmyStatusInfoListener) {
        m_currActivity = activity;
        m_jimmyInfoListener = jimmyStatusInfoListener;
    }

    public interface ScanDeviceListener {
        void scanDeviceList(ArrayList<BluetoothDevice> devices);
    }

    public interface ConnectStateListener {
        void onConnectionStateChange(int state);
    }

    public interface JimmyStatusInfoListener {
        void onInfoUpdate(JimmyInfo jimmyInfo);
    }

    public interface OnJimmySwitchModeListener{
        void onSwitchFinish();
    }


}
