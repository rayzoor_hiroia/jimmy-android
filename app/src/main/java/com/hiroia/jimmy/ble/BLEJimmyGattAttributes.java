package com.hiroia.jimmy.ble;


/**
 * Create By Ray on 2018/8/28
 * <p>
 * Filter :
 * 1. [ BLE_MAIN_TAG ] : 所有藍芽相關
 * 2. [ ACTION_ACK_M1_DATA_HANDLE ] : 所有機器 Protocol 對應 Ack
 * 3. [ BLE_HANDLE_MACHINE_INFO ] [ BLE_HANDLE_MACHINE_STATE ] : 機器狀態
 */
public class BLEJimmyGattAttributes {

    /**
     * All in JIMMY Service.
     * UUID services 0. 00001800-0000-1000-8000-00805f9b34fb
     * UUID services 1. 00001801-0000-1000-8000-00805f9b34fb
     * UUID services 2. 8e400001-f315-4f60-9fb8-838830daea50
     * UUID services 3. 06c31822-8682-4744-9211-febc93e3bece
     * UUID services 4. 0000180f-0000-1000-8000-00805f9b34fb
     */

    /**
     * All in JIMMY Characteristic 06c31822-8682-4744-9211-febc93e3bece. -> Characteristics
     * characteristics : 0. 06c31824-8682-4744-9211-febc93e3bece
     * characteristics : 1. 06c31823-8682-4744-9211-febc93e3bece -> Reset Time and Tare Weight
     * characteristics : 2. 06c31825-8682-4744-9211-febc93e3bece -> display mac address
     * characteristics : 3. 06c31826-8682-4744-9211-febc93e3bece -> scale mac address
     * characteristics : 4. 06c31827-8682-4744-9211-febc93e3bece -> display firmware version
     * characteristics : 5. 06c31828-8682-4744-9211-febc93e3bece -> scale firmware version
     * characteristics : 6. 06c31829-8682-4744-9211-febc93e3bece
     * characteristics : 7. 06c31830-8682-4744-9211-febc93e3bece -> has password
     * characteristics : 8. 06c31831-8682-4744-9211-febc93e3bece
     */

    //---------------------------------------------------------------
    // The UUIDs for the BLE Device
    //---------------------------------------------------------------
    // UUIDs Service for Jimmy. //
    // Service //
    public final static String JIMMY_SCAN_SERVICE_UUID = "4c3c11fe-25e5-369c-93f5-fb4eda1593b6"; //b95199ba-2476-3314-9bd0-9b78f24ed583
    public final static String JIMMY_SCAN_SERVICE_UUID_LATEST_VERSION = "b95199ba-2476-3314-9bd0-9b78f24ed583";
    public final static String JIMMY_SCAN_SERVICE_UUID_LATEST_VERSION_2_2 = "9d625846-e3ee-3340-99d4-573c72c44a1d";


    // Main Service //
    public final static String JIMMY_SERVICE_UUID = "06c31822-8682-4744-9211-febc93e3bece";  // 18-22
    public final static String JIMMY_BATTERY_SERVICE = "0000180f-0000-1000-8000-00805f9b34fb";

    // Characteristic //
    public final static String JIMMY_BATTERY_CHARACTERISTIC= "00002a19-0000-1000-8000-00805f9b34fb";

    public final static String JIMMY_CHARACTERISTIC_STATUS_UUID = "06c31823-8682-4744-9211-febc93e3bece"; // 18-23
    public final static String JIMMY_CHARACTERISTIC_INFO_UUID = "06c31824-8682-4744-9211-febc93e3bece"; // 18-24
    public final static String JIMMY_CHARACTERISTIC_CLIENT_CONFIG_UUID = "06c32902-8682-4744-9211-febc93e3bece"; // 29-02
    public final static String JIMMY_CHARACTERISTIC_DISPLAY_MAC_ADDRESS_UUID = "06c31825-8682-4744-9211-febc93e3bece";
    public final static String JIMMY_CHARACTERISTIC_SCALE_MAC_ADDRESS_UUID = "06c31826-8682-4744-9211-febc93e3bece";
    public final static String JIMMY_CHARACTERISTIC_DISPLAY_FM_VERSION = "06c31827-8682-4744-9211-febc93e3bece";
    public final static String JIMMY_CHARACTERISTIC_SCALE_FM_VERSION = "06c31828-8682-4744-9211-febc93e3bece";
    public final static String JIMMY_CHARACTERISTIC_RESET_TIME_AND_WEIGHT = "06c31823-8682-4744-9211-febc93e3bece";
    public final static String JIMMY_CHARACTERISTIC_HAS_PASSWORD = "06c31830-8682-4744-9211-febc93e3bece";

    // Descriptor //
    public final static String JIMMY_DESCRIPTOR_INFO_UUID = "00002902-0000-1000-8000-00805f9b34fb"; // 18-24 , Descriptor.
    public final static String JIMMY_BATTERY_DESCRIPTOR = "00002902-0000-1000-8000-00805f9b34fb";

    //---------------------------------------------------------------
    // BLE Connection Operate
    //---------------------------------------------------------------
    // BLE TAG //
    public final static String BLE_MAIN_TAG = " [ BLE_MAIN_TAG ] ";

    // Device Operate Log Tag //
    public final static String BLE_TAG_INIT = BLE_MAIN_TAG + " [ BLE_COMP_INIT ] ";
    public final static String BLE_TAG_SCAN = BLE_MAIN_TAG + " [ ACTION_SCAN ] ";
    public final static String BLE_TAG_CONNECT = BLE_MAIN_TAG + " [ ACTION_CONNECT ] ";
    public final static String BLE_TAG_CLEAR_DEVICE_CACHE = BLE_MAIN_TAG + " [ ACTION_CLEAR_DEVICE_CACHE ] ";
    public final static String BLE_TAG_HANDLE_JIMMY_INFO_DATA = BLE_MAIN_TAG + " [ ACTION_JIMMY_INFO_DATA ] ";
    public final static String BLE_TAG_THREAD = BLE_MAIN_TAG + " [ THREAD ] ";
    public final static String BLE_TAG_CHARACTERISTIC_NOTIFY = BLE_MAIN_TAG + " [ ACTION_CHARACTERISTIC_NOTIFY ] ";
    public final static String BLE_TAG_HANDLE_DATA = BLE_MAIN_TAG + " [ ACTION_HANDLE_DATA ] ";
    public final static String BLE_TAG_RECEIVER = " [ ACTION_BROADCAST_RECEIVER ] ";
    public final static String BLE_TAG_FIRMWARE_VERSION = BLE_MAIN_TAG + " [ BLE_TAG_FIRMWARE_VERSION ] ";
    public final static String BLE_TAG_SLEEP = " [ SLEEP ] ";
    public final static String BLE_TAG_CMD_DURING = " [ BLE_TAG_CMD_DURING ] ";

    // BLE Device Time Out //
    public final static long TIME_OUT_DATA_HANDLE = 300;
    public final static long TIME_OUT_SWITCH_JIMMY_MODE = 300;
    public final static long TIME_OUT_DATA_SET_RECIPE_STEP = 500;
    public final static long TIME_OUT_DATA_AFTER_DRAIN_CALL_P5 = 500;
    public final static long TIME_OUT_CONNECTION = 600;
    public final static long TIME_OUT_RECONNETION = 600;
    public final static long TIME_OUT_STOP_SCAN = 500;
    public final static long TIME_OUT_BEFORE_DISCOVER_SERVICE = 200;


    // Connection Page Comp //
    public final static String AFTER_CONNECTION_FINISH_PAGE = "AFTER_CONNECTION_FINISH_PAGE";

    //---------------------------------------------------------------
    // BLE CMD
    //---------------------------------------------------------------

    // Tare Weight //
    public final static byte[] CMD_TARE_WEIGHT_OLD_FM = {(byte) 0x07};
    public final static byte[] CMD_TARE_WEIGHT_NEW_FM = {(byte) 0x07, (byte) 0x00};

    // Reset Time //
    public final static byte[] CMD_RESET_TIME_OLD_FM = {(byte) 0x05};
    public final static byte[] CMD_RESET_TIME_NEW_FM = {(byte) 0x05, (byte) 0x00};

    public final static byte[] CMD_RESET_BOTH_TIME_AND_WEIGHT_OLD_FM = {(byte) 0x06}; // 這裡的 0x06 是 reset Weight && Time
    public final static byte[] CMD_RESET_BOTH_TIME_AND_WEIGHT_NEW_FM = {(byte) 0x06, (byte) 0x00};

    // Switch Mode //
    public final  static byte[] CMD_SWITCH_MODE_OLD_FM = {(byte) 0x04};
    public final  static byte[]  CMD_SWITCH_MODE_NEW_FM = {(byte) 0x04, (byte) 0x00};

}
