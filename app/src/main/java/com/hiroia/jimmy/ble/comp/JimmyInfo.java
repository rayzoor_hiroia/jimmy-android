package com.hiroia.jimmy.ble.comp;

import com.library.android_common.component.common.Opt;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.hms.MS;
import com.library.android_common.util.ByteUtil;
import com.library.android_common.util.IntUtil;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import static com.hiroia.jimmy.ble.comp.JimmyInfo.Mode.E1;
import static com.hiroia.jimmy.ble.comp.JimmyInfo.Mode.E2;
import static com.hiroia.jimmy.ble.comp.JimmyInfo.Mode.E3;
import static com.hiroia.jimmy.ble.comp.JimmyInfo.Mode.T1;
import static com.hiroia.jimmy.ble.comp.JimmyInfo.Mode.T2;
import static com.hiroia.jimmy.ble.comp.JimmyInfo.Mode.T3;

/**
 * Create By Ray on 2018/11/9
 */
public class JimmyInfo {

    private String m_sDeviceName = "HIROIA JIMMY";
    //    private int m_nStatus;
    private int m_lStatus;
    private String m_baStatus;
    private int m_nTime;
    private int m_nWeight;
    private double m_dWeight;
    private int m_batteryPercentage;

    //------------------------------------------------------------
    // Construct
    //------------------------------------------------------------

    public JimmyInfo(byte[] data) {

        m_lStatus = data[0];
        m_baStatus = Integer.toBinaryString((data[0] + 256) % 256);

        m_nTime = ((data[1] & 0xff) << 0) | ((data[2] & 0xff) << 8) | ((data[3] & 0xff) << 16);
        m_nWeight = ((data[4] & 0xff) << 0) | ((data[5] & 0xff) << 8) | ((data[6] & 0xff) << 16);

        // 真正的數值需要除以十，因為包含小數點 //
        m_nTime = (m_nTime / 10);
        m_dWeight = (new Double(m_nWeight) / 10);


        // 1677721  處理負數 // TODO 先使用這種計算方式，需要修正
        if (m_dWeight > 10000){
            m_dWeight = m_dWeight - 1677721 ;
        }
    }

    //------------------------------------------------------------
    // Private Method
    //------------------------------------------------------------

    // 取得單位 //
    private String getUnit() {
        return Opt.of(m_baStatus)
                .splitStrAll()
                .get()
                .reverse()
                .reSize(4)
                .reverse()
                .first();
    }

    // 取得 Scale Mode //
    private Lst<String> getScaleMode() {
        return Opt.of(m_baStatus)
                .splitStrAll()
                .get()
                .lastSub(0, 4)
                .removeFirst();
    }

    // 取得 Mode 對應的 index。 ex. T1 = 0, E1 = 3;
    private int getModeIndex(Mode m) {
        return Lst.ofArray(Mode.class.getEnumConstants()).find(m);
    }

    //------------------------------------------------------------
    // Public Method
    //------------------------------------------------------------

    public boolean isTimeAndWeightDefault(){
        return getTime() == 0 && getWeight() == 0.0;
    }

    public Mode getCurrMode() {
        switch (getScaleMode().toString()) {
            default:
                return T1;
            //------------------------------
            case "001":
                return T1;
            case "010":
                return T2;
            case "011":
                return T3;
            case "100":
                return E1;
            case "101":
                return E2;
            case "110":
                return E3;
            //------------------------------
        }
    }

    // 切換 Mode 只能一次一次切換所以要計算數 需要的 switch Mode //
    public int switchModeTo(Mode targetMode) {
        int currPos = getModeIndex(getCurrMode());
        int targetPos = getModeIndex(targetMode);
        int totalLength = Mode.class.getEnumConstants().length;
        return targetPos > currPos ?
                (targetPos - currPos) :
                totalLength - (currPos - targetPos);
    }

    // 單位 g //
    public boolean isGram() {
        return Opt.of(getUnit()).parseToInt().get() == 0;
    }

    // 單位 Oz //
    public boolean isOz() {
        return Opt.of(getUnit()).parseToInt().get() == 1;
    }

    // 取得現在的單位 //
    public Unit getCurrUnit(){
        return isGram() ? Unit.GRAM : Unit.OZ;
    }

    // 設定裝置 name, UI Layer 請勿呼叫  //
    public void setDeviceName(String deviceName) {
        m_sDeviceName = deviceName;
    }

    // 取得裝置名稱 //
    public String getDeviceName() {
        return m_sDeviceName;
    }

    // 取得時間 //
    public int getTime() {
        return m_nTime;
    }

    // 取得時間的 MS //
    public MS getMs(){
        return getCurrMode() == T1 ? MS.of(0) : MS.of(getTime());
    }

    // 取得時間的字串 //
    public String getTimeInfo() {
        return StrUtil.convertTimeToLabel(m_nTime);
    }

    // 取得重量 //
    public double getWeight() {
        return isGram() ? m_dWeight : m_dWeight / 100;
    }

    // 取得重量字串 //
    public String getWeightInfo() {
        return String.valueOf(m_dWeight);
    }

    public void println_d(Class<?> cls, String filter) {
        LogUtil.d(cls, filter + " is g = " + isGram() + ", is Oz = " + isOz());
        LogUtil.d(cls, filter + " Status = " + m_lStatus + ", ba = " + m_baStatus + ",  Mode  = " + getScaleMode().toString() + ", " + getCurrMode().name() + ", unit = " + getUnit());
        LogUtil.d(cls, filter + " Status = " + m_lStatus + " Status Byte Array = " + m_baStatus + " , Time = " + StrUtil.convertTimeToLabel(m_nTime) + " , Weight = " + m_dWeight);
    }

    //------------------------------------------------------------
    // Enum
    //------------------------------------------------------------

    public enum Unit {
        GRAM("g"), OZ("Oz");
        //---------------------------------
        private String m_unit;

        Unit(String unit) {
            m_unit = unit;
        }

        public String get() {
            return m_unit;
        }
    }

    public enum Mode {
        T1,
        T2,
        T3,
        E1,
        E2,
        E3
    }


}
