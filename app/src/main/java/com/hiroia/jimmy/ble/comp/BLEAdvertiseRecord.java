package com.hiroia.jimmy.ble.comp;

import android.bluetooth.le.AdvertiseData;
import android.util.Log;

import com.library.android_common.util.StrUtil;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Create By Ray on 2018/12/5
 * reference : https://stackoverflow.com/questions/22016224/ble-obtain-uuid-encoded-in-advertising-packet/49106868
 * 暫時不使用
 */
public class BLEAdvertiseRecord {

    private int m_nLength = 0;
    private int m_nType = 0;
    private byte[] m_datas;

    //---------------------------------------------------------------------

    public BLEAdvertiseRecord(int length, int type, byte[] data) {
        m_nLength = length;
        m_nType = type;
        m_datas = data;

        String decodedRecord = "";
        try {
            decodedRecord = new String(data, "UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

//        Log.d("DEBUG", "Length: " + length + " Type : " + type + " Data : " + StrUtil.bytesToHex(data));
    }


    //----------------------------------------------------------------------------
    // Public Method
    //----------------------------------------------------------------------------

    public int length() {
        return m_nLength;
    }

    public int type() {
        return m_nType;
    }

    public byte[] datas() {
        return m_datas;
    }

    public String strBytes() {
        return StrUtil.bytesToHex(datas());
    }


    public static List<BLEAdvertiseRecord> parseScanRecord(byte[] scanRecord) {

        List<BLEAdvertiseRecord> records = new ArrayList<>();

        int index = 0;
        while (index < scanRecord.length) {
            int length = scanRecord[index++];
            //Done once we run out of records
            if (length == 0) break;

            int type = scanRecord[index];
            //Done if our record isn't a valid type
            if (type == 0) break;

            byte[] data = Arrays.copyOfRange(scanRecord, index + 1, index + length);

            records.add(new BLEAdvertiseRecord(length, type, data));
            //Advance
            index += length;
        }

        return records;
    }

    //----------------------------------------------------------------
    // test

    public void println(){
//
//
//        try {
//            String decodedRecord = new String(scanRecord, "UTF-8");
////            LogUtil.d(BLEUtil.class, " Device = " + deviceName + " , decoded String : " + StrUtil.bytesToHex(scanRecord));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//
//        // Parse data bytes into individual records
//        List<BLEAdvertiseRecord> records = BLEAdvertiseRecord.parseScanRecord(scanRecord);
//
//        // Print individual records
//        if (records.size() == 0) {
//            LogUtil.e(BLEUtil.class, " Device = " + deviceName + " , Mac Address = " + macAddress + " , Scan Record Empty");
//        } else {
//
//            Lst.of(records).forEach(new Lst.IConsumer<BLEAdvertiseRecord>() {
//                @Override
//                public void runEach(int i, BLEAdvertiseRecord adRecord) {
//                    LogUtil.d(BLEUtil.class, " Device = " + deviceName + " , Mac Address = " + macAddress + " , length = " + adRecord.length() + ", type = " + adRecord.type() + "  record = " + adRecord.strBytes());
//
//                }
//            });
//        }
    }

}
