package com.hiroia.jimmy.ble.comp;

import android.bluetooth.BluetoothDevice;

import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Create By Ray on 2018/11/5
 * 多個 Bluetooth Device
 */
public class BLEDevices {

    private ArrayList<BluetoothDevice> m_devices;
    private BLEDevices m_self;

    //--------------------------------------------------------
    // Construct
    //--------------------------------------------------------

    public BLEDevices(Collection<BluetoothDevice> devices) {
        m_self = this;

        // init comp //
        m_devices = new ArrayList<>();
        for (BluetoothDevice d : devices){
            if (!m_devices.contains(d.getAddress())){
                m_devices.add(d);
            }
        }
    }

    public static BLEDevices of(Collection<BluetoothDevice> devices) {
        return new BLEDevices(devices);
    }

    //--------------------------------------------------------
    // Public Method
    //--------------------------------------------------------

    public ArrayList<BluetoothDevice> toList() {
        return m_devices;
    }

    public Lst<BluetoothDevice> toLst() {
        return Lst.of(m_devices);
    }

    public Lst<String> toDeviceNamesLst(){
        return Lst.of(toDeviceNamesList());
    }

    public ArrayList<String> toDeviceNamesList() {
        final ArrayList<String> nameList = new ArrayList<>();
        Lst.of(m_devices).forEach(new Lst.IConsumer<BluetoothDevice>() {
            @Override
            public void runEach(int i, BluetoothDevice device) {
                nameList.add(device.getName());
            }
        });

        return nameList;
    }

    public BluetoothDevice get(int pos) {
        return m_devices.get(pos);
    }

    public void add(BluetoothDevice device) {
        m_devices.add(device);
    }

    public void add(int pos, BluetoothDevice device) {
        m_devices.add(pos, device);
    }

    public void remove(int pos) {
        m_devices.remove(pos);
    }

    // Println //
    public BLEDevices println_d(Class<?> cls) {
        println_d(cls, StrUtil.EMPTY);
        return m_self;
    }

    public BLEDevices println_d(Class<?> cls, String filter) {
        for (BluetoothDevice device : m_devices) {
            LogUtil.d(cls, filter.concat(" device name : " + device.getName() + ", device mac address : " + device.getAddress()));
        }
        return m_self;
    }

}
