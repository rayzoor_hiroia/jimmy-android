package com.hiroia.jimmy.ble;

import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.CMD_RESET_BOTH_TIME_AND_WEIGHT_NEW_FM;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.CMD_RESET_BOTH_TIME_AND_WEIGHT_OLD_FM;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.CMD_RESET_TIME_NEW_FM;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.CMD_RESET_TIME_OLD_FM;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.CMD_SWITCH_MODE_NEW_FM;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.CMD_SWITCH_MODE_OLD_FM;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.CMD_TARE_WEIGHT_NEW_FM;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.CMD_TARE_WEIGHT_OLD_FM;

/**
 * Create By Ray on 2018/12/11
 * To Operate Cmd .
 * 新版本的韌體需要 傳兩個 Byte, 1、CMD , 2、0x00。
 * ex. byte[] ba = {(byte) 0x07, (byte) 0x00}
 */
public class BLECmd {

    private FMVersion m_fmVersion = FMVersion.OLD;

    //------------------------------------------------
    // Construct
    //------------------------------------------------
    public BLECmd (){

    }

    public BLECmd (boolean isOld){
        this(isOld? FMVersion.OLD : FMVersion.NEW);
    }

    public BLECmd (FMVersion fm){
        m_fmVersion = fm;
    }

    //------------------------------------------------
    // Public Method
    //------------------------------------------------

    // 設定版本 //
    public void setFMVersion(FMVersion fm){
        m_fmVersion = fm;
    }

    public void setFMVersion(boolean isOld){
        setFMVersion(isOld? FMVersion.OLD : FMVersion.NEW);
    }

    // Tare Weight //
    public byte[] cmdTareWeight(){
        return m_fmVersion == FMVersion.OLD ? CMD_TARE_WEIGHT_OLD_FM : CMD_TARE_WEIGHT_NEW_FM;
    }

    // Reset Time //
    public byte[] cmdResetTime(){
        return m_fmVersion == FMVersion.OLD ? CMD_RESET_TIME_OLD_FM : CMD_RESET_TIME_NEW_FM;
    }

    // Switch Mode //
    public byte[] cmdSwitchMode(){
        return m_fmVersion == FMVersion.OLD ? CMD_SWITCH_MODE_OLD_FM : CMD_SWITCH_MODE_NEW_FM;
    }

    // Reset Both(Time and Weight same time)
    public byte[] cmdResetBoth(){
        return m_fmVersion == FMVersion.OLD ? CMD_RESET_BOTH_TIME_AND_WEIGHT_OLD_FM : CMD_RESET_BOTH_TIME_AND_WEIGHT_NEW_FM;
    }

    //------------------------------------------------
    // Enum
    //------------------------------------------------

    public enum FMVersion{
        OLD, NEW
    }


}
