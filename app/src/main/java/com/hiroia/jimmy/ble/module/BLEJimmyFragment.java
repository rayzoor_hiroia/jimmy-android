package com.hiroia.jimmy.ble.module;

import android.app.Fragment;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.hiroia.jimmy.ble.BLEJimmy;
import com.hiroia.jimmy.ble.BLEService;
import com.hiroia.jimmy.ble.comp.JimmyInfo;
import com.hiroia.jimmy.view.base.BaseActivity;
import com.hiroia.jimmy.view.base.BaseFragment;
import com.library.android_common.component.ThreadPool;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.ThreadUtil;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.TIME_OUT_DATA_HANDLE;

/**
 * Create By Ray on 2018/11/12
 */
public abstract class BLEJimmyFragment extends BaseFragment implements BLEJimmy.ConnectStateListener, BLEJimmy.ScanDeviceListener, BLEJimmy.JimmyStatusInfoListener {


    // Connection State //
    public final static int CONNECTION_STATE_DISCONNECTED = BLEService.CONNECTION_STATE_DISCONNECTED;
    public final static int CONNECTION_STATE_DISCONNECTING = BLEService.CONNECTION_STATE_DISCONNECTING;
    public final static int CONNECTION_STATE_CONNECTED = BLEService.CONNECTION_STATE_CONNECTED;
    public final static int CONNECTION_STATE_CONNECTING = BLEService.CONNECTION_STATE_CONNECTING;

    // Comp //
    private BLEJimmy m_bleJimmy = null;
    private ThreadPool m_tp = new ThreadPool(ThreadPool.CORE_THREAD_SIZE_FIVE);
    private String m_clsName = this.getClass().getSimpleName();
    private BluetoothDevice m_currBLESelectDevice = null;

    // Check //
    private boolean m_bIsReconnectOn = false;

    // Abstract Method //
    public abstract void onScanDevicesUpdate(ArrayList<BluetoothDevice> devices);

    public abstract void onConnectionStateUpdate(int state);

    public abstract void onJimmyInfoUpdate(JimmyInfo jimmyInfo);

    public abstract void onBLEInit();


    /**
     * Fragment Life Cycle
     */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getParentActivity()._checkBTPermissions();
        _initBLEComp();
    }


    @Override
    public void onResume() {
        super.onResume();
        _initBLEComp();
    }

    private void _initBLEComp() {
        // init ble //
        if (BLEJimmy.getInstance() != null)
            m_bleJimmy = BLEJimmy.getInstance();
        else {
            m_bleJimmy = new BLEJimmy();
            m_bleJimmy.init(getParentActivity());
        }

        m_bleJimmy.onDeviceScan(getParentActivity(), this);
        m_bleJimmy.onUpdateStatus(getParentActivity(), this);
        m_bleJimmy.onJimmyInfoUpdate(getParentActivity(), this);
        onBLEInit();
    }


    /**
     * Comp Action
     */

    private BaseActivity getParentActivity() {
        return ((BaseActivity) getActivity());
    }


    /**
     * BLE Action
     */

    //-------------------------------------------------------------
    // Scan
    //-------------------------------------------------------------
    // 開始掃描藍牙裝置 //
    protected boolean startScan() {
        m_tp.setAction(m_clsName + " startScan()");
        return m_tp.execute(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return m_bleJimmy.startScan();
            }
        });
    }

    // 停止掃描藍牙裝置 //
    protected boolean stopScan() {
        m_tp.setAction(m_clsName + " stopScan()");
        return m_tp.execute(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return m_bleJimmy.stopScan();
            }
        });
    }

    // 停止掃描藍牙裝置 //
    protected boolean isScanning() {
        m_tp.setAction(m_clsName + " isScanning()");
        return m_tp.execute(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return m_bleJimmy.isScanning();
            }
        });
    }

    //-------------------------------------------------------------
    // Connection
    //-------------------------------------------------------------
    // 連線至藍牙裝置 //
    protected boolean connect(final BluetoothDevice device) {
        m_tp.setAction(m_clsName + " connect()");
        return m_tp.execute(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                boolean isConnected = m_bleJimmy.connect(device);
                if (isConnected)
                    m_currBLESelectDevice = device;
                return isConnected;
            }
        });
    }

    // 從已連線的裝置斷線 //
    protected boolean disconnect() {
        m_tp.setAction(m_clsName + " disconnect()");
        return m_tp.execute(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return m_bleJimmy.disconnect();
            }
        });
    }

    // 設定自動連線 //
    protected void setReconnect(final boolean enable) {
        m_bIsReconnectOn = enable;

        //-------------------------------------------
        m_tp.setAction(m_clsName + " setReconnect()");
        m_tp.execute(new Runnable() {
            @Override
            public void run() {
                m_bleJimmy.setReconnect(enable);
            }
        });
    }

    // 是否已連線 //
    protected boolean isConnected() {
        m_tp.setAction(m_clsName + " isConnected()");
        return m_tp.execute(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return m_bleJimmy.isConnected();
            }
        });
    }


    // 設定開啟自動掃描 //
    protected void setAutoScan(boolean isOn) {
        if (!isOn) return;
        m_tp.setAction(m_clsName + " setAutoScan()");
        m_tp.execute(new Runnable() {
            @Override
            public void run() {
                if (LogUtil.Check.d(!startScan(), BLEJimmy.class, " start scan ()")) {
                    ThreadUtil.sleepAndLog_d(TIME_OUT_DATA_HANDLE, "setAutoScan(), before scan sleep " + TIME_OUT_DATA_HANDLE + " ms.");
                    setAutoScan(true);
                }
            }
        });
    }

    //-------------------------------------------------------------
    // JIMMY Action
    //-------------------------------------------------------------
    protected void notifyJimmyInfo() {
        m_tp.setAction(m_clsName + " notifyInfo()");
        m_tp.execute(new Runnable() {
            @Override
            public void run() {
                m_bleJimmy.notifyJimmyInfo();
            }
        });
    }

    // Reset Time && Tare Weight //
    protected void resetTimeAndWeight() {
        resetJimmyTime();
        tareJimmyWeight();
    }

    // Tare Weight //
    protected void tareJimmyWeight() {
        m_tp.setAction(m_clsName + " tareJimmyWeight()");
        m_tp.execute(new Runnable() {
            @Override
            public void run() {
                m_bleJimmy.tareWeight();
            }
        });
    }

    // Reset Time  //
    protected void resetJimmyTime() {
        m_tp.setAction(m_clsName + " resetJimmyTime()");
        m_tp.execute(new Runnable() {
            @Override
            public void run() {
                m_bleJimmy.resetTime();
            }
        });
    }

    // Switch Jimmy Mode  //
    protected void switchJimmyMode(final int times, final BLEJimmy.OnJimmySwitchModeListener listener) {
        m_tp.setAction(m_clsName + " switchJimmyMode()");
        m_tp.execute(new Runnable() {
            @Override
            public void run() {
                m_bleJimmy.switchMode(times, listener);
            }
        });
    }

    // Reset Time and Weight //
    protected void resetBothTW() {
        m_tp.setAction(m_clsName + " resetBothTW()");
        m_tp.execute(new Runnable() {
            @Override
            public void run() {
                m_bleJimmy.resetBothTW();
            }
        });
    }

    // 檢查藍牙 Adapter 是否有被開啟 //
    protected boolean isBluetoothAdapaterOn(){
        m_tp.setAction(m_clsName + "isBluetoothAdapterOn()");
        return m_tp.execute(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return m_bleJimmy.isBluetoothAdpaterOn();
            }
        });
    }

    //-------------------------------------------------------------
    // Protocol Action
    //-------------------------------------------------------------

    @Override
    public void scanDeviceList(ArrayList<BluetoothDevice> devices) {
        onScanDevicesUpdate(devices);
    }

    @Override
    public void onConnectionStateChange(int state) {
        onConnectionStateUpdate(state);
    }

    @Override
    public void onInfoUpdate(JimmyInfo jimmyInfo) {
        onJimmyInfoUpdate(jimmyInfo);
    }

}
