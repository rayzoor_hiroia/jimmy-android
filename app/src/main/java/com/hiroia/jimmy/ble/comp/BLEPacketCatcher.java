package com.hiroia.jimmy.ble.comp;


import java.util.Timer;
import java.util.TimerTask;

/**
 * Create By Ray on 2019/1/25
 * To handle ble packet .
 */
public class BLEPacketCatcher {


    private BLEPacketCatcher m_self;
    private long m_interval = 1000;

    private Timer m_timer;
    private OnFlowRateUpdateListener m_onFlowRateUpdateListener;

    //--------------------------------------------------------------
    // Construct
    //--------------------------------------------------------------

    public BLEPacketCatcher(){
        m_self = this;
    }

    public static BLEPacketCatcher of(){
        return new BLEPacketCatcher();
    }

    //--------------------------------------------------------------
    // Public Method
    //--------------------------------------------------------------

    public BLEPacketCatcher setInterval(long ms){
        m_interval = ms;
        m_timer.schedule(m_timeTask, m_interval);
        return m_self;
    }

    public void cancel(){
        if (m_timer != null){
            m_timer.cancel();
            m_timer = null;
        }

        if (m_timeTask != null){
            m_timeTask.cancel();
            m_timeTask = null;
        }
    }

    public void launch(OnFlowRateUpdateListener listener){
        m_onFlowRateUpdateListener = listener;

    }


    //--------------------------------------------------------------
    // Private Preference
    //--------------------------------------------------------------

    private TimerTask m_timeTask = new TimerTask() {
        @Override
        public void run() {










        }
    };



    //--------------------------------------------------------------
    // Interface
    //--------------------------------------------------------------

    public interface OnFlowRateUpdateListener{
        void onUpdate(double sec, double weight);
    }

}
