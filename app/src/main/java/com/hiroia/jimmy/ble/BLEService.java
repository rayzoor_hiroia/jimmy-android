package com.hiroia.jimmy.ble;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.library.android_common.component.common.Opt;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.ByteUtil;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;
import com.library.android_common.util.ThreadUtil;

import java.sql.Struct;
import java.util.UUID;

import static android.bluetooth.BluetoothGatt.GATT_SUCCESS;
import static android.bluetooth.BluetoothProfile.STATE_CONNECTED;
import static android.bluetooth.BluetoothProfile.STATE_CONNECTING;
import static android.bluetooth.BluetoothProfile.STATE_DISCONNECTED;
import static android.bluetooth.BluetoothProfile.STATE_DISCONNECTING;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.BLE_MAIN_TAG;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.BLE_TAG_CONNECT;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.BLE_TAG_INIT;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.BLE_TAG_SCAN;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.BLE_TAG_SLEEP;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.JIMMY_BATTERY_CHARACTERISTIC;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.JIMMY_BATTERY_DESCRIPTOR;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.JIMMY_BATTERY_SERVICE;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.JIMMY_CHARACTERISTIC_HAS_PASSWORD;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.JIMMY_CHARACTERISTIC_INFO_UUID;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.JIMMY_CHARACTERISTIC_STATUS_UUID;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.JIMMY_DESCRIPTOR_INFO_UUID;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.JIMMY_SERVICE_UUID;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.TIME_OUT_BEFORE_DISCOVER_SERVICE;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.TIME_OUT_RECONNETION;
import static com.hiroia.jimmy.ble.BLEJimmyGattAttributes.TIME_OUT_STOP_SCAN;

/**
 * Create By Ray on 2018/11/1
 */
public class BLEService extends Service {

    // Comp //
    private boolean m_bIsReconnectOn = false;

    // BLE Action //
    public final static String ACTION_CONNECTION_STATE = "BLEService_ACTION_CONNECTION_STATE";
    public final static String ACTION_DEVICE_FOUND = "BLEService_ACTION_DEVICE_FOUND";
    public final static String ACTION_UPDATE_JIMMY_INFO = "BLEService_ACTION_UPDATE_JIMMY_INFO";
    public final static String ACTION_FIRMWARE_VERSION = "BLEService_ACTION_FIRMWARE_VERSION";
    public static Lst<String> sm_actionList =
            Lst.of(ACTION_DEVICE_FOUND, ACTION_CONNECTION_STATE, ACTION_UPDATE_JIMMY_INFO, ACTION_FIRMWARE_VERSION);

    // Intent Extra //
    public final static String EXTRA_DEVICE = "BLEService_EXTRA_DEVICE";
    public final static String EXTRA_RSSI = "BLEService_EXTRA_RSSI";
    public final static String EXTRA_CONNECTION_STATE = "BLEService_CONNECTION_STATE";
    public final static String EXTRA_JIMMY_INFO = "BLEService_Jimmy_info";
    public final static String EXTRA_JIMMY_IS_OLD_VERSION = "BLEService_JIMMY_IS_OLD_VERSION";

    // BLE General //
    private String m_bleDeviceAddress = StrUtil.EMPTY;
    private BluetoothManager m_bleManager;
    private BluetoothAdapter m_bleAdapter;
    private BluetoothGatt m_bleGatt;
    private BluetoothDevice m_bleDevice;

    // Connection State //
    public final static int CONNECTION_STATE_DISCONNECTED = 0;
    public final static int CONNECTION_STATE_DISCONNECTING = 1;
    public final static int CONNECTION_STATE_CONNECTED = 2;
    public final static int CONNECTION_STATE_CONNECTING = 3;

    // CONNECTION GATT TAG //
    private final String CONN_STATECHANGE = BLE_TAG_CONNECT + " onConnectionStateChange(), ";
    private final String CONN_SERVICE_DISCOVERY = BLE_TAG_CONNECT + " onServicesDiscovered(), ";
    private final String CONN_CHARACTERISTIC_READ = BLE_TAG_CONNECT + " onCharacteristicRead(), ";
    private final String CONN_CHARACTERISTIC_WRITE = BLE_TAG_CONNECT + " onCharacteristicWrite(), ";
    private final String CONN_CHARACTERISTIC_CHANGE = BLE_TAG_CONNECT + " onCharacteristicChange(), ";
    private final String CONN_DESCRIPTOR_READ = BLE_TAG_CONNECT + " onDescriptorRead(), ";
    private final String CONN_DESCRIPTOR_WRITE = BLE_TAG_CONNECT + " onDescriptorWrite(), ";

    //----------------------------------------------------------
    // init
    //----------------------------------------------------------
    public boolean init() {

        // BluetoothManager //
        if (m_bleManager == null) { // For API level 18 and above, get a reference to BluetoothAdapter through
            m_bleManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (LogUtil.Check.e(m_bleManager == null, BLEService.class, BLE_TAG_INIT + "Unable to initialize BluetoothManager."))
                return false;
        }

        // BluetoothAdapter //
        m_bleAdapter = m_bleManager.getAdapter();
        if (LogUtil.Check.e(m_bleAdapter == null, BLEService.class, BLE_TAG_INIT + "Unable to obtain a BluetoothAdapter.")) {
            return false;
        }

        // BluetoothGatt //
        if (m_bleGatt != null) {
            updateBroadcast(EXTRA_CONNECTION_STATE, ACTION_CONNECTION_STATE, Opt.of(CONNECTION_STATE_DISCONNECTED));
            m_bleGatt.close();
        }
        m_bleGatt = null;
        return true;
    }

    private void updateBroadcast(String action, String state, Opt<?> opt) {
        Intent it = new Intent(action);
        switch (state) {
            case EXTRA_CONNECTION_STATE:
                it.putExtra(EXTRA_CONNECTION_STATE, (Integer) opt.get());
                break;


            case EXTRA_JIMMY_INFO:
                it.putExtra(EXTRA_JIMMY_INFO, (byte[]) opt.get());
                break;


            case EXTRA_JIMMY_IS_OLD_VERSION:
                it.putExtra(EXTRA_JIMMY_IS_OLD_VERSION, (Boolean) opt.get());
                break;
        }
        sendBroadcast(it);
    }

    //----------------------------------------------------------
    // Scan //
    //----------------------------------------------------------
    private BluetoothAdapter.LeScanCallback m_btLeScanCallBack = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            // Check device //
            if (device == null || device.getName() == null) return;

            // Check if is not JIMMY //
            if (!BLEUtil.isJimmyDevice(device, scanRecord)) return;

            BLEUtil.println_record(device, scanRecord);

            // Log Device Info. //
            LogUtil.d(BLEService.class, BLE_TAG_SCAN +
                    "Device Name = " + device.getName() +
                    " , Mac Address = " + device.getAddress()
                    + " , Str Byte = " + ByteUtil.of(scanRecord).toByteArrayStr()
            );

            // Call BroadCast //
            Intent it = new Intent(ACTION_DEVICE_FOUND);
            it.putExtra(EXTRA_DEVICE, device);
            it.putExtra(EXTRA_RSSI, rssi);
            sendBroadcast(it);
        }
    };

    public void startScan() {
        LogUtil.d(BLEService.class, BLE_TAG_SCAN + " startScan(), Start to scan ble device.");

        if (m_bleAdapter == null) {
            if (!init()) {
                LogUtil.e(BLEService.class, "startScan error, cannot initialize");
                return;
            }
        }

        // Before ble scan, make sure is already stop scan. //
        stopScan();
        LogUtil.i(BLEService.class, BLE_TAG_SCAN + " Scan Mode: " + Integer.toString(m_bleAdapter.getScanMode()) + ", Discovering : " + Boolean.toString(m_bleAdapter.isDiscovering()));


        // TODO FIX : Service UUID Scan Filter //
        // Start Scan //
//        UUID[] uuids = {UUID.fromString(JIMMY_SERVICE_UUID)};
//
        if (!m_bleAdapter.startLeScan(m_btLeScanCallBack))
//        if (!m_bleAdapter.startLeScan(uuids, m_btLeScanCallBack))
            LogUtil.e(BLEService.class, BLE_TAG_SCAN + " startLeScan(), Error !");
        else
            LogUtil.d(BLEService.class, BLE_TAG_SCAN + " startLeScan(), Success !");
    }

    public void stopScan() {
        m_bleAdapter.stopLeScan(m_btLeScanCallBack);
        sleep(TIME_OUT_STOP_SCAN, BLE_TAG_SCAN, "stopScan()");
    }


    //----------------------------------------------------------
    // Connection
    //----------------------------------------------------------
    public boolean connect(String bleDeviceMacAddress) {
        // Connecting Broadcast //
        updateBroadcast(ACTION_CONNECTION_STATE, EXTRA_CONNECTION_STATE, Opt.of(CONNECTION_STATE_CONNECTING));

        //-----------------------------------------------------
        // Prepared Step //
        stopScan(); // Before connection, make sure scan action is already stopped.
        m_bleDeviceAddress = bleDeviceMacAddress; // Assign BLE Device Address
        LogUtil.d(BLEService.class, BLE_TAG_CONNECT + " connect() , Start to connect to address : " + bleDeviceMacAddress);

        //-----------------------------------------------------
        // Check ble status //
        if (m_bleAdapter == null || (bleDeviceMacAddress.isEmpty() || bleDeviceMacAddress == null)) {
            LogUtil.e(BLEService.class, BLE_TAG_CONNECT + " connect() , BluetoothAdapter not init or unspecified address.");
            return false;
        }

        //-----------------------------------------------------
        // Connect to exist gatt //
//        if (m_bleGatt != null) {
//            LogUtil.d(BLEService.class, BLE_TAG_CONNECT + " connect(), Trying to use an existing BluetoothGatt for connection ");
//            return m_bleGatt.connect();
//        }

        //-----------------------------------------------------
        // Create a new connection gatt //
        LogUtil.If.d(m_bleGatt == null, BLEService.class, BLE_TAG_CONNECT + " connect(), Trying to create a new connection. ");
        m_bleDevice = m_bleAdapter.getRemoteDevice(bleDeviceMacAddress); // Trying to declare a ble device with remote device .
        if (m_bleDevice == null) {
            LogUtil.e(BLEService.class, BLE_TAG_CONNECT + " connect(), Remote Device not found.  Unable to connect. ");
            return false;
        }

        //-----------------------------------------------------
        // Create gatt. /
        m_bleGatt = m_bleDevice.connectGatt(this, false, m_bleGattCallBack); // 此處的 autoConnect 如果給 false priority 較高

        return true;
    }

    public void disconnect() {
        if (m_bleAdapter == null || m_bleAdapter == null) {
            LogUtil.e(BLEService.class, BLE_TAG_CONNECT + " disconnect(), BluetoothAdapter or BluetoothGatt are not initialized");
            return;
        }

        m_bleGatt.disconnect();
        LogUtil.e(BLEService.class, BLE_TAG_CONNECT + " disconnect(), Is already disconnect from current ble device.");
    }

    // reconnect //
    public void reconnect() {
        if (m_bIsReconnectOn == false) {
            LogUtil.d(BLEService.class, BLE_TAG_CONNECT + " reconnect(), Reconnect action is off.");
            return;
        }

        LogUtil.d(BLEService.class, BLE_TAG_CONNECT + " reconnect(), Start to reconnect to device. ");

        // time out //
        sleep(TIME_OUT_RECONNETION, BLE_TAG_CONNECT, " reconnect()");

        // start to reconnect //
        if (m_bleDeviceAddress.isEmpty() || m_bleDeviceAddress == null) {
            LogUtil.e(BLEService.class, BLE_TAG_CONNECT + " reconnect(), Bluetooth Device Address is Null or isEmpty.");
            return;
        }

        if (connect(m_bleDeviceAddress)) // if is connected return.
            return;
        else
            reconnect(); // else reconnect again.
    }

    public void setReconnect(boolean enable) {
        LogUtil.d(BLEService.class, BLE_TAG_CONNECT + " setReconnect(), " + enable);
        m_bIsReconnectOn = enable;
    }

    private void closeConnect() {
        if (m_bleGatt != null) {
            m_bleGatt.close();
            m_bleGatt = null;
        }
        LogUtil.d(BLEService.class, BLE_TAG_CONNECT + " setReconnect(), BluetoothGatt is already release : " + (m_bleGatt == null));
    }

    // Connection with Gatt Server Call Back //
    private final BluetoothGattCallback m_bleGattCallBack = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {

            switch (status) {

                case GATT_SUCCESS:

                    //----------------------------------------------------------------------
                    //   State Connect Success //
                    if (newState == STATE_CONNECTED) {
                        LogUtil.d(BLEService.class, CONN_STATECHANGE + " Is already connected to Gatt Server.");

                        sleep(TIME_OUT_BEFORE_DISCOVER_SERVICE, BLE_TAG_CONNECT, "onConnectionStateChange(), Before start discover service ");  /** After connected to Gatt Server, To make sure discoverService callback need to set time out 300ms ~ 600ms, do not over than 1000 ms . Ray. 2018/09/01 */
                        updateBroadcast(ACTION_CONNECTION_STATE, EXTRA_CONNECTION_STATE, Opt.of(CONNECTION_STATE_CONNECTED)); // update connection state : is disconnected

                        // Attempting to start discover service //
                        LogUtil.d(BLEService.class, CONN_STATECHANGE + " Attempting to start discover service : " + m_bleGatt.discoverServices());
                    }

                    //----------------------------------------------------------------------
                    // State Disconnected //
                    if (newState == STATE_DISCONNECTED) {
                        LogUtil.e(BLEService.class, CONN_STATECHANGE + " Is already disconnected from Gatt Server. ");

                        closeConnect();
                        updateBroadcast(ACTION_CONNECTION_STATE, EXTRA_CONNECTION_STATE, Opt.of(CONNECTION_STATE_DISCONNECTED)); // update connection state : is disconnected
                        reconnect();
                    }

                    //----------------------------------------------------------------------
                    // State Connecting //
                    if (newState == STATE_CONNECTING) {
                        LogUtil.d(BLEService.class, CONN_STATECHANGE + " Is Connecting to Gatt Server, please wait. ");
                        updateBroadcast(ACTION_CONNECTION_STATE, EXTRA_CONNECTION_STATE, Opt.of(CONNECTION_STATE_CONNECTING));
                    }

                    //----------------------------------------------------------------------
                    // State DisConnecting //
                    if (newState == STATE_DISCONNECTING) {
                        LogUtil.e(BLEService.class, CONN_STATECHANGE + " Is Disconnecting, please wait and retry to connect. ");
                        updateBroadcast(ACTION_CONNECTION_STATE, EXTRA_CONNECTION_STATE, Opt.of(CONNECTION_STATE_DISCONNECTING));
                    }

                    //----------------------------------------------------------------------
                    // Connect State Change Error //
                    if (!Lst.of(STATE_CONNECTED, STATE_DISCONNECTED, STATE_CONNECTING, STATE_DISCONNECTING).contains(newState))
                        LogUtil.e(BLEService.class, CONN_STATECHANGE + " Error , Status =  " + status + " newState = " + newState);

                    break;

                // Error //
                default:
                    LogUtil.e(BLEService.class, CONN_STATECHANGE + " Error , Status = " + status);
                    closeConnect();
                    reconnect();
                    break;
            }

        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            LogUtil.d(BLEService.class, CONN_SERVICE_DISCOVERY + " received , status = " + status);

            //-------------------------------------------------------------
            // Log All Service: UUIDs //
            Lst.of(gatt.getServices()).forEach(new Lst.IConsumer<BluetoothGattService>() {
                @Override
                public void runEach(int i, BluetoothGattService service) {
                    LogUtil.d(BLEService.class, CONN_SERVICE_DISCOVERY + " UUID services " + i + StrUtil.SPACE_DOT + service.getUuid().toString());
                }
            });

            //-------------------------------------------------------------
            // Service Discovered Operate //

            if (status == GATT_SUCCESS) {
                LogUtil.d(BLEService.class, CONN_SERVICE_DISCOVERY + " received , Service Discovered success");

                // Bluetooth Service //
                BluetoothGattService service = m_bleGatt.getService(UUID.fromString(JIMMY_SERVICE_UUID));
                if (service == null) {
                    LogUtil.e(BLEService.class, CONN_SERVICE_DISCOVERY + " BluetoothGattService is null");
                    m_bleGatt.disconnect(); // if service is null , disconnect first after that close bleGatt.
                    return;
                }

                // Log All Characteristic //
                Lst.of(service.getCharacteristics()).forEach(new Lst.IConsumer<BluetoothGattCharacteristic>() {
                    @Override
                    public void runEach(int i, BluetoothGattCharacteristic c) {
                        String uuid = c.getUuid().toString();
                        boolean isHasPassword = uuid.equals(JIMMY_CHARACTERISTIC_HAS_PASSWORD);  // 判斷 (是不是有密碼的 UUID) 來決定是不是新版本
                        LogUtil.d(BLEService.class, CONN_SERVICE_DISCOVERY + " Characteristic : " + i + StrUtil.SPACE_DOT + uuid + " is has Password UUID = " + isHasPassword);

                        if (isHasPassword)
                            updateBroadcast(ACTION_FIRMWARE_VERSION, EXTRA_JIMMY_IS_OLD_VERSION, Opt.of(false));
                    }
                });

                // Characteristic //
                BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(JIMMY_CHARACTERISTIC_INFO_UUID));
                if (characteristic == null) {
                    LogUtil.e(BLEService.class, CONN_SERVICE_DISCOVERY + " BluetoothGattCharacteristic is null");
                    m_bleGatt.disconnect(); // if characteristic is null , disconnect first after that close bleGatt.
                    return;
                }

                // Log All descriptors //
                Lst.of(characteristic.getDescriptors()).forEach(new Lst.IConsumer<BluetoothGattDescriptor>() {
                    @Override
                    public void runEach(int i, BluetoothGattDescriptor d) {
                        LogUtil.d(BLEService.class, CONN_SERVICE_DISCOVERY + " descriptors : " + i + StrUtil.SPACE_DOT + d.getUuid().toString());
                    }
                });

                LogUtil.d(BLEService.class, CONN_SERVICE_DISCOVERY + "  Character Notification " + m_bleGatt.setCharacteristicNotification(characteristic, true));

                // After connected , notify JIMMY Info //
                askNotifyJimmyInfo();
//                notifyPowerPercentage();

            } else {
                LogUtil.e(BLEService.class, CONN_SERVICE_DISCOVERY + " received , status = " + status);
                m_bleGatt.disconnect();  // if error , disconnect first after that close bleGatt.
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            LogUtil.d(BLEService.class, CONN_CHARACTERISTIC_READ + " received , status = " + status + ", value = " + StrUtil.bytesToHex(characteristic.getValue()));
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            LogUtil.d(BLEService.class, CONN_CHARACTERISTIC_WRITE + " received , status = " + status + ", value = " + StrUtil.bytesToHex(characteristic.getValue()));
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            LogUtil.d(BLEService.class, CONN_CHARACTERISTIC_CHANGE + " received , value = " + StrUtil.bytesToHex(characteristic.getValue()));
            updateBroadcast(ACTION_UPDATE_JIMMY_INFO, EXTRA_JIMMY_INFO, Opt.of(characteristic.getValue()));
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            LogUtil.d(BLEService.class, CONN_DESCRIPTOR_READ + " received , status = " + status + ", value = " + StrUtil.bytesToHex(descriptor.getValue()));
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            LogUtil.d(BLEService.class, CONN_DESCRIPTOR_WRITE + " received , status = " + status + ", value = " + StrUtil.bytesToHex(descriptor.getValue()));
        }

    };

    //----------------------------------------------------------
    // Public Method
    //----------------------------------------------------------
    public boolean isBluetoothAdapterOn(){
        // Check bluetooth adapter is already init. //
        if (m_bleAdapter == null) {
            LogUtil.e(BLEService.class, "isBluetoothAdapterOn(), BluetoothAdapter is null.");
            return false;
        }
        return LogUtil.Check.d(m_bleAdapter.isEnabled(), BLEService.class, "isBluetoothAdapterOn(), Bluetooth adapter is on.");
    }

    public void sendData(CMD cmd) {
        switch (cmd) {
            case NOTIFY_INFO:
                askNotifyJimmyInfo();
                break;

            case NOTIFY_BATTERY:
                notifyPowerPercentage();
                break;
        }
    }

    public enum CMD {
        NOTIFY_INFO, NOTIFY_BATTERY;
    }

    //----------------------------------------------------------
    // Common
    //----------------------------------------------------------
    private void sleep(long mills, String tag, String desc) {
        LogUtil.d(BLEService.class, tag + BLE_TAG_SLEEP + desc + " , To sleep " + mills + " mills.");
        ThreadUtil.sleep(mills);
        LogUtil.d(BLEService.class, tag + BLE_TAG_SLEEP + desc + " , Sleep done.");
    }

    // 取得 Jimmy Info : Weight and Time //
    private void askNotifyJimmyInfo() {
        if (m_bleGatt == null) {
            LogUtil.e(BLEService.class, BLE_MAIN_TAG + "askNotifyJimmyInfo(), BluetoothGatt is Null");
            return;
        }

        BluetoothGattService s = m_bleGatt.getService(UUID.fromString(JIMMY_SERVICE_UUID));
        if (LogUtil.Check.e(s == null, BLEService.class, BLE_MAIN_TAG + "askNotifyJimmyInfo(), BluetoothGattService is null "))
            return;

        BluetoothGattCharacteristic c = s.getCharacteristic(UUID.fromString(JIMMY_CHARACTERISTIC_INFO_UUID));
        if (LogUtil.Check.e(c == null, BLEService.class, BLE_MAIN_TAG + "askNotifyJimmyInfo(), BluetoothGattCharacteristic is null ")) {
            return;
        }

        BluetoothGattDescriptor d = c.getDescriptor(UUID.fromString(JIMMY_DESCRIPTOR_INFO_UUID));
        if (LogUtil.Check.e(d == null, BLEService.class, BLE_MAIN_TAG + "askNotifyJimmyInfo(), BluetoothGattDescriptor is null ")) {
            return;
        }

        d.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        LogUtil.d(BLEService.class, BLE_MAIN_TAG + "askNotifyJimmyInfo(), send descriptor = " + m_bleGatt.writeDescriptor(d));
    }

    // 取得 Jimmy Info : 電量百分比 //
    private void notifyPowerPercentage(){
        if (m_bleGatt == null) {
            LogUtil.e(BLEService.class, BLE_MAIN_TAG + "notifyPowerPercentage(), BluetoothGatt is Null");
            return;
        }

        BluetoothGattService s = m_bleGatt.getService(UUID.fromString(JIMMY_BATTERY_SERVICE));
        if (LogUtil.Check.e(s == null, BLEService.class, BLE_MAIN_TAG + "notifyPowerPercentage(), BluetoothGattService is null "))
            return;

        BluetoothGattCharacteristic c = s.getCharacteristic(UUID.fromString(JIMMY_BATTERY_CHARACTERISTIC));
        if (LogUtil.Check.e(c == null, BLEService.class, BLE_MAIN_TAG + "notifyPowerPercentage(), BluetoothGattCharacteristic is null ")) {
            return;
        }

        BluetoothGattDescriptor d = c.getDescriptor(UUID.fromString(JIMMY_BATTERY_DESCRIPTOR));
        if (LogUtil.Check.e(d == null, BLEService.class, BLE_MAIN_TAG + "notifyPowerPercentage(), BluetoothGattDescriptor is null ")) {
            return;
        }

        d.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        LogUtil.d(BLEService.class, BLE_MAIN_TAG + "notifyPowerPercentage(), send descriptor = " + m_bleGatt.writeDescriptor(d));
    }

    // Send Another Jimmy Order //
    public void sendData(byte[] data) {
        if (m_bleGatt == null) {
            LogUtil.e(BLEService.class, BLE_MAIN_TAG + "sendData(), BluetoothGatt is Null");
            return;
        }

        BluetoothGattService s = m_bleGatt.getService(UUID.fromString(JIMMY_SERVICE_UUID));
        if (LogUtil.Check.e(s == null, BLEService.class, BLE_MAIN_TAG + "sendData(), BluetoothGattService is null "))
            return;

        BluetoothGattCharacteristic c = s.getCharacteristic(UUID.fromString(JIMMY_CHARACTERISTIC_STATUS_UUID));
        c.setValue(data);
        c.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

        LogUtil.d(BLEService.class, BLE_MAIN_TAG + "sendData(), Characteristic is null : " + (c == null) + " , descriptor size = " + c.getDescriptors().size());
        LogUtil.d(BLEService.class, BLE_MAIN_TAG + "sendData(), notify characteristic = " + m_bleGatt.setCharacteristicNotification(c, true));
        LogUtil.d(BLEService.class, BLE_MAIN_TAG + "sendData(), write characteristic = " + m_bleGatt.writeCharacteristic(c));
    }

    //----------------------------------------------------------
    // Binder
    //----------------------------------------------------------

    private final LocalBinder m_binder = new LocalBinder();

    public class LocalBinder extends Binder {
        public BLEService getService() {
            return BLEService.this;
        }
    }

    //----------------------------------------------------------
    // Service Life Cycle
    //----------------------------------------------------------

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return m_binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtil.d(BLEService.class, " BLE service started");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() { // on service Destroy
        super.onDestroy();
        LogUtil.d(BLEService.class, " application destroy");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        LogUtil.d(BLEService.class, " application task removed");
        stopSelf(); // stop service
    }
}
