package com.hiroia.jimmy.db.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.hiroia.jimmy.comp.comp.WaterVolStep;
import com.library.android_common.database.SQLTableCreator;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;


/**
 * Create By Ray on 2018/12/24
 * 根據 id 值儲存 chart data
 */
public class TModeChartDBA {

    // Comp //
    private static SQLiteDatabase sm_db;
    public static final String TABLE_NAME = "tmode_chart_table";

    // Column //
    public static final String COL_ID = "id";
    public static final String COL_SEC = "sec";
    public static final String COL_VOL = "vol";

    //----------------------------------------------------------
    // Construct
    //----------------------------------------------------------

    public TModeChartDBA(Context ctx) {
        sm_db = JimmySQLHelper.getDB(ctx);
        sm_db.execSQL(createTable());
    }

    public static TModeChartDBA open(Context ctx) {
        return new TModeChartDBA(ctx);
    }

    public static String createTable() {
        return SQLTableCreator.of(TABLE_NAME)
                .addIntCol(COL_ID)
                .addIntCol(COL_SEC)
                .addIntCol(COL_VOL)
                .getSQL();
    }

    //----------------------------------------------------------
    // CRUD
    //----------------------------------------------------------

    public void insert(long id, int sec, int vol) {
        sm_db.insert(TABLE_NAME, null, toContentValue(id, sec, vol));
    }

    public void update(long id, int sec, int vol) {
        delete(id);
        insert(id, sec, vol);
    }

    public void delete(long id) {
        sm_db.delete(TABLE_NAME, COL_ID + StrUtil.SPACE_EQUAL + id, null);
    }

    //----------------------------------------------------------
    // Public Method
    //----------------------------------------------------------

//    public void openDB(Context ctx) {
//        sm_db = JimmySQLHelper.getDB(ctx);
//    }

    public void closeDB() {
        sm_db.close();
    }


    public WaterVolStep getStepsById(long id) {
        WaterVolStep step = new WaterVolStep();

        Cursor c = sm_db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = " + id, null);
//        Cursor c = sm_db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        c.moveToFirst();

        for (int i = 0; i < c.getCount(); i++) {
            step.setId(id);
            step.addStep(c.getInt(c.getColumnIndex(COL_SEC)), c.getInt(c.getColumnIndex(COL_VOL)));
            LogUtil.d(TModeChartDBA.class, "getStepsById_i = " + i);
            c.moveToNext();
        }
        c.close();
        return step;
    }


    //----------------------------------------------------------
    // Private Preference
    //----------------------------------------------------------

    private ContentValues toContentValue(long id, double sec, int vol) {
        ContentValues cv = new ContentValues();
        cv.put(COL_ID, id);
        cv.put(COL_SEC, sec);
        cv.put(COL_VOL, vol);
        return cv;
    }


}
