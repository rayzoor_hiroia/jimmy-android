package com.hiroia.jimmy.db.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.hiroia.jimmy.comp.comp.RecordData;
import com.hiroia.jimmy.comp.comp.TrainingData;
import com.hiroia.jimmy.model.record.ModelJRecord;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.YMDHMS;
import com.library.android_common.database.SQLTableCreator;
import com.library.android_common.util.StrUtil;

/**
 * Create By Ray on 2019/1/30
 */
public class JRecordDBA {

    // Comp //
    private static SQLiteDatabase sm_db;
    public static final String TABLE_NAME = "j_record_table";

    // Column //
    public static final String COL_ID = "id";
    public static final String COL_UUID = "uuid";
    public static final String COL_JSCALE_ID = "jscale_id";
    public static final String COL_JRECIPE_ID = "jrecipe_id";
    public static final String COL_STANDARD = "standard";
    public static final String COL_WATER = "water";
    public static final String COL_TIME_1 = "time_1";
    public static final String COL_TIME_2 = "time_2";
    public static final String COL_SCORE = "score";
    public static final String COL_RECORD_TIME = "record_time";
    public static final String COL_CREATED_TIME = "created_time";
    public static final String COL_UPDATED_TIME = "updated_time";
    public static final String COL_MODE = "mode";
    public static final String COL_STATE = "state";
    public static final String COL_RECORD_DATA = "record_data";
    public static final String COL_TRAINING_DATA = "training_data";


    //----------------------------------------------------
    // Construct
    //----------------------------------------------------

    public JRecordDBA(Context ctx) {
        sm_db = JimmySQLHelper.getDB(ctx);
        sm_db.execSQL(createTable());
    }

    public static JRecordDBA open(Context ctx) {
        return new JRecordDBA(ctx);
    }

    public static String createTable() {
        return SQLTableCreator.of(TABLE_NAME)
                .addIntCol(COL_ID)
                .addTextCol(COL_UUID)
                .addIntCol(COL_JSCALE_ID)
                .addIntCol(COL_JRECIPE_ID)
                .addIntCol(COL_STANDARD)
                .addRealCol(COL_WATER)
                .addRealCol(COL_TIME_1)
                .addRealCol(COL_TIME_2)
                .addIntCol(COL_SCORE)
                .addTextCol(COL_RECORD_TIME)
                .addTextCol(COL_CREATED_TIME)
                .addTextCol(COL_UPDATED_TIME)
                .addIntCol(COL_MODE)
                .addIntCol(COL_STATE)
                .addTextCol(COL_RECORD_DATA)
                .addTextCol(COL_TRAINING_DATA)
                .getSQL();
    }

    //--------------------------------------------------
    // CRUD
    //--------------------------------------------------

    public void insert(ModelJRecord m) {
        sm_db.insert(TABLE_NAME, null, toCv(m));
    }

    public void update(ModelJRecord m) {
        delete(m);
        insert(m);
    }

    public void delete(ModelJRecord m) {
        sm_db.delete(TABLE_NAME, COL_ID + StrUtil.SPACE_EQUAL + m.getId(), null);
    }

    //--------------------------------------------------
    // Public Method
    //--------------------------------------------------

    public static void clearTable() {
        JimmySQLHelper.clearTable(TABLE_NAME);
    }

    public Lst<ModelJRecord> getTable() {

        Lst<ModelJRecord> ms = Lst.of();

        Cursor c = sm_db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        c.moveToFirst();

        for (int i = 0; i < c.getCount(); i++) {

            ModelJRecord m = new ModelJRecord();
            m.setId(c.getLong(c.getColumnIndex(COL_ID)));
            m.setUuid(c.getString(c.getColumnIndex(COL_UUID)));
            m.setJrecipe_id(c.getLong(c.getColumnIndex(COL_JRECIPE_ID)));
            m.setJscale_id(c.getLong(c.getColumnIndex(COL_JSCALE_ID)));
            m.setStandard(c.getInt(c.getColumnIndex(COL_STANDARD)) == 0 ? false : true);
            m.setWater(c.getDouble(c.getColumnIndex(COL_WATER)));
            m.setTime1(c.getDouble(c.getColumnIndex(COL_TIME_1)));
            m.setTime2(c.getDouble(c.getColumnIndex(COL_TIME_2)));
            m.setScore(c.getInt(c.getColumnIndex(COL_SCORE)));
            m.setRecord_time(YMDHMS.parseByISO8601(c.getString(c.getColumnIndex(COL_RECORD_TIME))));
            m.setCreated_time(YMDHMS.parseByISO8601(c.getString(c.getColumnIndex(COL_CREATED_TIME))));
            m.setUpdated_time(YMDHMS.parseByISO8601(c.getString(c.getColumnIndex(COL_UPDATED_TIME))));
            m.setMode(ModelJRecord.getMode(c.getInt(c.getColumnIndex(COL_MODE))));
            m.setState(ModelJRecord.getState(c.getInt(c.getColumnIndex(COL_STATE))));
            m.setRecordData(RecordData.parse(c.getString(c.getColumnIndex(COL_RECORD_DATA))));
            m.setTrainingData(TrainingData.parse(c.getString(c.getColumnIndex(COL_TRAINING_DATA))));

            ms.add(m);
            c.moveToNext();
        }

        c.close();
        return ms;
    }

    public Lst<ModelJRecord> getTable(ModelJRecord.JScaleMode... modes) {

        Lst<ModelJRecord> ms = Lst.of();
        Lst<Integer> modeIds = Lst.of();

        // 取得多個 mode id //
        for (ModelJRecord.JScaleMode m : modes)
            modeIds.add(m.id());

        // 取得對應 Mode 的 Records //
        for (ModelJRecord m : getDirtyJRecords().toList())
            for (Integer id : modeIds.toList())
                if (m.getMode().id() == id)
                    ms.add(m);

        return ms;
    }


    // 這裡的 Dirty 是指尚未與 cloud sync 過的 JRecord //
    public Lst<ModelJRecord> getDirtyJRecords() {

        Lst<ModelJRecord> dirtyModels = Lst.of();
        Lst<ModelJRecord> ms = getTable();

        for (ModelJRecord m : ms.toList())
            if (m.getId() + "".length() == 14) // 這裡判斷 14 是因為， dirty model 的 id 是 DateUtil.timeStamp;
                dirtyModels.add(m);

        return dirtyModels;
    }

    public Lst<ModelJRecord> getDirtyJRecords(ModelJRecord.JScaleMode... modes) {

        Lst<ModelJRecord> ms = Lst.of();
        Lst<Integer> modeIds = Lst.of();

        // 取得多個 mode id //
        for (ModelJRecord.JScaleMode m : modes)
            modeIds.add(m.id());

        // 取得對應 Mode 的 Records //
        for (ModelJRecord m : getDirtyJRecords().toList())
            for (Integer id : modeIds.toList())
                if (m.getMode().id() == id)
                    ms.add(m);

        return ms;
    }


    //--------------------------------------------------
    // Private Preference
    //--------------------------------------------------

    private ContentValues toCv(ModelJRecord m) {
        ContentValues cv = new ContentValues();
        cv.put(COL_ID, m.getId());
        cv.put(COL_UUID, m.getUuid());
        cv.put(COL_JSCALE_ID, m.getJscale_id());
        cv.put(COL_JRECIPE_ID, m.getJrecipe_id());
        cv.put(COL_STANDARD, m.isStandard());
        cv.put(COL_WATER, m.getWater());
        cv.put(COL_TIME_1, m.getTime1());
        cv.put(COL_TIME_2, m.getTime2());
        cv.put(COL_SCORE, m.getScore());
        cv.put(COL_RECORD_TIME, m.getRecord_time().formatISO8601());
        cv.put(COL_CREATED_TIME, m.getCreated_time().formatISO8601());
        cv.put(COL_UPDATED_TIME, m.getUpdated_time().formatISO8601());
        cv.put(COL_MODE, m.getMode().id());
        cv.put(COL_STATE, m.getState().id());
        cv.put(COL_RECORD_DATA, m.getRecordData().datas());
        cv.put(COL_TRAINING_DATA, m.getTrainingData().datas());
        return cv;
    }


}
