package com.hiroia.jimmy.db.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.hiroia.jimmy.model.ModelTModeRecord;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.ymd.YMD;
import com.library.android_common.database.SQLTableCreator;
import com.library.android_common.util.StrUtil;

/**
 * Create By Ray on 2018/12/13
 * T Mode 的遊玩紀錄
 */
public class TModeRecordDBA {

    // Comp //
    private static SQLiteDatabase sm_db;
    public static final String TABLE_NAME = "tmode_record_table";

    // Column //
    public static final String COL_ID = "id";
    public static final String COL_MODE = "mode"; // single(0) or random(1)
    public static final String COL_STARS = "stars";
    public static final String COL_SCORE = "score";
    public static final String COL_YMD = "ymd";

    //--------------------------------------------------
    // Construct
    //--------------------------------------------------

    public TModeRecordDBA(Context ctx) {
        openDB(ctx);
    }

    public static TModeRecordDBA open(Context ctx) {
        return new TModeRecordDBA(ctx);
    }

    public static String createTable() {
        return SQLTableCreator.of(TABLE_NAME)
                .addIntCol(COL_ID)
                .addIntCol(COL_MODE)
                .addIntCol(COL_STARS)
                .addIntCol(COL_SCORE)
                .addTextCol(COL_YMD)
                .getSQL();
    }

    //--------------------------------------------------
    // CRUD
    //--------------------------------------------------

    public void insert(ModelTModeRecord m) {
        ContentValues cv = toContentValue(m);
        sm_db.insert(TABLE_NAME, null, cv);
    }

    public void update(ModelTModeRecord m) {
        delete(m);
        insert(m);
    }

    public void delete(ModelTModeRecord m) {
        sm_db.delete(TABLE_NAME, COL_ID + StrUtil.SPACE_EQUAL + m.getId(), null);
    }

    //---------------------------------------------------
    // All Table //

    public void openDB(Context ctx){
        sm_db = JimmySQLHelper.getDB(ctx);
    }

    public void close(){
        sm_db.close();
    }

    public Lst<ModelTModeRecord> getTable() {
        Lst<ModelTModeRecord> ms = Lst.of();

        Cursor c = sm_db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        c.moveToFirst();

        for (int i = 0; i < c.getCount(); i++) {
            ModelTModeRecord m = new ModelTModeRecord();
            m.setId(c.getLong(c.getColumnIndex(COL_ID)));
            m.setMode(c.getInt(c.getColumnIndex(COL_MODE)));
            m.setScore(c.getInt(c.getColumnIndex(COL_SCORE)));
            m.setStars(c.getInt(c.getColumnIndex(COL_STARS)));
            m.setYmd(YMD.ofSlash(c.getString(c.getColumnIndex(COL_YMD))));

            ms.add(m);
            c.moveToNext();
        }
        c.close();
        return ms;
    }


    //--------------------------------------------------
    // Private Preference
    //--------------------------------------------------

    private ContentValues toContentValue(ModelTModeRecord m) {
        ContentValues cv = new ContentValues();
        cv.put(COL_ID, m.getId());
        cv.put(COL_MODE, m.getMode());
        cv.put(COL_STARS, m.getStars());
        cv.put(COL_SCORE, m.getScore());
        cv.put(COL_YMD, m.getYmd().toSlashString());
        return cv;
    }

}
