package com.hiroia.jimmy.db.sp;

import android.content.Context;

/**
 * Create By Ray on 2019/1/4
 *
 * SharePreference Manager
 */
public class SpManager {


    //------------------------------------------------------
    // Construct
    //------------------------------------------------------

    public static void init(Context context){
        SpDeviceName.init(context);
        SpAccount.init(context);
    }

}
