package com.hiroia.jimmy.db.sp;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Create By Ray on 2019/1/4
 * Save change device name by mac address
 *
 */
public class SpDeviceName {

    //--------------------------------------------------
    // private member
    //--------------------------------------------------
    static private SharedPreferences sm_sp;
    static private SharedPreferences.Editor sm_editor;

    private static final String JIMMY_DEVICE_NAME = "=JIMMY_DEVICE_NAME=";
    private static final String MAC_ADDRESS = "MAC_ADDRESS";

    //--------------------------------------------------
    // public method
    //--------------------------------------------------
    static public void init(Context ctx) {
        sm_sp = ctx.getSharedPreferences(JIMMY_DEVICE_NAME, Context.MODE_PRIVATE);
        sm_editor = sm_sp.edit();
    }

    static public void put(String deviceName, String macAddress) {
        sm_editor.putString(MAC_ADDRESS.concat(macAddress), deviceName);
        sm_editor.commit();
    }

    static public String get(String macAddress) {
        return sm_sp.getString(MAC_ADDRESS.concat(macAddress), "HIROIA_JIMMY");
    }

}
