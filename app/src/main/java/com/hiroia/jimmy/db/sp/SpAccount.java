package com.hiroia.jimmy.db.sp;

import android.content.Context;
import android.content.SharedPreferences;

import com.hiroia.jimmy.model.ModelAccount;
import com.library.android_common.component.date.ymd.YMD;
import com.library.android_common.util.StrUtil;

/**
 * Create By Ray on 2019/1/16
 */
public class SpAccount {


    //--------------------------------------------------
    // private member
    //--------------------------------------------------
    static private SharedPreferences sm_sp;
    static private SharedPreferences.Editor sm_editor;

    private static final String JIMMY_ACCOUNT = "[jimmy_account]";
    private static final String TOKEN = "TOKEN";
    private static final String ID = "ID";
    private static final String EMAIL = "EMAIL";
    private static final String PASSWORD = "PASSWORD";
    private static final String BIRTH = "BIRTH";
    private static final String SEXUAL = "SEXUAL";
    private static final String INTRO = "INTRO";
    private static final String PHOTOURL = "PHOTOURL";
    private static final String USER_NAME = "USER_NAME";

    private static final String UUID = "UUID";
    private static final String FB_ID = "FB_ID";
    private static final String FB_PHOTO = "FB_PHOTO";


    //--------------------------------------------------
    // public method
    //--------------------------------------------------
    static public void init(Context ctx) {
        sm_sp = ctx.getSharedPreferences(JIMMY_ACCOUNT, Context.MODE_PRIVATE);
        sm_editor = sm_sp.edit();
    }

    // put 從外部帶入一整個 model，在個別取出要 put 的對應參數
    static public void put(ModelAccount m) {
        sm_editor.putString(TOKEN, m.getToken());
        sm_editor.putLong(ID, m.getId());
        sm_editor.putString(EMAIL, m.getEmail());
        sm_editor.putString(PASSWORD, m.getPassword());
        sm_editor.putString(BIRTH, m.getBirth().toSlashString());
        sm_editor.putInt(SEXUAL, m.getSexual());
        sm_editor.putString(INTRO, m.getIntro());
        sm_editor.putString(PHOTOURL, m.getPhoto_url());
        sm_editor.putString(USER_NAME, m.getUserName());
        sm_editor.putString(UUID, m.getUuid());
        sm_editor.putString(FB_ID, m.getFbId());
        sm_editor.putString(FB_PHOTO, m.getFbPhoto());
        sm_editor.commit();
    }

    // get 的時候，是將 Sp 的儲存值 get 出後，在 set 到 model 本身，並回傳出整個 model，外部可一次取得整個 model，且能夠針對 key 取出對應的單項 value
    static public ModelAccount get() {
        return ModelAccount.create()
                .setToken(sm_sp.getString(TOKEN, StrUtil.EMPTY))
                .setId(sm_sp.getLong(ID, 0))
                .setBirth(YMD.ofSlash(sm_sp.getString(BIRTH, YMD.nowYMD().toSlashString())))
                .setPassword(sm_sp.getString(PASSWORD, StrUtil.EMPTY))
                .setEmail(sm_sp.getString(EMAIL, StrUtil.EMPTY))
                .setIntro(sm_sp.getString(INTRO, StrUtil.EMPTY))
                .setPhoto_url(sm_sp.getString(PHOTOURL, StrUtil.EMPTY))
                .setSexual(sm_sp.getInt(SEXUAL, 0))
                .setUserName(sm_sp.getString(USER_NAME, StrUtil.EMPTY))
                .setUuid(sm_sp.getString(UUID, StrUtil.EMPTY))
                .setFbId(sm_sp.getString(FB_ID, StrUtil.EMPTY))
                .setFbPhoto(sm_sp.getString(FB_PHOTO, StrUtil.EMPTY));
    }

    static public void clear() {
        sm_editor.clear();
        sm_editor.commit();
    }

    static public boolean isAlreadyLogin() {
        return !sm_sp.getString(TOKEN, StrUtil.EMPTY).isEmpty();

    }
}
