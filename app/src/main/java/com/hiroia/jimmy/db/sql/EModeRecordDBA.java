package com.hiroia.jimmy.db.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.hiroia.jimmy.model.ModelEModeRecord;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.component.date.hms.HMS;
import com.library.android_common.component.date.ymd.YMD;
import com.library.android_common.database.SQLTableCreator;
import com.library.android_common.util.StrUtil;

/**
 * Create By Ray on 2018/12/14
 */
public class EModeRecordDBA {

    // Comp //
    private OnQueryTableFinish m_onQueryTableFinish = null;

    private static SQLiteDatabase sm_db;
    public static final String TABLE_NAME = "emode_record_table";

    // Column //
    public static final String COL_ID = "id";
    public static final String COL_DATE = "ymd";
    public static final String COL_DEVICE_NAME = "deviceName";
    public static final String COL_WEIGHT = "weight";
    public static final String COL_TTIME = "ttime";
    public static final String COL_TIME = "time";
    public static final String COL_IS_STANDARD = "std";

    //----------------------------------------------------
    // Construct
    //----------------------------------------------------

    public EModeRecordDBA(Context ctx) {
        sm_db = JimmySQLHelper.getDB(ctx);
        sm_db.execSQL(createTable());
    }

    public static EModeRecordDBA open(Context ctx) {
        return new EModeRecordDBA(ctx);
    }

    public static String createTable() {
        return SQLTableCreator.of(TABLE_NAME)
                .addIntCol(COL_ID)
                .addTextCol(COL_DATE)
                .addTextCol(COL_DEVICE_NAME)
                .addRealCol(COL_WEIGHT)
                .addTextCol(COL_TTIME)
                .addIntCol(COL_TIME)
                .addIntCol(COL_IS_STANDARD)
                .getSQL();
    }

    //--------------------------------------------------
    // CRUD
    //--------------------------------------------------
    public void insert(ModelEModeRecord m) {
        sm_db.insert(TABLE_NAME, null, toContentValues(m));
    }

    public void update(ModelEModeRecord m) {
        delete(m);
        insert(m);
    }

    public void delete(ModelEModeRecord m) {
        sm_db.delete(TABLE_NAME, COL_ID + StrUtil.SPACE_EQUAL + m.getId(), null);
    }


    //---------------------------------------------------
    // All Table //
    public void openDB(Context ctx) {
        sm_db = JimmySQLHelper.getDB(ctx);
    }

    public void colse() {
        sm_db.close();
    }

    public static void clearTable(){
        JimmySQLHelper.clearTable(TABLE_NAME);
    }

    public Lst<ModelEModeRecord> getTable(){
        return getTable(null);
    }

    public Lst<ModelEModeRecord> getTable(OnQueryTableFinish onQueryTableFinish) {
        m_onQueryTableFinish = onQueryTableFinish;
        //---------------------------------------------------

        Lst<ModelEModeRecord> ms = Lst.of();

        Cursor c = sm_db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        c.moveToFirst();

        for (int i = 0; i < c.getCount(); i++) {
            ModelEModeRecord m = new ModelEModeRecord();
            m.setId(c.getLong(c.getColumnIndex(COL_ID)));
            m.setYMD(YMD.ofSlash(c.getString(c.getColumnIndex(COL_DATE))));
            m.setDeviceName(c.getString(c.getColumnIndex(COL_DEVICE_NAME)));
            m.setWeight(c.getDouble(c.getColumnIndex(COL_WEIGHT)));
            m.setTime(c.getLong(c.getColumnIndex(COL_TIME)));
            m.setTtime(c.getString(c.getColumnIndex(COL_TTIME)));
            m.setStandard(c.getInt(c.getColumnIndex(COL_IS_STANDARD)) == 0); // 0 = true, 1 = false;

            ms.add(m);
            c.moveToNext();
        }

        c.close();

        if (m_onQueryTableFinish != null)
            m_onQueryTableFinish.onFinish();
        return ms;
    }

    //--------------------------------------------------
    // Private Preference
    //--------------------------------------------------

    private ContentValues toContentValues(ModelEModeRecord m) {
        ContentValues cv = new ContentValues();
        cv.put(COL_ID, m.getId());
        cv.put(COL_DATE, m.getYMD().toSlashString());
        cv.put(COL_DEVICE_NAME, m.getDeviceName());
        cv.put(COL_WEIGHT, m.getWeight());
        cv.put(COL_TIME, m.getTime());
        cv.put(COL_TTIME, m.getTtime());
        cv.put(COL_IS_STANDARD, m.getNumStandard());
        return cv;
    }

    //------------------------------------------------------
    // Interface
    //------------------------------------------------------
    public interface OnQueryTableFinish{
        void onFinish();
    }

}
