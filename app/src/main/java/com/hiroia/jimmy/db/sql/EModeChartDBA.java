package com.hiroia.jimmy.db.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.hiroia.jimmy.comp.comp.WaterVolStep;
import com.library.android_common.database.SQLTableCreator;
import com.library.android_common.util.StrUtil;

/**
 * Create By Ray on 2018/12/26
 */
public class EModeChartDBA {

    // Comp //
    private static SQLiteDatabase sm_db;
    public static final String TABLE_NAME = "emode_chart_table";

    // Column //
    public static final String COL_ID = "id";
    public static final String COL_SEC = "sec";
    public static final String COL_VOL = "vol";

    //----------------------------------------------------------
    // Construct
    //----------------------------------------------------------

    public EModeChartDBA(Context ctx) {
        sm_db = JimmySQLHelper.getDB(ctx);
        sm_db.execSQL(createTable());
    }

    public static EModeChartDBA open(Context ctx) {
        return new EModeChartDBA(ctx);
    }

    public static String createTable() {
        return SQLTableCreator.of(TABLE_NAME)
                .addIntCol(COL_ID)
                .addIntCol(COL_SEC)
                .addIntCol(COL_VOL)
                .getSQL();
    }

    //----------------------------------------------------------
    // CRUD
    //----------------------------------------------------------

    public void insert(long id, int sec, int vol) {
        sm_db.insert(TABLE_NAME, null, toContentValue(id, sec, vol));
    }

    public void update(long id, int sec, int vol) {
        delete(id);
        insert(id, sec, vol);
    }

    public void delete(long id) {
        sm_db.delete(TABLE_NAME, COL_ID + StrUtil.SPACE_EQUAL + id, null);
    }

    //----------------------------------------------------------
    // Public Method
    //----------------------------------------------------------

    public void closeDB() {
        sm_db.close();
    }

    public WaterVolStep getTableById(long index) {
        WaterVolStep step = new WaterVolStep();

//        Cursor c = sm_db.rawQuery("SELECT * FROM " + TABLE_NAME , null);
        Cursor c = sm_db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = " + index, null);
        c.moveToFirst();

        for (int i = 0; i < c.getCount(); i++) {
            step.setId(index);
            step.addStep(c.getInt(c.getColumnIndex(COL_SEC)), c.getInt(c.getColumnIndex(COL_VOL)));
            c.moveToNext();
        }

        c.close();
        return step;
    }

    //----------------------------------------------------------
    // Private Preference
    //----------------------------------------------------------

    private ContentValues toContentValue(long id, double sec, int vol) {
        ContentValues cv = new ContentValues();
        cv.put(COL_ID, id);
        cv.put(COL_SEC, sec);
        cv.put(COL_VOL, vol);
        return cv;
    }

}
