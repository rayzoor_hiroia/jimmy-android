package com.hiroia.jimmy.db.sql;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.hiroia.jimmy.BuildConfig;
import com.library.android_common.component.common.Pair;
import com.library.android_common.component.common.Pairs;
import com.library.android_common.component.common.lst.Lst;
import com.library.android_common.util.LogUtil;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

/**
 * Create By Ray on 2018/12/13
 * Jimmy SQL Helper
 */
public class JimmySQLHelper extends SQLiteOpenHelper {

    // Comp //
    public static final int DB_VERSION = BuildConfig.VERSION_CODE;
    public static final String JIMMY_DATABASE = "JIMMY_SCALE_DATABASE";
    public static final String DROP_TABLE = "DROP TABLE IF EXISTS ";

    // Self //
    private static SQLiteDatabase m_db;

    //--------------------------------------------------
    // 所有的 Table, 若使有新增 Table, 只需要添加在此處即可。
    private static ArrayList<Pair<String, String>> allTables = Lst.of( // k : table name, v: create table sql
            Pair.of(TModeRecordDBA.TABLE_NAME, TModeRecordDBA.createTable()), // T Mode Record
            Pair.of(EModeRecordDBA.TABLE_NAME, EModeRecordDBA.createTable()), // E Mode Record
            Pair.of(TModeChartDBA.TABLE_NAME, TModeChartDBA.createTable()), // T Mode Chart
            Pair.of(EModeChartDBA.TABLE_NAME, EModeChartDBA.createTable()), // E Mode Chart
            //---------------------------------------------------------------
            Pair.of(JRecordDBA.TABLE_NAME, JRecordDBA.createTable()) // J Record

    ).toList();


    //--------------------------------------------------
    // Construct
    //--------------------------------------------------

    static public void init(Context ctx) {
        if (m_db == null || !m_db.isOpen())
            m_db = new JimmySQLHelper(ctx, JIMMY_DATABASE, null, DB_VERSION).getWritableDatabase();
    }

    public JimmySQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public JimmySQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    //--------------------------------------------------
    // Implement Method
    //--------------------------------------------------

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (Pair<String, String> p : allTables)
            db.execSQL(p.v()); // create table
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (Pair<String, String> p : allTables)
            db.execSQL(DROP_TABLE.concat(p.k())); // drop table
    }

    //-------------------------------------------------
    // public method
    //-------------------------------------------------

    public ArrayList<String> getAllTableName() {
        ArrayList<String> tables = new ArrayList<>();
        Cursor c = m_db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        c.moveToFirst();
        for (int i = 0; i < c.getCount(); i++) {
            tables.add(c.getString(0));
            c.moveToNext();
        }
        c.close();
        return tables;
    }

    public static SQLiteDatabase getDB(Context ctx) {
        if (m_db == null || !m_db.isOpen()) {
            m_db = new JimmySQLHelper(ctx, JIMMY_DATABASE, null, DB_VERSION).getWritableDatabase();
        }
        return m_db;
    }

    public static void clearAllTable() {
        Pairs.of(allTables).toKLst().forEach(new Lst.IConsumer<String>() {
            @Override
            public void runEach(int i, String tableName) {
                clearTable(tableName);
            }
        });
    }

    public static void clearTable(String sTableName) {
        m_db.execSQL(DROP_TABLE + sTableName);
        m_db.execSQL(Pairs.of(allTables).getValbyKey(sTableName));
    }

    //--------------------------------------------------
    // 版本新增後可能會更動Col，upgrade tables
    static public void updateTables() {
        for (Pair<String, String> p : allTables) {
            m_db.execSQL(DROP_TABLE.concat(p.k())); // drop table
            m_db.execSQL(p.v()); // create table
        }
    }

    //--------------------------------------------------
    // sync db version
    static public void syncSQLVersion() {
//        if (SpVersionManager.get() == BuildConfig.VERSION_CODE) return; // 如果 version 沒有更新 return;
//        //-------------------------------------------------------------
//        updateTables(); // 如果有 update tables
    }

    //--------------------------------------------------
    // Table Printer
    static public void println_d(String tableName, String filter) {

        String tableString = String.format("Table %s :\n", tableName + " Table Size = " + size(tableName));
        Cursor c = m_db.rawQuery("SELECT * FROM " + tableName, null);

        if (c.moveToFirst()) {
            String[] columnNames = c.getColumnNames();
            do {
                for (String name : columnNames)
                    tableString += String.format("%s: %s\n", name, c.getString(c.getColumnIndex(name)));

                tableString += StrUtil.DIVIDER;

            } while (c.moveToNext());
        }
        c.close();
        LogUtil.d(JimmySQLHelper.class, filter.concat(StrUtil.SPACE_DASH + tableString));
    }

    //--------------------------------------------------
    // Table Size
    static public int size(String tableName) {
        String statement = " SELECT * From " + tableName;
        Cursor cursor = m_db.rawQuery(statement, null);

        int size = cursor.getCount();
        cursor.close();
        return size;
    }

    static public void tablesPrintln_d(final String filter) {
//        CsLog.d(SamanthaDBHelper.class, filter.concat(StrUtil.DOUBLE_DIVIDER));
//        CsLog.d(SamanthaDBHelper.class, filter.concat(StrUtil.spaceDashOf("ALL TABLE")));
//        for (Pair<String, String> table : allTables) {
//            CsLog.d(SamanthaDBHelper.class, filter.concat(" - Table Name :[" + table.k() + "] - row size = " + size(table.k())));
//        }
//        CsLog.d(SamanthaDBHelper.class, filter.concat(StrUtil.DOUBLE_DIVIDER));
    }
}
